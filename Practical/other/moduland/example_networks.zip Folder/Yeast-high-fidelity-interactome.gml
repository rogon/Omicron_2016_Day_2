Creator "igraph version 0.5.3 Sat Apr 16 14:55:02 2011"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    y 0.461
    x 0.677
    z 0.5
    label "YOL133W"
  ]
  node
  [
    id 1
    y 0.3766
    x 0.7265
    z 0.5
    label "YLR127C"
  ]
  node
  [
    id 2
    y 0.554
    x 0.3841
    z 0.5
    label "YCR035C"
  ]
  node
  [
    id 3
    y 0.4444
    x 0.3286
    z 0.5
    label "YGR195W"
  ]
  node
  [
    id 4
    y 0.5124
    x 0.5636
    z 0.5
    label "YKL057C"
  ]
  node
  [
    id 5
    y 0.5351
    x 0.4768
    z 0.5
    label "YGL172W"
  ]
  node
  [
    id 6
    y 0.4235
    x 0.3833
    z 0.5
    label "YNL207W"
  ]
  node
  [
    id 7
    y 0.4867
    x 0.5074
    z 0.5
    label "YPL204W"
  ]
  node
  [
    id 8
    y 0.2269
    x 0.655
    z 0.5
    label "YGR261C"
  ]
  node
  [
    id 9
    y 0.1831
    x 0.5887
    z 0.5
    label "YPL195W"
  ]
  node
  [
    id 10
    y 0.4074
    x 0.565
    z 0.5
    label "YGR274C"
  ]
  node
  [
    id 11
    y 0.3581
    x 0.5691
    z 0.5
    label "YDR145W"
  ]
  node
  [
    id 12
    y 0.2216
    x 0.4518
    z 0.5
    label "YDR080W"
  ]
  node
  [
    id 13
    y 0.1497
    x 0.484
    z 0.5
    label "YDL077C"
  ]
  node
  [
    id 14
    y 0.5421
    x 0.6153
    z 0.5
    label "YPL174C"
  ]
  node
  [
    id 15
    y 0.4811
    x 0.6094
    z 0.5
    label "YBL106C"
  ]
  node
  [
    id 16
    y 0.28
    x 0.6503
    z 0.5
    label "YHR171W"
  ]
  node
  [
    id 17
    y 0.5054
    x 0.5198
    z 0.5
    label "YPL235W"
  ]
  node
  [
    id 18
    y 0.5411
    x 0.5856
    z 0.5
    label "YDR155C"
  ]
  node
  [
    id 19
    y 0.4405
    x 0.8473
    z 0.5
    label "YDR312W"
  ]
  node
  [
    id 20
    y 0.4448
    x 0.7383
    z 0.5
    label "YOR080W"
  ]
  node
  [
    id 21
    y 0.4574
    x 0.3573
    z 0.5
    label "YFR037C"
  ]
  node
  [
    id 22
    y 0.4499
    x 0.2933
    z 0.5
    label "YCR052W"
  ]
  node
  [
    id 23
    y 0.4346
    x 0.6402
    z 0.5
    label "YBR270C"
  ]
  node
  [
    id 24
    y 0.5121
    x 0.6904
    z 0.5
    label "YIL046W"
  ]
  node
  [
    id 25
    y 0.2697
    x 0.7307
    z 0.5
    label "YHR160C"
  ]
  node
  [
    id 26
    y 0.328
    x 0.7431
    z 0.5
    label "YLR191W"
  ]
  node
  [
    id 27
    y 0.6374
    x 0.6132
    z 0.5
    label "YBR135W"
  ]
  node
  [
    id 28
    y 0.6089
    x 0.5268
    z 0.5
    label "YGR108W"
  ]
  node
  [
    id 29
    y 0.5109
    x 0.4907
    z 0.5
    label "YLR371W"
  ]
  node
  [
    id 30
    y 0.4292
    x 0.4407
    z 0.5
    label "YBR143C"
  ]
  node
  [
    id 31
    y 0.6549
    x 0.5709
    z 0.5
    label "YDL155W"
  ]
  node
  [
    id 32
    y 0.5404
    x 0.6096
    z 0.5
    label "YGL003C"
  ]
  node
  [
    id 33
    y 0.3428
    x 0.6526
    z 0.5
    label "YFL028C"
  ]
  node
  [
    id 34
    y 0.622
    x 0.544
    z 0.5
    label "YOR160W"
  ]
  node
  [
    id 35
    y 0.5029
    x 0.5421
    z 0.5
    label "YDR192C"
  ]
  node
  [
    id 36
    y 0.3621
    x 0.3251
    z 0.5
    label "YDL098C"
  ]
  node
  [
    id 37
    y 0.3753
    x 0.4365
    z 0.5
    label "YNL147W"
  ]
  node
  [
    id 38
    y 0.6767
    x 0.5865
    z 0.5
    label "YDR342C"
  ]
  node
  [
    id 39
    y 0.5884
    x 0.5261
    z 0.5
    label "YOL100W"
  ]
  node
  [
    id 40
    y 0.5663
    x 0.5806
    z 0.5
    label "YDL101C"
  ]
  node
  [
    id 41
    y 0.3729
    x 0.5956
    z 0.5
    label "YLR071C"
  ]
  node
  [
    id 42
    y 0.4578
    x 0.6262
    z 0.5
    label "YBR253W"
  ]
  node
  [
    id 43
    y 0.4915
    x 0.37
    z 0.5
    label "YDL070W"
  ]
  node
  [
    id 44
    y 0.4637
    x 0.4601
    z 0.5
    label "YBR009C"
  ]
  node
  [
    id 45
    y 0.4127
    x 0.398
    z 0.5
    label "YGL019W"
  ]
  node
  [
    id 46
    y 0.4497
    x 0.466
    z 0.5
    label "YIL035C"
  ]
  node
  [
    id 47
    y 0.1361
    x 0.4637
    z 0.5
    label "YLR240W"
  ]
  node
  [
    id 48
    y 0.2185
    x 0.5105
    z 0.5
    label "YBR097W"
  ]
  node
  [
    id 49
    y 0.5179
    x 0.6005
    z 0.5
    label "YML109W"
  ]
  node
  [
    id 50
    y 0.508
    x 0.5519
    z 0.5
    label "YBR103W"
  ]
  node
  [
    id 51
    y 0.4286
    x 0.6119
    z 0.5
    label "YJL014W"
  ]
  node
  [
    id 52
    y 0.5419
    x 0.6501
    z 0.5
    label "YPL049C"
  ]
  node
  [
    id 53
    y 0.4812
    x 0.5841
    z 0.5
    label "YGR040W"
  ]
  node
  [
    id 54
    y 0.4635
    x 0.4393
    z 0.5
    label "YKR001C"
  ]
  node
  [
    id 55
    y 0.4862
    x 0.4317
    z 0.5
    label "YBR089C-A"
  ]
  node
  [
    id 56
    y 0.5157
    x 0.5679
    z 0.5
    label "YLR335W"
  ]
  node
  [
    id 57
    y 0.5107
    x 0.5297
    z 0.5
    label "YGL016W"
  ]
  node
  [
    id 58
    y 0.466
    x 0.5409
    z 0.5
    label "YDR101C"
  ]
  node
  [
    id 59
    y 0.422
    x 0.6014
    z 0.5
    label "YAL007C"
  ]
  node
  [
    id 60
    y 0.5989
    x 0.6126
    z 0.5
    label "YLR457C"
  ]
  node
  [
    id 61
    y 0.5742
    x 0.5508
    z 0.5
    label "YBR160W"
  ]
  node
  [
    id 62
    y 0.445
    x 0.6498
    z 0.5
    label "YLR423C"
  ]
  node
  [
    id 63
    y 0.4001
    x 0.5583
    z 0.5
    label "YBR217W"
  ]
  node
  [
    id 64
    y 0.4861
    x 0.3427
    z 0.5
    label "YLR295C"
  ]
  node
  [
    id 65
    y 0.4379
    x 0.4579
    z 0.5
    label "YBL099W"
  ]
  node
  [
    id 66
    y 0.3673
    x 0.4625
    z 0.5
    label "YMR116C"
  ]
  node
  [
    id 67
    y 0.396
    x 0.4118
    z 0.5
    label "YJL080C"
  ]
  node
  [
    id 68
    y 0.2211
    x 0.3403
    z 0.5
    label "YHR187W"
  ]
  node
  [
    id 69
    y 0.3184
    x 0.3796
    z 0.5
    label "YLR384C"
  ]
  node
  [
    id 70
    y 0.3306
    x 0.4068
    z 0.5
    label "YMR128W"
  ]
  node
  [
    id 71
    y 0.3901
    x 0.4425
    z 0.5
    label "YGR090W"
  ]
  node
  [
    id 72
    y 0.5677
    x 0.6029
    z 0.5
    label "YLR319C"
  ]
  node
  [
    id 73
    y 0.6312
    x 0.6969
    z 0.5
    label "YIL159W"
  ]
  node
  [
    id 74
    y 0.4777
    x 0.731
    z 0.5
    label "YGL198W"
  ]
  node
  [
    id 75
    y 0.4696
    x 0.8405
    z 0.5
    label "YDR084C"
  ]
  node
  [
    id 76
    y 0.3577
    x 0.6853
    z 0.5
    label "YDL126C"
  ]
  node
  [
    id 77
    y 0.2416
    x 0.5834
    z 0.5
    label "YGR048W"
  ]
  node
  [
    id 78
    y 0.5447
    x 0.4625
    z 0.5
    label "YBL051C"
  ]
  node
  [
    id 79
    y 0.2144
    x 0.5653
    z 0.5
    label "YKR096W"
  ]
  node
  [
    id 80
    y 0.4889
    x 0.3656
    z 0.5
    label "YDR240C"
  ]
  node
  [
    id 81
    y 0.3989
    x 0.3668
    z 0.5
    label "YFL017W-A"
  ]
  node
  [
    id 82
    y 0.4586
    x 0.6532
    z 0.5
    label "YDL065C"
  ]
  node
  [
    id 83
    y 0.5043
    x 0.5572
    z 0.5
    label "YMR047C"
  ]
  node
  [
    id 84
    y 0.7308
    x 0.518
    z 0.5
    label "YJR098C"
  ]
  node
  [
    id 85
    y 0.5824
    x 0.5084
    z 0.5
    label "YMR304W"
  ]
  node
  [
    id 86
    y 0.4157
    x 0.3863
    z 0.5
    label "YGR091W"
  ]
  node
  [
    id 87
    y 0.4161
    x 0.393
    z 0.5
    label "YLR438C-A"
  ]
  node
  [
    id 88
    y 0.5213
    x 0.4634
    z 0.5
    label "YJL041W"
  ]
  node
  [
    id 89
    y 0.5493
    x 0.457
    z 0.5
    label "YLR293C"
  ]
  node
  [
    id 90
    y 0.3458
    x 0.3498
    z 0.5
    label "YPL213W"
  ]
  node
  [
    id 91
    y 0.4086
    x 0.4738
    z 0.5
    label "YKL095W"
  ]
  node
  [
    id 92
    y 0.5345
    x 0.4095
    z 0.5
    label "YOR304W"
  ]
  node
  [
    id 93
    y 0.4914
    x 0.3347
    z 0.5
    label "YER164W"
  ]
  node
  [
    id 94
    y 0.7573
    x 0.7113
    z 0.5
    label "YOR026W"
  ]
  node
  [
    id 95
    y 0.8774
    x 0.6319
    z 0.5
    label "YGR188C"
  ]
  node
  [
    id 96
    y 0.3626
    x 0.5198
    z 0.5
    label "YCL059C"
  ]
  node
  [
    id 97
    y 0.2619
    x 0.5519
    z 0.5
    label "YDR087C"
  ]
  node
  [
    id 98
    y 0.3592
    x 0.4868
    z 0.5
    label "YNL061W"
  ]
  node
  [
    id 99
    y 0.3523
    x 0.4493
    z 0.5
    label "YML046W"
  ]
  node
  [
    id 100
    y 0.3808
    x 0.3972
    z 0.5
    label "YHR165C"
  ]
  node
  [
    id 101
    y 0.2221
    x 0.3693
    z 0.5
    label "YKL023W"
  ]
  node
  [
    id 102
    y 0.4184
    x 0.3771
    z 0.5
    label "YHR077C"
  ]
  node
  [
    id 103
    y 0.2592
    x 0.5035
    z 0.5
    label "YJR002W"
  ]
  node
  [
    id 104
    y 0.2613
    x 0.4323
    z 0.5
    label "YNL075W"
  ]
  node
  [
    id 105
    y 0.3674
    x 0.5712
    z 0.5
    label "YDR238C"
  ]
  node
  [
    id 106
    y 0.3809
    x 0.591
    z 0.5
    label "YFR051C"
  ]
  node
  [
    id 107
    y 0.3965
    x 0.5567
    z 0.5
    label "YOR259C"
  ]
  node
  [
    id 108
    y 0.3889
    x 0.5416
    z 0.5
    label "YDL147W"
  ]
  node
  [
    id 109
    y 0.4965
    x 0.6481
    z 0.5
    label "YNL025C"
  ]
  node
  [
    id 110
    y 0.5334
    x 0.5292
    z 0.5
    label "YAR002W"
  ]
  node
  [
    id 111
    y 0.459
    x 0.5283
    z 0.5
    label "YMR290C"
  ]
  node
  [
    id 112
    y 0.3914
    x 0.4658
    z 0.5
    label "YPR016C"
  ]
  node
  [
    id 113
    y 0.6359
    x 0.2736
    z 0.5
    label "YJL112W"
  ]
  node
  [
    id 114
    y 0.4135
    x 0.6404
    z 0.5
    label "YLR078C"
  ]
  node
  [
    id 115
    y 0.6484
    x 0.486
    z 0.5
    label "YJR065C"
  ]
  node
  [
    id 116
    y 0.5809
    x 0.5402
    z 0.5
    label "YCR088W"
  ]
  node
  [
    id 117
    y 0.6731
    x 0.5447
    z 0.5
    label "YHR102W"
  ]
  node
  [
    id 118
    y 0.7864
    x 0.5344
    z 0.5
    label "YOR353C"
  ]
  node
  [
    id 119
    y 0.6203
    x 0.3828
    z 0.5
    label "YIR002C"
  ]
  node
  [
    id 120
    y 0.5427
    x 0.4756
    z 0.5
    label "YDR097C"
  ]
  node
  [
    id 121
    y 0.621
    x 0.6297
    z 0.5
    label "YCR096C"
  ]
  node
  [
    id 122
    y 0.5531
    x 0.5391
    z 0.5
    label "YCR084C"
  ]
  node
  [
    id 123
    y 0.5438
    x 0.505
    z 0.5
    label "YDR388W"
  ]
  node
  [
    id 124
    y 0.5153
    x 0.3838
    z 0.5
    label "YPR088C"
  ]
  node
  [
    id 125
    y 0.4308
    x 0.3454
    z 0.5
    label "YER062C"
  ]
  node
  [
    id 126
    y 0.4081
    x 0.2503
    z 0.5
    label "YJL173C"
  ]
  node
  [
    id 127
    y 0.4748
    x 0.782
    z 0.5
    label "YGR166W"
  ]
  node
  [
    id 128
    y 0.4678
    x 0.6954
    z 0.5
    label "YBR254C"
  ]
  node
  [
    id 129
    y 0.4182
    x 0.4897
    z 0.5
    label "YDL140C"
  ]
  node
  [
    id 130
    y 0.3844
    x 0.3579
    z 0.5
    label "YDR138W"
  ]
  node
  [
    id 131
    y 0.3795
    x 0.2165
    z 0.5
    label "YGL090W"
  ]
  node
  [
    id 132
    y 0.4426
    x 0.1183
    z 0.5
    label "YOR005C"
  ]
  node
  [
    id 133
    y 0.4175
    x 0.7034
    z 0.5
    label "YJL047C"
  ]
  node
  [
    id 134
    y 0.7877
    x 0.547
    z 0.5
    label "YKL189W"
  ]
  node
  [
    id 135
    y 0.3264
    x 0.3772
    z 0.5
    label "YGR005C"
  ]
  node
  [
    id 136
    y 0.3117
    x 0.4561
    z 0.5
    label "YGL070C"
  ]
  node
  [
    id 137
    y 0.5872
    x 0.4941
    z 0.5
    label "YGR218W"
  ]
  node
  [
    id 138
    y 0.6772
    x 0.571
    z 0.5
    label "YDL189W"
  ]
  node
  [
    id 139
    y 0.5755
    x 0.6223
    z 0.5
    label "YPL031C"
  ]
  node
  [
    id 140
    y 0.5215
    x 0.5062
    z 0.5
    label "YGL134W"
  ]
  node
  [
    id 141
    y 0.5575
    x 0.5445
    z 0.5
    label "YNL094W"
  ]
  node
  [
    id 142
    y 0.6534
    x 0.5126
    z 0.5
    label "YFR024C"
  ]
  node
  [
    id 143
    y 0.6181
    x 0.4742
    z 0.5
    label "YPR168W"
  ]
  node
  [
    id 144
    y 0.2043
    x 0.4565
    z 0.5
    label "YFR011C"
  ]
  node
  [
    id 145
    y 0.3232
    x 0.4807
    z 0.5
    label "YNL113W"
  ]
  node
  [
    id 146
    y 0.4293
    x 0.5736
    z 0.5
    label "YDL097C"
  ]
  node
  [
    id 147
    y 0.4219
    x 0.5661
    z 0.5
    label "YER012W"
  ]
  node
  [
    id 148
    y 0.6053
    x 0.4937
    z 0.5
    label "YGR268C"
  ]
  node
  [
    id 149
    y 0.3926
    x 0.639
    z 0.5
    label "YLR268W"
  ]
  node
  [
    id 150
    y 0.3995
    x 0.6848
    z 0.5
    label "YFL038C"
  ]
  node
  [
    id 151
    y 0.587
    x 0.5692
    z 0.5
    label "YAL005C"
  ]
  node
  [
    id 152
    y 0.5599
    x 0.4194
    z 0.5
    label "YHR069C"
  ]
  node
  [
    id 153
    y 0.2051
    x 0.4391
    z 0.5
    label "YGR063C"
  ]
  node
  [
    id 154
    y 0.2824
    x 0.3972
    z 0.5
    label "YDR404C"
  ]
  node
  [
    id 155
    y 0.6942
    x 0.716
    z 0.5
    label "YCR086W"
  ]
  node
  [
    id 156
    y 0.6132
    x 0.6079
    z 0.5
    label "YDR309C"
  ]
  node
  [
    id 157
    y 0.3285
    x 0.5498
    z 0.5
    label "YLR129W"
  ]
  node
  [
    id 158
    y 0.3551
    x 0.5006
    z 0.5
    label "YCR057C"
  ]
  node
  [
    id 159
    y 0.6511
    x 0.6165
    z 0.5
    label "YDR214W"
  ]
  node
  [
    id 160
    y 0.5099
    x 0.5579
    z 0.5
    label "YDR523C"
  ]
  node
  [
    id 161
    y 0.4683
    x 0.3573
    z 0.5
    label "YHR064C"
  ]
  node
  [
    id 162
    y 0.3959
    x 0.3998
    z 0.5
    label "YBR175W"
  ]
  node
  [
    id 163
    y 0.45
    x 0.6034
    z 0.5
    label "YGL061C"
  ]
  node
  [
    id 164
    y 0.6103
    x 0.6839
    z 0.5
    label "YBR156C"
  ]
  node
  [
    id 165
    y 0.4713
    x 0.4827
    z 0.5
    label "YOR039W"
  ]
  node
  [
    id 166
    y 0.4936
    x 0.5924
    z 0.5
    label "YMR186W"
  ]
  node
  [
    id 167
    y 0.3596
    x 0.4519
    z 0.5
    label "YNR032W"
  ]
  node
  [
    id 168
    y 0.4618
    x 0.2565
    z 0.5
    label "YBR241C"
  ]
  node
  [
    id 169
    y 0.4797
    x 0.4054
    z 0.5
    label "YER081W"
  ]
  node
  [
    id 170
    y 0.4034
    x 0.36
    z 0.5
    label "YBL052C"
  ]
  node
  [
    id 171
    y 0.4994
    x 0.4385
    z 0.5
    label "YBR274W"
  ]
  node
  [
    id 172
    y 0.4239
    x 0.574
    z 0.5
    label "YGL137W"
  ]
  node
  [
    id 173
    y 0.3494
    x 0.6515
    z 0.5
    label "YKL078W"
  ]
  node
  [
    id 174
    y 0.4803
    x 0.3997
    z 0.5
    label "YNL118C"
  ]
  node
  [
    id 175
    y 0.4601
    x 0.408
    z 0.5
    label "YMR080C"
  ]
  node
  [
    id 176
    y 0.4743
    x 0.553
    z 0.5
    label "YBR017C"
  ]
  node
  [
    id 177
    y 0.4105
    x 0.3755
    z 0.5
    label "YIL021W"
  ]
  node
  [
    id 178
    y 0.3411
    x 0.4315
    z 0.5
    label "YOL005C"
  ]
  node
  [
    id 179
    y 0.3701
    x 0.4536
    z 0.5
    label "YNR053C"
  ]
  node
  [
    id 180
    y 0.3872
    x 0.4063
    z 0.5
    label "YCR077C"
  ]
  node
  [
    id 181
    y 0.9142
    x 0.3988
    z 0.5
    label "YBR077C"
  ]
  node
  [
    id 182
    y 0.8489
    x 0.4309
    z 0.5
    label "YKR007W"
  ]
  node
  [
    id 183
    y 0.4187
    x 0.6794
    z 0.5
    label "YPL065W"
  ]
  node
  [
    id 184
    y 0.456
    x 0.5995
    z 0.5
    label "YLL039C"
  ]
  node
  [
    id 185
    y 0.6779
    x 0.5294
    z 0.5
    label "YBR130C"
  ]
  node
  [
    id 186
    y 0.5092
    x 0.5406
    z 0.5
    label "YAL029C"
  ]
  node
  [
    id 187
    y 0.4927
    x 0.5222
    z 0.5
    label "YNL182C"
  ]
  node
  [
    id 188
    y 0.5911
    x 0.3903
    z 0.5
    label "YPL084W"
  ]
  node
  [
    id 189
    y 0.4131
    x 0.4474
    z 0.5
    label "YJL124C"
  ]
  node
  [
    id 190
    y 0.4496
    x 0.4488
    z 0.5
    label "YOL149W"
  ]
  node
  [
    id 191
    y 0.356
    x 0.2511
    z 0.5
    label "YMR033W"
  ]
  node
  [
    id 192
    y 0.4228
    x 0.3741
    z 0.5
    label "YBR289W"
  ]
  node
  [
    id 193
    y 0.5554
    x 0.6249
    z 0.5
    label "YDR499W"
  ]
  node
  [
    id 194
    y 0.6296
    x 0.584
    z 0.5
    label "YHR164C"
  ]
  node
  [
    id 195
    y 0.4306
    x 0.6012
    z 0.5
    label "YIL142W"
  ]
  node
  [
    id 196
    y 0.4508
    x 0.5354
    z 0.5
    label "YDR075W"
  ]
  node
  [
    id 197
    y 0.5957
    x 0.4536
    z 0.5
    label "YBR060C"
  ]
  node
  [
    id 198
    y 0.5494
    x 0.6052
    z 0.5
    label "YJL194W"
  ]
  node
  [
    id 199
    y 0.5546
    x 0.6167
    z 0.5
    label "YLR079W"
  ]
  node
  [
    id 200
    y 0.2189
    x 0.556
    z 0.5
    label "YDR390C"
  ]
  node
  [
    id 201
    y 0.6158
    x 0.651
    z 0.5
    label "YDL226C"
  ]
  node
  [
    id 202
    y 0.5244
    x 0.6771
    z 0.5
    label "YDR264C"
  ]
  node
  [
    id 203
    y 0.455
    x 0.7124
    z 0.5
    label "YGL161C"
  ]
  node
  [
    id 204
    y 0.4359
    x 0.7138
    z 0.5
    label "YOR036W"
  ]
  node
  [
    id 205
    y 0.5953
    x 0.4425
    z 0.5
    label "YOR185C"
  ]
  node
  [
    id 206
    y 0.5423
    x 0.4837
    z 0.5
    label "YLR347C"
  ]
  node
  [
    id 207
    y 0.3535
    x 0.5348
    z 0.5
    label "YLR197W"
  ]
  node
  [
    id 208
    y 0.496
    x 0.6875
    z 0.5
    label "YEL051W"
  ]
  node
  [
    id 209
    y 0.5619
    x 0.6712
    z 0.5
    label "YKR026C"
  ]
  node
  [
    id 210
    y 0.5164
    x 0.4971
    z 0.5
    label "YLR430W"
  ]
  node
  [
    id 211
    y 0.4274
    x 0.4223
    z 0.5
    label "YLR147C"
  ]
  node
  [
    id 212
    y 0.446
    x 0.6679
    z 0.5
    label "YDR259C"
  ]
  node
  [
    id 213
    y 0.4544
    x 0.6497
    z 0.5
    label "YER022W"
  ]
  node
  [
    id 214
    y 0.3726
    x 0.5841
    z 0.5
    label "YDL192W"
  ]
  node
  [
    id 215
    y 0.3752
    x 0.5262
    z 0.5
    label "YPR110C"
  ]
  node
  [
    id 216
    y 0.4799
    x 0.5363
    z 0.5
    label "YAL013W"
  ]
  node
  [
    id 217
    y 0.5534
    x 0.4845
    z 0.5
    label "YDL029W"
  ]
  node
  [
    id 218
    y 0.429
    x 0.5892
    z 0.5
    label "YPL248C"
  ]
  node
  [
    id 219
    y 0.3897
    x 0.6445
    z 0.5
    label "YER157W"
  ]
  node
  [
    id 220
    y 0.3845
    x 0.7223
    z 0.5
    label "YPR105C"
  ]
  node
  [
    id 221
    y 0.6881
    x 0.6941
    z 0.5
    label "YPL048W"
  ]
  node
  [
    id 222
    y 0.5268
    x 0.612
    z 0.5
    label "YBR118W"
  ]
  node
  [
    id 223
    y 0.3593
    x 0.5512
    z 0.5
    label "YER093C"
  ]
  node
  [
    id 224
    y 0.334
    x 0.4609
    z 0.5
    label "YOR310C"
  ]
  node
  [
    id 225
    y 0.2929
    x 0.4396
    z 0.5
    label "YPR137W"
  ]
  node
  [
    id 226
    y 0.6416
    x 0.1997
    z 0.5
    label "YJL154C"
  ]
  node
  [
    id 227
    y 0.6602
    x 0.2106
    z 0.5
    label "YJL053W"
  ]
  node
  [
    id 228
    y 0.7364
    x 0.7017
    z 0.5
    label "YHR043C"
  ]
  node
  [
    id 229
    y 0.6538
    x 0.6376
    z 0.5
    label "YHR107C"
  ]
  node
  [
    id 230
    y 0.5218
    x 0.5658
    z 0.5
    label "YHL035C"
  ]
  node
  [
    id 231
    y 0.5038
    x 0.7075
    z 0.5
    label "YHR166C"
  ]
  node
  [
    id 232
    y 0.5088
    x 0.7289
    z 0.5
    label "YKL022C"
  ]
  node
  [
    id 233
    y 0.5309
    x 0.6962
    z 0.5
    label "YLR447C"
  ]
  node
  [
    id 234
    y 0.2816
    x 0.4989
    z 0.5
    label "YFR044C"
  ]
  node
  [
    id 235
    y 0.4411
    x 0.5176
    z 0.5
    label "YDR170C"
  ]
  node
  [
    id 236
    y 0.5679
    x 0.5916
    z 0.5
    label "YNL298W"
  ]
  node
  [
    id 237
    y 0.5565
    x 0.5817
    z 0.5
    label "YPL115C"
  ]
  node
  [
    id 238
    y 0.3923
    x 0.3991
    z 0.5
    label "YNL005C"
  ]
  node
  [
    id 239
    y 0.431
    x 0.461
    z 0.5
    label "YGR162W"
  ]
  node
  [
    id 240
    y 0.3444
    x 0.5222
    z 0.5
    label "YOR341W"
  ]
  node
  [
    id 241
    y 0.4425
    x 0.5041
    z 0.5
    label "YPR010C"
  ]
  node
  [
    id 242
    y 0.2874
    x 0.6274
    z 0.5
    label "YCR081W"
  ]
  node
  [
    id 243
    y 0.3847
    x 0.5953
    z 0.5
    label "YOL051W"
  ]
  node
  [
    id 244
    y 0.4853
    x 0.6029
    z 0.5
    label "YHR099W"
  ]
  node
  [
    id 245
    y 0.4816
    x 0.5826
    z 0.5
    label "YGR252W"
  ]
  node
  [
    id 246
    y 0.5553
    x 0.3728
    z 0.5
    label "YOR001W"
  ]
  node
  [
    id 247
    y 0.4081
    x 0.5086
    z 0.5
    label "YER178W"
  ]
  node
  [
    id 248
    y 0.3393
    x 0.4541
    z 0.5
    label "YBR221C"
  ]
  node
  [
    id 249
    y 0.5058
    x 0.5854
    z 0.5
    label "YBL093C"
  ]
  node
  [
    id 250
    y 0.4399
    x 0.7766
    z 0.5
    label "YPL226W"
  ]
  node
  [
    id 251
    y 0.501
    x 0.627
    z 0.5
    label "YKL081W"
  ]
  node
  [
    id 252
    y 0.4614
    x 0.8266
    z 0.5
    label "YJL031C"
  ]
  node
  [
    id 253
    y 0.3024
    x 0.6621
    z 0.5
    label "YPR176C"
  ]
  node
  [
    id 254
    y 0.7177
    x 0.6454
    z 0.5
    label "YDL019C"
  ]
  node
  [
    id 255
    y 0.5801
    x 0.5858
    z 0.5
    label "YER120W"
  ]
  node
  [
    id 256
    y 0.3398
    x 0.4294
    z 0.5
    label "YJR052W"
  ]
  node
  [
    id 257
    y 0.5134
    x 0.5266
    z 0.5
    label "YLR442C"
  ]
  node
  [
    id 258
    y 0.3007
    x 0.3337
    z 0.5
    label "YOR179C"
  ]
  node
  [
    id 259
    y 0.3392
    x 0.4613
    z 0.5
    label "YAL043C"
  ]
  node
  [
    id 260
    y 0.3444
    x 0.4926
    z 0.5
    label "YER162C"
  ]
  node
  [
    id 261
    y 0.3063
    x 0.398
    z 0.5
    label "YFR021W"
  ]
  node
  [
    id 262
    y 0.3929
    x 0.4283
    z 0.5
    label "YBR055C"
  ]
  node
  [
    id 263
    y 0.4204
    x 0.3688
    z 0.5
    label "YPR082C"
  ]
  node
  [
    id 264
    y 0.4669
    x 0.5189
    z 0.5
    label "YDR394W"
  ]
  node
  [
    id 265
    y 0.4815
    x 0.4476
    z 0.5
    label "YER095W"
  ]
  node
  [
    id 266
    y 0.4513
    x 0.5774
    z 0.5
    label "YGL190C"
  ]
  node
  [
    id 267
    y 0.491
    x 0.5263
    z 0.5
    label "YDR502C"
  ]
  node
  [
    id 268
    y 0.3807
    x 0.6039
    z 0.5
    label "YDR062W"
  ]
  node
  [
    id 269
    y 0.4272
    x 0.3873
    z 0.5
    label "YML069W"
  ]
  node
  [
    id 270
    y 0.3911
    x 0.4212
    z 0.5
    label "YGL207W"
  ]
  node
  [
    id 271
    y 0.4872
    x 0.7279
    z 0.5
    label "YNL263C"
  ]
  node
  [
    id 272
    y 0.4911
    x 0.5219
    z 0.5
    label "YJR042W"
  ]
  node
  [
    id 273
    y 0.5149
    x 0.7017
    z 0.5
    label "YDR202C"
  ]
  node
  [
    id 274
    y 0.5095
    x 0.7029
    z 0.5
    label "YJR033C"
  ]
  node
  [
    id 275
    y 0.6585
    x 0.5751
    z 0.5
    label "YBR038W"
  ]
  node
  [
    id 276
    y 0.51
    x 0.8016
    z 0.5
    label "YDR118W"
  ]
  node
  [
    id 277
    y 0.487
    x 0.8008
    z 0.5
    label "YOR249C"
  ]
  node
  [
    id 278
    y 0.6196
    x 0.544
    z 0.5
    label "YJR059W"
  ]
  node
  [
    id 279
    y 0.4437
    x 0.5412
    z 0.5
    label "YOL123W"
  ]
  node
  [
    id 280
    y 0.4099
    x 0.4824
    z 0.5
    label "YER171W"
  ]
  node
  [
    id 281
    y 0.372
    x 0.2806
    z 0.5
    label "YDR462W"
  ]
  node
  [
    id 282
    y 0.4495
    x 0.6903
    z 0.5
    label "YDR054C"
  ]
  node
  [
    id 283
    y 0.4036
    x 0.4921
    z 0.5
    label "YDR301W"
  ]
  node
  [
    id 284
    y 0.3947
    x 0.4892
    z 0.5
    label "YJR093C"
  ]
  node
  [
    id 285
    y 0.3681
    x 0.35
    z 0.5
    label "YOR290C"
  ]
  node
  [
    id 286
    y 0.5548
    x 0.47
    z 0.5
    label "YJL141C"
  ]
  node
  [
    id 287
    y 0.5053
    x 0.5199
    z 0.5
    label "YNR052C"
  ]
  node
  [
    id 288
    y 0.3695
    x 0.4889
    z 0.5
    label "YJL109C"
  ]
  node
  [
    id 289
    y 0.3443
    x 0.5011
    z 0.5
    label "YJL069C"
  ]
  node
  [
    id 290
    y 0.4937
    x 0.5153
    z 0.5
    label "YBR155W"
  ]
  node
  [
    id 291
    y 0.4613
    x 0.6326
    z 0.5
    label "YPL240C"
  ]
  node
  [
    id 292
    y 0.3955
    x 0.4568
    z 0.5
    label "YMR201C"
  ]
  node
  [
    id 293
    y 0.5049
    x 0.3987
    z 0.5
    label "YPL022W"
  ]
  node
  [
    id 294
    y 0.4484
    x 0.7829
    z 0.5
    label "YDR265W"
  ]
  node
  [
    id 295
    y 0.4238
    x 0.4018
    z 0.5
    label "YJR022W"
  ]
  node
  [
    id 296
    y 0.4811
    x 0.3235
    z 0.5
    label "YBL103C"
  ]
  node
  [
    id 297
    y 0.5932
    x 0.3391
    z 0.5
    label "YOL067C"
  ]
  node
  [
    id 298
    y 0.468
    x 0.3158
    z 0.5
    label "YDR322W"
  ]
  node
  [
    id 299
    y 0.449
    x 0.2859
    z 0.5
    label "YNL185C"
  ]
  node
  [
    id 300
    y 0.3525
    x 0.4531
    z 0.5
    label "YGR156W"
  ]
  node
  [
    id 301
    y 0.4077
    x 0.4935
    z 0.5
    label "YLR115W"
  ]
  node
  [
    id 302
    y 0.5337
    x 0.641
    z 0.5
    label "YPR165W"
  ]
  node
  [
    id 303
    y 0.6493
    x 0.6048
    z 0.5
    label "YOR127W"
  ]
  node
  [
    id 304
    y 0.6451
    x 0.4485
    z 0.5
    label "YGR098C"
  ]
  node
  [
    id 305
    y 0.6522
    x 0.2051
    z 0.5
    label "YHR012W"
  ]
  node
  [
    id 306
    y 0.3985
    x 0.4153
    z 0.5
    label "YGR074W"
  ]
  node
  [
    id 307
    y 0.3886
    x 0.5915
    z 0.5
    label "YDL195W"
  ]
  node
  [
    id 308
    y 0.4259
    x 0.6378
    z 0.5
    label "YLR208W"
  ]
  node
  [
    id 309
    y 0.5218
    x 0.5341
    z 0.5
    label "YKR048C"
  ]
  node
  [
    id 310
    y 0.5544
    x 0.4022
    z 0.5
    label "YDR162C"
  ]
  node
  [
    id 311
    y 0.288
    x 0.4789
    z 0.5
    label "YBR169C"
  ]
  node
  [
    id 312
    y 0.3871
    x 0.6058
    z 0.5
    label "YDL005C"
  ]
  node
  [
    id 313
    y 0.5064
    x 0.7553
    z 0.5
    label "YJR086W"
  ]
  node
  [
    id 314
    y 0.4725
    x 0.6636
    z 0.5
    label "YOR212W"
  ]
  node
  [
    id 315
    y 0.6957
    x 0.5939
    z 0.5
    label "YEL013W"
  ]
  node
  [
    id 316
    y 0.7212
    x 0.7203
    z 0.5
    label "YPR185W"
  ]
  node
  [
    id 317
    y 0.438
    x 0.7888
    z 0.5
    label "YHR186C"
  ]
  node
  [
    id 318
    y 0.5978
    x 0.7074
    z 0.5
    label "YJR066W"
  ]
  node
  [
    id 319
    y 0.5933
    x 0.53
    z 0.5
    label "YOR181W"
  ]
  node
  [
    id 320
    y 0.6383
    x 0.4877
    z 0.5
    label "YHR016C"
  ]
  node
  [
    id 321
    y 0.5778
    x 0.5084
    z 0.5
    label "YIL156W"
  ]
  node
  [
    id 322
    y 0.5557
    x 0.525
    z 0.5
    label "YKL129C"
  ]
  node
  [
    id 323
    y 0.4148
    x 0.5103
    z 0.5
    label "YNL317W"
  ]
  node
  [
    id 324
    y 0.4482
    x 0.4059
    z 0.5
    label "YDR195W"
  ]
  node
  [
    id 325
    y 0.4789
    x 0.7359
    z 0.5
    label "YNL103W"
  ]
  node
  [
    id 326
    y 0.1476
    x 0.7018
    z 0.5
    label "YKR031C"
  ]
  node
  [
    id 327
    y 0.2521
    x 0.7331
    z 0.5
    label "YBR057C"
  ]
  node
  [
    id 328
    y 0.3046
    x 0.649
    z 0.5
    label "YOL082W"
  ]
  node
  [
    id 329
    y 0.372
    x 0.62
    z 0.5
    label "YKL103C"
  ]
  node
  [
    id 330
    y 0.6369
    x 0.6847
    z 0.5
    label "YPL258C"
  ]
  node
  [
    id 331
    y 0.512
    x 0.5958
    z 0.5
    label "YHR135C"
  ]
  node
  [
    id 332
    y 0.611
    x 0.5961
    z 0.5
    label "YIL106W"
  ]
  node
  [
    id 333
    y 0.5143
    x 0.6066
    z 0.5
    label "YGR092W"
  ]
  node
  [
    id 334
    y 0.5063
    x 0.5114
    z 0.5
    label "YLL026W"
  ]
  node
  [
    id 335
    y 0.2873
    x 0.3797
    z 0.5
    label "YBR065C"
  ]
  node
  [
    id 336
    y 0.305
    x 0.4054
    z 0.5
    label "YMR213W"
  ]
  node
  [
    id 337
    y 0.3674
    x 0.3867
    z 0.5
    label "YGR013W"
  ]
  node
  [
    id 338
    y 0.6932
    x 0.4635
    z 0.5
    label "YNL106C"
  ]
  node
  [
    id 339
    y 0.6057
    x 0.4719
    z 0.5
    label "YBL007C"
  ]
  node
  [
    id 340
    y 0.6799
    x 0.2907
    z 0.5
    label "YNL153C"
  ]
  node
  [
    id 341
    y 0.643
    x 0.3883
    z 0.5
    label "YLR212C"
  ]
  node
  [
    id 342
    y 0.4685
    x 0.5547
    z 0.5
    label "YML092C"
  ]
  node
  [
    id 343
    y 0.2849
    x 0.6131
    z 0.5
    label "YML098W"
  ]
  node
  [
    id 344
    y 0.4071
    x 0.5726
    z 0.5
    label "YDR167W"
  ]
  node
  [
    id 345
    y 0.3436
    x 0.3659
    z 0.5
    label "YAR003W"
  ]
  node
  [
    id 346
    y 0.4538
    x 0.5867
    z 0.5
    label "YPR181C"
  ]
  node
  [
    id 347
    y 0.4492
    x 0.6974
    z 0.5
    label "YLR026C"
  ]
  node
  [
    id 348
    y 0.5485
    x 0.5035
    z 0.5
    label "YPL004C"
  ]
  node
  [
    id 349
    y 0.5478
    x 0.5509
    z 0.5
    label "YDR490C"
  ]
  node
  [
    id 350
    y 0.5906
    x 0.3863
    z 0.5
    label "YML028W"
  ]
  node
  [
    id 351
    y 0.5366
    x 0.4626
    z 0.5
    label "YNL189W"
  ]
  node
  [
    id 352
    y 0.4105
    x 0.5403
    z 0.5
    label "YOL038W"
  ]
  node
  [
    id 353
    y 0.4635
    x 0.5685
    z 0.5
    label "YDL188C"
  ]
  node
  [
    id 354
    y 0.4071
    x 0.398
    z 0.5
    label "YDL051W"
  ]
  node
  [
    id 355
    y 0.4427
    x 0.3972
    z 0.5
    label "YHR089C"
  ]
  node
  [
    id 356
    y 0.3868
    x 0.5931
    z 0.5
    label "YPR103W"
  ]
  node
  [
    id 357
    y 0.3848
    x 0.5614
    z 0.5
    label "YHR200W"
  ]
  node
  [
    id 358
    y 0.3576
    x 0.7455
    z 0.5
    label "YIL162W"
  ]
  node
  [
    id 359
    y 0.4738
    x 0.8416
    z 0.5
    label "YLR378C"
  ]
  node
  [
    id 360
    y 0.2114
    x 0.3625
    z 0.5
    label "YJR140C"
  ]
  node
  [
    id 361
    y 0.5515
    x 0.5243
    z 0.5
    label "YGR086C"
  ]
  node
  [
    id 362
    y 0.5144
    x 0.4586
    z 0.5
    label "YBR010W"
  ]
  node
  [
    id 363
    y 0.5278
    x 0.562
    z 0.5
    label "YDR227W"
  ]
  node
  [
    id 364
    y 0.415
    x 0.6205
    z 0.5
    label "YCR012W"
  ]
  node
  [
    id 365
    y 0.3912
    x 0.5982
    z 0.5
    label "YOL135C"
  ]
  node
  [
    id 366
    y 0.5263
    x 0.6824
    z 0.5
    label "YDR328C"
  ]
  node
  [
    id 367
    y 0.5538
    x 0.7844
    z 0.5
    label "YML088W"
  ]
  node
  [
    id 368
    y 0.355
    x 0.5312
    z 0.5
    label "YLR044C"
  ]
  node
  [
    id 369
    y 0.3878
    x 0.5984
    z 0.5
    label "YIL038C"
  ]
  node
  [
    id 370
    y 0.4584
    x 0.4287
    z 0.5
    label "YBR084W"
  ]
  node
  [
    id 371
    y 0.2987
    x 0.5499
    z 0.5
    label "YBL024W"
  ]
  node
  [
    id 372
    y 0.5686
    x 0.4034
    z 0.5
    label "YML117W"
  ]
  node
  [
    id 373
    y 0.5513
    x 0.4069
    z 0.5
    label "YDR172W"
  ]
  node
  [
    id 374
    y 0.3622
    x 0.3948
    z 0.5
    label "YGR054W"
  ]
  node
  [
    id 375
    y 0.447
    x 0.5179
    z 0.5
    label "YGR245C"
  ]
  node
  [
    id 376
    y 0.4173
    x 0.5163
    z 0.5
    label "YER126C"
  ]
  node
  [
    id 377
    y 0.4303
    x 0.4032
    z 0.5
    label "YNL016W"
  ]
  node
  [
    id 378
    y 0.4204
    x 0.5381
    z 0.5
    label "YPR108W"
  ]
  node
  [
    id 379
    y 0.3858
    x 0.4419
    z 0.5
    label "YOR308C"
  ]
  node
  [
    id 380
    y 0.4002
    x 0.4585
    z 0.5
    label "YBL026W"
  ]
  node
  [
    id 381
    y 0.5472
    x 0.4762
    z 0.5
    label "YKL068W"
  ]
  node
  [
    id 382
    y 0.5016
    x 0.4747
    z 0.5
    label "YMR308C"
  ]
  node
  [
    id 383
    y 0.5137
    x 0.5262
    z 0.5
    label "YPL181W"
  ]
  node
  [
    id 384
    y 0.6059
    x 0.4026
    z 0.5
    label "YDL076C"
  ]
  node
  [
    id 385
    y 0.3165
    x 0.4381
    z 0.5
    label "YOR151C"
  ]
  node
  [
    id 386
    y 0.3271
    x 0.5054
    z 0.5
    label "YOR210W"
  ]
  node
  [
    id 387
    y 0.305
    x 0.516
    z 0.5
    label "YJR063W"
  ]
  node
  [
    id 388
    y 0.6196
    x 0.544
    z 0.5
    label "YNL339C"
  ]
  node
  [
    id 389
    y 0.2562
    x 0.4567
    z 0.5
    label "YNR054C"
  ]
  node
  [
    id 390
    y 0.4143
    x 0.3672
    z 0.5
    label "YNR023W"
  ]
  node
  [
    id 391
    y 0.4649
    x 0.5596
    z 0.5
    label "YDL042C"
  ]
  node
  [
    id 392
    y 0.5596
    x 0.2674
    z 0.5
    label "YIL068C"
  ]
  node
  [
    id 393
    y 0.4204
    x 0.5332
    z 0.5
    label "YBR102C"
  ]
  node
  [
    id 394
    y 0.4006
    x 0.5371
    z 0.5
    label "YDR427W"
  ]
  node
  [
    id 395
    y 0.4036
    x 0.5161
    z 0.5
    label "YER148W"
  ]
  node
  [
    id 396
    y 0.3779
    x 0.6423
    z 0.5
    label "YER016W"
  ]
  node
  [
    id 397
    y 0.4323
    x 0.5635
    z 0.5
    label "YGL011C"
  ]
  node
  [
    id 398
    y 0.6494
    x 0.5238
    z 0.5
    label "YBR112C"
  ]
  node
  [
    id 399
    y 0.5337
    x 0.3764
    z 0.5
    label "YHR028C"
  ]
  node
  [
    id 400
    y 0.5131
    x 0.4552
    z 0.5
    label "YBR109C"
  ]
  node
  [
    id 401
    y 0.4466
    x 0.5182
    z 0.5
    label "YDL007W"
  ]
  node
  [
    id 402
    y 0.4172
    x 0.5452
    z 0.5
    label "YEL037C"
  ]
  node
  [
    id 403
    y 0.4618
    x 0.7988
    z 0.5
    label "YBL084C"
  ]
  node
  [
    id 404
    y 0.3659
    x 0.4732
    z 0.5
    label "YHR052W"
  ]
  node
  [
    id 405
    y 0.6086
    x 0.5221
    z 0.5
    label "YER129W"
  ]
  node
  [
    id 406
    y 0.5393
    x 0.458
    z 0.5
    label "YGL115W"
  ]
  node
  [
    id 407
    y 0.4341
    x 0.7928
    z 0.5
    label "YNL172W"
  ]
  node
  [
    id 408
    y 0.4133
    x 0.7879
    z 0.5
    label "YDL008W"
  ]
  node
  [
    id 409
    y 0.3341
    x 0.6782
    z 0.5
    label "YDR484W"
  ]
  node
  [
    id 410
    y 0.3858
    x 0.7191
    z 0.5
    label "YDR027C"
  ]
  node
  [
    id 411
    y 0.6604
    x 0.5154
    z 0.5
    label "YNL078W"
  ]
  node
  [
    id 412
    y 0.6509
    x 0.417
    z 0.5
    label "YOL070C"
  ]
  node
  [
    id 413
    y 0.5499
    x 0.5668
    z 0.5
    label "YPL242C"
  ]
  node
  [
    id 414
    y 0.4117
    x 0.4858
    z 0.5
    label "YCL008C"
  ]
  node
  [
    id 415
    y 0.3764
    x 0.3784
    z 0.5
    label "YLR119W"
  ]
  node
  [
    id 416
    y 0.2976
    x 0.4118
    z 0.5
    label "YLR264W"
  ]
  node
  [
    id 417
    y 0.4103
    x 0.5316
    z 0.5
    label "YGL099W"
  ]
  node
  [
    id 418
    y 0.6289
    x 0.5633
    z 0.5
    label "YAL019W"
  ]
  node
  [
    id 419
    y 0.6514
    x 0.502
    z 0.5
    label "YOL045W"
  ]
  node
  [
    id 420
    y 0.6388
    x 0.3344
    z 0.5
    label "YDL214C"
  ]
  node
  [
    id 421
    y 0.376
    x 0.4734
    z 0.5
    label "YMR049C"
  ]
  node
  [
    id 422
    y 0.4558
    x 0.4957
    z 0.5
    label "YDL208W"
  ]
  node
  [
    id 423
    y 0.2955
    x 0.3739
    z 0.5
    label "YLR116W"
  ]
  node
  [
    id 424
    y 0.256
    x 0.4693
    z 0.5
    label "YKL074C"
  ]
  node
  [
    id 425
    y 0.6085
    x 0.3848
    z 0.5
    label "YMR224C"
  ]
  node
  [
    id 426
    y 0.355
    x 0.7664
    z 0.5
    label "YFR036W"
  ]
  node
  [
    id 427
    y 0.3726
    x 0.6597
    z 0.5
    label "YGR120C"
  ]
  node
  [
    id 428
    y 0.4737
    x 0.3137
    z 0.5
    label "YJL063C"
  ]
  node
  [
    id 429
    y 0.2284
    x 0.328
    z 0.5
    label "YMR312W"
  ]
  node
  [
    id 430
    y 0.2119
    x 0.3512
    z 0.5
    label "YPL101W"
  ]
  node
  [
    id 431
    y 0.463
    x 0.5216
    z 0.5
    label "YOR117W"
  ]
  node
  [
    id 432
    y 0.5359
    x 0.5513
    z 0.5
    label "YNL037C"
  ]
  node
  [
    id 433
    y 0.5668
    x 0.5669
    z 0.5
    label "YPL140C"
  ]
  node
  [
    id 434
    y 0.5544
    x 0.56
    z 0.5
    label "YFL039C"
  ]
  node
  [
    id 435
    y 0.4384
    x 0.5332
    z 0.5
    label "YGL048C"
  ]
  node
  [
    id 436
    y 0.4948
    x 0.5144
    z 0.5
    label "YFR034C"
  ]
  node
  [
    id 437
    y 0.5012
    x 0.6055
    z 0.5
    label "YLR216C"
  ]
  node
  [
    id 438
    y 0.5558
    x 0.5206
    z 0.5
    label "YMR109W"
  ]
  node
  [
    id 439
    y 0.805
    x 0.6485
    z 0.5
    label "YNL233W"
  ]
  node
  [
    id 440
    y 0.7066
    x 0.6166
    z 0.5
    label "YGR014W"
  ]
  node
  [
    id 441
    y 0.4211
    x 0.6566
    z 0.5
    label "YGL100W"
  ]
  node
  [
    id 442
    y 0.3705
    x 0.6128
    z 0.5
    label "YLR421C"
  ]
  node
  [
    id 443
    y 0.3401
    x 0.6119
    z 0.5
    label "YML015C"
  ]
  node
  [
    id 444
    y 0.5977
    x 0.398
    z 0.5
    label "YFL059W"
  ]
  node
  [
    id 445
    y 0.6771
    x 0.3141
    z 0.5
    label "YFL060C"
  ]
  node
  [
    id 446
    y 0.4491
    x 0.8004
    z 0.5
    label "YOR085W"
  ]
  node
  [
    id 447
    y 0.4379
    x 0.7987
    z 0.5
    label "YOR103C"
  ]
  node
  [
    id 448
    y 0.314
    x 0.639
    z 0.5
    label "YGL237C"
  ]
  node
  [
    id 449
    y 0.3709
    x 0.3951
    z 0.5
    label "YDR378C"
  ]
  node
  [
    id 450
    y 0.4972
    x 0.5362
    z 0.5
    label "YJL081C"
  ]
  node
  [
    id 451
    y 0.7158
    x 0.4842
    z 0.5
    label "YPL120W"
  ]
  node
  [
    id 452
    y 0.8115
    x 0.477
    z 0.5
    label "YBR128C"
  ]
  node
  [
    id 453
    y 0.3647
    x 0.433
    z 0.5
    label "YLR277C"
  ]
  node
  [
    id 454
    y 0.4106
    x 0.3674
    z 0.5
    label "YKL018W"
  ]
  node
  [
    id 455
    y 0.6144
    x 0.3685
    z 0.5
    label "YNL333W"
  ]
  node
  [
    id 456
    y 0.3834
    x 0.5663
    z 0.5
    label "YNR016C"
  ]
  node
  [
    id 457
    y 0.4368
    x 0.5053
    z 0.5
    label "YDL047W"
  ]
  node
  [
    id 458
    y 0.594
    x 0.4795
    z 0.5
    label "YDR356W"
  ]
  node
  [
    id 459
    y 0.3954
    x 0.3495
    z 0.5
    label "YPR020W"
  ]
  node
  [
    id 460
    y 0.395
    x 0.4203
    z 0.5
    label "YJR121W"
  ]
  node
  [
    id 461
    y 0.4987
    x 0.8006
    z 0.5
    label "YNL044W"
  ]
  node
  [
    id 462
    y 0.3977
    x 0.7733
    z 0.5
    label "YDR468C"
  ]
  node
  [
    id 463
    y 0.4235
    x 0.3974
    z 0.5
    label "YER155C"
  ]
  node
  [
    id 464
    y 0.5194
    x 0.3378
    z 0.5
    label "YNL102W"
  ]
  node
  [
    id 465
    y 0.6371
    x 0.3016
    z 0.5
    label "YJR043C"
  ]
  node
  [
    id 466
    y 0.6021
    x 0.6074
    z 0.5
    label "YKR101W"
  ]
  node
  [
    id 467
    y 0.6015
    x 0.5965
    z 0.5
    label "YML065W"
  ]
  node
  [
    id 468
    y 0.4586
    x 0.5805
    z 0.5
    label "YDR216W"
  ]
  node
  [
    id 469
    y 0.5295
    x 0.6355
    z 0.5
    label "YGR123C"
  ]
  node
  [
    id 470
    y 0.4349
    x 0.6149
    z 0.5
    label "YDR443C"
  ]
  node
  [
    id 471
    y 0.4546
    x 0.594
    z 0.5
    label "YPL042C"
  ]
  node
  [
    id 472
    y 0.5336
    x 0.6609
    z 0.5
    label "YJL005W"
  ]
  node
  [
    id 473
    y 0.4366
    x 0.5368
    z 0.5
    label "YBR142W"
  ]
  node
  [
    id 474
    y 0.5474
    x 0.7558
    z 0.5
    label "YML038C"
  ]
  node
  [
    id 475
    y 0.5236
    x 0.6811
    z 0.5
    label "YLR452C"
  ]
  node
  [
    id 476
    y 0.461
    x 0.3493
    z 0.5
    label "YGR264C"
  ]
  node
  [
    id 477
    y 0.4783
    x 0.5781
    z 0.5
    label "YGL112C"
  ]
  node
  [
    id 478
    y 0.5273
    x 0.5389
    z 0.5
    label "YDR190C"
  ]
  node
  [
    id 479
    y 0.4516
    x 0.5948
    z 0.5
    label "YAL021C"
  ]
  node
  [
    id 480
    y 0.5716
    x 0.3987
    z 0.5
    label "YBL035C"
  ]
  node
  [
    id 481
    y 0.468
    x 0.4709
    z 0.5
    label "YKL104C"
  ]
  node
  [
    id 482
    y 0.366
    x 0.5159
    z 0.5
    label "YOR361C"
  ]
  node
  [
    id 483
    y 0.4115
    x 0.4168
    z 0.5
    label "YPL001W"
  ]
  node
  [
    id 484
    y 0.5723
    x 0.4109
    z 0.5
    label "YLR370C"
  ]
  node
  [
    id 485
    y 0.4126
    x 0.2273
    z 0.5
    label "YHR056C"
  ]
  node
  [
    id 486
    y 0.4918
    x 0.37
    z 0.5
    label "YDR303C"
  ]
  node
  [
    id 487
    y 0.5438
    x 0.4391
    z 0.5
    label "YOR373W"
  ]
  node
  [
    id 488
    y 0.5918
    x 0.4783
    z 0.5
    label "YDL239C"
  ]
  node
  [
    id 489
    y 0.3952
    x 0.7406
    z 0.5
    label "YIL105C"
  ]
  node
  [
    id 490
    y 0.3532
    x 0.6334
    z 0.5
    label "YNL307C"
  ]
  node
  [
    id 491
    y 0.652
    x 0.3302
    z 0.5
    label "YOL059W"
  ]
  node
  [
    id 492
    y 0.5151
    x 0.5388
    z 0.5
    label "YNR031C"
  ]
  node
  [
    id 493
    y 0.3848
    x 0.5178
    z 0.5
    label "YDL043C"
  ]
  node
  [
    id 494
    y 0.4073
    x 0.5055
    z 0.5
    label "YLR180W"
  ]
  node
  [
    id 495
    y 0.2742
    x 0.4547
    z 0.5
    label "YER090W"
  ]
  node
  [
    id 496
    y 0.4466
    x 0.5067
    z 0.5
    label "YFR004W"
  ]
  node
  [
    id 497
    y 0.4232
    x 0.4312
    z 0.5
    label "YER112W"
  ]
  node
  [
    id 498
    y 0.5238
    x 0.5484
    z 0.5
    label "YGL092W"
  ]
  node
  [
    id 499
    y 0.4987
    x 0.8008
    z 0.5
    label "YPL152W"
  ]
  node
  [
    id 500
    y 0.4757
    x 0.6539
    z 0.5
    label "YAL016W"
  ]
  node
  [
    id 501
    y 0.4336
    x 0.5593
    z 0.5
    label "YNL287W"
  ]
  node
  [
    id 502
    y 0.3489
    x 0.628
    z 0.5
    label "YER122C"
  ]
  node
  [
    id 503
    y 0.5145
    x 0.6217
    z 0.5
    label "YOR027W"
  ]
  node
  [
    id 504
    y 0.5025
    x 0.5346
    z 0.5
    label "YGR192C"
  ]
  node
  [
    id 505
    y 0.4244
    x 0.4069
    z 0.5
    label "YOR014W"
  ]
  node
  [
    id 506
    y 0.4054
    x 0.5485
    z 0.5
    label "YHL030W"
  ]
  node
  [
    id 507
    y 0.6518
    x 0.6103
    z 0.5
    label "YCR002C"
  ]
  node
  [
    id 508
    y 0.5731
    x 0.4233
    z 0.5
    label "YNL312W"
  ]
  node
  [
    id 509
    y 0.5208
    x 0.4381
    z 0.5
    label "YOL108C"
  ]
  node
  [
    id 510
    y 0.5723
    x 0.4911
    z 0.5
    label "YPL249C-A"
  ]
  node
  [
    id 511
    y 0.4477
    x 0.7644
    z 0.5
    label "YOR281C"
  ]
  node
  [
    id 512
    y 0.2063
    x 0.7876
    z 0.5
    label "YDL217C"
  ]
  node
  [
    id 513
    y 0.1434
    x 0.8237
    z 0.5
    label "YOR297C"
  ]
  node
  [
    id 514
    y 0.6108
    x 0.493
    z 0.5
    label "YLR144C"
  ]
  node
  [
    id 515
    y 0.6226
    x 0.6922
    z 0.5
    label "YJR076C"
  ]
  node
  [
    id 516
    y 0.5645
    x 0.8668
    z 0.5
    label "YLR306W"
  ]
  node
  [
    id 517
    y 0.5035
    x 0.7849
    z 0.5
    label "YDR139C"
  ]
  node
  [
    id 518
    y 0.5223
    x 0.4372
    z 0.5
    label "YER173W"
  ]
  node
  [
    id 519
    y 0.6052
    x 0.3361
    z 0.5
    label "YBR087W"
  ]
  node
  [
    id 520
    y 0.4668
    x 0.5377
    z 0.5
    label "YMR314W"
  ]
  node
  [
    id 521
    y 0.1976
    x 0.7176
    z 0.5
    label "YOL062C"
  ]
  node
  [
    id 522
    y 0.1247
    x 0.7326
    z 0.5
    label "YBL037W"
  ]
  node
  [
    id 523
    y 0.5349
    x 0.5721
    z 0.5
    label "YHR030C"
  ]
  node
  [
    id 524
    y 0.5748
    x 0.5752
    z 0.5
    label "YLR182W"
  ]
  node
  [
    id 525
    y 0.2004
    x 0.2598
    z 0.5
    label "YKR092C"
  ]
  node
  [
    id 526
    y 0.3306
    x 0.6207
    z 0.5
    label "YPR070W"
  ]
  node
  [
    id 527
    y 0.5501
    x 0.4745
    z 0.5
    label "YGR119C"
  ]
  node
  [
    id 528
    y 0.3334
    x 0.6934
    z 0.5
    label "YCR024C-A"
  ]
  node
  [
    id 529
    y 0.4881
    x 0.6286
    z 0.5
    label "YGL008C"
  ]
  node
  [
    id 530
    y 0.4849
    x 0.3204
    z 0.5
    label "YGL163C"
  ]
  node
  [
    id 531
    y 0.6506
    x 0.4574
    z 0.5
    label "YMR255W"
  ]
  node
  [
    id 532
    y 0.6213
    x 0.5528
    z 0.5
    label "YDL207W"
  ]
  node
  [
    id 533
    y 0.3831
    x 0.2946
    z 0.5
    label "YJL176C"
  ]
  node
  [
    id 534
    y 0.6546
    x 0.5316
    z 0.5
    label "YAL009W"
  ]
  node
  [
    id 535
    y 0.5752
    x 0.5911
    z 0.5
    label "YGL206C"
  ]
  node
  [
    id 536
    y 0.4886
    x 0.4544
    z 0.5
    label "YDL059C"
  ]
  node
  [
    id 537
    y 0.6228
    x 0.4122
    z 0.5
    label "YNL331C"
  ]
  node
  [
    id 538
    y 0.7375
    x 0.566
    z 0.5
    label "YGR294W"
  ]
  node
  [
    id 539
    y 0.6083
    x 0.5727
    z 0.5
    label "YJR091C"
  ]
  node
  [
    id 540
    y 0.4861
    x 0.5786
    z 0.5
    label "YDR212W"
  ]
  node
  [
    id 541
    y 0.541
    x 0.6054
    z 0.5
    label "YGL116W"
  ]
  node
  [
    id 542
    y 0.5337
    x 0.3764
    z 0.5
    label "YFR054C"
  ]
  node
  [
    id 543
    y 0.4353
    x 0.285
    z 0.5
    label "YPR057W"
  ]
  node
  [
    id 544
    y 0.376
    x 0.4152
    z 0.5
    label "YML081C-A"
  ]
  node
  [
    id 545
    y 0.3658
    x 0.3784
    z 0.5
    label "YDR377W"
  ]
  node
  [
    id 546
    y 0.437
    x 0.396
    z 0.5
    label "YKL016C"
  ]
  node
  [
    id 547
    y 0.3824
    x 0.6565
    z 0.5
    label "YHR041C"
  ]
  node
  [
    id 548
    y 0.6745
    x 0.2214
    z 0.5
    label "YKL019W"
  ]
  node
  [
    id 549
    y 0.7193
    x 0.1588
    z 0.5
    label "YGL155W"
  ]
  node
  [
    id 550
    y 0.196
    x 0.3881
    z 0.5
    label "YNL242W"
  ]
  node
  [
    id 551
    y 0.5493
    x 0.709
    z 0.5
    label "YDL132W"
  ]
  node
  [
    id 552
    y 0.4269
    x 0.368
    z 0.5
    label "YML095C"
  ]
  node
  [
    id 553
    y 0.3925
    x 0.5991
    z 0.5
    label "YDL165W"
  ]
  node
  [
    id 554
    y 0.5721
    x 0.7653
    z 0.5
    label "YLL016W"
  ]
  node
  [
    id 555
    y 0.4921
    x 0.6915
    z 0.5
    label "YLR310C"
  ]
  node
  [
    id 556
    y 0.2795
    x 0.5168
    z 0.5
    label "YDL148C"
  ]
  node
  [
    id 557
    y 0.6132
    x 0.5028
    z 0.5
    label "YDR395W"
  ]
  node
  [
    id 558
    y 0.3475
    x 0.3664
    z 0.5
    label "YMR288W"
  ]
  node
  [
    id 559
    y 0.3867
    x 0.428
    z 0.5
    label "YMR240C"
  ]
  node
  [
    id 560
    y 0.5478
    x 0.605
    z 0.5
    label "YJR072C"
  ]
  node
  [
    id 561
    y 0.4956
    x 0.5292
    z 0.5
    label "YKL021C"
  ]
  node
  [
    id 562
    y 0.3713
    x 0.4262
    z 0.5
    label "YKL012W"
  ]
  node
  [
    id 563
    y 0.4563
    x 0.4389
    z 0.5
    label "YIL061C"
  ]
  node
  [
    id 564
    y 0.5698
    x 0.4989
    z 0.5
    label "YOR098C"
  ]
  node
  [
    id 565
    y 0.623
    x 0.3892
    z 0.5
    label "YNL273W"
  ]
  node
  [
    id 566
    y 0.5175
    x 0.3046
    z 0.5
    label "YOL006C"
  ]
  node
  [
    id 567
    y 0.3029
    x 0.512
    z 0.5
    label "YDL150W"
  ]
  node
  [
    id 568
    y 0.3404
    x 0.4434
    z 0.5
    label "YOR116C"
  ]
  node
  [
    id 569
    y 0.5839
    x 0.6368
    z 0.5
    label "YHR061C"
  ]
  node
  [
    id 570
    y 0.5954
    x 0.6282
    z 0.5
    label "YLR229C"
  ]
  node
  [
    id 571
    y 0.3773
    x 0.6346
    z 0.5
    label "YER094C"
  ]
  node
  [
    id 572
    y 0.5604
    x 0.7031
    z 0.5
    label "YMR043W"
  ]
  node
  [
    id 573
    y 0.369
    x 0.5391
    z 0.5
    label "YBR279W"
  ]
  node
  [
    id 574
    y 0.3194
    x 0.52
    z 0.5
    label "YOR123C"
  ]
  node
  [
    id 575
    y 0.488
    x 0.5416
    z 0.5
    label "YER165W"
  ]
  node
  [
    id 576
    y 0.6338
    x 0.5947
    z 0.5
    label "YDL229W"
  ]
  node
  [
    id 577
    y 0.6114
    x 0.6142
    z 0.5
    label "YNL064C"
  ]
  node
  [
    id 578
    y 0.6851
    x 0.294
    z 0.5
    label "YGR078C"
  ]
  node
  [
    id 579
    y 0.5219
    x 0.3213
    z 0.5
    label "YMR263W"
  ]
  node
  [
    id 580
    y 0.5283
    x 0.4541
    z 0.5
    label "YOL004W"
  ]
  node
  [
    id 581
    y 0.6841
    x 0.5186
    z 0.5
    label "YFL008W"
  ]
  node
  [
    id 582
    y 0.479
    x 0.5269
    z 0.5
    label "YGL086W"
  ]
  node
  [
    id 583
    y 0.5129
    x 0.5157
    z 0.5
    label "YPR154W"
  ]
  node
  [
    id 584
    y 0.3651
    x 0.7049
    z 0.5
    label "YBL050W"
  ]
  node
  [
    id 585
    y 0.4659
    x 0.2768
    z 0.5
    label "YMR318C"
  ]
  node
  [
    id 586
    y 0.4964
    x 0.4209
    z 0.5
    label "YJR035W"
  ]
  node
  [
    id 587
    y 0.7301
    x 0.6471
    z 0.5
    label "YBR295W"
  ]
  node
  [
    id 588
    y 0.5736
    x 0.5312
    z 0.5
    label "YPR111W"
  ]
  node
  [
    id 589
    y 0.4563
    x 0.4606
    z 0.5
    label "YDR225W"
  ]
  node
  [
    id 590
    y 0.3199
    x 0.3015
    z 0.5
    label "YGR021W"
  ]
  node
  [
    id 591
    y 0.3762
    x 0.4299
    z 0.5
    label "YGL120C"
  ]
  node
  [
    id 592
    y 0.5024
    x 0.4769
    z 0.5
    label "YBR011C"
  ]
  node
  [
    id 593
    y 0.5088
    x 0.4951
    z 0.5
    label "YMR172W"
  ]
  node
  [
    id 594
    y 0.4207
    x 0.2951
    z 0.5
    label "YJR109C"
  ]
  node
  [
    id 595
    y 0.4371
    x 0.1916
    z 0.5
    label "YOR303W"
  ]
  node
  [
    id 596
    y 0.507
    x 0.6165
    z 0.5
    label "YLR362W"
  ]
  node
  [
    id 597
    y 0.496
    x 0.6946
    z 0.5
    label "YJL128C"
  ]
  node
  [
    id 598
    y 0.6228
    x 0.4408
    z 0.5
    label "YPR171W"
  ]
  node
  [
    id 599
    y 0.5298
    x 0.524
    z 0.5
    label "YHL002W"
  ]
  node
  [
    id 600
    y 0.3806
    x 0.6749
    z 0.5
    label "YGL200C"
  ]
  node
  [
    id 601
    y 0.3056
    x 0.5413
    z 0.5
    label "YBR236C"
  ]
  node
  [
    id 602
    y 0.26
    x 0.4114
    z 0.5
    label "YGR047C"
  ]
  node
  [
    id 603
    y 0.3171
    x 0.5509
    z 0.5
    label "YOR110W"
  ]
  node
  [
    id 604
    y 0.3482
    x 0.5579
    z 0.5
    label "YDR311W"
  ]
  node
  [
    id 605
    y 0.4074
    x 0.3045
    z 0.5
    label "YLR298C"
  ]
  node
  [
    id 606
    y 0.4132
    x 0.3264
    z 0.5
    label "YDL087C"
  ]
  node
  [
    id 607
    y 0.492
    x 0.4839
    z 0.5
    label "YDR127W"
  ]
  node
  [
    id 608
    y 0.3606
    x 0.5985
    z 0.5
    label "YJR045C"
  ]
  node
  [
    id 609
    y 0.2445
    x 0.6668
    z 0.5
    label "YIL022W"
  ]
  node
  [
    id 610
    y 0.386
    x 0.3875
    z 0.5
    label "YLR275W"
  ]
  node
  [
    id 611
    y 0.5263
    x 0.4918
    z 0.5
    label "YER125W"
  ]
  node
  [
    id 612
    y 0.4106
    x 0.4036
    z 0.5
    label "YBL032W"
  ]
  node
  [
    id 613
    y 0.3696
    x 0.5854
    z 0.5
    label "YGR135W"
  ]
  node
  [
    id 614
    y 0.5278
    x 0.5533
    z 0.5
    label "YKL130C"
  ]
  node
  [
    id 615
    y 0.2026
    x 0.6177
    z 0.5
    label "YFL016C"
  ]
  node
  [
    id 616
    y 0.4143
    x 0.5226
    z 0.5
    label "YNL110C"
  ]
  node
  [
    id 617
    y 0.4107
    x 0.5273
    z 0.5
    label "YER021W"
  ]
  node
  [
    id 618
    y 0.5128
    x 0.6098
    z 0.5
    label "YBL105C"
  ]
  node
  [
    id 619
    y 0.5535
    x 0.6553
    z 0.5
    label "YER118C"
  ]
  node
  [
    id 620
    y 0.6042
    x 0.6447
    z 0.5
    label "YJR090C"
  ]
  node
  [
    id 621
    y 0.5739
    x 0.6122
    z 0.5
    label "YPL256C"
  ]
  node
  [
    id 622
    y 0.6085
    x 0.5609
    z 0.5
    label "YKR082W"
  ]
  node
  [
    id 623
    y 0.4034
    x 0.4833
    z 0.5
    label "YNL132W"
  ]
  node
  [
    id 624
    y 0.4192
    x 0.6206
    z 0.5
    label "YDR448W"
  ]
  node
  [
    id 625
    y 0.5814
    x 0.6229
    z 0.5
    label "YOR231W"
  ]
  node
  [
    id 626
    y 0.5166
    x 0.7222
    z 0.5
    label "YNL154C"
  ]
  node
  [
    id 627
    y 0.477
    x 0.6674
    z 0.5
    label "YML001W"
  ]
  node
  [
    id 628
    y 0.5906
    x 0.5636
    z 0.5
    label "YLR337C"
  ]
  node
  [
    id 629
    y 0.4177
    x 0.5719
    z 0.5
    label "YCR093W"
  ]
  node
  [
    id 630
    y 0.3413
    x 0.3354
    z 0.5
    label "YBR258C"
  ]
  node
  [
    id 631
    y 0.3363
    x 0.3402
    z 0.5
    label "YDR469W"
  ]
  node
  [
    id 632
    y 0.7131
    x 0.4746
    z 0.5
    label "YPL124W"
  ]
  node
  [
    id 633
    y 0.2093
    x 0.6542
    z 0.5
    label "YNR017W"
  ]
  node
  [
    id 634
    y 0.2236
    x 0.7657
    z 0.5
    label "YMR203W"
  ]
  node
  [
    id 635
    y 0.3154
    x 0.7824
    z 0.5
    label "YGR082W"
  ]
  node
  [
    id 636
    y 0.768
    x 0.5061
    z 0.5
    label "YOR298C-A"
  ]
  node
  [
    id 637
    y 0.6585
    x 0.5384
    z 0.5
    label "YDR099W"
  ]
  node
  [
    id 638
    y 0.7012
    x 0.3858
    z 0.5
    label "YJL048C"
  ]
  node
  [
    id 639
    y 0.4493
    x 0.7013
    z 0.5
    label "YGR161C"
  ]
  node
  [
    id 640
    y 0.4095
    x 0.4142
    z 0.5
    label "YPL082C"
  ]
  node
  [
    id 641
    y 0.6513
    x 0.4128
    z 0.5
    label "YBR049C"
  ]
  node
  [
    id 642
    y 0.7044
    x 0.5389
    z 0.5
    label "YNR065C"
  ]
  node
  [
    id 643
    y 0.3074
    x 0.3372
    z 0.5
    label "YFL066C"
  ]
  node
  [
    id 644
    y 0.4098
    x 0.3955
    z 0.5
    label "YER146W"
  ]
  node
  [
    id 645
    y 0.6467
    x 0.6028
    z 0.5
    label "YGR221C"
  ]
  node
  [
    id 646
    y 0.3724
    x 0.746
    z 0.5
    label "YDR244W"
  ]
  node
  [
    id 647
    y 0.3633
    x 0.8406
    z 0.5
    label "YGL205W"
  ]
  node
  [
    id 648
    y 0.1634
    x 0.5402
    z 0.5
    label "YMR025W"
  ]
  node
  [
    id 649
    y 0.2625
    x 0.5452
    z 0.5
    label "YDL216C"
  ]
  node
  [
    id 650
    y 0.5485
    x 0.5349
    z 0.5
    label "YML057W"
  ]
  node
  [
    id 651
    y 0.3625
    x 0.546
    z 0.5
    label "YOR206W"
  ]
  node
  [
    id 652
    y 0.377
    x 0.4577
    z 0.5
    label "YDR060W"
  ]
  node
  [
    id 653
    y 0.6468
    x 0.5007
    z 0.5
    label "YLR178C"
  ]
  node
  [
    id 654
    y 0.6391
    x 0.4906
    z 0.5
    label "YGL097W"
  ]
  node
  [
    id 655
    y 0.4788
    x 0.8185
    z 0.5
    label "YJR060W"
  ]
  node
  [
    id 656
    y 0.4165
    x 0.5583
    z 0.5
    label "YPR086W"
  ]
  node
  [
    id 657
    y 0.5307
    x 0.5248
    z 0.5
    label "YHR027C"
  ]
  node
  [
    id 658
    y 0.461
    x 0.5456
    z 0.5
    label "YPR055W"
  ]
  node
  [
    id 659
    y 0.4411
    x 0.4632
    z 0.5
    label "YMR012W"
  ]
  node
  [
    id 660
    y 0.4964
    x 0.6327
    z 0.5
    label "YJL164C"
  ]
  node
  [
    id 661
    y 0.5386
    x 0.5288
    z 0.5
    label "YLR438W"
  ]
  node
  [
    id 662
    y 0.7026
    x 0.6924
    z 0.5
    label "YKL092C"
  ]
  node
  [
    id 663
    y 0.3803
    x 0.6032
    z 0.5
    label "YER179W"
  ]
  node
  [
    id 664
    y 0.4218
    x 0.3445
    z 0.5
    label "YPL078C"
  ]
  node
  [
    id 665
    y 0.4672
    x 0.4497
    z 0.5
    label "YML074C"
  ]
  node
  [
    id 666
    y 0.4301
    x 0.588
    z 0.5
    label "YDL145C"
  ]
  node
  [
    id 667
    y 0.2606
    x 0.716
    z 0.5
    label "YIL160C"
  ]
  node
  [
    id 668
    y 0.3767
    x 0.6622
    z 0.5
    label "YDR142C"
  ]
  node
  [
    id 669
    y 0.6061
    x 0.4137
    z 0.5
    label "YGR163W"
  ]
  node
  [
    id 670
    y 0.2293
    x 0.6609
    z 0.5
    label "YFL037W"
  ]
  node
  [
    id 671
    y 0.2206
    x 0.6224
    z 0.5
    label "YNL223W"
  ]
  node
  [
    id 672
    y 0.3601
    x 0.4921
    z 0.5
    label "YPL129W"
  ]
  node
  [
    id 673
    y 0.3072
    x 0.7899
    z 0.5
    label "YPL232W"
  ]
  node
  [
    id 674
    y 0.396
    x 0.8208
    z 0.5
    label "YMR017W"
  ]
  node
  [
    id 675
    y 0.4703
    x 0.689
    z 0.5
    label "YDL006W"
  ]
  node
  [
    id 676
    y 0.3164
    x 0.5374
    z 0.5
    label "YGR184C"
  ]
  node
  [
    id 677
    y 0.3215
    x 0.5493
    z 0.5
    label "YGL058W"
  ]
  node
  [
    id 678
    y 0.448
    x 0.5392
    z 0.5
    label "YKL145W"
  ]
  node
  [
    id 679
    y 0.3533
    x 0.3496
    z 0.5
    label "YGR234W"
  ]
  node
  [
    id 680
    y 0.4588
    x 0.532
    z 0.5
    label "YPL093W"
  ]
  node
  [
    id 681
    y 0.7104
    x 0.4176
    z 0.5
    label "YIL026C"
  ]
  node
  [
    id 682
    y 0.6943
    x 0.4894
    z 0.5
    label "YDL003W"
  ]
  node
  [
    id 683
    y 0.4772
    x 0.5379
    z 0.5
    label "YGL195W"
  ]
  node
  [
    id 684
    y 0.4527
    x 0.4172
    z 0.5
    label "YPR178W"
  ]
  node
  [
    id 685
    y 0.3994
    x 0.4197
    z 0.5
    label "YDR473C"
  ]
  node
  [
    id 686
    y 0.2741
    x 0.4279
    z 0.5
    label "YGR225W"
  ]
  node
  [
    id 687
    y 0.6647
    x 0.5474
    z 0.5
    label "YBR024W"
  ]
  node
  [
    id 688
    y 0.5897
    x 0.6103
    z 0.5
    label "YBR200W"
  ]
  node
  [
    id 689
    y 0.6221
    x 0.5377
    z 0.5
    label "YPR054W"
  ]
  node
  [
    id 690
    y 0.4716
    x 0.6553
    z 0.5
    label "YLR342W"
  ]
  node
  [
    id 691
    y 0.335
    x 0.6288
    z 0.5
    label "YML130C"
  ]
  node
  [
    id 692
    y 0.1177
    x 0.5118
    z 0.5
    label "YLL041C"
  ]
  node
  [
    id 693
    y 0.2038
    x 0.5337
    z 0.5
    label "YBR044C"
  ]
  node
  [
    id 694
    y 0.5089
    x 0.4723
    z 0.5
    label "YBL047C"
  ]
  node
  [
    id 695
    y 0.4469
    x 0.3829
    z 0.5
    label "YOL054W"
  ]
  node
  [
    id 696
    y 0.3076
    x 0.4881
    z 0.5
    label "YLR418C"
  ]
  node
  [
    id 697
    y 0.2884
    x 0.5292
    z 0.5
    label "YHR020W"
  ]
  node
  [
    id 698
    y 0.7796
    x 0.4286
    z 0.5
    label "YNL188W"
  ]
  node
  [
    id 699
    y 0.7995
    x 0.4298
    z 0.5
    label "YPL255W"
  ]
  node
  [
    id 700
    y 0.5032
    x 0.6692
    z 0.5
    label "YHR084W"
  ]
  node
  [
    id 701
    y 0.443
    x 0.4768
    z 0.5
    label "YAL036C"
  ]
  node
  [
    id 702
    y 0.6436
    x 0.5396
    z 0.5
    label "YPR141C"
  ]
  node
  [
    id 703
    y 0.4218
    x 0.6267
    z 0.5
    label "YBL041W"
  ]
  node
  [
    id 704
    y 0.2236
    x 0.5024
    z 0.5
    label "YPR112C"
  ]
  node
  [
    id 705
    y 0.374
    x 0.5297
    z 0.5
    label "YER082C"
  ]
  node
  [
    id 706
    y 0.6032
    x 0.4724
    z 0.5
    label "YPR175W"
  ]
  node
  [
    id 707
    y 0.3969
    x 0.4403
    z 0.5
    label "YGL130W"
  ]
  node
  [
    id 708
    y 0.512
    x 0.3204
    z 0.5
    label "YLR263W"
  ]
  node
  [
    id 709
    y 0.5149
    x 0.4363
    z 0.5
    label "YER133W"
  ]
  node
  [
    id 710
    y 0.5697
    x 0.3949
    z 0.5
    label "YML032C"
  ]
  node
  [
    id 711
    y 0.3078
    x 0.5949
    z 0.5
    label "YJL029C"
  ]
  node
  [
    id 712
    y 0.5747
    x 0.5877
    z 0.5
    label "YDR146C"
  ]
  node
  [
    id 713
    y 0.5851
    x 0.65
    z 0.5
    label "YBL016W"
  ]
  node
  [
    id 714
    y 0.5975
    x 0.6309
    z 0.5
    label "YNL053W"
  ]
  node
  [
    id 715
    y 0.5158
    x 0.6039
    z 0.5
    label "YMR273C"
  ]
  node
  [
    id 716
    y 0.4259
    x 0.5064
    z 0.5
    label "YOR358W"
  ]
  node
  [
    id 717
    y 0.2636
    x 0.5715
    z 0.5
    label "YBL021C"
  ]
  node
  [
    id 718
    y 0.387
    x 0.6287
    z 0.5
    label "YFR050C"
  ]
  node
  [
    id 719
    y 0.5758
    x 0.3853
    z 0.5
    label "YBL076C"
  ]
  node
  [
    id 720
    y 0.3002
    x 0.4616
    z 0.5
    label "YBR058C"
  ]
  node
  [
    id 721
    y 0.7164
    x 0.3751
    z 0.5
    label "YLR105C"
  ]
  node
  [
    id 722
    y 0.6638
    x 0.4765
    z 0.5
    label "YHR114W"
  ]
  node
  [
    id 723
    y 0.6652
    x 0.5777
    z 0.5
    label "YGR238C"
  ]
  node
  [
    id 724
    y 0.3896
    x 0.6613
    z 0.5
    label "YIL045W"
  ]
  node
  [
    id 725
    y 0.4786
    x 0.456
    z 0.5
    label "YAR014C"
  ]
  node
  [
    id 726
    y 0.392
    x 0.4954
    z 0.5
    label "YIL150C"
  ]
  node
  [
    id 727
    y 0.529
    x 0.5225
    z 0.5
    label "YBL023C"
  ]
  node
  [
    id 728
    y 0.6958
    x 0.7706
    z 0.5
    label "YNL314W"
  ]
  node
  [
    id 729
    y 0.5491
    x 0.7393
    z 0.5
    label "YGL022W"
  ]
  node
  [
    id 730
    y 0.4581
    x 0.8
    z 0.5
    label "YDL232W"
  ]
  node
  [
    id 731
    y 0.2931
    x 0.4447
    z 0.5
    label "YDR416W"
  ]
  node
  [
    id 732
    y 0.4455
    x 0.6514
    z 0.5
    label "YGL151W"
  ]
  node
  [
    id 733
    y 0.339
    x 0.4954
    z 0.5
    label "YKR002W"
  ]
  node
  [
    id 734
    y 0.2731
    x 0.5937
    z 0.5
    label "YFL006W"
  ]
  node
  [
    id 735
    y 0.3673
    x 0.4793
    z 0.5
    label "YDR496C"
  ]
  node
  [
    id 736
    y 0.3791
    x 0.5307
    z 0.5
    label "YLR175W"
  ]
  node
  [
    id 737
    y 0.6995
    x 0.6716
    z 0.5
    label "YAR042W"
  ]
  node
  [
    id 738
    y 0.4851
    x 0.4753
    z 0.5
    label "YBL039C"
  ]
  node
  [
    id 739
    y 0.5145
    x 0.6547
    z 0.5
    label "YJL106W"
  ]
  node
  [
    id 740
    y 0.3316
    x 0.3274
    z 0.5
    label "YPL138C"
  ]
  node
  [
    id 741
    y 0.4264
    x 0.6104
    z 0.5
    label "YDR176W"
  ]
  node
  [
    id 742
    y 0.4661
    x 0.6702
    z 0.5
    label "YPL254W"
  ]
  node
  [
    id 743
    y 0.379
    x 0.7067
    z 0.5
    label "YNL041C"
  ]
  node
  [
    id 744
    y 0.3845
    x 0.4226
    z 0.5
    label "YDL031W"
  ]
  node
  [
    id 745
    y 0.2947
    x 0.7576
    z 0.5
    label "YAL030W"
  ]
  node
  [
    id 746
    y 0.3249
    x 0.6761
    z 0.5
    label "YGR009C"
  ]
  node
  [
    id 747
    y 0.3981
    x 0.3786
    z 0.5
    label "YER172C"
  ]
  node
  [
    id 748
    y 0.2903
    x 0.3057
    z 0.5
    label "YPL105C"
  ]
  node
  [
    id 749
    y 0.3393
    x 0.4264
    z 0.5
    label "YHR170W"
  ]
  node
  [
    id 750
    y 0.3109
    x 0.5281
    z 0.5
    label "YNL315C"
  ]
  node
  [
    id 751
    y 0.4483
    x 0.6464
    z 0.5
    label "YDL134C"
  ]
  node
  [
    id 752
    y 0.3754
    x 0.3938
    z 0.5
    label "YDR460W"
  ]
  node
  [
    id 753
    y 0.329
    x 0.4649
    z 0.5
    label "YML010W"
  ]
  node
  [
    id 754
    y 0.3701
    x 0.3075
    z 0.5
    label "YBR003W"
  ]
  node
  [
    id 755
    y 0.3301
    x 0.417
    z 0.5
    label "YAL032C"
  ]
  node
  [
    id 756
    y 0.3497
    x 0.4773
    z 0.5
    label "YPL151C"
  ]
  node
  [
    id 757
    y 0.6871
    x 0.4263
    z 0.5
    label "YDR359C"
  ]
  node
  [
    id 758
    y 0.5032
    x 0.5981
    z 0.5
    label "YOR244W"
  ]
  node
  [
    id 759
    y 0.2186
    x 0.4838
    z 0.5
    label "YER154W"
  ]
  node
  [
    id 760
    y 0.5775
    x 0.5083
    z 0.5
    label "YNL257C"
  ]
  node
  [
    id 761
    y 0.3393
    x 0.4199
    z 0.5
    label "YER029C"
  ]
  node
  [
    id 762
    y 0.4452
    x 0.4936
    z 0.5
    label "YBR272C"
  ]
  node
  [
    id 763
    y 0.4682
    x 0.8328
    z 0.5
    label "YGL210W"
  ]
  node
  [
    id 764
    y 0.4597
    x 0.733
    z 0.5
    label "YER136W"
  ]
  node
  [
    id 765
    y 0.5109
    x 0.5406
    z 0.5
    label "YOR326W"
  ]
  node
  [
    id 766
    y 0.661
    x 0.5476
    z 0.5
    label "YDR001C"
  ]
  node
  [
    id 767
    y 0.6847
    x 0.5067
    z 0.5
    label "YER177W"
  ]
  node
  [
    id 768
    y 0.4557
    x 0.5898
    z 0.5
    label "YEL009C"
  ]
  node
  [
    id 769
    y 0.4188
    x 0.4473
    z 0.5
    label "YBL003C"
  ]
  node
  [
    id 770
    y 0.6108
    x 0.493
    z 0.5
    label "YJR083C"
  ]
  node
  [
    id 771
    y 0.3615
    x 0.5721
    z 0.5
    label "YGL044C"
  ]
  node
  [
    id 772
    y 0.3606
    x 0.5427
    z 0.5
    label "YOR250C"
  ]
  node
  [
    id 773
    y 0.5735
    x 0.6865
    z 0.5
    label "YOR270C"
  ]
  node
  [
    id 774
    y 0.6278
    x 0.7827
    z 0.5
    label "YKL119C"
  ]
  node
  [
    id 775
    y 0.5062
    x 0.8303
    z 0.5
    label "YBR264C"
  ]
  node
  [
    id 776
    y 0.2465
    x 0.6003
    z 0.5
    label "YBR170C"
  ]
  node
  [
    id 777
    y 0.4755
    x 0.442
    z 0.5
    label "YGL043W"
  ]
  node
  [
    id 778
    y 0.4023
    x 0.4503
    z 0.5
    label "YMR061W"
  ]
  node
  [
    id 779
    y 0.6166
    x 0.4102
    z 0.5
    label "YHR112C"
  ]
  node
  [
    id 780
    y 0.3725
    x 0.4245
    z 0.5
    label "YEL015W"
  ]
  node
  [
    id 781
    y 0.3219
    x 0.7255
    z 0.5
    label "YBL018C"
  ]
  node
  [
    id 782
    y 0.5118
    x 0.5226
    z 0.5
    label "YIL094C"
  ]
  node
  [
    id 783
    y 0.401
    x 0.4555
    z 0.5
    label "YOL145C"
  ]
  node
  [
    id 784
    y 0.4924
    x 0.5056
    z 0.5
    label "YGL241W"
  ]
  node
  [
    id 785
    y 0.6196
    x 0.544
    z 0.5
    label "YPR174C"
  ]
  node
  [
    id 786
    y 0.6959
    x 0.2996
    z 0.5
    label "YAR008W"
  ]
  node
  [
    id 787
    y 0.3913
    x 0.5411
    z 0.5
    label "YBR079C"
  ]
  node
  [
    id 788
    y 0.2973
    x 0.567
    z 0.5
    label "YML085C"
  ]
  node
  [
    id 789
    y 0.6433
    x 0.4748
    z 0.5
    label "YOL001W"
  ]
  node
  [
    id 790
    y 0.5938
    x 0.6851
    z 0.5
    label "YNL261W"
  ]
  node
  [
    id 791
    y 0.5938
    x 0.6882
    z 0.5
    label "YPR162C"
  ]
  node
  [
    id 792
    y 0.5888
    x 0.3409
    z 0.5
    label "YJR144W"
  ]
  node
  [
    id 793
    y 0.4444
    x 0.4527
    z 0.5
    label "YHR086W"
  ]
  node
  [
    id 794
    y 0.3824
    x 0.5421
    z 0.5
    label "YPR041W"
  ]
  node
  [
    id 795
    y 0.2693
    x 0.6922
    z 0.5
    label "YEL022W"
  ]
  node
  [
    id 796
    y 0.3912
    x 0.5358
    z 0.5
    label "YFR052W"
  ]
  node
  [
    id 797
    y 0.5843
    x 0.4477
    z 0.5
    label "YOR140W"
  ]
  node
  [
    id 798
    y 0.6986
    x 0.616
    z 0.5
    label "YDR171W"
  ]
  node
  [
    id 799
    y 0.4879
    x 0.5293
    z 0.5
    label "YHR072W-A"
  ]
  node
  [
    id 800
    y 0.3605
    x 0.4748
    z 0.5
    label "YDL014W"
  ]
  node
  [
    id 801
    y 0.7212
    x 0.456
    z 0.5
    label "YBR098W"
  ]
  node
  [
    id 802
    y 0.5007
    x 0.69
    z 0.5
    label "YIL007C"
  ]
  node
  [
    id 803
    y 0.6319
    x 0.736
    z 0.5
    label "YKR055W"
  ]
  node
  [
    id 804
    y 0.4136
    x 0.5157
    z 0.5
    label "YGR103W"
  ]
  node
  [
    id 805
    y 0.3287
    x 0.6648
    z 0.5
    label "YMR005W"
  ]
  node
  [
    id 806
    y 0.5437
    x 0.2961
    z 0.5
    label "YPL154C"
  ]
  node
  [
    id 807
    y 0.4594
    x 0.4459
    z 0.5
    label "YBR114W"
  ]
  node
  [
    id 808
    y 0.6046
    x 0.593
    z 0.5
    label "YHL007C"
  ]
  node
  [
    id 809
    y 0.4788
    x 0.4926
    z 0.5
    label "YJL203W"
  ]
  node
  [
    id 810
    y 0.3116
    x 0.5616
    z 0.5
    label "YLR005W"
  ]
  node
  [
    id 811
    y 0.5131
    x 0.6245
    z 0.5
    label "YLL024C"
  ]
  node
  [
    id 812
    y 0.5763
    x 0.4116
    z 0.5
    label "YFR002W"
  ]
  node
  [
    id 813
    y 0.328
    x 0.5345
    z 0.5
    label "YPR107C"
  ]
  node
  [
    id 814
    y 0.289
    x 0.6145
    z 0.5
    label "YPL051W"
  ]
  node
  [
    id 815
    y 0.1868
    x 0.6243
    z 0.5
    label "YKR030W"
  ]
  node
  [
    id 816
    y 0.2197
    x 0.633
    z 0.5
    label "YKL125W"
  ]
  node
  [
    id 817
    y 0.3303
    x 0.6167
    z 0.5
    label "YML126C"
  ]
  node
  [
    id 818
    y 0.3697
    x 0.5209
    z 0.5
    label "YLR409C"
  ]
  node
  [
    id 819
    y 0.4625
    x 0.4199
    z 0.5
    label "YMR024W"
  ]
  node
  [
    id 820
    y 0.4036
    x 0.2663
    z 0.5
    label "YNL284C"
  ]
  node
  [
    id 821
    y 0.5179
    x 0.5042
    z 0.5
    label "YLR432W"
  ]
  node
  [
    id 822
    y 0.398
    x 0.5104
    z 0.5
    label "YDL213C"
  ]
  node
  [
    id 823
    y 0.4839
    x 0.3546
    z 0.5
    label "YMR190C"
  ]
  node
  [
    id 824
    y 0.5105
    x 0.4313
    z 0.5
    label "YAL061W"
  ]
  node
  [
    id 825
    y 0.2156
    x 0.3457
    z 0.5
    label "YGR200C"
  ]
  node
  [
    id 826
    y 0.6497
    x 0.4354
    z 0.5
    label "YBR108W"
  ]
  node
  [
    id 827
    y 0.3518
    x 0.6396
    z 0.5
    label "YDL156W"
  ]
  node
  [
    id 828
    y 0.5324
    x 0.7484
    z 0.5
    label "YPL094C"
  ]
  node
  [
    id 829
    y 0.4164
    x 0.6331
    z 0.5
    label "YKL058W"
  ]
  node
  [
    id 830
    y 0.6777
    x 0.5611
    z 0.5
    label "YPR191W"
  ]
  node
  [
    id 831
    y 0.6497
    x 0.4378
    z 0.5
    label "YLR309C"
  ]
  node
  [
    id 832
    y 0.629
    x 0.6796
    z 0.5
    label "YCL032W"
  ]
  node
  [
    id 833
    y 0.5603
    x 0.805
    z 0.5
    label "YKL080W"
  ]
  node
  [
    id 834
    y 0.5521
    x 0.7256
    z 0.5
    label "YBR127C"
  ]
  node
  [
    id 835
    y 0.3888
    x 0.326
    z 0.5
    label "YBR152W"
  ]
  node
  [
    id 836
    y 0.4401
    x 0.4908
    z 0.5
    label "YLR106C"
  ]
  node
  [
    id 837
    y 0.6301
    x 0.787
    z 0.5
    label "YHR039C-A"
  ]
  node
  [
    id 838
    y 0.5944
    x 0.6552
    z 0.5
    label "YGL194C"
  ]
  node
  [
    id 839
    y 0.5126
    x 0.4961
    z 0.5
    label "YGR094W"
  ]
  node
  [
    id 840
    y 0.4681
    x 0.3428
    z 0.5
    label "YDL058W"
  ]
  node
  [
    id 841
    y 0.5622
    x 0.4232
    z 0.5
    label "YER107C"
  ]
  node
  [
    id 842
    y 0.4921
    x 0.4171
    z 0.5
    label "YNL330C"
  ]
  node
  [
    id 843
    y 0.3073
    x 0.404
    z 0.5
    label "YLR363C"
  ]
  node
  [
    id 844
    y 0.2452
    x 0.4734
    z 0.5
    label "YEL050C"
  ]
  node
  [
    id 845
    y 0.6948
    x 0.3015
    z 0.5
    label "YML094W"
  ]
  node
  [
    id 846
    y 0.3583
    x 0.8598
    z 0.5
    label "YFL041W"
  ]
  node
  [
    id 847
    y 0.4117
    x 0.7613
    z 0.5
    label "YBR207W"
  ]
  node
  [
    id 848
    y 0.6891
    x 0.7062
    z 0.5
    label "YNL107W"
  ]
  node
  [
    id 849
    y 0.4435
    x 0.3169
    z 0.5
    label "YPL271W"
  ]
  node
  [
    id 850
    y 0.1793
    x 0.5724
    z 0.5
    label "YDR174W"
  ]
  node
  [
    id 851
    y 0.235
    x 0.5419
    z 0.5
    label "YNL135C"
  ]
  node
  [
    id 852
    y 0.4031
    x 0.6469
    z 0.5
    label "YNL006W"
  ]
  node
  [
    id 853
    y 0.319
    x 0.5863
    z 0.5
    label "YOL078W"
  ]
  node
  [
    id 854
    y 0.3096
    x 0.514
    z 0.5
    label "YPL126W"
  ]
  node
  [
    id 855
    y 0.4578
    x 0.2386
    z 0.5
    label "YIR009W"
  ]
  node
  [
    id 856
    y 0.5017
    x 0.5038
    z 0.5
    label "YAR007C"
  ]
  node
  [
    id 857
    y 0.4479
    x 0.4308
    z 0.5
    label "YDR224C"
  ]
  node
  [
    id 858
    y 0.3587
    x 0.6833
    z 0.5
    label "YLR070C"
  ]
  node
  [
    id 859
    y 0.4454
    x 0.5981
    z 0.5
    label "YML064C"
  ]
  node
  [
    id 860
    y 0.56
    x 0.5586
    z 0.5
    label "YPR193C"
  ]
  node
  [
    id 861
    y 0.3087
    x 0.4098
    z 0.5
    label "YPR101W"
  ]
  node
  [
    id 862
    y 0.4288
    x 0.4277
    z 0.5
    label "YDR432W"
  ]
  node
  [
    id 863
    y 0.494
    x 0.4138
    z 0.5
    label "YPL178W"
  ]
  node
  [
    id 864
    y 0.4453
    x 0.369
    z 0.5
    label "YNL088W"
  ]
  node
  [
    id 865
    y 0.6409
    x 0.6879
    z 0.5
    label "YKL082C"
  ]
  node
  [
    id 866
    y 0.5961
    x 0.7586
    z 0.5
    label "YJL149W"
  ]
  node
  [
    id 867
    y 0.2807
    x 0.4043
    z 0.5
    label "YOR202W"
  ]
  node
  [
    id 868
    y 0.5769
    x 0.3226
    z 0.5
    label "YDR412W"
  ]
  node
  [
    id 869
    y 0.5397
    x 0.6598
    z 0.5
    label "YMR319C"
  ]
  node
  [
    id 870
    y 0.639
    x 0.607
    z 0.5
    label "YLR238W"
  ]
  node
  [
    id 871
    y 0.5859
    x 0.7786
    z 0.5
    label "YDL180W"
  ]
  node
  [
    id 872
    y 0.3864
    x 0.4727
    z 0.5
    label "YBL004W"
  ]
  node
  [
    id 873
    y 0.6769
    x 0.7837
    z 0.5
    label "YIR023W"
  ]
  node
  [
    id 874
    y 0.3235
    x 0.5975
    z 0.5
    label "YLR340W"
  ]
  node
  [
    id 875
    y 0.1582
    x 0.4768
    z 0.5
    label "YLR287C"
  ]
  node
  [
    id 876
    y 0.308
    x 0.5129
    z 0.5
    label "YGR128C"
  ]
  node
  [
    id 877
    y 0.3304
    x 0.2891
    z 0.5
    label "YOR038C"
  ]
  node
  [
    id 878
    y 0.407
    x 0.3762
    z 0.5
    label "YBL008W"
  ]
  node
  [
    id 879
    y 0.2143
    x 0.3588
    z 0.5
    label "YML059C"
  ]
  node
  [
    id 880
    y 0.3675
    x 0.4975
    z 0.5
    label "YLR074C"
  ]
  node
  [
    id 881
    y 0.3454
    x 0.6043
    z 0.5
    label "YFR010W"
  ]
  node
  [
    id 882
    y 0.4547
    x 0.7125
    z 0.5
    label "YOR101W"
  ]
  node
  [
    id 883
    y 0.4142
    x 0.548
    z 0.5
    label "YOR351C"
  ]
  node
  [
    id 884
    y 0.4459
    x 0.509
    z 0.5
    label "YGL049C"
  ]
  node
  [
    id 885
    y 0.3877
    x 0.4928
    z 0.5
    label "YDR228C"
  ]
  node
  [
    id 886
    y 0.6379
    x 0.4345
    z 0.5
    label "YIL063C"
  ]
  node
  [
    id 887
    y 0.4881
    x 0.5202
    z 0.5
    label "YJL058C"
  ]
  node
  [
    id 888
    y 0.3522
    x 0.6379
    z 0.5
    label "YKL203C"
  ]
  node
  [
    id 889
    y 0.562
    x 0.7857
    z 0.5
    label "YDR306C"
  ]
  node
  [
    id 890
    y 0.6343
    x 0.4483
    z 0.5
    label "YHR082C"
  ]
  node
  [
    id 891
    y 0.482
    x 0.593
    z 0.5
    label "YBR081C"
  ]
  node
  [
    id 892
    y 0.4182
    x 0.5541
    z 0.5
    label "YLL011W"
  ]
  node
  [
    id 893
    y 0.3134
    x 0.7575
    z 0.5
    label "YLR093C"
  ]
  node
  [
    id 894
    y 0.4047
    x 0.7145
    z 0.5
    label "YMR197C"
  ]
  node
  [
    id 895
    y 0.3359
    x 0.5735
    z 0.5
    label "YJL076W"
  ]
  node
  [
    id 896
    y 0.8268
    x 0.567
    z 0.5
    label "YGR185C"
  ]
  node
  [
    id 897
    y 0.7088
    x 0.4986
    z 0.5
    label "YGR229C"
  ]
  node
  [
    id 898
    y 0.7303
    x 0.4954
    z 0.5
    label "YOR257W"
  ]
  node
  [
    id 899
    y 0.5985
    x 0.4016
    z 0.5
    label "YHL020C"
  ]
  node
  [
    id 900
    y 0.6466
    x 0.4518
    z 0.5
    label "YIR008C"
  ]
  node
  [
    id 901
    y 0.5402
    x 0.4862
    z 0.5
    label "YDR343C"
  ]
  node
  [
    id 902
    y 0.7135
    x 0.6159
    z 0.5
    label "YLR019W"
  ]
  node
  [
    id 903
    y 0.4654
    x 0.3615
    z 0.5
    label "YKR024C"
  ]
  node
  [
    id 904
    y 0.4548
    x 0.2545
    z 0.5
    label "YLR233C"
  ]
  node
  [
    id 905
    y 0.8131
    x 0.7235
    z 0.5
    label "YER018C"
  ]
  node
  [
    id 906
    y 0.7491
    x 0.6687
    z 0.5
    label "YMR117C"
  ]
  node
  [
    id 907
    y 0.5741
    x 0.7659
    z 0.5
    label "YDL130W"
  ]
  node
  [
    id 908
    y 0.6519
    x 0.6519
    z 0.5
    label "YDR382W"
  ]
  node
  [
    id 909
    y 0.6032
    x 0.7523
    z 0.5
    label "YLR368W"
  ]
  node
  [
    id 910
    y 0.2863
    x 0.7299
    z 0.5
    label "YOR106W"
  ]
  node
  [
    id 911
    y 0.4718
    x 0.4778
    z 0.5
    label "YNL288W"
  ]
  node
  [
    id 912
    y 0.4907
    x 0.5993
    z 0.5
    label "YHR185C"
  ]
  node
  [
    id 913
    y 0.5075
    x 0.7691
    z 0.5
    label "YHR014W"
  ]
  node
  [
    id 914
    y 0.478
    x 0.5122
    z 0.5
    label "YJL138C"
  ]
  node
  [
    id 915
    y 0.4799
    x 0.3868
    z 0.5
    label "YJL222W"
  ]
  node
  [
    id 916
    y 0.4719
    x 0.5315
    z 0.5
    label "YOR261C"
  ]
  node
  [
    id 917
    y 0.5055
    x 0.4834
    z 0.5
    label "YDR381W"
  ]
  node
  [
    id 918
    y 0.3299
    x 0.4406
    z 0.5
    label "YKL009W"
  ]
  node
  [
    id 919
    y 0.4218
    x 0.697
    z 0.5
    label "YKR014C"
  ]
  node
  [
    id 920
    y 0.5678
    x 0.5797
    z 0.5
    label "YLL021W"
  ]
  node
  [
    id 921
    y 0.6196
    x 0.544
    z 0.5
    label "YHL050C"
  ]
  node
  [
    id 922
    y 0.476
    x 0.723
    z 0.5
    label "YMR112C"
  ]
  node
  [
    id 923
    y 0.4154
    x 0.5147
    z 0.5
    label "YER006W"
  ]
  node
  [
    id 924
    y 0.4155
    x 0.4966
    z 0.5
    label "YDL060W"
  ]
  node
  [
    id 925
    y 0.5395
    x 0.4252
    z 0.5
    label "YLL001W"
  ]
  node
  [
    id 926
    y 0.4158
    x 0.6358
    z 0.5
    label "YMR236W"
  ]
  node
  [
    id 927
    y 0.2517
    x 0.6873
    z 0.5
    label "YAL038W"
  ]
  node
  [
    id 928
    y 0.4485
    x 0.2151
    z 0.5
    label "YKR072C"
  ]
  node
  [
    id 929
    y 0.4709
    x 0.3227
    z 0.5
    label "YML016C"
  ]
  node
  [
    id 930
    y 0.2423
    x 0.5827
    z 0.5
    label "YJL111W"
  ]
  node
  [
    id 931
    y 0.3454
    x 0.5851
    z 0.5
    label "YMR028W"
  ]
  node
  [
    id 932
    y 0.6486
    x 0.7552
    z 0.5
    label "YER124C"
  ]
  node
  [
    id 933
    y 0.6014
    x 0.6544
    z 0.5
    label "YBL085W"
  ]
  node
  [
    id 934
    y 0.582
    x 0.6366
    z 0.5
    label "YDR385W"
  ]
  node
  [
    id 935
    y 0.2811
    x 0.7287
    z 0.5
    label "YDR510W"
  ]
  node
  [
    id 936
    y 0.6871
    x 0.7599
    z 0.5
    label "YMR038C"
  ]
  node
  [
    id 937
    y 0.4078
    x 0.7722
    z 0.5
    label "YDR329C"
  ]
  node
  [
    id 938
    y 0.6079
    x 0.7712
    z 0.5
    label "YGR100W"
  ]
  node
  [
    id 939
    y 0.4967
    x 0.6512
    z 0.5
    label "YLR262C"
  ]
  node
  [
    id 940
    y 0.2235
    x 0.7958
    z 0.5
    label "YOR156C"
  ]
  node
  [
    id 941
    y 0.7285
    x 0.3795
    z 0.5
    label "YOR144C"
  ]
  node
  [
    id 942
    y 0.682
    x 0.3364
    z 0.5
    label "YOL094C"
  ]
  node
  [
    id 943
    y 0.3431
    x 0.567
    z 0.5
    label "YIR004W"
  ]
  node
  [
    id 944
    y 0.5069
    x 0.53
    z 0.5
    label "YLL008W"
  ]
  node
  [
    id 945
    y 0.6586
    x 0.6365
    z 0.5
    label "YDR200C"
  ]
  node
  [
    id 946
    y 0.38
    x 0.7444
    z 0.5
    label "YGL153W"
  ]
  node
  [
    id 947
    y 0.5795
    x 0.5737
    z 0.5
    label "YNL138W"
  ]
  node
  [
    id 948
    y 0.6397
    x 0.6124
    z 0.5
    label "YOR122C"
  ]
  node
  [
    id 949
    y 0.6257
    x 0.4157
    z 0.5
    label "YDR453C"
  ]
  node
  [
    id 950
    y 0.4675
    x 0.5846
    z 0.5
    label "YOR157C"
  ]
  node
  [
    id 951
    y 0.4006
    x 0.6604
    z 0.5
    label "YPL085W"
  ]
  node
  [
    id 952
    y 0.483
    x 0.4749
    z 0.5
    label "YGR240C"
  ]
  node
  [
    id 953
    y 0.4127
    x 0.4643
    z 0.5
    label "YPL043W"
  ]
  node
  [
    id 954
    y 0.3391
    x 0.3699
    z 0.5
    label "YLR427W"
  ]
  node
  [
    id 955
    y 0.3108
    x 0.4296
    z 0.5
    label "YDL209C"
  ]
  node
  [
    id 956
    y 0.4423
    x 0.5003
    z 0.5
    label "YOL086C"
  ]
  node
  [
    id 957
    y 0.2657
    x 0.6179
    z 0.5
    label "YCR027C"
  ]
  node
  [
    id 958
    y 0.5879
    x 0.6401
    z 0.5
    label "YMR199W"
  ]
  node
  [
    id 959
    y 0.3189
    x 0.4506
    z 0.5
    label "YOR224C"
  ]
  node
  [
    id 960
    y 0.633
    x 0.3003
    z 0.5
    label "YJL039C"
  ]
  node
  [
    id 961
    y 0.6167
    x 0.4561
    z 0.5
    label "YER036C"
  ]
  node
  [
    id 962
    y 0.3856
    x 0.7782
    z 0.5
    label "YGL212W"
  ]
  node
  [
    id 963
    y 0.6995
    x 0.3811
    z 0.5
    label "YLR453C"
  ]
  node
  [
    id 964
    y 0.7081
    x 0.59
    z 0.5
    label "YBR275C"
  ]
  node
  [
    id 965
    y 0.4639
    x 0.7637
    z 0.5
    label "YGL226C-A"
  ]
  node
  [
    id 966
    y 0.2813
    x 0.3865
    z 0.5
    label "YOR375C"
  ]
  node
  [
    id 967
    y 0.5669
    x 0.6702
    z 0.5
    label "YOR284W"
  ]
  node
  [
    id 968
    y 0.5957
    x 0.4884
    z 0.5
    label "YDR217C"
  ]
  node
  [
    id 969
    y 0.3429
    x 0.6924
    z 0.5
    label "YKL196C"
  ]
  node
  [
    id 970
    y 0.6004
    x 0.5902
    z 0.5
    label "YDR507C"
  ]
  node
  [
    id 971
    y 0.3533
    x 0.3368
    z 0.5
    label "YHR119W"
  ]
  node
  [
    id 972
    y 0.3829
    x 0.582
    z 0.5
    label "YPR040W"
  ]
  node
  [
    id 973
    y 0.579
    x 0.5489
    z 0.5
    label "YLR096W"
  ]
  node
  [
    id 974
    y 0.6103
    x 0.677
    z 0.5
    label "YLL004W"
  ]
  node
  [
    id 975
    y 0.3342
    x 0.3967
    z 0.5
    label "YLR015W"
  ]
  node
  [
    id 976
    y 0.6644
    x 0.4575
    z 0.5
    label "YJR115W"
  ]
  node
  [
    id 977
    y 0.5243
    x 0.4527
    z 0.5
    label "YDL002C"
  ]
  node
  [
    id 978
    y 0.4574
    x 0.5728
    z 0.5
    label "YOR362C"
  ]
  node
  [
    id 979
    y 0.443
    x 0.3558
    z 0.5
    label "YIR001C"
  ]
  node
  [
    id 980
    y 0.6697
    x 0.5599
    z 0.5
    label "YCL024W"
  ]
  node
  [
    id 981
    y 0.2783
    x 0.7101
    z 0.5
    label "YGR239C"
  ]
  node
  [
    id 982
    y 0.6268
    x 0.3235
    z 0.5
    label "YHR015W"
  ]
  node
  [
    id 983
    y 0.3131
    x 0.553
    z 0.5
    label "YHR066W"
  ]
  node
  [
    id 984
    y 0.4362
    x 0.6879
    z 0.5
    label "YLR076C"
  ]
  node
  [
    id 985
    y 0.5061
    x 0.5173
    z 0.5
    label "YER068W"
  ]
  node
  [
    id 986
    y 0.3036
    x 0.5839
    z 0.5
    label "YBR162C"
  ]
  node
  [
    id 987
    y 0.1021
    x 0.4433
    z 0.5
    label "YBL014C"
  ]
  node
  [
    id 988
    y 0.1764
    x 0.4531
    z 0.5
    label "YJL025W"
  ]
  node
  [
    id 989
    y 0.5666
    x 0.6341
    z 0.5
    label "YDL225W"
  ]
  node
  [
    id 990
    y 0.6229
    x 0.6246
    z 0.5
    label "YLR314C"
  ]
  node
  [
    id 991
    y 0.4518
    x 0.6999
    z 0.5
    label "YJL002C"
  ]
  node
  [
    id 992
    y 0.4514
    x 0.6018
    z 0.5
    label "YDL017W"
  ]
  node
  [
    id 993
    y 0.6196
    x 0.544
    z 0.5
    label "YJL060W"
  ]
  node
  [
    id 994
    y 0.4219
    x 0.4204
    z 0.5
    label "YKL139W"
  ]
  node
  [
    id 995
    y 0.3021
    x 0.305
    z 0.5
    label "YJL011C"
  ]
  node
  [
    id 996
    y 0.3126
    x 0.4215
    z 0.5
    label "YNR003C"
  ]
  node
  [
    id 997
    y 0.5381
    x 0.434
    z 0.5
    label "YGL181W"
  ]
  node
  [
    id 998
    y 0.4833
    x 0.4595
    z 0.5
    label "YCL011C"
  ]
  node
  [
    id 999
    y 0.4877
    x 0.6939
    z 0.5
    label "YER144C"
  ]
  node
  [
    id 1000
    y 0.2823
    x 0.5608
    z 0.5
    label "YLR039C"
  ]
  node
  [
    id 1001
    y 0.5364
    x 0.3333
    z 0.5
    label "YDR137W"
  ]
  node
  [
    id 1002
    y 0.4908
    x 0.3655
    z 0.5
    label "YNL251C"
  ]
  node
  [
    id 1003
    y 0.5848
    x 0.5193
    z 0.5
    label "YMR001C"
  ]
  node
  [
    id 1004
    y 0.5507
    x 0.5367
    z 0.5
    label "YDR052C"
  ]
  node
  [
    id 1005
    y 0.3712
    x 0.778
    z 0.5
    label "YCR099C"
  ]
  node
  [
    id 1006
    y 0.4852
    x 0.4245
    z 0.5
    label "YBR123C"
  ]
  node
  [
    id 1007
    y 0.3774
    x 0.4406
    z 0.5
    label "YGR246C"
  ]
  node
  [
    id 1008
    y 0.3556
    x 0.4817
    z 0.5
    label "YLR222C"
  ]
  node
  [
    id 1009
    y 0.2495
    x 0.47
    z 0.5
    label "YDL166C"
  ]
  node
  [
    id 1010
    y 0.3927
    x 0.7054
    z 0.5
    label "YGR083C"
  ]
  node
  [
    id 1011
    y 0.3273
    x 0.6269
    z 0.5
    label "YNL148C"
  ]
  node
  [
    id 1012
    y 0.1904
    x 0.6725
    z 0.5
    label "YGL124C"
  ]
  node
  [
    id 1013
    y 0.4387
    x 0.4918
    z 0.5
    label "YMR205C"
  ]
  node
  [
    id 1014
    y 0.4267
    x 0.4474
    z 0.5
    label "YPL012W"
  ]
  node
  [
    id 1015
    y 0.2787
    x 0.5192
    z 0.5
    label "YPL217C"
  ]
  node
  [
    id 1016
    y 0.3273
    x 0.5312
    z 0.5
    label "YKR062W"
  ]
  node
  [
    id 1017
    y 0.5135
    x 0.5595
    z 0.5
    label "YDL116W"
  ]
  node
  [
    id 1018
    y 0.6613
    x 0.4497
    z 0.5
    label "YIL147C"
  ]
  node
  [
    id 1019
    y 0.2977
    x 0.3874
    z 0.5
    label "YMR268C"
  ]
  node
  [
    id 1020
    y 0.5579
    x 0.7755
    z 0.5
    label "YMR218C"
  ]
  node
  [
    id 1021
    y 0.4445
    x 0.7489
    z 0.5
    label "YKR068C"
  ]
  node
  [
    id 1022
    y 0.4202
    x 0.6599
    z 0.5
    label "YOL033W"
  ]
  node
  [
    id 1023
    y 0.457
    x 0.5362
    z 0.5
    label "YMR309C"
  ]
  node
  [
    id 1024
    y 0.4587
    x 0.298
    z 0.5
    label "YGR158C"
  ]
  node
  [
    id 1025
    y 0.4491
    x 0.2068
    z 0.5
    label "YLR345W"
  ]
  node
  [
    id 1026
    y 0.6391
    x 0.5877
    z 0.5
    label "YLR006C"
  ]
  node
  [
    id 1027
    y 0.7196
    x 0.6093
    z 0.5
    label "YKL224C"
  ]
  node
  [
    id 1028
    y 0.2049
    x 0.7764
    z 0.5
    label "YDR446W"
  ]
  node
  [
    id 1029
    y 0.5186
    x 0.6208
    z 0.5
    label "YDL185W"
  ]
  node
  [
    id 1030
    y 0.689
    x 0.4076
    z 0.5
    label "YNL262W"
  ]
  node
  [
    id 1031
    y 0.4254
    x 0.5941
    z 0.5
    label "YKL166C"
  ]
  node
  [
    id 1032
    y 0.5459
    x 0.5927
    z 0.5
    label "YGL178W"
  ]
  node
  [
    id 1033
    y 0.3257
    x 0.7334
    z 0.5
    label "YBR131W"
  ]
  node
  [
    id 1034
    y 0.3912
    x 0.407
    z 0.5
    label "YGR285C"
  ]
  node
  [
    id 1035
    y 0.745
    x 0.2407
    z 0.5
    label "YEL003W"
  ]
  node
  [
    id 1036
    y 0.7007
    x 0.3059
    z 0.5
    label "YLR200W"
  ]
  node
  [
    id 1037
    y 0.6055
    x 0.6366
    z 0.5
    label "YLR033W"
  ]
  node
  [
    id 1038
    y 0.3565
    x 0.4265
    z 0.5
    label "YGL111W"
  ]
  node
  [
    id 1039
    y 0.6086
    x 0.7667
    z 0.5
    label "YDR319C"
  ]
  node
  [
    id 1040
    y 0.556
    x 0.2272
    z 0.5
    label "YPL036W"
  ]
  node
  [
    id 1041
    y 0.3841
    x 0.503
    z 0.5
    label "YKL193C"
  ]
  node
  [
    id 1042
    y 0.434
    x 0.4936
    z 0.5
    label "YLR276C"
  ]
  node
  [
    id 1043
    y 0.6403
    x 0.5642
    z 0.5
    label "YDR129C"
  ]
  node
  [
    id 1044
    y 0.3222
    x 0.5093
    z 0.5
    label "YKL020C"
  ]
  node
  [
    id 1045
    y 0.2729
    x 0.5547
    z 0.5
    label "YPR180W"
  ]
  node
  [
    id 1046
    y 0.5338
    x 0.3764
    z 0.5
    label "YGR050C"
  ]
  node
  [
    id 1047
    y 0.5621
    x 0.7394
    z 0.5
    label "YGR179C"
  ]
  node
  [
    id 1048
    y 0.526
    x 0.6495
    z 0.5
    label "YDR103W"
  ]
  node
  [
    id 1049
    y 0.5121
    x 0.5155
    z 0.5
    label "YNL250W"
  ]
  node
  [
    id 1050
    y 0.1899
    x 0.4368
    z 0.5
    label "YNL071W"
  ]
  node
  [
    id 1051
    y 0.4118
    x 0.5693
    z 0.5
    label "YBR198C"
  ]
  node
  [
    id 1052
    y 0.3239
    x 0.5269
    z 0.5
    label "YMR093W"
  ]
  node
  [
    id 1053
    y 0.3979
    x 0.7491
    z 0.5
    label "YNL214W"
  ]
  node
  [
    id 1054
    y 0.5563
    x 0.2725
    z 0.5
    label "YPL210C"
  ]
  node
  [
    id 1055
    y 0.4347
    x 0.4918
    z 0.5
    label "YDR283C"
  ]
  node
  [
    id 1056
    y 0.3818
    x 0.8384
    z 0.5
    label "YKL052C"
  ]
  node
  [
    id 1057
    y 0.4048
    x 0.8448
    z 0.5
    label "YKR083C"
  ]
  node
  [
    id 1058
    y 0.5337
    x 0.3764
    z 0.5
    label "YLL051C"
  ]
  node
  [
    id 1059
    y 0.5836
    x 0.6081
    z 0.5
    label "YER008C"
  ]
  node
  [
    id 1060
    y 0.67
    x 0.2975
    z 0.5
    label "YMR096W"
  ]
  node
  [
    id 1061
    y 0.1309
    x 0.4584
    z 0.5
    label "YDR405W"
  ]
  node
  [
    id 1062
    y 0.6129
    x 0.252
    z 0.5
    label "YDR436W"
  ]
  node
  [
    id 1063
    y 0.4873
    x 0.5301
    z 0.5
    label "YLR312W-A"
  ]
  node
  [
    id 1064
    y 0.6105
    x 0.5273
    z 0.5
    label "YDL179W"
  ]
  node
  [
    id 1065
    y 0.601
    x 0.299
    z 0.5
    label "YPL074W"
  ]
  node
  [
    id 1066
    y 0.639
    x 0.5044
    z 0.5
    label "YPL150W"
  ]
  node
  [
    id 1067
    y 0.3836
    x 0.6276
    z 0.5
    label "YPL010W"
  ]
  node
  [
    id 1068
    y 0.5502
    x 0.6698
    z 0.5
    label "YDL159W"
  ]
  node
  [
    id 1069
    y 0.596
    x 0.6555
    z 0.5
    label "YOL115W"
  ]
  node
  [
    id 1070
    y 0.7672
    x 0.5338
    z 0.5
    label "YHR161C"
  ]
  node
  [
    id 1071
    y 0.4163
    x 0.5118
    z 0.5
    label "YOR272W"
  ]
  node
  [
    id 1072
    y 0.1871
    x 0.5754
    z 0.5
    label "YCL029C"
  ]
  node
  [
    id 1073
    y 0.4336
    x 0.5709
    z 0.5
    label "YDR166C"
  ]
  node
  [
    id 1074
    y 0.3349
    x 0.8776
    z 0.5
    label "YOL098C"
  ]
  node
  [
    id 1075
    y 0.4022
    x 0.8094
    z 0.5
    label "YBL087C"
  ]
  node
  [
    id 1076
    y 0.5604
    x 0.6017
    z 0.5
    label "YJL187C"
  ]
  node
  [
    id 1077
    y 0.5687
    x 0.5795
    z 0.5
    label "YNL271C"
  ]
  node
  [
    id 1078
    y 0.5702
    x 0.65
    z 0.5
    label "YJR094C"
  ]
  node
  [
    id 1079
    y 0.5709
    x 0.2766
    z 0.5
    label "YDL092W"
  ]
  node
  [
    id 1080
    y 0.4343
    x 0.5596
    z 0.5
    label "YLR196W"
  ]
  node
  [
    id 1081
    y 0.5337
    x 0.3764
    z 0.5
    label "YLR443W"
  ]
  node
  [
    id 1082
    y 0.2871
    x 0.4936
    z 0.5
    label "YBR238C"
  ]
  node
  [
    id 1083
    y 0.4364
    x 0.0477
    z 0.5
    label "YCR014C"
  ]
  node
  [
    id 1084
    y 0.5902
    x 0.4084
    z 0.5
    label "YER032W"
  ]
  node
  [
    id 1085
    y 0.6242
    x 0.4788
    z 0.5
    label "YDR130C"
  ]
  node
  [
    id 1086
    y 0.5807
    x 0.7269
    z 0.5
    label "YDR085C"
  ]
  node
  [
    id 1087
    y 0.3955
    x 0.6329
    z 0.5
    label "YIL004C"
  ]
  node
  [
    id 1088
    y 0.6348
    x 0.2889
    z 0.5
    label "YOR069W"
  ]
  node
  [
    id 1089
    y 0.3772
    x 0.6222
    z 0.5
    label "YIL109C"
  ]
  node
  [
    id 1090
    y 0.3711
    x 0.4442
    z 0.5
    label "YGR145W"
  ]
  node
  [
    id 1091
    y 0.2482
    x 0.7672
    z 0.5
    label "YNL131W"
  ]
  node
  [
    id 1092
    y 0.5958
    x 0.3035
    z 0.5
    label "YDR004W"
  ]
  node
  [
    id 1093
    y 0.3235
    x 0.3532
    z 0.5
    label "YLR424W"
  ]
  node
  [
    id 1094
    y 0.3272
    x 0.811
    z 0.5
    label "YOL044W"
  ]
  node
  [
    id 1095
    y 0.2783
    x 0.3319
    z 0.5
    label "YJR113C"
  ]
  node
  [
    id 1096
    y 0.5238
    x 0.7281
    z 0.5
    label "YOR028C"
  ]
  node
  [
    id 1097
    y 0.5544
    x 0.3347
    z 0.5
    label "YJL017W"
  ]
  node
  [
    id 1098
    y 0.705
    x 0.3102
    z 0.5
    label "YBR115C"
  ]
  node
  [
    id 1099
    y 0.5557
    x 0.5882
    z 0.5
    label "YAL059W"
  ]
  node
  [
    id 1100
    y 0.6306
    x 0.5054
    z 0.5
    label "YPR018W"
  ]
  node
  [
    id 1101
    y 0.6888
    x 0.4299
    z 0.5
    label "YAL047C"
  ]
  node
  [
    id 1102
    y 0.4194
    x 0.4844
    z 0.5
    label "YBL056W"
  ]
  node
  [
    id 1103
    y 0.5412
    x 0.3243
    z 0.5
    label "YPL139C"
  ]
  node
  [
    id 1104
    y 0.5964
    x 0.2419
    z 0.5
    label "YLR273C"
  ]
  node
  [
    id 1105
    y 0.5288
    x 0.3411
    z 0.5
    label "YLR258W"
  ]
  node
  [
    id 1106
    y 0.5286
    x 0.2102
    z 0.5
    label "YIL072W"
  ]
  node
  [
    id 1107
    y 0.1677
    x 0.7314
    z 0.5
    label "YPL063W"
  ]
  node
  [
    id 1108
    y 0.1743
    x 0.7346
    z 0.5
    label "YML054C"
  ]
  node
  [
    id 1109
    y 0.7263
    x 0.4222
    z 0.5
    label "YOL069W"
  ]
  node
  [
    id 1110
    y 0.7797
    x 0.3309
    z 0.5
    label "YEL061C"
  ]
  node
  [
    id 1111
    y 0.4281
    x 0.7214
    z 0.5
    label "YOL018C"
  ]
  node
  [
    id 1112
    y 0.5366
    x 0.3411
    z 0.5
    label "YKL072W"
  ]
  node
  [
    id 1113
    y 0.4343
    x 0.534
    z 0.5
    label "YNL244C"
  ]
  node
  [
    id 1114
    y 0.6227
    x 0.6533
    z 0.5
    label "YAL041W"
  ]
  node
  [
    id 1115
    y 0.6768
    x 0.5722
    z 0.5
    label "YBR018C"
  ]
  node
  [
    id 1116
    y 0.6454
    x 0.5023
    z 0.5
    label "YNL161W"
  ]
  node
  [
    id 1117
    y 0.5187
    x 0.482
    z 0.5
    label "YJL089W"
  ]
  node
  [
    id 1118
    y 0.5491
    x 0.4496
    z 0.5
    label "YER027C"
  ]
  node
  [
    id 1119
    y 0.537
    x 0.6977
    z 0.5
    label "YGR282C"
  ]
  node
  [
    id 1120
    y 0.2943
    x 0.6677
    z 0.5
    label "YJR005W"
  ]
  node
  [
    id 1121
    y 0.1618
    x 0.6648
    z 0.5
    label "YLR014C"
  ]
  node
  [
    id 1122
    y 0.5766
    x 0.5035
    z 0.5
    label "YCR009C"
  ]
  node
  [
    id 1123
    y 0.7414
    x 0.4327
    z 0.5
    label "YMR232W"
  ]
  node
  [
    id 1124
    y 0.3408
    x 0.5926
    z 0.5
    label "YLL034C"
  ]
  node
  [
    id 1125
    y 0.4589
    x 0.5347
    z 0.5
    label "YGL004C"
  ]
  node
  [
    id 1126
    y 0.458
    x 0.461
    z 0.5
    label "YMR310C"
  ]
  node
  [
    id 1127
    y 0.7521
    x 0.7377
    z 0.5
    label "YLR254C"
  ]
  node
  [
    id 1128
    y 0.4759
    x 0.5276
    z 0.5
    label "YOR269W"
  ]
  node
  [
    id 1129
    y 0.6397
    x 0.4365
    z 0.5
    label "YDR002W"
  ]
  node
  [
    id 1130
    y 0.2536
    x 0.4914
    z 0.5
    label "YNL222W"
  ]
  node
  [
    id 1131
    y 0.5313
    x 0.3033
    z 0.5
    label "YOL142W"
  ]
  node
  [
    id 1132
    y 0.4439
    x 0.3526
    z 0.5
    label "YDR280W"
  ]
  node
  [
    id 1133
    y 0.4808
    x 0.7107
    z 0.5
    label "YGR172C"
  ]
  node
  [
    id 1134
    y 0.4003
    x 0.5883
    z 0.5
    label "YMR106C"
  ]
  node
  [
    id 1135
    y 0.7096
    x 0.7017
    z 0.5
    label "YKR034W"
  ]
  node
  [
    id 1136
    y 0.6315
    x 0.752
    z 0.5
    label "YHR060W"
  ]
  node
  [
    id 1137
    y 0.3962
    x 0.7195
    z 0.5
    label "YKR020W"
  ]
  node
  [
    id 1138
    y 0.6846
    x 0.6374
    z 0.5
    label "YJL092W"
  ]
  node
  [
    id 1139
    y 0.4192
    x 0.539
    z 0.5
    label "YGR232W"
  ]
  node
  [
    id 1140
    y 0.4981
    x 0.472
    z 0.5
    label "YER110C"
  ]
  node
  [
    id 1141
    y 0.5105
    x 0.4531
    z 0.5
    label "YOL127W"
  ]
  node
  [
    id 1142
    y 0.6607
    x 0.5926
    z 0.5
    label "YNL068C"
  ]
  node
  [
    id 1143
    y 0.5023
    x 0.556
    z 0.5
    label "YML007W"
  ]
  node
  [
    id 1144
    y 0.3136
    x 0.3515
    z 0.5
    label "YGR186W"
  ]
  node
  [
    id 1145
    y 0.6216
    x 0.6151
    z 0.5
    label "YJL157C"
  ]
  node
  [
    id 1146
    y 0.4817
    x 0.7933
    z 0.5
    label "YAL055W"
  ]
  node
  [
    id 1147
    y 0.5692
    x 0.69
    z 0.5
    label "YOR057W"
  ]
  node
  [
    id 1148
    y 0.5355
    x 0.7776
    z 0.5
    label "YNL055C"
  ]
  node
  [
    id 1149
    y 0.464
    x 0.6737
    z 0.5
    label "YNL093W"
  ]
  node
  [
    id 1150
    y 0.4618
    x 0.415
    z 0.5
    label "YOR061W"
  ]
  node
  [
    id 1151
    y 0.5162
    x 0.4676
    z 0.5
    label "YOL090W"
  ]
  node
  [
    id 1152
    y 0.2876
    x 0.2844
    z 0.5
    label "YCL063W"
  ]
  node
  [
    id 1153
    y 0.226
    x 0.233
    z 0.5
    label "YJL043W"
  ]
  node
  [
    id 1154
    y 0.4576
    x 0.4854
    z 0.5
    label "YNL201C"
  ]
  node
  [
    id 1155
    y 0.5397
    x 0.4431
    z 0.5
    label "YPL153C"
  ]
  node
  [
    id 1156
    y 0.736
    x 0.4288
    z 0.5
    label "YCR030C"
  ]
  node
  [
    id 1157
    y 0.2928
    x 0.5832
    z 0.5
    label "YDL137W"
  ]
  node
  [
    id 1158
    y 0.2492
    x 0.634
    z 0.5
    label "YHR188C"
  ]
  node
  [
    id 1159
    y 0.6192
    x 0.4961
    z 0.5
    label "YDR379W"
  ]
  node
  [
    id 1160
    y 0.7502
    x 0.486
    z 0.5
    label "YHR184W"
  ]
  node
  [
    id 1161
    y 0.8374
    x 0.4685
    z 0.5
    label "YLR185W"
  ]
  node
  [
    id 1162
    y 0.3788
    x 0.5014
    z 0.5
    label "YDL160C"
  ]
  node
  [
    id 1163
    y 0.5659
    x 0.4899
    z 0.5
    label "YER167W"
  ]
  node
  [
    id 1164
    y 0.6449
    x 0.7997
    z 0.5
    label "YLR215C"
  ]
  node
  [
    id 1165
    y 0.5773
    x 0.7019
    z 0.5
    label "YNL116W"
  ]
  node
  [
    id 1166
    y 0.78
    x 0.7529
    z 0.5
    label "YLR376C"
  ]
  node
  [
    id 1167
    y 0.8693
    x 0.7525
    z 0.5
    label "YIL132C"
  ]
  node
  [
    id 1168
    y 0.4812
    x 0.5248
    z 0.5
    label "YJR032W"
  ]
  node
  [
    id 1169
    y 0.3343
    x 0.7594
    z 0.5
    label "YBR171W"
  ]
  node
  [
    id 1170
    y 0.6286
    x 0.45
    z 0.5
    label "YBR234C"
  ]
  node
  [
    id 1171
    y 0.5201
    x 0.375
    z 0.5
    label "YKL152C"
  ]
  node
  [
    id 1172
    y 0.3253
    x 0.6538
    z 0.5
    label "YOR020C"
  ]
  node
  [
    id 1173
    y 0.5258
    x 0.6438
    z 0.5
    label "YBL017C"
  ]
  node
  [
    id 1174
    y 0.4488
    x 0.4365
    z 0.5
    label "YOL139C"
  ]
  node
  [
    id 1175
    y 0.3611
    x 0.3649
    z 0.5
    label "YOR276W"
  ]
  node
  [
    id 1176
    y 0.2498
    x 0.6631
    z 0.5
    label "YER007W"
  ]
  node
  [
    id 1177
    y 0.3557
    x 0.6685
    z 0.5
    label "YOR349W"
  ]
  node
  [
    id 1178
    y 0.427
    x 0.2021
    z 0.5
    label "YPR100W"
  ]
  node
  [
    id 1179
    y 0.4429
    x 0.3317
    z 0.5
    label "YBL038W"
  ]
  node
  [
    id 1180
    y 0.6308
    x 0.6328
    z 0.5
    label "YHR206W"
  ]
  node
  [
    id 1181
    y 0.3762
    x 0.4964
    z 0.5
    label "YJL033W"
  ]
  node
  [
    id 1182
    y 0.6864
    x 0.3413
    z 0.5
    label "YNL290W"
  ]
  node
  [
    id 1183
    y 0.2451
    x 0.3437
    z 0.5
    label "YJR050W"
  ]
  node
  [
    id 1184
    y 0.2607
    x 0.319
    z 0.5
    label "YGR129W"
  ]
  node
  [
    id 1185
    y 0.6196
    x 0.544
    z 0.5
    label "YLR223C"
  ]
  node
  [
    id 1186
    y 0.6073
    x 0.3963
    z 0.5
    label "YLR328W"
  ]
  node
  [
    id 1187
    y 0.6159
    x 0.4344
    z 0.5
    label "YGR010W"
  ]
  node
  [
    id 1188
    y 0.5886
    x 0.6636
    z 0.5
    label "YJR104C"
  ]
  node
  [
    id 1189
    y 0.4798
    x 0.5089
    z 0.5
    label "YPL125W"
  ]
  node
  [
    id 1190
    y 0.3998
    x 0.7962
    z 0.5
    label "YFL005W"
  ]
  node
  [
    id 1191
    y 0.4682
    x 0.718
    z 0.5
    label "YNL304W"
  ]
  node
  [
    id 1192
    y 0.5114
    x 0.2789
    z 0.5
    label "YGR095C"
  ]
  node
  [
    id 1193
    y 0.5462
    x 0.415
    z 0.5
    label "YOL021C"
  ]
  node
  [
    id 1194
    y 0.4798
    x 0.3868
    z 0.5
    label "YKR054C"
  ]
  node
  [
    id 1195
    y 0.5337
    x 0.3764
    z 0.5
    label "YGR144W"
  ]
  node
  [
    id 1196
    y 0.6452
    x 0.4672
    z 0.5
    label "YFR031C"
  ]
  node
  [
    id 1197
    y 0.323
    x 0.4026
    z 0.5
    label "YGL128C"
  ]
  node
  [
    id 1198
    y 0.5838
    x 0.7732
    z 0.5
    label "YGR018C"
  ]
  node
  [
    id 1199
    y 0.3871
    x 0.7785
    z 0.5
    label "YGL240W"
  ]
  node
  [
    id 1200
    y 0.6658
    x 0.7125
    z 0.5
    label "YPL219W"
  ]
  node
  [
    id 1201
    y 0.4318
    x 0.5371
    z 0.5
    label "YMR276W"
  ]
  node
  [
    id 1202
    y 0.5749
    x 0.4781
    z 0.5
    label "YNL309W"
  ]
  node
  [
    id 1203
    y 0.4563
    x 0.2732
    z 0.5
    label "YML062C"
  ]
  node
  [
    id 1204
    y 0.3104
    x 0.4832
    z 0.5
    label "YDL030W"
  ]
  node
  [
    id 1205
    y 0.7508
    x 0.5548
    z 0.5
    label "YHR193C"
  ]
  node
  [
    id 1206
    y 0.6316
    x 0.6271
    z 0.5
    label "YOL068C"
  ]
  node
  [
    id 1207
    y 0.532
    x 0.4088
    z 0.5
    label "YJR132W"
  ]
  node
  [
    id 1208
    y 0.2441
    x 0.4604
    z 0.5
    label "YOR227W"
  ]
  node
  [
    id 1209
    y 0.4834
    x 0.723
    z 0.5
    label "YOR089C"
  ]
  node
  [
    id 1210
    y 0.4095
    x 0.6334
    z 0.5
    label "YGR104C"
  ]
  node
  [
    id 1211
    y 0.5788
    x 0.4622
    z 0.5
    label "YER009W"
  ]
  node
  [
    id 1212
    y 0.2719
    x 0.4384
    z 0.5
    label "YLL036C"
  ]
  node
  [
    id 1213
    y 0.6588
    x 0.6951
    z 0.5
    label "YOR262W"
  ]
  node
  [
    id 1214
    y 0.6959
    x 0.6154
    z 0.5
    label "YDL056W"
  ]
  node
  [
    id 1215
    y 0.3977
    x 0.3893
    z 0.5
    label "YPR182W"
  ]
  node
  [
    id 1216
    y 0.6024
    x 0.6474
    z 0.5
    label "YER114C"
  ]
  node
  [
    id 1217
    y 0.4717
    x 0.4006
    z 0.5
    label "YMR125W"
  ]
  node
  [
    id 1218
    y 0.4643
    x 0.6758
    z 0.5
    label "YFL007W"
  ]
  node
  [
    id 1219
    y 0.3992
    x 0.3343
    z 0.5
    label "YDR322C-A"
  ]
  node
  [
    id 1220
    y 0.4445
    x 0.4585
    z 0.5
    label "YCL037C"
  ]
  node
  [
    id 1221
    y 0.503
    x 0.7334
    z 0.5
    label "YOR370C"
  ]
  node
  [
    id 1222
    y 0.1399
    x 0.6161
    z 0.5
    label "YBR288C"
  ]
  node
  [
    id 1223
    y 0.3562
    x 0.7374
    z 0.5
    label "YDL020C"
  ]
  node
  [
    id 1224
    y 0.3221
    x 0.5606
    z 0.5
    label "YIL075C"
  ]
  node
  [
    id 1225
    y 0.4071
    x 0.4124
    z 0.5
    label "YLR206W"
  ]
  node
  [
    id 1226
    y 0.5513
    x 0.5933
    z 0.5
    label "YML123C"
  ]
  node
  [
    id 1227
    y 0.4191
    x 0.3483
    z 0.5
    label "YBR119W"
  ]
  node
  [
    id 1228
    y 0.4131
    x 0.6638
    z 0.5
    label "YFR009W"
  ]
  node
  [
    id 1229
    y 0.5452
    x 0.731
    z 0.5
    label "YPL171C"
  ]
  node
  [
    id 1230
    y 0.384
    x 0.4594
    z 0.5
    label "YBR154C"
  ]
  node
  [
    id 1231
    y 0.7306
    x 0.4534
    z 0.5
    label "YDR181C"
  ]
  node
  [
    id 1232
    y 0.5696
    x 0.7789
    z 0.5
    label "YER188W"
  ]
  node
  [
    id 1233
    y 0.4819
    x 0.5477
    z 0.5
    label "YIL033C"
  ]
  node
  [
    id 1234
    y 0.3326
    x 0.841
    z 0.5
    label "YOR254C"
  ]
  node
  [
    id 1235
    y 0.3348
    x 0.8523
    z 0.5
    label "YLR292C"
  ]
  node
  [
    id 1236
    y 0.3504
    x 0.5074
    z 0.5
    label "YNL002C"
  ]
  node
  [
    id 1237
    y 0.4349
    x 0.4987
    z 0.5
    label "YJL098W"
  ]
  node
  [
    id 1238
    y 0.4883
    x 0.3782
    z 0.5
    label "YBR245C"
  ]
  node
  [
    id 1239
    y 0.5106
    x 0.507
    z 0.5
    label "YOR136W"
  ]
  node
  [
    id 1240
    y 0.3385
    x 0.702
    z 0.5
    label "YKL141W"
  ]
  node
  [
    id 1241
    y 0.3055
    x 0.6674
    z 0.5
    label "YLR170C"
  ]
  node
  [
    id 1242
    y 0.2161
    x 0.5846
    z 0.5
    label "YHL019C"
  ]
  node
  [
    id 1243
    y 0.5325
    x 0.7034
    z 0.5
    label "YMR054W"
  ]
  node
  [
    id 1244
    y 0.2276
    x 0.8094
    z 0.5
    label "YOL034W"
  ]
  node
  [
    id 1245
    y 0.1937
    x 0.8761
    z 0.5
    label "YLR007W"
  ]
  node
  [
    id 1246
    y 0.3567
    x 0.405
    z 0.5
    label "YLR117C"
  ]
  node
  [
    id 1247
    y 0.4728
    x 0.3238
    z 0.5
    label "YIL126W"
  ]
  node
  [
    id 1248
    y 0.5643
    x 0.346
    z 0.5
    label "YHR178W"
  ]
  node
  [
    id 1249
    y 0.5386
    x 0.7957
    z 0.5
    label "YDR318W"
  ]
  node
  [
    id 1250
    y 0.7202
    x 0.5833
    z 0.5
    label "YNL293W"
  ]
  node
  [
    id 1251
    y 0.3451
    x 0.6004
    z 0.5
    label "YHR174W"
  ]
  node
  [
    id 1252
    y 0.3552
    x 0.535
    z 0.5
    label "YNL127W"
  ]
  node
  [
    id 1253
    y 0.3149
    x 0.6811
    z 0.5
    label "YFL031W"
  ]
  node
  [
    id 1254
    y 0.4023
    x 0.3342
    z 0.5
    label "YDR235W"
  ]
  node
  [
    id 1255
    y 0.6165
    x 0.3699
    z 0.5
    label "YJR133W"
  ]
  node
  [
    id 1256
    y 0.3933
    x 0.6002
    z 0.5
    label "YPR072W"
  ]
  node
  [
    id 1257
    y 0.6144
    x 0.4028
    z 0.5
    label "YDL088C"
  ]
  node
  [
    id 1258
    y 0.2201
    x 0.7469
    z 0.5
    label "YJR058C"
  ]
  node
  [
    id 1259
    y 0.347
    x 0.7789
    z 0.5
    label "YPR017C"
  ]
  node
  [
    id 1260
    y 0.4446
    x 0.7898
    z 0.5
    label "YER031C"
  ]
  node
  [
    id 1261
    y 0.4333
    x 0.316
    z 0.5
    label "YLR439W"
  ]
  node
  [
    id 1262
    y 0.5463
    x 0.2428
    z 0.5
    label "YHR128W"
  ]
  node
  [
    id 1263
    y 0.5024
    x 0.4018
    z 0.5
    label "YHR152W"
  ]
  node
  [
    id 1264
    y 0.6109
    x 0.5108
    z 0.5
    label "YDR113C"
  ]
  node
  [
    id 1265
    y 0.4872
    x 0.5299
    z 0.5
    label "YDR116C"
  ]
  node
  [
    id 1266
    y 0.5943
    x 0.5979
    z 0.5
    label "YAL017W"
  ]
  node
  [
    id 1267
    y 0.6057
    x 0.6227
    z 0.5
    label "YKR029C"
  ]
  node
  [
    id 1268
    y 0.4406
    x 0.493
    z 0.5
    label "YDR298C"
  ]
  node
  [
    id 1269
    y 0.2251
    x 0.6841
    z 0.5
    label "YHR005C-A"
  ]
  node
  [
    id 1270
    y 0.4799
    x 0.3868
    z 0.5
    label "YER065C"
  ]
  node
  [
    id 1271
    y 0.4437
    x 0.3319
    z 0.5
    label "YDR123C"
  ]
  node
  [
    id 1272
    y 0.5465
    x 0.6748
    z 0.5
    label "YLR134W"
  ]
  node
  [
    id 1273
    y 0.4551
    x 0.5071
    z 0.5
    label "YPL122C"
  ]
  node
  [
    id 1274
    y 0.1948
    x 0.3832
    z 0.5
    label "YPL086C"
  ]
  node
  [
    id 1275
    y 0.3292
    x 0.7511
    z 0.5
    label "YER066W"
  ]
  node
  [
    id 1276
    y 0.4748
    x 0.4842
    z 0.5
    label "YBR025C"
  ]
  node
  [
    id 1277
    y 0.6445
    x 0.598
    z 0.5
    label "YOR247W"
  ]
  node
  [
    id 1278
    y 0.496
    x 0.6438
    z 0.5
    label "YGR134W"
  ]
  node
  [
    id 1279
    y 0.6949
    x 0.68
    z 0.5
    label "YGL233W"
  ]
  node
  [
    id 1280
    y 0.5436
    x 0.6911
    z 0.5
    label "YLR166C"
  ]
  node
  [
    id 1281
    y 0.5485
    x 0.5851
    z 0.5
    label "YIL112W"
  ]
  node
  [
    id 1282
    y 0.6105
    x 0.5273
    z 0.5
    label "YDL127W"
  ]
  node
  [
    id 1283
    y 0.4412
    x 0.548
    z 0.5
    label "YJR064W"
  ]
  node
  [
    id 1284
    y 0.6356
    x 0.4742
    z 0.5
    label "YNL084C"
  ]
  node
  [
    id 1285
    y 0.4623
    x 0.4782
    z 0.5
    label "YOR047C"
  ]
  node
  [
    id 1286
    y 0.3169
    x 0.6495
    z 0.5
    label "YBR086C"
  ]
  node
  [
    id 1287
    y 0.2404
    x 0.7142
    z 0.5
    label "YMR183C"
  ]
  node
  [
    id 1288
    y 0.3591
    x 0.6239
    z 0.5
    label "YDR128W"
  ]
  node
  [
    id 1289
    y 0.795
    x 0.4831
    z 0.5
    label "YFR027W"
  ]
  node
  [
    id 1290
    y 0.4634
    x 0.3598
    z 0.5
    label "YOR319W"
  ]
  node
  [
    id 1291
    y 0.7747
    x 0.5481
    z 0.5
    label "YML102W"
  ]
  node
  [
    id 1292
    y 0.4716
    x 0.923
    z 0.5
    label "YDR086C"
  ]
  node
  [
    id 1293
    y 0.3662
    x 0.4567
    z 0.5
    label "YAL035W"
  ]
  node
  [
    id 1294
    y 0.4828
    x 0.3061
    z 0.5
    label "YDR076W"
  ]
  node
  [
    id 1295
    y 0.6691
    x 0.5733
    z 0.5
    label "YNL007C"
  ]
  node
  [
    id 1296
    y 0.283
    x 0.4551
    z 0.5
    label "YDR364C"
  ]
  node
  [
    id 1297
    y 0.5833
    x 0.4107
    z 0.5
    label "YJL115W"
  ]
  node
  [
    id 1298
    y 0.3897
    x 0.5613
    z 0.5
    label "YDR188W"
  ]
  node
  [
    id 1299
    y 0.6558
    x 0.6504
    z 0.5
    label "YCL040W"
  ]
  node
  [
    id 1300
    y 0.72
    x 0.7136
    z 0.5
    label "YPL059W"
  ]
  node
  [
    id 1301
    y 0.4892
    x 0.4428
    z 0.5
    label "YKL214C"
  ]
  node
  [
    id 1302
    y 0.5754
    x 0.5841
    z 0.5
    label "YNL243W"
  ]
  node
  [
    id 1303
    y 0.5762
    x 0.5296
    z 0.5
    label "YEL060C"
  ]
  node
  [
    id 1304
    y 0.6158
    x 0.6392
    z 0.5
    label "YMR104C"
  ]
  node
  [
    id 1305
    y 0.3764
    x 0.6227
    z 0.5
    label "YIL076W"
  ]
  node
  [
    id 1306
    y 0.5181
    x 0.5563
    z 0.5
    label "YKL101W"
  ]
  node
  [
    id 1307
    y 0.4052
    x 0.6153
    z 0.5
    label "YHR088W"
  ]
  node
  [
    id 1308
    y 0.242
    x 0.4188
    z 0.5
    label "YCL054W"
  ]
  node
  [
    id 1309
    y 0.3212
    x 0.46
    z 0.5
    label "YLR186W"
  ]
  node
  [
    id 1310
    y 0.6994
    x 0.5021
    z 0.5
    label "YBR293W"
  ]
  node
  [
    id 1311
    y 0.7287
    x 0.5837
    z 0.5
    label "YDR438W"
  ]
  node
  [
    id 1312
    y 0.3965
    x 0.2993
    z 0.5
    label "YGL173C"
  ]
  node
  [
    id 1313
    y 0.2735
    x 0.5834
    z 0.5
    label "YKL011C"
  ]
  node
  [
    id 1314
    y 0.3054
    x 0.4941
    z 0.5
    label "YNL101W"
  ]
  node
  [
    id 1315
    y 0.5228
    x 0.703
    z 0.5
    label "YGR113W"
  ]
  node
  [
    id 1316
    y 0.4268
    x 0.4486
    z 0.5
    label "YCR073C"
  ]
  node
  [
    id 1317
    y 0.5099
    x 0.2715
    z 0.5
    label "YNL232W"
  ]
  node
  [
    id 1318
    y 0.7315
    x 0.6541
    z 0.5
    label "YAR027W"
  ]
  node
  [
    id 1319
    y 0.8151
    x 0.674
    z 0.5
    label "YAR030C"
  ]
  node
  [
    id 1320
    y 0.7154
    x 0.508
    z 0.5
    label "YNL196C"
  ]
  node
  [
    id 1321
    y 0.2895
    x 0.4938
    z 0.5
    label "YIL071C"
  ]
  node
  [
    id 1322
    y 0.5592
    x 0.5203
    z 0.5
    label "YGL158W"
  ]
  node
  [
    id 1323
    y 0.7096
    x 0.4259
    z 0.5
    label "YNL126W"
  ]
  node
  [
    id 1324
    y 0.5109
    x 0.3554
    z 0.5
    label "YPR161C"
  ]
  node
  [
    id 1325
    y 0.4757
    x 0.5229
    z 0.5
    label "YLR248W"
  ]
  node
  [
    id 1326
    y 0.4572
    x 0.6677
    z 0.5
    label "YAR019C"
  ]
  node
  [
    id 1327
    y 0.417
    x 0.7192
    z 0.5
    label "YDR044W"
  ]
  node
  [
    id 1328
    y 0.314
    x 0.4925
    z 0.5
    label "YDR449C"
  ]
  node
  [
    id 1329
    y 0.5337
    x 0.3764
    z 0.5
    label "YDR231C"
  ]
  node
  [
    id 1330
    y 0.6698
    x 0.7116
    z 0.5
    label "YHR071W"
  ]
  node
  [
    id 1331
    y 0.4789
    x 0.5263
    z 0.5
    label "YBR059C"
  ]
  node
  [
    id 1332
    y 0.4902
    x 0.5299
    z 0.5
    label "YGL213C"
  ]
  node
  [
    id 1333
    y 0.6354
    x 0.4953
    z 0.5
    label "YLR249W"
  ]
  node
  [
    id 1334
    y 0.4601
    x 0.775
    z 0.5
    label "YOR115C"
  ]
  node
  [
    id 1335
    y 0.5712
    x 0.5339
    z 0.5
    label "YJL074C"
  ]
  node
  [
    id 1336
    y 0.477
    x 0.5507
    z 0.5
    label "YHR023W"
  ]
  node
  [
    id 1337
    y 0.3637
    x 0.6123
    z 0.5
    label "YPR032W"
  ]
  node
  [
    id 1338
    y 0.3401
    x 0.4109
    z 0.5
    label "YKL060C"
  ]
  node
  [
    id 1339
    y 0.5762
    x 0.3635
    z 0.5
    label "YGL133W"
  ]
  node
  [
    id 1340
    y 0.4659
    x 0.4784
    z 0.5
    label "YFR028C"
  ]
  node
  [
    id 1341
    y 0.5388
    x 0.5688
    z 0.5
    label "YBR216C"
  ]
  node
  [
    id 1342
    y 0.5219
    x 0.7356
    z 0.5
    label "YLR109W"
  ]
  node
  [
    id 1343
    y 0.3195
    x 0.5954
    z 0.5
    label "YDR457W"
  ]
  node
  [
    id 1344
    y 0.6528
    x 0.4687
    z 0.5
    label "YOR033C"
  ]
  node
  [
    id 1345
    y 0.553
    x 0.6936
    z 0.5
    label "YLR291C"
  ]
  node
  [
    id 1346
    y 0.2234
    x 0.8145
    z 0.5
    label "YBR091C"
  ]
  node
  [
    id 1347
    y 0.4113
    x 0.36
    z 0.5
    label "YEL056W"
  ]
  node
  [
    id 1348
    y 0.2378
    x 0.6847
    z 0.5
    label "YJL143W"
  ]
  node
  [
    id 1349
    y 0.645
    x 0.586
    z 0.5
    label "YHR118C"
  ]
  node
  [
    id 1350
    y 0.2563
    x 0.5729
    z 0.5
    label "YDR273W"
  ]
  node
  [
    id 1351
    y 0.5804
    x 0.5207
    z 0.5
    label "YDR335W"
  ]
  node
  [
    id 1352
    y 0.3506
    x 0.7107
    z 0.5
    label "YMR297W"
  ]
  node
  [
    id 1353
    y 0.3922
    x 0.7367
    z 0.5
    label "YOL147C"
  ]
  node
  [
    id 1354
    y 0.3708
    x 0.5741
    z 0.5
    label "YPL011C"
  ]
  node
  [
    id 1355
    y 0.5131
    x 0.5634
    z 0.5
    label "YDR207C"
  ]
  node
  [
    id 1356
    y 0.5149
    x 0.6795
    z 0.5
    label "YFL009W"
  ]
  node
  [
    id 1357
    y 0.3852
    x 0.5243
    z 0.5
    label "YAR018C"
  ]
  node
  [
    id 1358
    y 0.6071
    x 0.7172
    z 0.5
    label "YPL161C"
  ]
  node
  [
    id 1359
    y 0.6681
    x 0.7008
    z 0.5
    label "YGR152C"
  ]
  node
  [
    id 1360
    y 0.7273
    x 0.6746
    z 0.5
    label "YJL110C"
  ]
  node
  [
    id 1361
    y 0.8197
    x 0.7691
    z 0.5
    label "YHL011C"
  ]
  node
  [
    id 1362
    y 0.7448
    x 0.7461
    z 0.5
    label "YKL181W"
  ]
  node
  [
    id 1363
    y 0.605
    x 0.3583
    z 0.5
    label "YLR357W"
  ]
  node
  [
    id 1364
    y 0.7475
    x 0.2883
    z 0.5
    label "YCL028W"
  ]
  node
  [
    id 1365
    y 0.4902
    x 0.5288
    z 0.5
    label "YNL124W"
  ]
  node
  [
    id 1366
    y 0.7674
    x 0.5146
    z 0.5
    label "YIL144W"
  ]
  node
  [
    id 1367
    y 0.316
    x 0.5536
    z 0.5
    label "YLR192C"
  ]
  node
  [
    id 1368
    y 0.5406
    x 0.3807
    z 0.5
    label "YFL003C"
  ]
  node
  [
    id 1369
    y 0.4053
    x 0.6475
    z 0.5
    label "YOR260W"
  ]
  node
  [
    id 1370
    y 0.2544
    x 0.7705
    z 0.5
    label "YOR180C"
  ]
  node
  [
    id 1371
    y 0.621
    x 0.2899
    z 0.5
    label "YDL090C"
  ]
  node
  [
    id 1372
    y 0.6196
    x 0.5441
    z 0.5
    label "YKR089C"
  ]
  node
  [
    id 1373
    y 0.597
    x 0.6593
    z 0.5
    label "YDR043C"
  ]
  node
  [
    id 1374
    y 0.5277
    x 0.5718
    z 0.5
    label "YER040W"
  ]
  node
  [
    id 1375
    y 0.3766
    x 0.5909
    z 0.5
    label "YJL001W"
  ]
  node
  [
    id 1376
    y 0.3826
    x 0.2507
    z 0.5
    label "YOR368W"
  ]
  node
  [
    id 1377
    y 0.3205
    x 0.4085
    z 0.5
    label "YDR324C"
  ]
  node
  [
    id 1378
    y 0.4583
    x 0.5175
    z 0.5
    label "YMR105C"
  ]
  node
  [
    id 1379
    y 0.3168
    x 0.4585
    z 0.5
    label "YOL077C"
  ]
  node
  [
    id 1380
    y 0.6069
    x 0.4339
    z 0.5
    label "YNR035C"
  ]
  node
  [
    id 1381
    y 0.5255
    x 0.5488
    z 0.5
    label "YLR429W"
  ]
  node
  [
    id 1382
    y 0.7355
    x 0.5301
    z 0.5
    label "YGR089W"
  ]
  node
  [
    id 1383
    y 0.5876
    x 0.5018
    z 0.5
    label "YLR086W"
  ]
  node
  [
    id 1384
    y 0.361
    x 0.4603
    z 0.5
    label "YNL112W"
  ]
  node
  [
    id 1385
    y 0.5597
    x 0.3153
    z 0.5
    label "YDL111C"
  ]
  node
  [
    id 1386
    y 0.5455
    x 0.5378
    z 0.5
    label "YIL079C"
  ]
  node
  [
    id 1387
    y 0.7405
    x 0.4138
    z 0.5
    label "YOR299W"
  ]
  node
  [
    id 1388
    y 0.4115
    x 0.4455
    z 0.5
    label "YKL210W"
  ]
  node
  [
    id 1389
    y 0.3509
    x 0.2992
    z 0.5
    label "YER100W"
  ]
  node
  [
    id 1390
    y 0.5648
    x 0.6536
    z 0.5
    label "YJR053W"
  ]
  node
  [
    id 1391
    y 0.5954
    x 0.706
    z 0.5
    label "YMR042W"
  ]
  node
  [
    id 1392
    y 0.568
    x 0.2744
    z 0.5
    label "YML105C"
  ]
  node
  [
    id 1393
    y 0.3836
    x 0.7299
    z 0.5
    label "YMR026C"
  ]
  node
  [
    id 1394
    y 0.5754
    x 0.4188
    z 0.5
    label "YGL238W"
  ]
  node
  [
    id 1395
    y 0.4502
    x 0.7567
    z 0.5
    label "YGR140W"
  ]
  node
  [
    id 1396
    y 0.5238
    x 0.7878
    z 0.5
    label "YJR089W"
  ]
  node
  [
    id 1397
    y 0.2877
    x 0.5028
    z 0.5
    label "YMR270C"
  ]
  node
  [
    id 1398
    y 0.4879
    x 0.6481
    z 0.5
    label "YHR033W"
  ]
  node
  [
    id 1399
    y 0.5057
    x 0.6857
    z 0.5
    label "YKL073W"
  ]
  node
  [
    id 1400
    y 0.5082
    x 0.5098
    z 0.5
    label "YDR477W"
  ]
  node
  [
    id 1401
    y 0.4844
    x 0.5291
    z 0.5
    label "YPR056W"
  ]
  node
  [
    id 1402
    y 0.4527
    x 0.5667
    z 0.5
    label "YKR036C"
  ]
  node
  [
    id 1403
    y 0.4171
    x 0.6281
    z 0.5
    label "YBR193C"
  ]
  node
  [
    id 1404
    y 0.6591
    x 0.6667
    z 0.5
    label "YFL030W"
  ]
  node
  [
    id 1405
    y 0.6565
    x 0.6474
    z 0.5
    label "YOL081W"
  ]
  node
  [
    id 1406
    y 0.5515
    x 0.5951
    z 0.5
    label "YMR139W"
  ]
  node
  [
    id 1407
    y 0.3932
    x 0.3813
    z 0.5
    label "YBL074C"
  ]
  node
  [
    id 1408
    y 0.5552
    x 0.515
    z 0.5
    label "YGL035C"
  ]
  node
  [
    id 1409
    y 0.206
    x 0.3689
    z 0.5
    label "YHR103W"
  ]
  node
  [
    id 1410
    y 0.7781
    x 0.6351
    z 0.5
    label "YDR034C"
  ]
  node
  [
    id 1411
    y 0.3624
    x 0.4112
    z 0.5
    label "YDR148C"
  ]
  node
  [
    id 1412
    y 0.3447
    x 0.2825
    z 0.5
    label "YFR049W"
  ]
  node
  [
    id 1413
    y 0.5166
    x 0.2287
    z 0.5
    label "YPL179W"
  ]
  node
  [
    id 1414
    y 0.5095
    x 0.5495
    z 0.5
    label "YPL237W"
  ]
  node
  [
    id 1415
    y 0.7124
    x 0.5031
    z 0.5
    label "YBL031W"
  ]
  node
  [
    id 1416
    y 0.5943
    x 0.4373
    z 0.5
    label "YGL060W"
  ]
  node
  [
    id 1417
    y 0.4188
    x 0.727
    z 0.5
    label "YJR056C"
  ]
  node
  [
    id 1418
    y 0.6416
    x 0.4963
    z 0.5
    label "YLR103C"
  ]
  node
  [
    id 1419
    y 0.2532
    x 0.5844
    z 0.5
    label "YBL025W"
  ]
  node
  [
    id 1420
    y 0.6004
    x 0.7713
    z 0.5
    label "YBR280C"
  ]
  node
  [
    id 1421
    y 0.5492
    x 0.6942
    z 0.5
    label "YAL003W"
  ]
  node
  [
    id 1422
    y 0.7036
    x 0.6705
    z 0.5
    label "YML058W"
  ]
  node
  [
    id 1423
    y 0.2753
    x 0.5162
    z 0.5
    label "YPR190C"
  ]
  node
  [
    id 1424
    y 0.6385
    x 0.4314
    z 0.5
    label "YBR088C"
  ]
  node
  [
    id 1425
    y 0.6592
    x 0.7744
    z 0.5
    label "YEL054C"
  ]
  node
  [
    id 1426
    y 0.6096
    x 0.3427
    z 0.5
    label "YLR449W"
  ]
  node
  [
    id 1427
    y 0.5182
    x 0.5404
    z 0.5
    label "YPR160W"
  ]
  node
  [
    id 1428
    y 0.6291
    x 0.4248
    z 0.5
    label "YMR192W"
  ]
  node
  [
    id 1429
    y 0.5641
    x 0.4608
    z 0.5
    label "YMR153W"
  ]
  node
  [
    id 1430
    y 0.4066
    x 0.7288
    z 0.5
    label "YDR529C"
  ]
  node
  [
    id 1431
    y 0.4775
    x 0.8889
    z 0.5
    label "YDR313C"
  ]
  node
  [
    id 1432
    y 0.7704
    x 0.5075
    z 0.5
    label "YFL034C-B"
  ]
  node
  [
    id 1433
    y 0.1917
    x 0.4982
    z 0.5
    label "YOL117W"
  ]
  node
  [
    id 1434
    y 0.3783
    x 0.4463
    z 0.5
    label "YBR247C"
  ]
  node
  [
    id 1435
    y 0.5213
    x 0.4301
    z 0.5
    label "YBR126C"
  ]
  node
  [
    id 1436
    y 0.6484
    x 0.6047
    z 0.5
    label "YNR047W"
  ]
  node
  [
    id 1437
    y 0.4376
    x 0.4456
    z 0.5
    label "YKR025W"
  ]
  node
  [
    id 1438
    y 0.5628
    x 0.7106
    z 0.5
    label "YNL098C"
  ]
  node
  [
    id 1439
    y 0.5144
    x 0.3995
    z 0.5
    label "YER105C"
  ]
  node
  [
    id 1440
    y 0.7517
    x 0.7519
    z 0.5
    label "YIL066C"
  ]
  node
  [
    id 1441
    y 0.5522
    x 0.7864
    z 0.5
    label "YMR258C"
  ]
  node
  [
    id 1442
    y 0.5709
    x 0.4971
    z 0.5
    label "YDR386W"
  ]
  node
  [
    id 1443
    y 0.68
    x 0.5246
    z 0.5
    label "YLR210W"
  ]
  node
  [
    id 1444
    y 0.489
    x 0.6847
    z 0.5
    label "YMR227C"
  ]
  node
  [
    id 1445
    y 0.5007
    x 0.505
    z 0.5
    label "YNL085W"
  ]
  node
  [
    id 1446
    y 0.7673
    x 0.388
    z 0.5
    label "YLR045C"
  ]
  node
  [
    id 1447
    y 0.6158
    x 0.5256
    z 0.5
    label "YJL095W"
  ]
  node
  [
    id 1448
    y 0.5378
    x 0.6501
    z 0.5
    label "YDR480W"
  ]
  node
  [
    id 1449
    y 0.5609
    x 0.3854
    z 0.5
    label "YGR281W"
  ]
  node
  [
    id 1450
    y 0.5663
    x 0.6129
    z 0.5
    label "YPL209C"
  ]
  node
  [
    id 1451
    y 0.481
    x 0.5431
    z 0.5
    label "YNL175C"
  ]
  node
  [
    id 1452
    y 0.5482
    x 0.613
    z 0.5
    label "YPR119W"
  ]
  node
  [
    id 1453
    y 0.4772
    x 0.5259
    z 0.5
    label "YOR264W"
  ]
  node
  [
    id 1454
    y 0.499
    x 0.3826
    z 0.5
    label "YBL002W"
  ]
  node
  [
    id 1455
    y 0.6249
    x 0.7277
    z 0.5
    label "YOL113W"
  ]
  node
  [
    id 1456
    y 0.6109
    x 0.3762
    z 0.5
    label "YGR088W"
  ]
  node
  [
    id 1457
    y 0.6084
    x 0.691
    z 0.5
    label "YBR140C"
  ]
  node
  [
    id 1458
    y 0.422
    x 0.5076
    z 0.5
    label "YKR028W"
  ]
  node
  [
    id 1459
    y 0.3946
    x 0.6642
    z 0.5
    label "YOR230W"
  ]
  node
  [
    id 1460
    y 0.3908
    x 0.5523
    z 0.5
    label "YNL236W"
  ]
  node
  [
    id 1461
    y 0.2431
    x 0.4894
    z 0.5
    label "YGR210C"
  ]
  node
  [
    id 1462
    y 0.7264
    x 0.6682
    z 0.5
    label "YKL007W"
  ]
  node
  [
    id 1463
    y 0.8346
    x 0.5449
    z 0.5
    label "YDR252W"
  ]
  node
  [
    id 1464
    y 0.6141
    x 0.3989
    z 0.5
    label "YCR092C"
  ]
  node
  [
    id 1465
    y 0.5521
    x 0.6658
    z 0.5
    label "YGR087C"
  ]
  node
  [
    id 1466
    y 0.623
    x 0.3387
    z 0.5
    label "YNL082W"
  ]
  node
  [
    id 1467
    y 0.6193
    x 0.4694
    z 0.5
    label "YBL079W"
  ]
  node
  [
    id 1468
    y 0.6233
    x 0.4615
    z 0.5
    label "YJL061W"
  ]
  node
  [
    id 1469
    y 0.2457
    x 0.4545
    z 0.5
    label "YPL211W"
  ]
  node
  [
    id 1470
    y 0.7336
    x 0.6124
    z 0.5
    label "YCL016C"
  ]
  node
  [
    id 1471
    y 0.7078
    x 0.3835
    z 0.5
    label "YHR191C"
  ]
  node
  [
    id 1472
    y 0.3676
    x 0.6853
    z 0.5
    label "YBR281C"
  ]
  node
  [
    id 1473
    y 0.5604
    x 0.4955
    z 0.5
    label "YDR243C"
  ]
  node
  [
    id 1474
    y 0.7551
    x 0.6313
    z 0.5
    label "YLR190W"
  ]
  node
  [
    id 1475
    y 0.6295
    x 0.5844
    z 0.5
    label "YIL050W"
  ]
  node
  [
    id 1476
    y 0.4208
    x 0.9098
    z 0.5
    label "YMR214W"
  ]
  node
  [
    id 1477
    y 0.4014
    x 0.8293
    z 0.5
    label "YJL034W"
  ]
  node
  [
    id 1478
    y 0.1302
    x 0.5237
    z 0.5
    label "YKL211C"
  ]
  node
  [
    id 1479
    y 0.634
    x 0.666
    z 0.5
    label "YNL218W"
  ]
  node
  [
    id 1480
    y 0.2953
    x 0.5391
    z 0.5
    label "YMR056C"
  ]
  node
  [
    id 1481
    y 0.2154
    x 0.4227
    z 0.5
    label "YDR464W"
  ]
  node
  [
    id 1482
    y 0.2673
    x 0.6551
    z 0.5
    label "YML051W"
  ]
  node
  [
    id 1483
    y 0.1795
    x 0.6855
    z 0.5
    label "YBR020W"
  ]
  node
  [
    id 1484
    y 0.2899
    x 0.5954
    z 0.5
    label "YKL028W"
  ]
  node
  [
    id 1485
    y 0.4831
    x 0.6626
    z 0.5
    label "YHR005C"
  ]
  node
  [
    id 1486
    y 0.6251
    x 0.4721
    z 0.5
    label "YIL115C"
  ]
  node
  [
    id 1487
    y 0.5174
    x 0.5351
    z 0.5
    label "YPR023C"
  ]
  node
  [
    id 1488
    y 0.6196
    x 0.544
    z 0.5
    label "YKL116C"
  ]
  node
  [
    id 1489
    y 0.3922
    x 0.7367
    z 0.5
    label "YJL210W"
  ]
  node
  [
    id 1490
    y 0.1505
    x 0.6811
    z 0.5
    label "YBL034C"
  ]
  node
  [
    id 1491
    y 0.6361
    x 0.5257
    z 0.5
    label "YDL106C"
  ]
  node
  [
    id 1492
    y 0.4804
    x 0.7725
    z 0.5
    label "YLR358C"
  ]
  node
  [
    id 1493
    y 0.9024
    x 0.5646
    z 0.5
    label "YLL010C"
  ]
  node
  [
    id 1494
    y 0.8241
    x 0.5834
    z 0.5
    label "YOR043W"
  ]
  node
  [
    id 1495
    y 0.3279
    x 0.4353
    z 0.5
    label "YPL228W"
  ]
  node
  [
    id 1496
    y 0.5802
    x 0.5639
    z 0.5
    label "YDL108W"
  ]
  node
  [
    id 1497
    y 0.2718
    x 0.5699
    z 0.5
    label "YGL156W"
  ]
  node
  [
    id 1498
    y 0.2604
    x 0.3345
    z 0.5
    label "YHR183W"
  ]
  node
  [
    id 1499
    y 0.5793
    x 0.2418
    z 0.5
    label "YMR059W"
  ]
  node
  [
    id 1500
    y 0.3568
    x 0.2405
    z 0.5
    label "YOR064C"
  ]
  node
  [
    id 1501
    y 0.4674
    x 0.3072
    z 0.5
    label "YER086W"
  ]
  node
  [
    id 1502
    y 0.6072
    x 0.7269
    z 0.5
    label "YDL135C"
  ]
  node
  [
    id 1503
    y 0.7259
    x 0.4065
    z 0.5
    label "YOR046C"
  ]
  node
  [
    id 1504
    y 0.1461
    x 0.7013
    z 0.5
    label "YGR181W"
  ]
  node
  [
    id 1505
    y 0.1382
    x 0.6829
    z 0.5
    label "YJR135W-A"
  ]
  node
  [
    id 1506
    y 0.5398
    x 0.4941
    z 0.5
    label "YGL245W"
  ]
  node
  [
    id 1507
    y 0.6482
    x 0.7719
    z 0.5
    label "YDR173C"
  ]
  node
  [
    id 1508
    y 0.3405
    x 0.3108
    z 0.5
    label "YML112W"
  ]
  node
  [
    id 1509
    y 0.2954
    x 0.3465
    z 0.5
    label "YDR088C"
  ]
  node
  [
    id 1510
    y 0.3066
    x 0.5213
    z 0.5
    label "YER013W"
  ]
  node
  [
    id 1511
    y 0.5337
    x 0.3764
    z 0.5
    label "YGR034W"
  ]
  node
  [
    id 1512
    y 0.4799
    x 0.3868
    z 0.5
    label "YFR006W"
  ]
  node
  [
    id 1513
    y 0.7613
    x 0.5543
    z 0.5
    label "YHR070W"
  ]
  node
  [
    id 1514
    y 0.5307
    x 0.2555
    z 0.5
    label "YIL074C"
  ]
  node
  [
    id 1515
    y 0.1659
    x 0.5511
    z 0.5
    label "YMR231W"
  ]
  node
  [
    id 1516
    y 0.7459
    x 0.4769
    z 0.5
    label "YAL028W"
  ]
  node
  [
    id 1517
    y 0.278
    x 0.4913
    z 0.5
    label "YOR207C"
  ]
  node
  [
    id 1518
    y 0.5641
    x 0.8001
    z 0.5
    label "YHR115C"
  ]
  node
  [
    id 1519
    y 0.5299
    x 0.4984
    z 0.5
    label "YBR125C"
  ]
  node
  [
    id 1520
    y 0.5215
    x 0.8603
    z 0.5
    label "YGR115C"
  ]
  node
  [
    id 1521
    y 0.5296
    x 0.357
    z 0.5
    label "YOR191W"
  ]
  node
  [
    id 1522
    y 0.4619
    x 0.7916
    z 0.5
    label "YPL180W"
  ]
  node
  [
    id 1523
    y 0.55
    x 0.6995
    z 0.5
    label "YER079W"
  ]
  node
  [
    id 1524
    y 0.5398
    x 0.5342
    z 0.5
    label "YGL201C"
  ]
  node
  [
    id 1525
    y 0.4659
    x 0.7386
    z 0.5
    label "YPL187W"
  ]
  node
  [
    id 1526
    y 0.4064
    x 0.6445
    z 0.5
    label "YPL218W"
  ]
  node
  [
    id 1527
    y 0.6185
    x 0.4499
    z 0.5
    label "YNL079C"
  ]
  node
  [
    id 1528
    y 0.8177
    x 0.4598
    z 0.5
    label "YER020W"
  ]
  node
  [
    id 1529
    y 0.8792
    x 0.4122
    z 0.5
    label "YDL035C"
  ]
  node
  [
    id 1530
    y 0.766
    x 0.5803
    z 0.5
    label "YGR167W"
  ]
  node
  [
    id 1531
    y 0.8619
    x 0.6073
    z 0.5
    label "YDL247W"
  ]
  node
  [
    id 1532
    y 0.2725
    x 0.6143
    z 0.5
    label "YLR125W"
  ]
  node
  [
    id 1533
    y 0.1587
    x 0.6442
    z 0.5
    label "YDL110C"
  ]
  node
  [
    id 1534
    y 0.2279
    x 0.5713
    z 0.5
    label "YOR232W"
  ]
  node
  [
    id 1535
    y 0.5096
    x 0.8649
    z 0.5
    label "YNL145W"
  ]
  node
  [
    id 1536
    y 0.784
    x 0.4648
    z 0.5
    label "YML121W"
  ]
  node
  [
    id 1537
    y 0.5659
    x 0.2088
    z 0.5
    label "YHR004C"
  ]
  node
  [
    id 1538
    y 0.3775
    x 0.5761
    z 0.5
    label "YOR229W"
  ]
  node
  [
    id 1539
    y 0.6146
    x 0.6182
    z 0.5
    label "YBR133C"
  ]
  node
  [
    id 1540
    y 0.6863
    x 0.4813
    z 0.5
    label "YCL034W"
  ]
  node
  [
    id 1541
    y 0.516
    x 0.667
    z 0.5
    label "YMR055C"
  ]
  node
  [
    id 1542
    y 0.7066
    x 0.7094
    z 0.5
    label "YMR238W"
  ]
  node
  [
    id 1543
    y 0.6785
    x 0.759
    z 0.5
    label "YDR218C"
  ]
  node
  [
    id 1544
    y 0.4192
    x 0.2249
    z 0.5
    label "YLR399C"
  ]
  node
  [
    id 1545
    y 0.4937
    x 0.6214
    z 0.5
    label "YOR332W"
  ]
  node
  [
    id 1546
    y 0.6308
    x 0.4057
    z 0.5
    label "YKL205W"
  ]
  node
  [
    id 1547
    y 0.3887
    x 0.3864
    z 0.5
    label "YLR288C"
  ]
  node
  [
    id 1548
    y 0.4614
    x 0.3232
    z 0.5
    label "YPL194W"
  ]
  node
  [
    id 1549
    y 0.5229
    x 0.8417
    z 0.5
    label "YPL018W"
  ]
  node
  [
    id 1550
    y 0.2913
    x 0.5676
    z 0.5
    label "YOL010W"
  ]
  node
  [
    id 1551
    y 0.7581
    x 0.5897
    z 0.5
    label "YNL313C"
  ]
  node
  [
    id 1552
    y 0.467
    x 0.7116
    z 0.5
    label "YNL311C"
  ]
  node
  [
    id 1553
    y 0.3887
    x 0.6653
    z 0.5
    label "YBR080C"
  ]
  node
  [
    id 1554
    y 0.3502
    x 0.3626
    z 0.5
    label "YHR143W-A"
  ]
  node
  [
    id 1555
    y 0.1679
    x 0.6674
    z 0.5
    label "YLR008C"
  ]
  node
  [
    id 1556
    y 0.4498
    x 0.4829
    z 0.5
    label "YIL143C"
  ]
  node
  [
    id 1557
    y 0.29
    x 0.6363
    z 0.5
    label "YBL078C"
  ]
  node
  [
    id 1558
    y 0.5816
    x 0.3107
    z 0.5
    label "YDL154W"
  ]
  node
  [
    id 1559
    y 0.5156
    x 0.3392
    z 0.5
    label "YPL190C"
  ]
  node
  [
    id 1560
    y 0.5984
    x 0.7541
    z 0.5
    label "YLR352W"
  ]
  node
  [
    id 1561
    y 0.5085
    x 0.8149
    z 0.5
    label "YDR409W"
  ]
  node
  [
    id 1562
    y 0.5945
    x 0.5171
    z 0.5
    label "YEL032W"
  ]
  node
  [
    id 1563
    y 0.6559
    x 0.4746
    z 0.5
    label "YBR202W"
  ]
  node
  [
    id 1564
    y 0.5168
    x 0.3578
    z 0.5
    label "YNL139C"
  ]
  node
  [
    id 1565
    y 0.7225
    x 0.5084
    z 0.5
    label "YNL047C"
  ]
  node
  [
    id 1566
    y 0.5883
    x 0.5025
    z 0.5
    label "YLR433C"
  ]
  node
  [
    id 1567
    y 0.3911
    x 0.2805
    z 0.5
    label "YLL018C"
  ]
  node
  [
    id 1568
    y 0.7125
    x 0.511
    z 0.5
    label "YMR180C"
  ]
  node
  [
    id 1569
    y 0.3487
    x 0.4268
    z 0.5
    label "YKL059C"
  ]
  node
  [
    id 1570
    y 0.5422
    x 0.637
    z 0.5
    label "YDL084W"
  ]
  node
  [
    id 1571
    y 0.6172
    x 0.3009
    z 0.5
    label "YHR079C-A"
  ]
  node
  [
    id 1572
    y 0.5214
    x 0.4438
    z 0.5
    label "YDR028C"
  ]
  node
  [
    id 1573
    y 0.7347
    x 0.4601
    z 0.5
    label "YHR134W"
  ]
  node
  [
    id 1574
    y 0.4422
    x 0.6286
    z 0.5
    label "YCR072C"
  ]
  node
  [
    id 1575
    y 0.5268
    x 0.3279
    z 0.5
    label "YDR369C"
  ]
  node
  [
    id 1576
    y 0.7518
    x 0.6857
    z 0.5
    label "YCR097W"
  ]
  node
  [
    id 1577
    y 0.4778
    x 0.3813
    z 0.5
    label "YKL135C"
  ]
  node
  [
    id 1578
    y 0.7255
    x 0.4388
    z 0.5
    label "YOR213C"
  ]
  node
  [
    id 1579
    y 0.6232
    x 0.5066
    z 0.5
    label "YLR035C"
  ]
  node
  [
    id 1580
    y 0.3533
    x 0.4222
    z 0.5
    label "YGR067C"
  ]
  node
  [
    id 1581
    y 0.4836
    x 0.2378
    z 0.5
    label "YPR135W"
  ]
  node
  [
    id 1582
    y 0.2718
    x 0.5377
    z 0.5
    label "YKL144C"
  ]
  node
  [
    id 1583
    y 0.3862
    x 0.4362
    z 0.5
    label "YOR194C"
  ]
  node
  [
    id 1584
    y 0.4482
    x 0.2565
    z 0.5
    label "YDR524C"
  ]
  node
  [
    id 1585
    y 0.6493
    x 0.6346
    z 0.5
    label "YIL034C"
  ]
  node
  [
    id 1586
    y 0.5497
    x 0.2679
    z 0.5
    label "YIL177C"
  ]
  node
  [
    id 1587
    y 0.4926
    x 0.7841
    z 0.5
    label "YDR246W"
  ]
  node
  [
    id 1588
    y 0.7459
    x 0.6402
    z 0.5
    label "YPL253C"
  ]
  node
  [
    id 1589
    y 0.4076
    x 0.5027
    z 0.5
    label "YJL020C"
  ]
  node
  [
    id 1590
    y 0.3722
    x 0.382
    z 0.5
    label "YKL173W"
  ]
  node
  [
    id 1591
    y 0.3027
    x 0.3805
    z 0.5
    label "YMR282C"
  ]
  node
  [
    id 1592
    y 0.5177
    x 0.5282
    z 0.5
    label "YAR073W"
  ]
  node
  [
    id 1593
    y 0.6456
    x 0.6669
    z 0.5
    label "YGR233C"
  ]
  node
  [
    id 1594
    y 0.6272
    x 0.4863
    z 0.5
    label "YOR204W"
  ]
  node
  [
    id 1595
    y 0.4934
    x 0.5282
    z 0.5
    label "YDL175C"
  ]
  node
  [
    id 1596
    y 0.632
    x 0.4619
    z 0.5
    label "YKL013C"
  ]
  node
  [
    id 1597
    y 0.7592
    x 0.3697
    z 0.5
    label "YML021C"
  ]
  node
  [
    id 1598
    y 0.336
    x 0.395
    z 0.5
    label "YOR056C"
  ]
  node
  [
    id 1599
    y 0.5658
    x 0.2725
    z 0.5
    label "YLR332W"
  ]
  node
  [
    id 1600
    y 0.37
    x 0.343
    z 0.5
    label "YKL161C"
  ]
  node
  [
    id 1601
    y 0.628
    x 0.1104
    z 0.5
    label "YDR020C"
  ]
  node
  [
    id 1602
    y 0.6371
    x 0.1901
    z 0.5
    label "YNR012W"
  ]
  node
  [
    id 1603
    y 0.2691
    x 0.4632
    z 0.5
    label "YBL022C"
  ]
  node
  [
    id 1604
    y 0.4991
    x 0.7179
    z 0.5
    label "YMR094W"
  ]
  node
  [
    id 1605
    y 0.5886
    x 0.5002
    z 0.5
    label "YDR229W"
  ]
  node
  [
    id 1606
    y 0.5715
    x 0.8051
    z 0.5
    label "YLR417W"
  ]
  node
  [
    id 1607
    y 0.2647
    x 0.3302
    z 0.5
    label "YGR256W"
  ]
  node
  [
    id 1608
    y 0.2795
    x 0.5216
    z 0.5
    label "YKR060W"
  ]
  node
  [
    id 1609
    y 0.5337
    x 0.3764
    z 0.5
    label "YGL063W"
  ]
  node
  [
    id 1610
    y 0.6465
    x 0.3936
    z 0.5
    label "YFR022W"
  ]
  node
  [
    id 1611
    y 0.4817
    x 0.326
    z 0.5
    label "YOL097C"
  ]
  node
  [
    id 1612
    y 0.5062
    x 0.3858
    z 0.5
    label "YDR071C"
  ]
  node
  [
    id 1613
    y 0.5928
    x 0.6487
    z 0.5
    label "YKL117W"
  ]
  node
  [
    id 1614
    y 0.6966
    x 0.6572
    z 0.5
    label "YNL166C"
  ]
  node
  [
    id 1615
    y 0.474
    x 0.5186
    z 0.5
    label "YGR253C"
  ]
  node
  [
    id 1616
    y 0.3186
    x 0.5019
    z 0.5
    label "YIL129C"
  ]
  node
  [
    id 1617
    y 0.5597
    x 0.3557
    z 0.5
    label "YMR239C"
  ]
  node
  [
    id 1618
    y 0.5327
    x 0.7659
    z 0.5
    label "YMR168C"
  ]
  node
  [
    id 1619
    y 0.775
    x 0.4403
    z 0.5
    label "YGL001C"
  ]
  node
  [
    id 1620
    y 0.8585
    x 0.4672
    z 0.5
    label "YGR060W"
  ]
  node
  [
    id 1621
    y 0.3154
    x 0.7018
    z 0.5
    label "YAR002C-A"
  ]
  node
  [
    id 1622
    y 0.6915
    x 0.5381
    z 0.5
    label "YDR295C"
  ]
  node
  [
    id 1623
    y 0.6681
    x 0.5637
    z 0.5
    label "YNL021W"
  ]
  node
  [
    id 1624
    y 0.2821
    x 0.5332
    z 0.5
    label "YPR144C"
  ]
  node
  [
    id 1625
    y 0.5406
    x 0.3164
    z 0.5
    label "YOR329C"
  ]
  node
  [
    id 1626
    y 0.4281
    x 0.7333
    z 0.5
    label "YDR016C"
  ]
  node
  [
    id 1627
    y 0.4228
    x 0.7823
    z 0.5
    label "YHL031C"
  ]
  node
  [
    id 1628
    y 0.307
    x 0.4242
    z 0.5
    label "YJL140W"
  ]
  node
  [
    id 1629
    y 0.3268
    x 0.6908
    z 0.5
    label "YNL258C"
  ]
  node
  [
    id 1630
    y 0.2385
    x 0.7069
    z 0.5
    label "YGL145W"
  ]
  node
  [
    id 1631
    y 0.6912
    x 0.4021
    z 0.5
    label "YGL105W"
  ]
  node
  [
    id 1632
    y 0.5164
    x 0.5169
    z 0.5
    label "YIL118W"
  ]
  node
  [
    id 1633
    y 0.6656
    x 0.4333
    z 0.5
    label "YMR167W"
  ]
  node
  [
    id 1634
    y 0.7705
    x 0.4053
    z 0.5
    label "YOL043C"
  ]
  node
  [
    id 1635
    y 0.3275
    x 0.4241
    z 0.5
    label "YPR187W"
  ]
  node
  [
    id 1636
    y 0.2822
    x 0.3595
    z 0.5
    label "YNL224C"
  ]
  node
  [
    id 1637
    y 0.4983
    x 0.4033
    z 0.5
    label "YLR055C"
  ]
  node
  [
    id 1638
    y 0.7317
    x 0.5825
    z 0.5
    label "YKR105C"
  ]
  node
  [
    id 1639
    y 0.297
    x 0.3509
    z 0.5
    label "YHR019C"
  ]
  node
  [
    id 1640
    y 0.0956
    x 0.5755
    z 0.5
    label "YJL024C"
  ]
  node
  [
    id 1641
    y 0.8745
    x 0.5406
    z 0.5
    label "YER147C"
  ]
  node
  [
    id 1642
    y 0.7968
    x 0.4978
    z 0.5
    label "YDR180W"
  ]
  node
  [
    id 1643
    y 0.3383
    x 0.6786
    z 0.5
    label "YDR021W"
  ]
  node
  [
    id 1644
    y 0.2513
    x 0.6852
    z 0.5
    label "YML067C"
  ]
  node
  [
    id 1645
    y 0.4697
    x 0.6869
    z 0.5
    label "YBL049W"
  ]
  node
  [
    id 1646
    y 0.7762
    x 0.5917
    z 0.5
    label "YCR091W"
  ]
  node
  [
    id 1647
    y 0.5101
    x 0.3839
    z 0.5
    label "YOR176W"
  ]
  node
  [
    id 1648
    y 0.3379
    x 0.4809
    z 0.5
    label "YNL039W"
  ]
  node
  [
    id 1649
    y 0.5665
    x 0.8
    z 0.5
    label "YDL064W"
  ]
  node
  [
    id 1650
    y 0.4185
    x 0.7028
    z 0.5
    label "YOR173W"
  ]
  node
  [
    id 1651
    y 0.4977
    x 0.5073
    z 0.5
    label "YPR025C"
  ]
  node
  [
    id 1652
    y 0.625
    x 0.332
    z 0.5
    label "YPL164C"
  ]
  node
  [
    id 1653
    y 0.6927
    x 0.7977
    z 0.5
    label "YGL185C"
  ]
  node
  [
    id 1654
    y 0.5121
    x 0.3499
    z 0.5
    label "YFR013W"
  ]
  node
  [
    id 1655
    y 0.6721
    x 0.6073
    z 0.5
    label "YLR081W"
  ]
  node
  [
    id 1656
    y 0.6219
    x 0.3229
    z 0.5
    label "YMR291W"
  ]
  node
  [
    id 1657
    y 0.613
    x 0.4249
    z 0.5
    label "YNL027W"
  ]
  node
  [
    id 1658
    y 0.3627
    x 0.3121
    z 0.5
    label "YPR029C"
  ]
  node
  [
    id 1659
    y 0.5806
    x 0.7794
    z 0.5
    label "YMR004W"
  ]
  node
  [
    id 1660
    y 0.3226
    x 0.4716
    z 0.5
    label "YKR081C"
  ]
  node
  [
    id 1661
    y 0.4465
    x 0.6403
    z 0.5
    label "YDR194C"
  ]
  node
  [
    id 1662
    y 0.4992
    x 0.6901
    z 0.5
    label "YNL049C"
  ]
  node
  [
    id 1663
    y 0.2708
    x 0.4945
    z 0.5
    label "YKL172W"
  ]
  node
  [
    id 1664
    y 0.802
    x 0.6892
    z 0.5
    label "YLR123C"
  ]
  node
  [
    id 1665
    y 0.3765
    x 0.7129
    z 0.5
    label "YDL190C"
  ]
  node
  [
    id 1666
    y 0.5606
    x 0.6719
    z 0.5
    label "YGL208W"
  ]
  node
  [
    id 1667
    y 0.1818
    x 0.8157
    z 0.5
    label "YJR119C"
  ]
  node
  [
    id 1668
    y 0.5334
    x 0.7421
    z 0.5
    label "YMR149W"
  ]
  node
  [
    id 1669
    y 0.6301
    x 0.3582
    z 0.5
    label "YPR035W"
  ]
  node
  [
    id 1670
    y 0.454
    x 0.3423
    z 0.5
    label "YDR247W"
  ]
  node
  [
    id 1671
    y 0.5514
    x 0.5695
    z 0.5
    label "YNL216W"
  ]
  node
  [
    id 1672
    y 0.7356
    x 0.5707
    z 0.5
    label "YLR092W"
  ]
  node
  [
    id 1673
    y 0.4027
    x 0.6341
    z 0.5
    label "YER143W"
  ]
  node
  [
    id 1674
    y 0.8678
    x 0.4014
    z 0.5
    label "YGL075C"
  ]
  node
  [
    id 1675
    y 0.7162
    x 0.5665
    z 0.5
    label "YPL075W"
  ]
  node
  [
    id 1676
    y 0.1136
    x 0.574
    z 0.5
    label "YJR101W"
  ]
  node
  [
    id 1677
    y 0.0455
    x 0.6432
    z 0.5
    label "YHL004W"
  ]
  node
  [
    id 1678
    y 0.2488
    x 0.5311
    z 0.5
    label "YDR091C"
  ]
  node
  [
    id 1679
    y 0.6675
    x 0.5921
    z 0.5
    label "YAL040C"
  ]
  node
  [
    id 1680
    y 0.3367
    x 0.5237
    z 0.5
    label "YBL075C"
  ]
  node
  [
    id 1681
    y 0.529
    x 0.7348
    z 0.5
    label "YPR121W"
  ]
  node
  [
    id 1682
    y 0.4798
    x 0.5168
    z 0.5
    label "YPR036W"
  ]
  node
  [
    id 1683
    y 0.3163
    x 0.6267
    z 0.5
    label "YOR174W"
  ]
  node
  [
    id 1684
    y 0.3452
    x 0.6182
    z 0.5
    label "YDR308C"
  ]
  node
  [
    id 1685
    y 0.4569
    x 0.879
    z 0.5
    label "YPL192C"
  ]
  node
  [
    id 1686
    y 0.7711
    x 0.7263
    z 0.5
    label "YER070W"
  ]
  node
  [
    id 1687
    y 0.5257
    x 0.632
    z 0.5
    label "YGL170C"
  ]
  node
  [
    id 1688
    y 0.5422
    x 0.2426
    z 0.5
    label "YHR073W"
  ]
  node
  [
    id 1689
    y 0.4639
    x 0.7815
    z 0.5
    label "YDR407C"
  ]
  node
  [
    id 1690
    y 0.4483
    x 0.3491
    z 0.5
    label "YOR076C"
  ]
  node
  [
    id 1691
    y 0.5794
    x 0.3804
    z 0.5
    label "YDR440W"
  ]
  node
  [
    id 1692
    y 0.5992
    x 0.6747
    z 0.5
    label "YDR211W"
  ]
  node
  [
    id 1693
    y 0.508
    x 0.783
    z 0.5
    label "YML077W"
  ]
  node
  [
    id 1694
    y 0.3511
    x 0.6413
    z 0.5
    label "YNL230C"
  ]
  node
  [
    id 1695
    y 0.7194
    x 0.4486
    z 0.5
    label "YKL042W"
  ]
  node
  [
    id 1696
    y 0.608
    x 0.6185
    z 0.5
    label "YOR177C"
  ]
  node
  [
    id 1697
    y 0.3058
    x 0.4855
    z 0.5
    label "YBR173C"
  ]
  node
  [
    id 1698
    y 0.5939
    x 0.4392
    z 0.5
    label "YJR068W"
  ]
  node
  [
    id 1699
    y 0.4782
    x 0.5449
    z 0.5
    label "YHL034C"
  ]
  node
  [
    id 1700
    y 0.7048
    x 0.425
    z 0.5
    label "YMR078C"
  ]
  node
  [
    id 1701
    y 0.629
    x 0.4051
    z 0.5
    label "YDR121W"
  ]
  node
  [
    id 1702
    y 0.7305
    x 0.2619
    z 0.5
    label "YJR006W"
  ]
  node
  [
    id 1703
    y 0.7143
    x 0.2479
    z 0.5
    label "YDL102W"
  ]
  node
  [
    id 1704
    y 0.51
    x 0.4313
    z 0.5
    label "YGR081C"
  ]
  node
  [
    id 1705
    y 0.2874
    x 0.7344
    z 0.5
    label "YDL081C"
  ]
  node
  [
    id 1706
    y 0.371
    x 0.3726
    z 0.5
    label "YDR037W"
  ]
  node
  [
    id 1707
    y 0.5965
    x 0.6381
    z 0.5
    label "YLR313C"
  ]
  node
  [
    id 1708
    y 0.2326
    x 0.5667
    z 0.5
    label "YBR267W"
  ]
  node
  [
    id 1709
    y 0.2715
    x 0.4397
    z 0.5
    label "YFL002C"
  ]
  node
  [
    id 1710
    y 0.2724
    x 0.4071
    z 0.5
    label "YDL040C"
  ]
  node
  [
    id 1711
    y 0.1451
    x 0.5141
    z 0.5
    label "YGR204W"
  ]
  node
  [
    id 1712
    y 0.4607
    x 0.605
    z 0.5
    label "YOL148C"
  ]
  node
  [
    id 1713
    y 0.7185
    x 0.4658
    z 0.5
    label "YLR270W"
  ]
  node
  [
    id 1714
    y 0.7289
    x 0.5793
    z 0.5
    label "YDR320C"
  ]
  node
  [
    id 1715
    y 0.5379
    x 0.2968
    z 0.5
    label "YLR163C"
  ]
  node
  [
    id 1716
    y 0.2992
    x 0.3878
    z 0.5
    label "YLR221C"
  ]
  node
  [
    id 1717
    y 0.1133
    x 0.4535
    z 0.5
    label "YDR347W"
  ]
  node
  [
    id 1718
    y 0.2136
    x 0.4033
    z 0.5
    label "YBR251W"
  ]
  node
  [
    id 1719
    y 0.5969
    x 0.6292
    z 0.5
    label "YCL027W"
  ]
  node
  [
    id 1720
    y 0.3415
    x 0.5802
    z 0.5
    label "YPL096W"
  ]
  node
  [
    id 1721
    y 0.6014
    x 0.4115
    z 0.5
    label "YBR037C"
  ]
  node
  [
    id 1722
    y 0.6196
    x 0.544
    z 0.5
    label "YNL278W"
  ]
  node
  [
    id 1723
    y 0.4794
    x 0.5054
    z 0.5
    label "YMR235C"
  ]
  node
  [
    id 1724
    y 0.466
    x 0.5529
    z 0.5
    label "YER127W"
  ]
  node
  [
    id 1725
    y 0.4271
    x 0.6419
    z 0.5
    label "YOR178C"
  ]
  node
  [
    id 1726
    y 0.5337
    x 0.3764
    z 0.5
    label "YFR014C"
  ]
  node
  [
    id 1727
    y 0.2771
    x 0.6376
    z 0.5
    label "YNR007C"
  ]
  node
  [
    id 1728
    y 0.6372
    x 0.3392
    z 0.5
    label "YMR193W"
  ]
  node
  [
    id 1729
    y 0.4817
    x 0.5683
    z 0.5
    label "YDR392W"
  ]
  node
  [
    id 1730
    y 0.7706
    x 0.3157
    z 0.5
    label "YLR121C"
  ]
  node
  [
    id 1731
    y 0.7132
    x 0.548
    z 0.5
    label "YOR367W"
  ]
  node
  [
    id 1732
    y 0.3726
    x 0.8669
    z 0.5
    label "YNL272C"
  ]
  node
  [
    id 1733
    y 0.3985
    x 0.804
    z 0.5
    label "YLR102C"
  ]
  node
  [
    id 1734
    y 0.3085
    x 0.3897
    z 0.5
    label "YDR073W"
  ]
  node
  [
    id 1735
    y 0.5924
    x 0.179
    z 0.5
    label "YER098W"
  ]
  node
  [
    id 1736
    y 0.5669
    x 0.2557
    z 0.5
    label "YDR267C"
  ]
  node
  [
    id 1737
    y 0.6601
    x 0.4865
    z 0.5
    label "YPR159W"
  ]
  node
  [
    id 1738
    y 0.6634
    x 0.679
    z 0.5
    label "YKL127W"
  ]
  node
  [
    id 1739
    y 0.6695
    x 0.5597
    z 0.5
    label "YPL014W"
  ]
  node
  [
    id 1740
    y 0.2752
    x 0.5055
    z 0.5
    label "YKL099C"
  ]
  node
  [
    id 1741
    y 0.5439
    x 0.6637
    z 0.5
    label "YDL178W"
  ]
  node
  [
    id 1742
    y 0.6051
    x 0.6183
    z 0.5
    label "YHR158C"
  ]
  node
  [
    id 1743
    y 0.5644
    x 0.6587
    z 0.5
    label "YCR033W"
  ]
  node
  [
    id 1744
    y 0.2837
    x 0.643
    z 0.5
    label "YOR075W"
  ]
  node
  [
    id 1745
    y 0.7494
    x 0.6905
    z 0.5
    label "YHR044C"
  ]
  node
  [
    id 1746
    y 0.651
    x 0.5827
    z 0.5
    label "YBR260C"
  ]
  node
  [
    id 1747
    y 0.4122
    x 0.8376
    z 0.5
    label "YDR201W"
  ]
  node
  [
    id 1748
    y 0.3936
    x 0.718
    z 0.5
    label "YKR037C"
  ]
  node
  [
    id 1749
    y 0.3629
    x 0.2845
    z 0.5
    label "YLL015W"
  ]
  node
  [
    id 1750
    y 0.1551
    x 0.2999
    z 0.5
    label "YLR327C"
  ]
  node
  [
    id 1751
    y 0.5912
    x 0.6813
    z 0.5
    label "YGR020C"
  ]
  node
  [
    id 1752
    y 0.3239
    x 0.3186
    z 0.5
    label "YIL125W"
  ]
  node
  [
    id 1753
    y 0.6677
    x 0.7568
    z 0.5
    label "YHL032C"
  ]
  node
  [
    id 1754
    y 0.4872
    x 0.5201
    z 0.5
    label "YIL124W"
  ]
  node
  [
    id 1755
    y 0.5
    x 0.5298
    z 0.5
    label "YLR398C"
  ]
  node
  [
    id 1756
    y 0.3322
    x 0.3322
    z 0.5
    label "YHR035W"
  ]
  node
  [
    id 1757
    y 0.6675
    x 0.5856
    z 0.5
    label "YEL043W"
  ]
  node
  [
    id 1758
    y 0.4285
    x 0.657
    z 0.5
    label "YJR007W"
  ]
  node
  [
    id 1759
    y 0.5337
    x 0.3764
    z 0.5
    label "YFL010W-A"
  ]
  node
  [
    id 1760
    y 0.4967
    x 0.7698
    z 0.5
    label "YNL090W"
  ]
  node
  [
    id 1761
    y 0.5121
    x 0.6802
    z 0.5
    label "YMR241W"
  ]
  node
  [
    id 1762
    y 0.6275
    x 0.6197
    z 0.5
    label "YPL083C"
  ]
  node
  [
    id 1763
    y 0.562
    x 0.7838
    z 0.5
    label "YLR224W"
  ]
  node
  [
    id 1764
    y 0.2641
    x 0.6143
    z 0.5
    label "YBR073W"
  ]
  node
  [
    id 1765
    y 0.4053
    x 0.4827
    z 0.5
    label "YBR211C"
  ]
  node
  [
    id 1766
    y 0.4799
    x 0.3868
    z 0.5
    label "YGR170W"
  ]
  node
  [
    id 1767
    y 0.594
    x 0.423
    z 0.5
    label "YKL045W"
  ]
  node
  [
    id 1768
    y 0.6166
    x 0.7555
    z 0.5
    label "YDL240W"
  ]
  node
  [
    id 1769
    y 0.6067
    x 0.3932
    z 0.5
    label "YBR050C"
  ]
  node
  [
    id 1770
    y 0.5974
    x 0.7957
    z 0.5
    label "YLR426W"
  ]
  node
  [
    id 1771
    y 0.1915
    x 0.5915
    z 0.5
    label "YPL045W"
  ]
  node
  [
    id 1772
    y 0.5702
    x 0.4283
    z 0.5
    label "YHL009C"
  ]
  node
  [
    id 1773
    y 0.3488
    x 0.3209
    z 0.5
    label "YGL096W"
  ]
  node
  [
    id 1774
    y 0.2773
    x 0.3599
    z 0.5
    label "YHR121W"
  ]
  node
  [
    id 1775
    y 0.2129
    x 0.592
    z 0.5
    label "YOR340C"
  ]
  node
  [
    id 1776
    y 0.8011
    x 0.5842
    z 0.5
    label "YMR102C"
  ]
  node
  [
    id 1777
    y 0.5594
    x 0.3843
    z 0.5
    label "YJL168C"
  ]
  node
  [
    id 1778
    y 0.4796
    x 0.8012
    z 0.5
    label "YDL186W"
  ]
  node
  [
    id 1779
    y 0.5398
    x 0.2653
    z 0.5
    label "YPR152C"
  ]
  node
  [
    id 1780
    y 0.5526
    x 0.6542
    z 0.5
    label "YLR113W"
  ]
  node
  [
    id 1781
    y 0.4887
    x 0.5295
    z 0.5
    label "YER161C"
  ]
  node
  [
    id 1782
    y 0.3769
    x 0.6007
    z 0.5
    label "YMR146C"
  ]
  node
  [
    id 1783
    y 0.5792
    x 0.3242
    z 0.5
    label "YAL014C"
  ]
  node
  [
    id 1784
    y 0.3759
    x 0.2789
    z 0.5
    label "YNR050C"
  ]
  node
  [
    id 1785
    y 0.2887
    x 0.3611
    z 0.5
    label "YDL124W"
  ]
  node
  [
    id 1786
    y 0.449
    x 0.6308
    z 0.5
    label "YMR303C"
  ]
  node
  [
    id 1787
    y 0.5552
    x 0.7053
    z 0.5
    label "YER115C"
  ]
  node
  [
    id 1788
    y 0.74
    x 0.6268
    z 0.5
    label "YDL028C"
  ]
  node
  [
    id 1789
    y 0.6187
    x 0.4212
    z 0.5
    label "YDR362C"
  ]
  node
  [
    id 1790
    y 0.6315
    x 0.3953
    z 0.5
    label "YPL088W"
  ]
  node
  [
    id 1791
    y 0.7044
    x 0.5273
    z 0.5
    label "YJL100W"
  ]
  node
  [
    id 1792
    y 0.4083
    x 0.4476
    z 0.5
    label "YOL041C"
  ]
  node
  [
    id 1793
    y 0.6068
    x 0.4102
    z 0.5
    label "YMR226C"
  ]
  node
  [
    id 1794
    y 0.5442
    x 0.6992
    z 0.5
    label "YGR178C"
  ]
  node
  [
    id 1795
    y 0.4669
    x 0.4266
    z 0.5
    label "YKL085W"
  ]
  node
  [
    id 1796
    y 0.5337
    x 0.3764
    z 0.5
    label "YKL175W"
  ]
  node
  [
    id 1797
    y 0.4977
    x 0.4811
    z 0.5
    label "YBR136W"
  ]
  node
  [
    id 1798
    y 0.5087
    x 0.2585
    z 0.5
    label "YJL180C"
  ]
  node
  [
    id 1799
    y 0.4295
    x 0.7771
    z 0.5
    label "YIR033W"
  ]
  node
  [
    id 1800
    y 0.4884
    x 0.2839
    z 0.5
    label "YMR315W"
  ]
  node
  [
    id 1801
    y 0.6451
    x 0.4034
    z 0.5
    label "YLR394W"
  ]
  node
  [
    id 1802
    y 0.7052
    x 0.4591
    z 0.5
    label "YMR124W"
  ]
  node
  [
    id 1803
    y 0.5255
    x 0.4274
    z 0.5
    label "YBL088C"
  ]
  node
  [
    id 1804
    y 0.6572
    x 0.5252
    z 0.5
    label "YFL029C"
  ]
  node
  [
    id 1805
    y 0.7067
    x 0.4144
    z 0.5
    label "YJL137C"
  ]
  node
  [
    id 1806
    y 0.6057
    x 0.4697
    z 0.5
    label "YFR015C"
  ]
  node
  [
    id 1807
    y 0.3985
    x 0.6692
    z 0.5
    label "YMR154C"
  ]
  node
  [
    id 1808
    y 0.2895
    x 0.836
    z 0.5
    label "YDL113C"
  ]
  node
  [
    id 1809
    y 0.5302
    x 0.3566
    z 0.5
    label "YDR292C"
  ]
  node
  [
    id 1810
    y 0.2526
    x 0.7501
    z 0.5
    label "YDR164C"
  ]
  node
  [
    id 1811
    y 0.405
    x 0.2865
    z 0.5
    label "YGR075C"
  ]
  node
  [
    id 1812
    y 0.6578
    x 0.6963
    z 0.5
    label "YER099C"
  ]
  node
  [
    id 1813
    y 0.6773
    x 0.6786
    z 0.5
    label "YOL061W"
  ]
  node
  [
    id 1814
    y 0.2002
    x 0.4172
    z 0.5
    label "YGR193C"
  ]
  node
  [
    id 1815
    y 0.6523
    x 0.4387
    z 0.5
    label "YPR179C"
  ]
  node
  [
    id 1816
    y 0.7348
    x 0.3283
    z 0.5
    label "YGL249W"
  ]
  node
  [
    id 1817
    y 0.2612
    x 0.595
    z 0.5
    label "YDR179C"
  ]
  node
  [
    id 1818
    y 0.7671
    x 0.6499
    z 0.5
    label "YOR363C"
  ]
  node
  [
    id 1819
    y 0.2775
    x 0.4968
    z 0.5
    label "YIL128W"
  ]
  node
  [
    id 1820
    y 0.4516
    x 0.839
    z 0.5
    label "YIR017C"
  ]
  node
  [
    id 1821
    y 0.2676
    x 0.4722
    z 0.5
    label "YCL030C"
  ]
  node
  [
    id 1822
    y 0.3775
    x 0.2324
    z 0.5
    label "YIR005W"
  ]
  node
  [
    id 1823
    y 0.5395
    x 0.7263
    z 0.5
    label "YEL002C"
  ]
  node
  [
    id 1824
    y 0.5906
    x 0.2795
    z 0.5
    label "YHR081W"
  ]
  node
  [
    id 1825
    y 0.3201
    x 0.2936
    z 0.5
    label "YJR084W"
  ]
  node
  [
    id 1826
    y 0.2759
    x 0.5126
    z 0.5
    label "YNL151C"
  ]
  node
  [
    id 1827
    y 0.6196
    x 0.544
    z 0.5
    label "YDR501W"
  ]
  node
  [
    id 1828
    y 0.1354
    x 0.4437
    z 0.5
    label "YPL233W"
  ]
  node
  [
    id 1829
    y 0.2099
    x 0.4754
    z 0.5
    label "YBR069C"
  ]
  node
  [
    id 1830
    y 0.4849
    x 0.529
    z 0.5
    label "YGR258C"
  ]
  node
  [
    id 1831
    y 0.273
    x 0.7761
    z 0.5
    label "YCR082W"
  ]
  node
  [
    id 1832
    y 0.3034
    x 0.6712
    z 0.5
    label "YOR023C"
  ]
  node
  [
    id 1833
    y 0.6603
    x 0.5317
    z 0.5
    label "YDR285W"
  ]
  node
  [
    id 1834
    y 0.7093
    x 0.4272
    z 0.5
    label "YKR099W"
  ]
  node
  [
    id 1835
    y 0.5728
    x 0.3561
    z 0.5
    label "YMR053C"
  ]
  node
  [
    id 1836
    y 0.4641
    x 0.7985
    z 0.5
    label "YML019W"
  ]
  node
  [
    id 1837
    y 0.6397
    x 0.4037
    z 0.5
    label "YGR024C"
  ]
  node
  [
    id 1838
    y 0.6564
    x 0.4883
    z 0.5
    label "YIL149C"
  ]
  node
  [
    id 1839
    y 0.4473
    x 0.8428
    z 0.5
    label "YPL038W"
  ]
  node
  [
    id 1840
    y 0.3873
    x 0.6949
    z 0.5
    label "YML012W"
  ]
  node
  [
    id 1841
    y 0.3066
    x 0.63
    z 0.5
    label "YNL029C"
  ]
  node
  [
    id 1842
    y 0.7636
    x 0.5462
    z 0.5
    label "YNL267W"
  ]
  node
  [
    id 1843
    y 0.2335
    x 0.319
    z 0.5
    label "YBR094W"
  ]
  node
  [
    id 1844
    y 0.2134
    x 0.3983
    z 0.5
    label "YBR208C"
  ]
  node
  [
    id 1845
    y 0.3806
    x 0.7467
    z 0.5
    label "YDR100W"
  ]
  node
  [
    id 1846
    y 0.4133
    x 0.7798
    z 0.5
    label "YLR289W"
  ]
  node
  [
    id 1847
    y 0.6855
    x 0.1968
    z 0.5
    label "YIL065C"
  ]
  node
  [
    id 1848
    y 0.6674
    x 0.5417
    z 0.5
    label "YKL108W"
  ]
  node
  [
    id 1849
    y 0.776
    x 0.5772
    z 0.5
    label "YJL090C"
  ]
  node
  [
    id 1850
    y 0.6809
    x 0.5
    z 0.5
    label "YFR024C-A"
  ]
  node
  [
    id 1851
    y 0.7018
    x 0.3217
    z 0.5
    label "YOR217W"
  ]
  node
  [
    id 1852
    y 0.4325
    x 0.3167
    z 0.5
    label "YHR079C"
  ]
  node
  [
    id 1853
    y 0.4557
    x 0.2563
    z 0.5
    label "YAL060W"
  ]
  node
  [
    id 1854
    y 0.5027
    x 0.1818
    z 0.5
    label "YOR302W"
  ]
  node
  [
    id 1855
    y 0.5282
    x 0.5145
    z 0.5
    label "YGL106W"
  ]
  node
  [
    id 1856
    y 0.7036
    x 0.517
    z 0.5
    label "YHR133C"
  ]
  node
  [
    id 1857
    y 0.5322
    x 0.5308
    z 0.5
    label "YMR267W"
  ]
  node
  [
    id 1858
    y 0.455
    x 0.3982
    z 0.5
    label "YLR239C"
  ]
  node
  [
    id 1859
    y 0.4746
    x 0.4457
    z 0.5
    label "YKL112W"
  ]
  node
  [
    id 1860
    y 0.6858
    x 0.3888
    z 0.5
    label "YJL026W"
  ]
  node
  [
    id 1861
    y 0.2033
    x 0.4837
    z 0.5
    label "YGL251C"
  ]
  node
  [
    id 1862
    y 0.4337
    x 0.2731
    z 0.5
    label "YGR049W"
  ]
  node
  [
    id 1863
    y 0.4427
    x 0.665
    z 0.5
    label "YLR234W"
  ]
  node
  [
    id 1864
    y 0.5723
    x 0.7261
    z 0.5
    label "YHR090C"
  ]
  node
  [
    id 1865
    y 0.657
    x 0.5098
    z 0.5
    label "YLR183C"
  ]
  node
  [
    id 1866
    y 0.5067
    x 0.2737
    z 0.5
    label "YML100W"
  ]
  node
  [
    id 1867
    y 0.3588
    x 0.3142
    z 0.5
    label "YKR104W"
  ]
  node
  [
    id 1868
    y 0.6049
    x 0.4331
    z 0.5
    label "YIL062C"
  ]
  node
  [
    id 1869
    y 0.3087
    x 0.3765
    z 0.5
    label "YLL022C"
  ]
  node
  [
    id 1870
    y 0.6473
    x 0.5688
    z 0.5
    label "YGR136W"
  ]
  node
  [
    id 1871
    y 0.4841
    x 0.5284
    z 0.5
    label "YIL108W"
  ]
  node
  [
    id 1872
    y 0.4894
    x 0.5303
    z 0.5
    label "YGL244W"
  ]
  node
  [
    id 1873
    y 0.2501
    x 0.5914
    z 0.5
    label "YPL026C"
  ]
  node
  [
    id 1874
    y 0.4239
    x 0.3675
    z 0.5
    label "YBL046W"
  ]
  node
  [
    id 1875
    y 0.5417
    x 0.8629
    z 0.5
    label "YKL089W"
  ]
  node
  [
    id 1876
    y 0.6455
    x 0.5828
    z 0.5
    label "YMR092C"
  ]
  node
  [
    id 1877
    y 0.3319
    x 0.5977
    z 0.5
    label "YDR365C"
  ]
  node
  [
    id 1878
    y 0.5717
    x 0.5445
    z 0.5
    label "YEL030W"
  ]
  node
  [
    id 1879
    y 0.3752
    x 0.2796
    z 0.5
    label "YDR135C"
  ]
  node
  [
    id 1880
    y 0.4228
    x 0.2798
    z 0.5
    label "YGR072W"
  ]
  node
  [
    id 1881
    y 0.5563
    x 0.8102
    z 0.5
    label "YIR035C"
  ]
  node
  [
    id 1882
    y 0.1875
    x 0.9584
    z 0.5
    label "YLR383W"
  ]
  node
  [
    id 1883
    y 0.5876
    x 0.2826
    z 0.5
    label "YPL243W"
  ]
  node
  [
    id 1884
    y 0.5151
    x 0.6634
    z 0.5
    label "YPL089C"
  ]
  node
  [
    id 1885
    y 0.2609
    x 0.502
    z 0.5
    label "YHR148W"
  ]
  node
  [
    id 1886
    y 0.4374
    x 0.4
    z 0.5
    label "YJR138W"
  ]
  node
  [
    id 1887
    y 0.5454
    x 0.592
    z 0.5
    label "YMR294W"
  ]
  node
  [
    id 1888
    y 0.537
    x 0.3954
    z 0.5
    label "YDL171C"
  ]
  node
  [
    id 1889
    y 0.4495
    x 0.2058
    z 0.5
    label "YDR237W"
  ]
  node
  [
    id 1890
    y 0.3296
    x 0.5668
    z 0.5
    label "YHR169W"
  ]
  node
  [
    id 1891
    y 0.0951
    x 0.4302
    z 0.5
    label "YPR102C"
  ]
  node
  [
    id 1892
    y 0.1647
    x 0.4626
    z 0.5
    label "YOR294W"
  ]
  node
  [
    id 1893
    y 0.3374
    x 0.2922
    z 0.5
    label "YDR213W"
  ]
  node
  [
    id 1894
    y 0.4272
    x 0.416
    z 0.5
    label "YER025W"
  ]
  node
  [
    id 1895
    y 0.4244
    x 0.3214
    z 0.5
    label "YDL004W"
  ]
  node
  [
    id 1896
    y 0.4193
    x 0.6723
    z 0.5
    label "YDR168W"
  ]
  node
  [
    id 1897
    y 0.4537
    x 0.4983
    z 0.5
    label "YOL017W"
  ]
  node
  [
    id 1898
    y 0.3241
    x 0.7986
    z 0.5
    label "YDR049W"
  ]
  node
  [
    id 1899
    y 0.5337
    x 0.3764
    z 0.5
    label "YNL202W"
  ]
  node
  [
    id 1900
    y 0.6506
    x 0.7029
    z 0.5
    label "YLR243W"
  ]
  node
  [
    id 1901
    y 0.2013
    x 0.3735
    z 0.5
    label "YCR063W"
  ]
  node
  [
    id 1902
    y 0.3147
    x 0.7823
    z 0.5
    label "YKL213C"
  ]
  node
  [
    id 1903
    y 0.6294
    x 0.2654
    z 0.5
    label "YIL070C"
  ]
  node
  [
    id 1904
    y 0.7495
    x 0.6374
    z 0.5
    label "YDL224C"
  ]
  node
  [
    id 1905
    y 0.6329
    x 0.611
    z 0.5
    label "YER059W"
  ]
  node
  [
    id 1906
    y 0.5479
    x 0.7875
    z 0.5
    label "YJL204C"
  ]
  node
  [
    id 1907
    y 0.3095
    x 0.6032
    z 0.5
    label "YHR197W"
  ]
  node
  [
    id 1908
    y 0.4668
    x 0.2356
    z 0.5
    label "YLR321C"
  ]
  node
  [
    id 1909
    y 0.6823
    x 0.5452
    z 0.5
    label "YLL050C"
  ]
  node
  [
    id 1910
    y 0.6099
    x 0.3524
    z 0.5
    label "YLR422W"
  ]
  node
  [
    id 1911
    y 0.4847
    x 0.5283
    z 0.5
    label "YMR284W"
  ]
  node
  [
    id 1912
    y 0.4765
    x 0.5164
    z 0.5
    label "YLR256W"
  ]
  node
  [
    id 1913
    y 0.6741
    x 0.5564
    z 0.5
    label "YLR274W"
  ]
  node
  [
    id 1914
    y 0.2496
    x 0.7709
    z 0.5
    label "YGR132C"
  ]
  node
  [
    id 1915
    y 0.1764
    x 0.5834
    z 0.5
    label "YLR396C"
  ]
  node
  [
    id 1916
    y 0.4006
    x 0.282
    z 0.5
    label "YDL078C"
  ]
  node
  [
    id 1917
    y 0.4634
    x 0.5865
    z 0.5
    label "YPL259C"
  ]
  node
  [
    id 1918
    y 0.7123
    x 0.4286
    z 0.5
    label "YHR172W"
  ]
  node
  [
    id 1919
    y 0.3772
    x 0.665
    z 0.5
    label "YGL066W"
  ]
  node
  [
    id 1920
    y 0.6622
    x 0.3906
    z 0.5
    label "YOR141C"
  ]
  node
  [
    id 1921
    y 0.1772
    x 0.449
    z 0.5
    label "YML049C"
  ]
  node
  [
    id 1922
    y 0.3904
    x 0.3388
    z 0.5
    label "YDL181W"
  ]
  node
  [
    id 1923
    y 0.4799
    x 0.3868
    z 0.5
    label "YEL046C"
  ]
  node
  [
    id 1924
    y 0.3362
    x 0.7767
    z 0.5
    label "YOR327C"
  ]
  node
  [
    id 1925
    y 0.5626
    x 0.3103
    z 0.5
    label "YDR169C"
  ]
  node
  [
    id 1926
    y 0.2428
    x 0.6708
    z 0.5
    label "YAL042W"
  ]
  node
  [
    id 1927
    y 0.7102
    x 0.5325
    z 0.5
    label "YLR037C"
  ]
  node
  [
    id 1928
    y 0.191
    x 0.5141
    z 0.5
    label "YDR156W"
  ]
  node
  [
    id 1929
    y 0.5288
    x 0.3028
    z 0.5
    label "YNL234W"
  ]
  node
  [
    id 1930
    y 0.3081
    x 0.4478
    z 0.5
    label "YCL052C"
  ]
  node
  [
    id 1931
    y 0.5798
    x 0.7658
    z 0.5
    label "YCL066W"
  ]
  node
  [
    id 1932
    y 0.6257
    x 0.747
    z 0.5
    label "YDR398W"
  ]
  node
  [
    id 1933
    y 0.695
    x 0.619
    z 0.5
    label "YER149C"
  ]
  node
  [
    id 1934
    y 0.4799
    x 0.3868
    z 0.5
    label "YMR176W"
  ]
  node
  [
    id 1935
    y 0.7333
    x 0.4215
    z 0.5
    label "YJL151C"
  ]
  node
  [
    id 1936
    y 0.3503
    x 0.6467
    z 0.5
    label "YKR067W"
  ]
  node
  [
    id 1937
    y 0.3542
    x 0.5475
    z 0.5
    label "YDR299W"
  ]
  node
  [
    id 1938
    y 0.5178
    x 0.5677
    z 0.5
    label "YJR009C"
  ]
  node
  [
    id 1939
    y 0.367
    x 0.2205
    z 0.5
    label "YKR085C"
  ]
  node
  [
    id 1940
    y 0.3105
    x 0.6529
    z 0.5
    label "YGR097W"
  ]
  node
  [
    id 1941
    y 0.514
    x 0.3034
    z 0.5
    label "YPL066W"
  ]
  node
  [
    id 1942
    y 0.5303
    x 0.503
    z 0.5
    label "YGL252C"
  ]
  node
  [
    id 1943
    y 0.7767
    x 0.368
    z 0.5
    label "YML056C"
  ]
  node
  [
    id 1944
    y 0.557
    x 0.2551
    z 0.5
    label "YHR167W"
  ]
  node
  [
    id 1945
    y 0.3119
    x 0.4126
    z 0.5
    label "YMR219W"
  ]
  node
  [
    id 1946
    y 0.4732
    x 0.6724
    z 0.5
    label "YML099C"
  ]
  node
  [
    id 1947
    y 0.6672
    x 0.6361
    z 0.5
    label "YJR028W"
  ]
  node
  [
    id 1948
    y 0.5174
    x 0.5295
    z 0.5
    label "YOR188W"
  ]
  node
  [
    id 1949
    y 0.2405
    x 0.4824
    z 0.5
    label "YGR102C"
  ]
  node
  [
    id 1950
    y 0.4474
    x 0.8199
    z 0.5
    label "YKL001C"
  ]
  node
  [
    id 1951
    y 0.5484
    x 0.5495
    z 0.5
    label "YER111C"
  ]
  node
  [
    id 1952
    y 0.6549
    x 0.4648
    z 0.5
    label "YIL095W"
  ]
  node
  [
    id 1953
    y 0.4752
    x 0.8458
    z 0.5
    label "YDR253C"
  ]
  node
  [
    id 1954
    y 0.51
    x 0.4313
    z 0.5
    label "YPL020C"
  ]
  node
  [
    id 1955
    y 0.4194
    x 0.3693
    z 0.5
    label "YER054C"
  ]
  node
  [
    id 1956
    y 0.4891
    x 0.2881
    z 0.5
    label "YLR226W"
  ]
  node
  [
    id 1957
    y 0.5465
    x 0.3181
    z 0.5
    label "YMR311C"
  ]
  node
  [
    id 1958
    y 0.4835
    x 0.6199
    z 0.5
    label "YFL024C"
  ]
  node
  [
    id 1959
    y 0.2426
    x 0.505
    z 0.5
    label "YMR300C"
  ]
  node
  [
    id 1960
    y 0.1479
    x 0.5309
    z 0.5
    label "YER052C"
  ]
  node
  [
    id 1961
    y 0.7094
    x 0.4028
    z 0.5
    label "YOR007C"
  ]
  node
  [
    id 1962
    y 0.2424
    x 0.29
    z 0.5
    label "YKR022C"
  ]
  node
  [
    id 1963
    y 0.3454
    x 0.2698
    z 0.5
    label "YKL170W"
  ]
  node
  [
    id 1964
    y 0.7852
    x 0.4734
    z 0.5
    label "YGL162W"
  ]
  node
  [
    id 1965
    y 0.2818
    x 0.616
    z 0.5
    label "YLR228C"
  ]
  node
  [
    id 1966
    y 0.324
    x 0.327
    z 0.5
    label "YLR403W"
  ]
  node
  [
    id 1967
    y 0.2431
    x 0.4965
    z 0.5
    label "YNR043W"
  ]
  node
  [
    id 1968
    y 0.5259
    x 0.5253
    z 0.5
    label "YFL033C"
  ]
  node
  [
    id 1969
    y 0.5444
    x 0.1092
    z 0.5
    label "YGR194C"
  ]
  node
  [
    id 1970
    y 0.5283
    x 0.1806
    z 0.5
    label "YNL265C"
  ]
  node
  [
    id 1971
    y 0.635
    x 0.4029
    z 0.5
    label "YOL091W"
  ]
  node
  [
    id 1972
    y 0.7389
    x 0.5513
    z 0.5
    label "YNL024C"
  ]
  node
  [
    id 1973
    y 0.5653
    x 0.6007
    z 0.5
    label "YLR131C"
  ]
  node
  [
    id 1974
    y 0.4093
    x 0.7295
    z 0.5
    label "YJL179W"
  ]
  node
  [
    id 1975
    y 0.7921
    x 0.3746
    z 0.5
    label "YLL040C"
  ]
  node
  [
    id 1976
    y 0.2587
    x 0.7127
    z 0.5
    label "YLR148W"
  ]
  node
  [
    id 1977
    y 0.6304
    x 0.1943
    z 0.5
    label "YOR132W"
  ]
  node
  [
    id 1978
    y 0.2527
    x 0.4367
    z 0.5
    label "YML030W"
  ]
  node
  [
    id 1979
    y 0.5276
    x 0.1547
    z 0.5
    label "YLR010C"
  ]
  node
  [
    id 1980
    y 0.517
    x 0.1542
    z 0.5
    label "YDR082W"
  ]
  node
  [
    id 1981
    y 0.3427
    x 0.5742
    z 0.5
    label "YFR019W"
  ]
  node
  [
    id 1982
    y 0.2747
    x 0.3796
    z 0.5
    label "YOR120W"
  ]
  node
  [
    id 1983
    y 0.2733
    x 0.5653
    z 0.5
    label "YNL248C"
  ]
  node
  [
    id 1984
    y 0.5713
    x 0.2025
    z 0.5
    label "YHR024C"
  ]
  node
  [
    id 1985
    y 0.5906
    x 0.6461
    z 0.5
    label "YHR098C"
  ]
  node
  [
    id 1986
    y 0.3368
    x 0.5543
    z 0.5
    label "YLR386W"
  ]
  node
  [
    id 1987
    y 0.506
    x 0.4227
    z 0.5
    label "YPL111W"
  ]
  node
  [
    id 1988
    y 0.5293
    x 0.5175
    z 0.5
    label "YHR179W"
  ]
  node
  [
    id 1989
    y 0.7147
    x 0.494
    z 0.5
    label "YBR235W"
  ]
  node
  [
    id 1990
    y 0.67
    x 0.6296
    z 0.5
    label "YPL158C"
  ]
  node
  [
    id 1991
    y 0.6774
    x 0.4067
    z 0.5
    label "YER092W"
  ]
  node
  [
    id 1992
    y 0.657
    x 0.4688
    z 0.5
    label "YGL025C"
  ]
  node
  [
    id 1993
    y 0.4453
    x 0.8093
    z 0.5
    label "YKL006C-A"
  ]
  node
  [
    id 1994
    y 0.41
    x 0.8854
    z 0.5
    label "YGR133W"
  ]
  node
  [
    id 1995
    y 0.301
    x 0.7322
    z 0.5
    label "YGR191W"
  ]
  node
  [
    id 1996
    y 0.701
    x 0.6636
    z 0.5
    label "YMR032W"
  ]
  node
  [
    id 1997
    y 0.373
    x 0.3433
    z 0.5
    label "YAL027W"
  ]
  node
  [
    id 1998
    y 0.3801
    x 0.3042
    z 0.5
    label "YHR034C"
  ]
  node
  [
    id 1999
    y 0.5337
    x 0.3764
    z 0.5
    label "YMR250W"
  ]
  node
  [
    id 2000
    y 0.3407
    x 0.3715
    z 0.5
    label "YOR243C"
  ]
  node
  [
    id 2001
    y 0.5008
    x 0.2847
    z 0.5
    label "YOR323C"
  ]
  node
  [
    id 2002
    y 0.4981
    x 0.5271
    z 0.5
    label "YNL227C"
  ]
  node
  [
    id 2003
    y 0.8423
    x 0.7276
    z 0.5
    label "YBL061C"
  ]
  node
  [
    id 2004
    y 0.7151
    x 0.5283
    z 0.5
    label "YIR003W"
  ]
  node
  [
    id 2005
    y 0.2587
    x 0.5417
    z 0.5
    label "YMR323W"
  ]
  node
  [
    id 2006
    y 0.7242
    x 0.5313
    z 0.5
    label "YKL113C"
  ]
  node
  [
    id 2007
    y 0.3514
    x 0.6719
    z 0.5
    label "YKL067W"
  ]
  node
  [
    id 2008
    y 0.7068
    x 0.4544
    z 0.5
    label "YOR371C"
  ]
  node
  [
    id 2009
    y 0.8158
    x 0.4496
    z 0.5
    label "YGL121C"
  ]
  node
  [
    id 2010
    y 0.5782
    x 0.5739
    z 0.5
    label "YGR296W"
  ]
  node
  [
    id 2011
    y 0.7131
    x 0.5071
    z 0.5
    label "YLL060C"
  ]
  node
  [
    id 2012
    y 0.651
    x 0.4252
    z 0.5
    label "YOR018W"
  ]
  node
  [
    id 2013
    y 0.5702
    x 0.3522
    z 0.5
    label "YMR019W"
  ]
  node
  [
    id 2014
    y 0.6119
    x 0.4704
    z 0.5
    label "YLR436C"
  ]
  node
  [
    id 2015
    y 0.4935
    x 0.5151
    z 0.5
    label "YNR006W"
  ]
  node
  [
    id 2016
    y 0.3321
    x 0.5614
    z 0.5
    label "YBR085W"
  ]
  node
  [
    id 2017
    y 0.452
    x 0.2033
    z 0.5
    label "YGR220C"
  ]
  node
  [
    id 2018
    y 0.1647
    x 0.5472
    z 0.5
    label "YGR052W"
  ]
  node
  [
    id 2019
    y 0.266
    x 0.5008
    z 0.5
    label "YOR008C"
  ]
  node
  [
    id 2020
    y 0.7796
    x 0.5053
    z 0.5
    label "YGR058W"
  ]
  node
  [
    id 2021
    y 0.5114
    x 0.5151
    z 0.5
    label "YBR045C"
  ]
  node
  [
    id 2022
    y 0.1974
    x 0.5633
    z 0.5
    label "YPL177C"
  ]
  node
  [
    id 2023
    y 0.2884
    x 0.6529
    z 0.5
    label "YDR498C"
  ]
  node
  [
    id 2024
    y 0.5788
    x 0.6847
    z 0.5
    label "YNL229C"
  ]
  node
  [
    id 2025
    y 0.6614
    x 0.5753
    z 0.5
    label "YPR120C"
  ]
  node
  [
    id 2026
    y 0.2359
    x 0.6641
    z 0.5
    label "YMR296C"
  ]
  node
  [
    id 2027
    y 0.6921
    x 0.424
    z 0.5
    label "YNL225C"
  ]
  node
  [
    id 2028
    y 0.6686
    x 0.3532
    z 0.5
    label "YIL122W"
  ]
  node
  [
    id 2029
    y 0.6195
    x 0.544
    z 0.5
    label "YOR081C"
  ]
  node
  [
    id 2030
    y 0.5484
    x 0.7811
    z 0.5
    label "YJL057C"
  ]
  node
  [
    id 2031
    y 0.2577
    x 0.8294
    z 0.5
    label "YJL064W"
  ]
  node
  [
    id 2032
    y 0.4157
    x 0.7317
    z 0.5
    label "YER049W"
  ]
  node
  [
    id 2033
    y 0.1688
    x 0.6983
    z 0.5
    label "YAL002W"
  ]
  node
  [
    id 2034
    y 0.8013
    x 0.6068
    z 0.5
    label "YHR195W"
  ]
  node
  [
    id 2035
    y 0.363
    x 0.6953
    z 0.5
    label "YGL223C"
  ]
  node
  [
    id 2036
    y 0.3028
    x 0.8352
    z 0.5
    label "YBL059W"
  ]
  node
  [
    id 2037
    y 0.147
    x 0.5405
    z 0.5
    label "YNL023C"
  ]
  node
  [
    id 2038
    y 0.7983
    x 0.4188
    z 0.5
    label "YIR036C"
  ]
  node
  [
    id 2039
    y 0.4857
    x 0.6579
    z 0.5
    label "YPL203W"
  ]
  node
  [
    id 2040
    y 0.2886
    x 0.5139
    z 0.5
    label "YCR042C"
  ]
  node
  [
    id 2041
    y 0.4941
    x 0.7839
    z 0.5
    label "YDR472W"
  ]
  node
  [
    id 2042
    y 0.5275
    x 0.2761
    z 0.5
    label "YDR074W"
  ]
  node
  [
    id 2043
    y 0.655
    x 0.3495
    z 0.5
    label "YCL046W"
  ]
  node
  [
    id 2044
    y 0.6196
    x 0.5441
    z 0.5
    label "YKL048C"
  ]
  node
  [
    id 2045
    y 0.6196
    x 0.544
    z 0.5
    label "YPL155C"
  ]
  node
  [
    id 2046
    y 0.4736
    x 0.5178
    z 0.5
    label "YNL167C"
  ]
  node
  [
    id 2047
    y 0.2789
    x 0.6609
    z 0.5
    label "YGL078C"
  ]
  node
  [
    id 2048
    y 0.6645
    x 0.4289
    z 0.5
    label "YCL014W"
  ]
  node
  [
    id 2049
    y 0.1985
    x 0.5185
    z 0.5
    label "YGL131C"
  ]
  node
  [
    id 2050
    y 0.5458
    x 0.5444
    z 0.5
    label "YJR159W"
  ]
  node
  [
    id 2051
    y 0.7883
    x 0.3464
    z 0.5
    label "YOL111C"
  ]
  node
  [
    id 2052
    y 0.6196
    x 0.544
    z 0.5
    label "YGL216W"
  ]
  node
  [
    id 2053
    y 0.56
    x 0.5586
    z 0.5
    label "YEL066W"
  ]
  node
  [
    id 2054
    y 0.4857
    x 0.5281
    z 0.5
    label "YPL070W"
  ]
  node
  [
    id 2055
    y 0.384
    x 0.8144
    z 0.5
    label "YGL095C"
  ]
  node
  [
    id 2056
    y 0.7097
    x 0.2159
    z 0.5
    label "YEL049W"
  ]
  node
  [
    id 2057
    y 0.6437
    x 0.2726
    z 0.5
    label "YKL002W"
  ]
  node
  [
    id 2058
    y 0.2916
    x 0.7043
    z 0.5
    label "YIL077C"
  ]
  node
  [
    id 2059
    y 0.5362
    x 0.5577
    z 0.5
    label "YBL005W"
  ]
  node
  [
    id 2060
    y 0.337
    x 0.6406
    z 0.5
    label "YGL036W"
  ]
  node
  [
    id 2061
    y 0.5458
    x 0.5444
    z 0.5
    label "YDL246C"
  ]
  node
  [
    id 2062
    y 0.7284
    x 0.4469
    z 0.5
    label "YMR127C"
  ]
  node
  [
    id 2063
    y 0.6195
    x 0.544
    z 0.5
    label "YOR058C"
  ]
  node
  [
    id 2064
    y 0.2528
    x 0.5279
    z 0.5
    label "YNL308C"
  ]
  node
  [
    id 2065
    y 0.1562
    x 0.5679
    z 0.5
    label "YIL141W"
  ]
  node
  [
    id 2066
    y 0.4681
    x 0.721
    z 0.5
    label "YDL080C"
  ]
  node
  [
    id 2067
    y 0.7019
    x 0.4392
    z 0.5
    label "YIR006C"
  ]
  node
  [
    id 2068
    y 0.5032
    x 0.2592
    z 0.5
    label "YCR005C"
  ]
  node
  [
    id 2069
    y 0.7576
    x 0.6349
    z 0.5
    label "YIR040C"
  ]
  node
  [
    id 2070
    y 0.4406
    x 0.7237
    z 0.5
    label "YJL172W"
  ]
  node
  [
    id 2071
    y 0.6116
    x 0.47
    z 0.5
    label "YMR287C"
  ]
  node
  [
    id 2072
    y 0.5258
    x 0.2871
    z 0.5
    label "YAL001C"
  ]
  node
  [
    id 2073
    y 0.1276
    x 0.3881
    z 0.5
    label "YDR430C"
  ]
  node
  [
    id 2074
    y 0.3034
    x 0.2775
    z 0.5
    label "YKR052C"
  ]
  node
  [
    id 2075
    y 0.3339
    x 0.6252
    z 0.5
    label "YIL009C-A"
  ]
  node
  [
    id 2076
    y 0.2306
    x 0.4716
    z 0.5
    label "YKL109W"
  ]
  node
  [
    id 2077
    y 0.6256
    x 0.4452
    z 0.5
    label "YHR108W"
  ]
  node
  [
    id 2078
    y 0.5896
    x 0.3638
    z 0.5
    label "YPL169C"
  ]
  node
  [
    id 2079
    y 0.552
    x 0.2124
    z 0.5
    label "YCL039W"
  ]
  node
  [
    id 2080
    y 0.3179
    x 0.5406
    z 0.5
    label "YDR314C"
  ]
  node
  [
    id 2081
    y 0.4267
    x 0.8228
    z 0.5
    label "YNL092W"
  ]
  node
  [
    id 2082
    y 0.1559
    x 0.3011
    z 0.5
    label "YKL110C"
  ]
  node
  [
    id 2083
    y 0.4801
    x 0.3147
    z 0.5
    label "YML025C"
  ]
  node
  [
    id 2084
    y 0.6196
    x 0.544
    z 0.5
    label "YOR195W"
  ]
  node
  [
    id 2085
    y 0.2355
    x 0.3778
    z 0.5
    label "YDR045C"
  ]
  node
  [
    id 2086
    y 0.6681
    x 0.3396
    z 0.5
    label "YMR129W"
  ]
  node
  [
    id 2087
    y 0.6367
    x 0.7002
    z 0.5
    label "YJL013C"
  ]
  node
  [
    id 2088
    y 0.7571
    x 0.5099
    z 0.5
    label "YDR017C"
  ]
  node
  [
    id 2089
    y 0.4485
    x 0.6974
    z 0.5
    label "YPR034W"
  ]
  node
  [
    id 2090
    y 0.64
    x 0.7249
    z 0.5
    label "YHR147C"
  ]
  node
  [
    id 2091
    y 0.6043
    x 0.3313
    z 0.5
    label "YDL193W"
  ]
  node
  [
    id 2092
    y 0.3922
    x 0.1798
    z 0.5
    label "YML009C"
  ]
  node
  [
    id 2093
    y 0.5677
    x 0.5928
    z 0.5
    label "YCR008W"
  ]
  node
  [
    id 2094
    y 0.4961
    x 0.3033
    z 0.5
    label "YDL203C"
  ]
  node
  [
    id 2095
    y 0.7901
    x 0.716
    z 0.5
    label "YER071C"
  ]
  node
  [
    id 2096
    y 0.6543
    x 0.2832
    z 0.5
    label "YNL334C"
  ]
  node
  [
    id 2097
    y 0.2773
    x 0.6856
    z 0.5
    label "YCL009C"
  ]
  node
  [
    id 2098
    y 0.1876
    x 0.7241
    z 0.5
    label "YMR108W"
  ]
  node
  [
    id 2099
    y 0.5337
    x 0.3764
    z 0.5
    label "YBR229C"
  ]
  node
  [
    id 2100
    y 0.6371
    x 0.7521
    z 0.5
    label "YGR032W"
  ]
  node
  [
    id 2101
    y 0.6618
    x 0.2947
    z 0.5
    label "YGR180C"
  ]
  node
  [
    id 2102
    y 0.7472
    x 0.6674
    z 0.5
    label "YHR168W"
  ]
  node
  [
    id 2103
    y 0.5157
    x 0.2377
    z 0.5
    label "YDL220C"
  ]
  node
  [
    id 2104
    y 0.4091
    x 0.5475
    z 0.5
    label "YMR216C"
  ]
  node
  [
    id 2105
    y 0.6196
    x 0.544
    z 0.5
    label "YJL084C"
  ]
  node
  [
    id 2106
    y 0.6543
    x 0.2837
    z 0.5
    label "YMR095C"
  ]
  node
  [
    id 2107
    y 0.3895
    x 0.2631
    z 0.5
    label "YPR189W"
  ]
  node
  [
    id 2108
    y 0.5338
    x 0.7898
    z 0.5
    label "YGL149W"
  ]
  node
  [
    id 2109
    y 0.3649
    x 0.4505
    z 0.5
    label "YJR077C"
  ]
  node
  [
    id 2110
    y 0.6772
    x 0.3042
    z 0.5
    label "YGL234W"
  ]
  node
  [
    id 2111
    y 0.2997
    x 0.4493
    z 0.5
    label "YDR069C"
  ]
  node
  [
    id 2112
    y 0.3646
    x 0.7361
    z 0.5
    label "YDR189W"
  ]
  node
  [
    id 2113
    y 0.6048
    x 0.4462
    z 0.5
    label "YML103C"
  ]
  node
  [
    id 2114
    y 0.2432
    x 0.5111
    z 0.5
    label "YHR196W"
  ]
  node
  [
    id 2115
    y 0.5673
    x 0.7435
    z 0.5
    label "YOR197W"
  ]
  node
  [
    id 2116
    y 0.5742
    x 0.6274
    z 0.5
    label "YHL024W"
  ]
  node
  [
    id 2117
    y 0.484
    x 0.5218
    z 0.5
    label "YJR134C"
  ]
  node
  [
    id 2118
    y 0.4069
    x 0.3302
    z 0.5
    label "YBR039W"
  ]
  node
  [
    id 2119
    y 0.5337
    x 0.3764
    z 0.5
    label "YGL229C"
  ]
  node
  [
    id 2120
    y 0.2969
    x 0.6382
    z 0.5
    label "YPR115W"
  ]
  node
  [
    id 2121
    y 0.7482
    x 0.4815
    z 0.5
    label "YML053C"
  ]
  node
  [
    id 2122
    y 0.681
    x 0.5498
    z 0.5
    label "YKR091W"
  ]
  node
  [
    id 2123
    y 0.4132
    x 0.3541
    z 0.5
    label "YOR159C"
  ]
  node
  [
    id 2124
    y 0.7027
    x 0.5271
    z 0.5
    label "YPL246C"
  ]
  node
  [
    id 2125
    y 0.7695
    x 0.5965
    z 0.5
    label "YMR198W"
  ]
  node
  [
    id 2126
    y 0.3802
    x 0.7098
    z 0.5
    label "YDR186C"
  ]
  node
  [
    id 2127
    y 0.5149
    x 0.3586
    z 0.5
    label "YIL084C"
  ]
  node
  [
    id 2128
    y 0.5249
    x 0.4016
    z 0.5
    label "YIR011C"
  ]
  node
  [
    id 2129
    y 0.7737
    x 0.5571
    z 0.5
    label "YBR195C"
  ]
  node
  [
    id 2130
    y 0.2276
    x 0.3389
    z 0.5
    label "YLR094C"
  ]
  node
  [
    id 2131
    y 0.3226
    x 0.2165
    z 0.5
    label "YJL006C"
  ]
  node
  [
    id 2132
    y 0.3651
    x 0.3363
    z 0.5
    label "YBR105C"
  ]
  node
  [
    id 2133
    y 0.681
    x 0.2792
    z 0.5
    label "YNR034W"
  ]
  node
  [
    id 2134
    y 0.4694
    x 0.7947
    z 0.5
    label "YML042W"
  ]
  node
  [
    id 2135
    y 0.6072
    x 0.2683
    z 0.5
    label "YPL007C"
  ]
  node
  [
    id 2136
    y 0.2584
    x 0.579
    z 0.5
    label "YGL062W"
  ]
  node
  [
    id 2137
    y 0.2814
    x 0.5548
    z 0.5
    label "YFR040W"
  ]
  node
  [
    id 2138
    y 0.576
    x 0.6351
    z 0.5
    label "YOR208W"
  ]
  node
  [
    id 2139
    y 0.607
    x 0.3633
    z 0.5
    label "YDR399W"
  ]
  node
  [
    id 2140
    y 0.3835
    x 0.3029
    z 0.5
    label "YNL050C"
  ]
  node
  [
    id 2141
    y 0.5257
    x 0.632
    z 0.5
    label "YOL144W"
  ]
  node
  [
    id 2142
    y 0.7484
    x 0.6604
    z 0.5
    label "YOR279C"
  ]
  node
  [
    id 2143
    y 0.752
    x 0.6616
    z 0.5
    label "YDR310C"
  ]
  node
  [
    id 2144
    y 0.154
    x 0.5533
    z 0.5
    label "YJR087W"
  ]
  node
  [
    id 2145
    y 0.6534
    x 0.3794
    z 0.5
    label "YFL013C"
  ]
  node
  [
    id 2146
    y 0.1772
    x 0.4362
    z 0.5
    label "YPL046C"
  ]
  node
  [
    id 2147
    y 0.768
    x 0.6074
    z 0.5
    label "YDR122W"
  ]
  node
  [
    id 2148
    y 0.7055
    x 0.2552
    z 0.5
    label "YDR144C"
  ]
  node
  [
    id 2149
    y 0.6154
    x 0.4889
    z 0.5
    label "YGR270W"
  ]
  node
  [
    id 2150
    y 0.1948
    x 0.819
    z 0.5
    label "YNL121C"
  ]
  node
  [
    id 2151
    y 0.5337
    x 0.3764
    z 0.5
    label "YMR200W"
  ]
  node
  [
    id 2152
    y 0.4329
    x 0.6453
    z 0.5
    label "YEL034W"
  ]
  node
  [
    id 2153
    y 0.3084
    x 0.5451
    z 0.5
    label "YGL171W"
  ]
  node
  [
    id 2154
    y 0.5257
    x 0.3946
    z 0.5
    label "YDR277C"
  ]
  node
  [
    id 2155
    y 0.7833
    x 0.3414
    z 0.5
    label "YHR215W"
  ]
  node
  [
    id 2156
    y 0.275
    x 0.3954
    z 0.5
    label "YPL119C"
  ]
  node
  [
    id 2157
    y 0.4093
    x 0.7532
    z 0.5
    label "YDR050C"
  ]
  node
  [
    id 2158
    y 0.3538
    x 0.7996
    z 0.5
    label "YLR124W"
  ]
  node
  [
    id 2159
    y 0.5268
    x 0.6801
    z 0.5
    label "YMR223W"
  ]
  node
  [
    id 2160
    y 0.7928
    x 0.5033
    z 0.5
    label "YJL004C"
  ]
  node
  [
    id 2161
    y 0.457
    x 0.526
    z 0.5
    label "YGL122C"
  ]
  node
  [
    id 2162
    y 0.3757
    x 0.6811
    z 0.5
    label "YBL030C"
  ]
  node
  [
    id 2163
    y 0.7034
    x 0.641
    z 0.5
    label "YML018C"
  ]
  node
  [
    id 2164
    y 0.3458
    x 0.318
    z 0.5
    label "YGR159C"
  ]
  node
  [
    id 2165
    y 0.2834
    x 0.3734
    z 0.5
    label "YFL018C"
  ]
  node
  [
    id 2166
    y 0.611
    x 0.5847
    z 0.5
    label "YOR267C"
  ]
  node
  [
    id 2167
    y 0.5836
    x 0.7979
    z 0.5
    label "YMR280C"
  ]
  node
  [
    id 2168
    y 0.2134
    x 0.6522
    z 0.5
    label "YEL020W-A"
  ]
  node
  [
    id 2169
    y 0.5213
    x 0.7907
    z 0.5
    label "YLR097C"
  ]
  node
  [
    id 2170
    y 0.6119
    x 0.4704
    z 0.5
    label "YDR515W"
  ]
  node
  [
    id 2171
    y 0.4895
    x 0.7701
    z 0.5
    label "YLR322W"
  ]
  node
  [
    id 2172
    y 0.4361
    x 0.6463
    z 0.5
    label "YGR155W"
  ]
  node
  [
    id 2173
    y 0.6302
    x 0.3839
    z 0.5
    label "YJR074W"
  ]
  node
  [
    id 2174
    y 0.5929
    x 0.3882
    z 0.5
    label "YDR256C"
  ]
  node
  [
    id 2175
    y 0.507
    x 0.878
    z 0.5
    label "YPR049C"
  ]
  node
  [
    id 2176
    y 0.4822
    x 0.517
    z 0.5
    label "YGL180W"
  ]
  node
  [
    id 2177
    y 0.2431
    x 0.0416
    z 0.5
    label "YGL220W"
  ]
  node
  [
    id 2178
    y 0.2638
    x 0.1073
    z 0.5
    label "YDR098C"
  ]
  node
  [
    id 2179
    y 0.3516
    x 0.5406
    z 0.5
    label "YDR429C"
  ]
  node
  [
    id 2180
    y 0.4311
    x 0.3282
    z 0.5
    label "YER047C"
  ]
  node
  [
    id 2181
    y 0.6324
    x 0.5299
    z 0.5
    label "YMR159C"
  ]
  node
  [
    id 2182
    y 0.9239
    x 0.4329
    z 0.5
    label "YLR100W"
  ]
  node
  [
    id 2183
    y 0.6599
    x 0.7209
    z 0.5
    label "YNL289W"
  ]
  node
  [
    id 2184
    y 0.5347
    x 0.5401
    z 0.5
    label "YGL221C"
  ]
  node
  [
    id 2185
    y 0.6338
    x 0.3346
    z 0.5
    label "YNR064C"
  ]
  node
  [
    id 2186
    y 0.4981
    x 0.6636
    z 0.5
    label "YCL010C"
  ]
  node
  [
    id 2187
    y 0.7329
    x 0.6311
    z 0.5
    label "YMR029C"
  ]
  node
  [
    id 2188
    y 0.6083
    x 0.5317
    z 0.5
    label "YNL086W"
  ]
  node
  [
    id 2189
    y 0.8581
    x 0.6248
    z 0.5
    label "YDL138W"
  ]
  node
  [
    id 2190
    y 0.6337
    x 0.2983
    z 0.5
    label "YML031W"
  ]
  node
  [
    id 2191
    y 0.5432
    x 0.2808
    z 0.5
    label "YOR150W"
  ]
  node
  [
    id 2192
    y 0.7145
    x 0.5451
    z 0.5
    label "YKR069W"
  ]
  node
  [
    id 2193
    y 0.5535
    x 0.3176
    z 0.5
    label "YPL032C"
  ]
  node
  [
    id 2194
    y 0.7519
    x 0.5263
    z 0.5
    label "YDL235C"
  ]
  node
  [
    id 2195
    y 0.6848
    x 0.7019
    z 0.5
    label "YGL215W"
  ]
  node
  [
    id 2196
    y 0.2563
    x 0.5305
    z 0.5
    label "YFR001W"
  ]
  node
  [
    id 2197
    y 0.1833
    x 0.6127
    z 0.5
    label "YML124C"
  ]
  node
  [
    id 2198
    y 0.4191
    x 0.7824
    z 0.5
    label "YGR003W"
  ]
  node
  [
    id 2199
    y 0.6611
    x 0.5747
    z 0.5
    label "YDL025C"
  ]
  node
  [
    id 2200
    y 0.4923
    x 0.5205
    z 0.5
    label "YML078W"
  ]
  node
  [
    id 2201
    y 0.2487
    x 0.5218
    z 0.5
    label "YNL286W"
  ]
  node
  [
    id 2202
    y 0.5838
    x 0.28
    z 0.5
    label "YKL122C"
  ]
  node
  [
    id 2203
    y 0.5697
    x 0.7414
    z 0.5
    label "YGL073W"
  ]
  node
  [
    id 2204
    y 0.5707
    x 0.7127
    z 0.5
    label "YMR229C"
  ]
  node
  [
    id 2205
    y 0.4181
    x 0.3727
    z 0.5
    label "YGL104C"
  ]
  node
  [
    id 2206
    y 0.4029
    x 0.204
    z 0.5
    label "YLR051C"
  ]
  node
  [
    id 2207
    y 0.4819
    x 0.24
    z 0.5
    label "YBR158W"
  ]
  node
  [
    id 2208
    y 0.4516
    x 0.6837
    z 0.5
    label "YHR058C"
  ]
  node
  [
    id 2209
    y 0.2712
    x 0.7435
    z 0.5
    label "YLR259C"
  ]
  node
  [
    id 2210
    y 0.5414
    x 0.7321
    z 0.5
    label "YER123W"
  ]
  node
  [
    id 2211
    y 0.3153
    x 0.2546
    z 0.5
    label "YHR216W"
  ]
  node
  [
    id 2212
    y 0.3262
    x 0.289
    z 0.5
    label "YGR250C"
  ]
  node
  [
    id 2213
    y 0.4601
    x 0.2941
    z 0.5
    label "YNL004W"
  ]
  node
  [
    id 2214
    y 0.1992
    x 0.4161
    z 0.5
    label "YCR066W"
  ]
  node
  [
    id 2215
    y 0.1415
    x 0.4084
    z 0.5
    label "YLR032W"
  ]
  node
  [
    id 2216
    y 0.1776
    x 0.7541
    z 0.5
    label "YJL054W"
  ]
  node
  [
    id 2217
    y 0.7816
    x 0.6918
    z 0.5
    label "YBL068W"
  ]
  node
  [
    id 2218
    y 0.6846
    x 0.4698
    z 0.5
    label "YMR065W"
  ]
  node
  [
    id 2219
    y 0.4319
    x 0.7847
    z 0.5
    label "YJL102W"
  ]
  node
  [
    id 2220
    y 0.3379
    x 0.3925
    z 0.5
    label "YBR205W"
  ]
  node
  [
    id 2221
    y 0.2407
    x 0.3246
    z 0.5
    label "YNR058W"
  ]
  node
  [
    id 2222
    y 0.8106
    x 0.8301
    z 0.5
    label "YDR078C"
  ]
  node
  [
    id 2223
    y 0.5605
    x 0.8393
    z 0.5
    label "YGR279C"
  ]
  node
  [
    id 2224
    y 0.2514
    x 0.3348
    z 0.5
    label "YBR188C"
  ]
  node
  [
    id 2225
    y 0.3074
    x 0.649
    z 0.5
    label "YML114C"
  ]
  node
  [
    id 2226
    y 0.5233
    x 0.3067
    z 0.5
    label "YMR251W"
  ]
  node
  [
    id 2227
    y 0.5155
    x 0.8474
    z 0.5
    label "YMR246W"
  ]
  node
  [
    id 2228
    y 0.2053
    x 0.3721
    z 0.5
    label "YGR278W"
  ]
  node
  [
    id 2229
    y 0.5825
    x 0.5525
    z 0.5
    label "YLR187W"
  ]
  node
  [
    id 2230
    y 0.7619
    x 0.7332
    z 0.5
    label "YNL152W"
  ]
  node
  [
    id 2231
    y 0.4999
    x 0.3195
    z 0.5
    label "YOR388C"
  ]
  node
  [
    id 2232
    y 0.3522
    x 0.8391
    z 0.5
    label "YGR077C"
  ]
  node
  [
    id 2233
    y 0.6635
    x 0.7917
    z 0.5
    label "YFL021W"
  ]
  node
  [
    id 2234
    y 0.8423
    x 0.6682
    z 0.5
    label "YAL051W"
  ]
  node
  [
    id 2235
    y 0.8885
    x 0.4514
    z 0.5
    label "YAL056W"
  ]
  node
  [
    id 2236
    y 0.277
    x 0.4701
    z 0.5
    label "YER002W"
  ]
  node
  [
    id 2237
    y 0.4398
    x 0.4686
    z 0.5
    label "YGL197W"
  ]
  node
  [
    id 2238
    y 0.379
    x 0.3166
    z 0.5
    label "YKL038W"
  ]
  node
  [
    id 2239
    y 0.6943
    x 0.5654
    z 0.5
    label "YIL138C"
  ]
  node
  [
    id 2240
    y 0.6257
    x 0.7569
    z 0.5
    label "YDR389W"
  ]
  node
  [
    id 2241
    y 0.5337
    x 0.3764
    z 0.5
    label "YAR064W"
  ]
  node
  [
    id 2242
    y 0.5104
    x 0.2381
    z 0.5
    label "YLR132C"
  ]
  node
  [
    id 2243
    y 0.7422
    x 0.778
    z 0.5
    label "YDR439W"
  ]
  node
  [
    id 2244
    y 0.706
    x 0.6513
    z 0.5
    label "YGL015C"
  ]
  node
  [
    id 2245
    y 0.3821
    x 0.3703
    z 0.5
    label "YER103W"
  ]
  node
  [
    id 2246
    y 0.2763
    x 0.368
    z 0.5
    label "YLR397C"
  ]
  node
  [
    id 2247
    y 0.7204
    x 0.5392
    z 0.5
    label "YML006C"
  ]
  node
  [
    id 2248
    y 0.5818
    x 0.5609
    z 0.5
    label "YKL061W"
  ]
  node
  [
    id 2249
    y 0.7446
    x 0.6108
    z 0.5
    label "YJL050W"
  ]
  node
  [
    id 2250
    y 0.3172
    x 0.6942
    z 0.5
    label "YPL149W"
  ]
  node
  [
    id 2251
    y 0.5845
    x 0.1864
    z 0.5
    label "YOL109W"
  ]
  node
  [
    id 2252
    y 0.3655
    x 0.6577
    z 0.5
    label "YDR023W"
  ]
  node
  [
    id 2253
    y 0.5998
    x 0.3363
    z 0.5
    label "YIR032C"
  ]
  node
  [
    id 2254
    y 0.5248
    x 0.8161
    z 0.5
    label "YGR143W"
  ]
  node
  [
    id 2255
    y 0.2939
    x 0.5792
    z 0.5
    label "YDR363W"
  ]
  node
  [
    id 2256
    y 0.6417
    x 0.2475
    z 0.5
    label "YKL154W"
  ]
  node
  [
    id 2257
    y 0.6857
    x 0.6723
    z 0.5
    label "YJL030W"
  ]
  node
  [
    id 2258
    y 0.6696
    x 0.5948
    z 0.5
    label "YAL024C"
  ]
  node
  [
    id 2259
    y 0.2902
    x 0.5935
    z 0.5
    label "YEL055C"
  ]
  node
  [
    id 2260
    y 0.5337
    x 0.3764
    z 0.5
    label "YNL066W"
  ]
  node
  [
    id 2261
    y 0.2623
    x 0.5902
    z 0.5
    label "YBR218C"
  ]
  node
  [
    id 2262
    y 0.6484
    x 0.2641
    z 0.5
    label "YER088C"
  ]
  node
  [
    id 2263
    y 0.7056
    x 0.7708
    z 0.5
    label "YKL079W"
  ]
  node
  [
    id 2264
    y 0.7678
    x 0.5165
    z 0.5
    label "YGR214W"
  ]
  node
  [
    id 2265
    y 0.2087
    x 0.36
    z 0.5
    label "YNR011C"
  ]
  node
  [
    id 2266
    y 0.6384
    x 0.3353
    z 0.5
    label "YGL179C"
  ]
  node
  [
    id 2267
    y 0.6392
    x 0.4025
    z 0.5
    label "YDR422C"
  ]
  node
  [
    id 2268
    y 0.5288
    x 0.3228
    z 0.5
    label "YNL097C"
  ]
  node
  [
    id 2269
    y 0.6407
    x 0.4432
    z 0.5
    label "YPR019W"
  ]
  node
  [
    id 2270
    y 0.5819
    x 0.6935
    z 0.5
    label "YBR028C"
  ]
  node
  [
    id 2271
    y 0.5896
    x 0.3292
    z 0.5
    label "YNL076W"
  ]
  node
  [
    id 2272
    y 0.322
    x 0.5901
    z 0.5
    label "YKL014C"
  ]
  node
  [
    id 2273
    y 0.5751
    x 0.1735
    z 0.5
    label "YHR122W"
  ]
  node
  [
    id 2274
    y 0.4014
    x 0.6685
    z 0.5
    label "YDR030C"
  ]
  node
  [
    id 2275
    y 0.7008
    x 0.4042
    z 0.5
    label "YJL201W"
  ]
  node
  [
    id 2276
    y 0.5337
    x 0.3764
    z 0.5
    label "YOR029W"
  ]
  node
  [
    id 2277
    y 0.2384
    x 0.5522
    z 0.5
    label "YNL014W"
  ]
  node
  [
    id 2278
    y 0.7793
    x 0.5735
    z 0.5
    label "YGR016W"
  ]
  node
  [
    id 2279
    y 0.5119
    x 0.7173
    z 0.5
    label "YPR188C"
  ]
  node
  [
    id 2280
    y 0.6939
    x 0.6618
    z 0.5
    label "YDR488C"
  ]
  node
  [
    id 2281
    y 0.7611
    x 0.5761
    z 0.5
    label "YCL051W"
  ]
  node
  [
    id 2282
    y 0.5128
    x 0.3074
    z 0.5
    label "YMR075W"
  ]
  node
  [
    id 2283
    y 0.4869
    x 0.5298
    z 0.5
    label "YDR159W"
  ]
  node
  [
    id 2284
    y 0.4568
    x 0.2929
    z 0.5
    label "YMR091C"
  ]
  node
  [
    id 2285
    y 0.4799
    x 0.3868
    z 0.5
    label "YMR021C"
  ]
  node
  [
    id 2286
    y 0.1389
    x 0.6544
    z 0.5
    label "YOR265W"
  ]
  node
  [
    id 2287
    y 0.602
    x 0.6168
    z 0.5
    label "YER075C"
  ]
  node
  [
    id 2288
    y 0.2449
    x 0.4402
    z 0.5
    label "YOR078W"
  ]
  node
  [
    id 2289
    y 0.5861
    x 0.1527
    z 0.5
    label "YOR054C"
  ]
  node
  [
    id 2290
    y 0.7641
    x 0.5624
    z 0.5
    label "YNL199C"
  ]
  node
  [
    id 2291
    y 0.7442
    x 0.3398
    z 0.5
    label "YHR031C"
  ]
  node
  [
    id 2292
    y 0.9313
    x 0.7003
    z 0.5
    label "YBR023C"
  ]
  node
  [
    id 2293
    y 0.5022
    x 0.3187
    z 0.5
    label "YBR095C"
  ]
  node
  [
    id 2294
    y 0.2043
    x 0.4034
    z 0.5
    label "YER139C"
  ]
  node
  [
    id 2295
    y 0.7035
    x 0.6693
    z 0.5
    label "YDR150W"
  ]
  node
  [
    id 2296
    y 0.4799
    x 0.3868
    z 0.5
    label "YDR300C"
  ]
  node
  [
    id 2297
    y 0.2919
    x 0.5983
    z 0.5
    label "YDL052C"
  ]
  node
  [
    id 2298
    y 0.4756
    x 0.2604
    z 0.5
    label "YML091C"
  ]
  node
  [
    id 2299
    y 0.2455
    x 0.217
    z 0.5
    label "YER150W"
  ]
  node
  [
    id 2300
    y 0.3407
    x 0.1545
    z 0.5
    label "YGL174W"
  ]
  node
  [
    id 2301
    y 0.7441
    x 0.3723
    z 0.5
    label "YNL192W"
  ]
  node
  [
    id 2302
    y 0.5661
    x 0.9103
    z 0.5
    label "YKL049C"
  ]
  node
  [
    id 2303
    y 0.8508
    x 0.594
    z 0.5
    label "YHR144C"
  ]
  node
  [
    id 2304
    y 0.6101
    x 0.3816
    z 0.5
    label "YLR425W"
  ]
  node
  [
    id 2305
    y 0.5628
    x 0.3483
    z 0.5
    label "YMR072W"
  ]
  node
  [
    id 2306
    y 0.5116
    x 0.6817
    z 0.5
    label "YIL113W"
  ]
  node
  [
    id 2307
    y 0.2959
    x 0.8551
    z 0.5
    label "YGL006W"
  ]
  node
  [
    id 2308
    y 0.6211
    x 0.3884
    z 0.5
    label "YER089C"
  ]
  node
  [
    id 2309
    y 0.6227
    x 0.7478
    z 0.5
    label "YOR317W"
  ]
  node
  [
    id 2310
    y 0.5955
    x 0.8051
    z 0.5
    label "YKL178C"
  ]
  node
  [
    id 2311
    y 0.3365
    x 0.4317
    z 0.5
    label "YEL026W"
  ]
  node
  [
    id 2312
    y 0.2583
    x 0.5664
    z 0.5
    label "YDR337W"
  ]
  node
  [
    id 2313
    y 0.2811
    x 0.6161
    z 0.5
    label "YNL253W"
  ]
  node
  [
    id 2314
    y 0.5093
    x 0.7161
    z 0.5
    label "YGR254W"
  ]
  node
  [
    id 2315
    y 0.5096
    x 0.2613
    z 0.5
    label "YML097C"
  ]
  node
  [
    id 2316
    y 0.422
    x 0.3615
    z 0.5
    label "YDL143W"
  ]
  node
  [
    id 2317
    y 0.6713
    x 0.4348
    z 0.5
    label "YOL012C"
  ]
  node
  [
    id 2318
    y 0.6028
    x 0.3604
    z 0.5
    label "YOL016C"
  ]
  node
  [
    id 2319
    y 0.3438
    x 0.7937
    z 0.5
    label "YGL246C"
  ]
  node
  [
    id 2320
    y 0.357
    x 0.3725
    z 0.5
    label "YDR350C"
  ]
  node
  [
    id 2321
    y 0.2441
    x 0.3957
    z 0.5
    label "YPL106C"
  ]
  node
  [
    id 2322
    y 0.4885
    x 0.2575
    z 0.5
    label "YNR001C"
  ]
  node
  [
    id 2323
    y 0.4799
    x 0.3868
    z 0.5
    label "YPR077C"
  ]
  node
  [
    id 2324
    y 0.3513
    x 0.2597
    z 0.5
    label "YKR086W"
  ]
  node
  [
    id 2325
    y 0.217
    x 0.3016
    z 0.5
    label "YLL013C"
  ]
  node
  [
    id 2326
    y 0.4965
    x 0.4882
    z 0.5
    label "YJL008C"
  ]
  node
  [
    id 2327
    y 0.5569
    x 0.315
    z 0.5
    label "YMR261C"
  ]
  node
  [
    id 2328
    y 0.607
    x 0.3742
    z 0.5
    label "YPL127C"
  ]
  node
  [
    id 2329
    y 0.2529
    x 0.4266
    z 0.5
    label "YIR026C"
  ]
  node
  [
    id 2330
    y 0.4721
    x 0.2478
    z 0.5
    label "YKR008W"
  ]
  node
  [
    id 2331
    y 0.7337
    x 0.545
    z 0.5
    label "YNR048W"
  ]
  node
  [
    id 2332
    y 0.6502
    x 0.3116
    z 0.5
    label "YPL160W"
  ]
  node
  [
    id 2333
    y 0.5911
    x 0.3297
    z 0.5
    label "YJL042W"
  ]
  node
  [
    id 2334
    y 0.4854
    x 0.1971
    z 0.5
    label "YKR010C"
  ]
  node
  [
    id 2335
    y 0.4545
    x 0.7088
    z 0.5
    label "YMR275C"
  ]
  node
  [
    id 2336
    y 0.3686
    x 0.2459
    z 0.5
    label "YFL049W"
  ]
  node
  [
    id 2337
    y 0.3443
    x 0.6682
    z 0.5
    label "YBL104C"
  ]
  node
  [
    id 2338
    y 0.7319
    x 0.6494
    z 0.5
    label "YDR184C"
  ]
  node
  [
    id 2339
    y 0.7391
    x 0.4376
    z 0.5
    label "YLR150W"
  ]
  node
  [
    id 2340
    y 0.4925
    x 0.1812
    z 0.5
    label "YCR079W"
  ]
  node
  [
    id 2341
    y 0.1976
    x 0.5867
    z 0.5
    label "YDR092W"
  ]
  node
  [
    id 2342
    y 0.2538
    x 0.2489
    z 0.5
    label "YGR006W"
  ]
  node
  [
    id 2343
    y 0.6196
    x 0.544
    z 0.5
    label "YIR031C"
  ]
  node
  [
    id 2344
    y 0.1034
    x 0.5796
    z 0.5
    label "YPR104C"
  ]
  node
  [
    id 2345
    y 0.2149
    x 0.6226
    z 0.5
    label "YNL119W"
  ]
  node
  [
    id 2346
    y 0.6218
    x 0.8455
    z 0.5
    label "YGR284C"
  ]
  node
  [
    id 2347
    y 0.1173
    x 0.6195
    z 0.5
    label "YHL045W"
  ]
  node
  [
    id 2348
    y 0.1879
    x 0.7886
    z 0.5
    label "YNR049C"
  ]
  node
  [
    id 2349
    y 0.737
    x 0.5386
    z 0.5
    label "YPR003C"
  ]
  node
  [
    id 2350
    y 0.3381
    x 0.2725
    z 0.5
    label "YKL183W"
  ]
  node
  [
    id 2351
    y 0.5828
    x 0.3326
    z 0.5
    label "YOL128C"
  ]
  node
  [
    id 2352
    y 0.6115
    x 0.4699
    z 0.5
    label "YCL036W"
  ]
  node
  [
    id 2353
    y 0.6948
    x 0.5816
    z 0.5
    label "YGR080W"
  ]
  node
  [
    id 2354
    y 0.1904
    x 0.2406
    z 0.5
    label "YBL010C"
  ]
  node
  [
    id 2355
    y 0.7269
    x 0.5813
    z 0.5
    label "YPL270W"
  ]
  node
  [
    id 2356
    y 0.2703
    x 0.4594
    z 0.5
    label "YMR064W"
  ]
  node
  [
    id 2357
    y 0.5337
    x 0.3764
    z 0.5
    label "YOR035C"
  ]
  node
  [
    id 2358
    y 0.3915
    x 0.3321
    z 0.5
    label "YLR058C"
  ]
  node
  [
    id 2359
    y 0.1044
    x 0.5301
    z 0.5
    label "YGL087C"
  ]
  node
  [
    id 2360
    y 0.2941
    x 0.7201
    z 0.5
    label "YMR068W"
  ]
  node
  [
    id 2361
    y 0.4721
    x 0.7685
    z 0.5
    label "YDR183W"
  ]
  node
  [
    id 2362
    y 0.7155
    x 0.4911
    z 0.5
    label "YJR061W"
  ]
  node
  [
    id 2363
    y 0.6205
    x 0.3744
    z 0.5
    label "YJL052W"
  ]
  node
  [
    id 2364
    y 0.1633
    x 0.4366
    z 0.5
    label "YBR172C"
  ]
  node
  [
    id 2365
    y 0.6196
    x 0.544
    z 0.5
    label "YCR065W"
  ]
  node
  [
    id 2366
    y 0.7854
    x 0.6111
    z 0.5
    label "YGR196C"
  ]
  node
  [
    id 2367
    y 0.5524
    x 0.1094
    z 0.5
    label "YML071C"
  ]
  node
  [
    id 2368
    y 0.5233
    x 0.6791
    z 0.5
    label "YKL120W"
  ]
  node
  [
    id 2369
    y 0.5856
    x 0.4083
    z 0.5
    label "YER011W"
  ]
  node
  [
    id 2370
    y 0.5936
    x 0.3668
    z 0.5
    label "YLR303W"
  ]
  node
  [
    id 2371
    y 0.4461
    x 0.202
    z 0.5
    label "YLR189C"
  ]
  node
  [
    id 2372
    y 0.3278
    x 0.4718
    z 0.5
    label "YKR071C"
  ]
  node
  [
    id 2373
    y 0.4991
    x 0.1975
    z 0.5
    label "YMR233W"
  ]
  node
  [
    id 2374
    y 0.8755
    x 0.5243
    z 0.5
    label "YMR076C"
  ]
  node
  [
    id 2375
    y 0.3398
    x 0.6763
    z 0.5
    label "YFR031C-A"
  ]
  node
  [
    id 2376
    y 0.7232
    x 0.3237
    z 0.5
    label "YCR015C"
  ]
  node
  [
    id 2377
    y 0.2848
    x 0.1771
    z 0.5
    label "YGR262C"
  ]
  node
  [
    id 2378
    y 0.3029
    x 0.3443
    z 0.5
    label "YBR149W"
  ]
  node
  [
    id 2379
    y 0.7788
    x 0.5827
    z 0.5
    label "YPR106W"
  ]
  node
  [
    id 2380
    y 0.4016
    x 0.2471
    z 0.5
    label "YDL115C"
  ]
  node
  [
    id 2381
    y 0.7017
    x 0.6747
    z 0.5
    label "YAR044W"
  ]
  node
  [
    id 2382
    y 0.8245
    x 0.6392
    z 0.5
    label "YHL023C"
  ]
  node
  [
    id 2383
    y 0.4335
    x 0.342
    z 0.5
    label "YLR247C"
  ]
  node
  [
    id 2384
    y 0.6116
    x 0.4701
    z 0.5
    label "YJL085W"
  ]
  node
  [
    id 2385
    y 0.2777
    x 0.4246
    z 0.5
    label "YLR002C"
  ]
  node
  [
    id 2386
    y 0.5708
    x 0.7057
    z 0.5
    label "YHR149C"
  ]
  node
  [
    id 2387
    y 0.7681
    x 0.5404
    z 0.5
    label "YLR177W"
  ]
  node
  [
    id 2388
    y 0.7103
    x 0.8362
    z 0.5
    label "YOL039W"
  ]
  node
  [
    id 2389
    y 0.291
    x 0.3532
    z 0.5
    label "YDL094C"
  ]
  node
  [
    id 2390
    y 0.4712
    x 0.7294
    z 0.5
    label "YMR118C"
  ]
  node
  [
    id 2391
    y 0.5258
    x 0.632
    z 0.5
    label "YGR056W"
  ]
  node
  [
    id 2392
    y 0.2401
    x 0.6225
    z 0.5
    label "YAL026C"
  ]
  node
  [
    id 2393
    y 0.5993
    x 0.8748
    z 0.5
    label "YPL002C"
  ]
  node
  [
    id 2394
    y 0.1755
    x 0.6764
    z 0.5
    label "YDR009W"
  ]
  node
  [
    id 2395
    y 0.4546
    x 0.7661
    z 0.5
    label "YIL047C"
  ]
  node
  [
    id 2396
    y 0.5337
    x 0.3764
    z 0.5
    label "YJR073C"
  ]
  node
  [
    id 2397
    y 0.2249
    x 0.5173
    z 0.5
    label "YNL062C"
  ]
  node
  [
    id 2398
    y 0.1226
    x 0.5326
    z 0.5
    label "YKL148C"
  ]
  node
  [
    id 2399
    y 0.3199
    x 0.791
    z 0.5
    label "YBL058W"
  ]
  node
  [
    id 2400
    y 0.6629
    x 0.7126
    z 0.5
    label "YHR129C"
  ]
  node
  [
    id 2401
    y 0.3475
    x 0.233
    z 0.5
    label "YPL201C"
  ]
  node
  [
    id 2402
    y 0.2947
    x 0.1677
    z 0.5
    label "YIL053W"
  ]
  node
  [
    id 2403
    y 0.5337
    x 0.3764
    z 0.5
    label "YBR213W"
  ]
  node
  [
    id 2404
    y 0.5852
    x 0.267
    z 0.5
    label "YDL168W"
  ]
  node
  [
    id 2405
    y 0.7277
    x 0.6947
    z 0.5
    label "YBL045C"
  ]
  node
  [
    id 2406
    y 0.5995
    x 0.7169
    z 0.5
    label "YAL004W"
  ]
  node
  [
    id 2407
    y 0.6076
    x 0.3507
    z 0.5
    label "YOR344C"
  ]
  node
  [
    id 2408
    y 0.6588
    x 0.6469
    z 0.5
    label "YJR027W"
  ]
  node
  [
    id 2409
    y 0.2766
    x 0.3332
    z 0.5
    label "YPL029W"
  ]
  node
  [
    id 2410
    y 0.3931
    x 0.3174
    z 0.5
    label "YKL088W"
  ]
  node
  [
    id 2411
    y 0.2892
    x 0.2456
    z 0.5
    label "YMR277W"
  ]
  node
  [
    id 2412
    y 0.5554
    x 0.7857
    z 0.5
    label "YDR131C"
  ]
  node
  [
    id 2413
    y 0.8857
    x 0.8508
    z 0.5
    label "YIL152W"
  ]
  node
  [
    id 2414
    y 0.7197
    x 0.4898
    z 0.5
    label "YNL020C"
  ]
  node
  [
    id 2415
    y 0.2519
    x 0.3459
    z 0.5
    label "YDR005C"
  ]
  node
  [
    id 2416
    y 0.64
    x 0.7426
    z 0.5
    label "YER132C"
  ]
  node
  [
    id 2417
    y 0.5662
    x 0.7025
    z 0.5
    label "YEL023C"
  ]
  node
  [
    id 2418
    y 0.8042
    x 0.4196
    z 0.5
    label "YDL161W"
  ]
  node
  [
    id 2419
    y 0.6771
    x 0.5058
    z 0.5
    label "YKL171W"
  ]
  node
  [
    id 2420
    y 0.3012
    x 0.8263
    z 0.5
    label "YBR283C"
  ]
  node
  [
    id 2421
    y 0.7572
    x 0.5985
    z 0.5
    label "YBR001C"
  ]
  node
  [
    id 2422
    y 0.5205
    x 0.2397
    z 0.5
    label "YLR016C"
  ]
  node
  [
    id 2423
    y 0.3306
    x 0.2952
    z 0.5
    label "YHR156C"
  ]
  node
  [
    id 2424
    y 0.7102
    x 0.3618
    z 0.5
    label "YKL190W"
  ]
  node
  [
    id 2425
    y 0.7054
    x 0.6291
    z 0.5
    label "YBR271W"
  ]
  node
  [
    id 2426
    y 0.7928
    x 0.5083
    z 0.5
    label "YPR030W"
  ]
  node
  [
    id 2427
    y 0.5831
    x 0.6741
    z 0.5
    label "YML111W"
  ]
  node
  [
    id 2428
    y 0.4389
    x 0.482
    z 0.5
    label "YKL143W"
  ]
  node
  [
    id 2429
    y 0.6048
    x 0.3396
    z 0.5
    label "YBR265W"
  ]
  node
  [
    id 2430
    y 0.4944
    x 0.238
    z 0.5
    label "YJR148W"
  ]
  node
  [
    id 2431
    y 0.853
    x 0.8866
    z 0.5
    label "YHL006C"
  ]
  node
  [
    id 2432
    y 0.8012
    x 0.4091
    z 0.5
    label "YGL094C"
  ]
  node
  [
    id 2433
    y 0.341
    x 0.3248
    z 0.5
    label "YIL173W"
  ]
  node
  [
    id 2434
    y 0.8054
    x 0.5899
    z 0.5
    label "YEL005C"
  ]
  node
  [
    id 2435
    y 0.5337
    x 0.3764
    z 0.5
    label "YER023W"
  ]
  node
  [
    id 2436
    y 0.6957
    x 0.3434
    z 0.5
    label "YHR177W"
  ]
  node
  [
    id 2437
    y 0.3042
    x 0.6264
    z 0.5
    label "YDR296W"
  ]
  node
  [
    id 2438
    y 0.6706
    x 0.3325
    z 0.5
    label "YPL247C"
  ]
  node
  [
    id 2439
    y 0.6378
    x 0.4204
    z 0.5
    label "YBR066C"
  ]
  node
  [
    id 2440
    y 0.5829
    x 0.3256
    z 0.5
    label "YLR411W"
  ]
  node
  [
    id 2441
    y 0.3553
    x 0.7374
    z 0.5
    label "YGL013C"
  ]
  node
  [
    id 2442
    y 0.6568
    x 0.3834
    z 0.5
    label "YGL150C"
  ]
  node
  [
    id 2443
    y 0.054
    x 0.6696
    z 0.5
    label "YKR006C"
  ]
  edge
  [
    source 1
    target 0
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 133
    target 0
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 282
    target 0
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1846
    target 0
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 366
    target 0
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 551
    target 0
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1356
    target 0
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 620
    target 0
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2219
    target 0
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2198
    target 0
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 172
    target 0
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 173
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 172
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 172
    target 106
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 502
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1087
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1298
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 251
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 540
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1067
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 264
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 364
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1936
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 992
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 629
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1031
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 978
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 172
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1782
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1553
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 441
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 666
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1162
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 493
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1305
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 247
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 215
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 195
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 172
    target 105
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 501
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 333
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1049
    target 172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1049
    target 286
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1316
    target 1049
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1049
    target 40
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1575
    target 1049
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1049
    target 425
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1049
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1801
    target 425
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1575
    target 425
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1801
    target 1092
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1816
    target 1801
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1801
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1801
    target 1558
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1833
    target 1801
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1833
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 61
    target 60
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 199
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 230
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 275
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 278
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 388
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 645
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 702
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 706
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 723
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 760
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 785
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 322
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 921
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 970
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 467
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 687
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 993
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 564
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 333
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1100
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1159
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1163
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1185
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 210
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1004
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1372
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 973
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1436
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 61
    target 32
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 61
    target 27
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 541
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1488
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 237
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 524
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1145
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 488
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1026
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1491
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 540
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1003
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 458
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1722
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1202
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1804
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1059
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 968
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1827
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1443
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1076
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 332
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1865
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1383
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2010
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1985
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2029
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 958
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2044
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2045
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 72
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2052
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2063
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1905
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 618
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2084
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2105
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2122
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1696
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 539
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1739
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1562
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2199
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1142
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 870
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1085
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 194
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1806
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1032
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1450
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2149
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 766
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1679
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 236
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1848
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 620
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 198
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 61
    target 28
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 621
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1973
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 255
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1452
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 588
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 418
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 303
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2343
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1742
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2025
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1264
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2365
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 61
    target 31
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1349
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 61
    target 39
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 920
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1579
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 712
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 405
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1281
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 657
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2229
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2258
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1475
    target 61
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1475
    target 1474
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1593
    target 1475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1475
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1904
    target 1475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1905
    target 1904
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 140
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 958
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1188
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1200
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 789
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1282
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1330
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2093
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2183
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2195
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1968
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1593
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1905
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2061
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1064
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 139
    target 109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1404
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2050
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1651
    target 139
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1651
    target 1650
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1651
    target 1496
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1651
    target 752
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1651
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 112
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 867
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 112
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 874
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 354
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 804
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 561
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 179
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 735
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 944
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 417
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1660
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 421
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1236
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1071
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1307
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 787
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2196
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 880
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2236
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 744
    target 112
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 744
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 804
    target 744
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 744
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 616
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 872
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 923
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1236
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1071
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1307
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1014
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1038
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1379
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1124
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 616
    target 58
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 836
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 944
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 616
    target 404
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 616
    target 375
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 652
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 680
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 804
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 616
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 616
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 954
    target 953
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 652
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 355
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1042
    target 953
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 944
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 288
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 207
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 589
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 623
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 983
    target 953
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1792
    target 953
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 804
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 473
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 736
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1917
    target 953
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1071
    target 953
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 923
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1236
    target 953
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 872
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 651
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1451
    target 953
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 836
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 680
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 735
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 800
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1014
    target 953
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1379
    target 953
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 404
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 953
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 800
    target 799
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 892
    target 800
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 800
    target 355
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1550
    target 800
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 800
    target 736
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 800
    target 422
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 800
    target 207
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1308
    target 800
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 800
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 800
    target 224
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2164
    target 800
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2164
    target 1041
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1041
    target 1040
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1062
    target 1041
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1413
    target 1041
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1041
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1041
    target 529
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 529
    target 528
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 529
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 529
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 53
    target 52
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 111
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1022
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1068
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 241
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 195
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 596
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 700
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1946
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1398
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1506
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 478
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1048
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 560
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2089
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 901
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1226
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 237
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1163
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1448
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 184
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 210
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 146
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 869
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 481
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 475
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1032
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1445
    target 53
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1445
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1445
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1445
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 475
    target 474
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 871
    target 475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1032
    target 475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 475
    target 204
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1039
    target 475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1198
    target 475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1232
    target 475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 475
    target 73
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1606
    target 475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1659
    target 475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1778
    target 475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 475
    target 60
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 619
    target 475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1485
    target 475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2030
    target 475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1742
    target 475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1111
    target 475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1133
    target 475
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1133
    target 271
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1645
    target 1133
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1133
    target 74
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1133
    target 462
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1260
    target 1133
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1743
    target 1133
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1133
    target 150
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1133
    target 15
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1133
    target 203
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1133
    target 461
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 462
    target 461
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 461
    target 271
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1431
    target 461
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 204
    target 203
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 203
    target 75
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 763
    target 203
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 775
    target 203
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1260
    target 203
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 203
    target 74
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1190
    target 203
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1191
    target 203
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 919
    target 203
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 203
    target 150
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1209
    target 203
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1149
    target 203
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1845
    target 203
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 627
    target 203
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 628
    target 627
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 962
    target 627
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 627
    target 271
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1031
    target 627
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 627
    target 74
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 764
    target 627
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1033
    target 627
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1033
    target 1012
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1771
    target 1033
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1771
    target 12
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1771
    target 13
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2033
    target 1771
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1915
    target 1771
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1976
    target 1771
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1771
    target 1515
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1515
    target 12
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1915
    target 1515
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1515
    target 1012
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1976
    target 1515
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1976
    target 1012
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1976
    target 894
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2033
    target 1976
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1976
    target 1915
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1976
    target 910
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 910
    target 584
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 910
    target 893
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 910
    target 8
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1557
    target 910
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1915
    target 910
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 910
    target 894
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 962
    target 910
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 969
    target 910
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 910
    target 9
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 9
    target 8
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1222
    target 9
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1640
    target 9
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 12
    target 9
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1640
    target 1222
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1222
    target 8
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 969
    target 962
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 969
    target 584
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 969
    target 893
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 969
    target 219
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 969
    target 894
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 969
    target 347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 347
    target 346
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 347
    target 219
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 584
    target 347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 347
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1089
    target 347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 347
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 427
    target 347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 347
    target 220
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1087
    target 347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 743
    target 347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1985
    target 347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1993
    target 347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1662
    target 347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 894
    target 347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2112
    target 347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2035
    target 347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1627
    target 347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1627
    target 219
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2035
    target 427
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2035
    target 219
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2112
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2112
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2112
    target 1087
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1662
    target 346
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1662
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1662
    target 951
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 951
    target 307
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1089
    target 951
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 951
    target 308
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 951
    target 346
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 308
    target 307
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 441
    target 308
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 813
    target 308
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1005
    target 308
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 308
    target 272
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 498
    target 308
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1288
    target 308
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 600
    target 308
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 308
    target 4
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1017
    target 308
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 691
    target 308
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1840
    target 308
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 991
    target 308
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 991
    target 446
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 991
    target 730
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 991
    target 965
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1836
    target 991
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 991
    target 447
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 991
    target 729
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1782
    target 991
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 991
    target 441
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 991
    target 691
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1668
    target 991
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 991
    target 618
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1823
    target 991
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1823
    target 729
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1823
    target 730
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1823
    target 618
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1823
    target 446
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1823
    target 447
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1823
    target 965
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1823
    target 1668
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1668
    target 447
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1668
    target 730
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1668
    target 618
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1668
    target 965
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1668
    target 446
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 730
    target 729
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 729
    target 447
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 965
    target 729
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 729
    target 618
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 729
    target 446
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 447
    target 446
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 730
    target 447
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 965
    target 447
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 965
    target 446
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 965
    target 730
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 965
    target 882
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 883
    target 882
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 882
    target 555
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1438
    target 882
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1438
    target 555
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1438
    target 472
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 473
    target 472
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 472
    target 333
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1457
    target 472
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 555
    target 472
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 947
    target 472
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1593
    target 472
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 948
    target 947
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1513
    target 947
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1876
    target 947
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 947
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 947
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 947
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 434
    target 315
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 450
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1043
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 434
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1299
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 688
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 434
    target 222
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1731
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1527
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1336
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1909
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1876
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 434
    target 72
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 434
    target 186
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 977
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 434
    target 413
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 434
    target 201
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 611
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 758
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 434
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 948
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 434
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2239
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 808
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1302
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1077
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1741
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2353
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 908
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1988
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1719
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 628
    target 434
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1719
    target 73
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1719
    target 618
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1719
    target 1077
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1719
    target 1266
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1266
    target 419
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1266
    target 477
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1427
    target 1266
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1266
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1266
    target 478
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 637
    target 636
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1085
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1250
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 766
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 637
    target 469
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1769
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1842
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 767
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2088
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 637
    target 156
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1942
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2264
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2281
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 808
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2290
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2387
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2421
    target 637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2290
    target 1675
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1675
    target 1671
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1671
    target 257
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1671
    target 363
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1671
    target 963
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1671
    target 964
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1671
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1671
    target 798
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 798
    target 467
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1295
    target 798
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1295
    target 151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1295
    target 1116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1295
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2069
    target 1295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1116
    target 1115
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1432
    target 1116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1116
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1961
    target 1116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1116
    target 85
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1878
    target 1116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1303
    target 1116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1116
    target 332
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1304
    target 1303
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 1303
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1930
    target 1303
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1303
    target 160
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1303
    target 433
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 433
    target 432
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 433
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 523
    target 433
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 539
    target 433
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1746
    target 433
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 560
    target 433
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 782
    target 433
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1447
    target 433
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1707
    target 433
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 478
    target 433
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 657
    target 433
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 541
    target 433
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 920
    target 433
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1707
    target 625
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1707
    target 596
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1707
    target 1068
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 625
    target 233
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1447
    target 625
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 920
    target 625
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 625
    target 618
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 625
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 233
    target 212
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 739
    target 233
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1754
    target 233
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1770
    target 233
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1545
    target 233
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1881
    target 233
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1136
    target 233
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 773
    target 233
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1243
    target 233
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2270
    target 233
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1096
    target 233
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1096
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2081
    target 1096
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2081
    target 212
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 213
    target 212
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 218
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 768
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 922
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1143
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1403
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 741
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1240
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 213
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 243
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 572
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 624
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1210
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 365
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2208
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 547
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 471
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1406
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2390
    target 213
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1406
    target 1405
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1406
    target 1078
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1406
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1406
    target 765
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1947
    target 1406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1406
    target 1355
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1990
    target 1406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1406
    target 186
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1813
    target 1406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1457
    target 1406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1406
    target 411
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1406
    target 527
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1812
    target 1406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2408
    target 1406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1406
    target 970
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1813
    target 1812
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1812
    target 1362
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1362
    target 1361
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 527
    target 206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 527
    target 34
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 527
    target 176
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 527
    target 272
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 527
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1017
    target 527
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 527
    target 498
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 527
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 527
    target 4
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 811
    target 527
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 812
    target 527
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1189
    target 527
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1887
    target 527
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1351
    target 527
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 527
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1140
    target 527
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 575
    target 527
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 557
    target 527
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 622
    target 527
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 527
    target 334
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 527
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 527
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 653
    target 527
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 527
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 89
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 812
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1063
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1265
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1468
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1728
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 428
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1211
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1546
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 557
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 819
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 88
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2283
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1486
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 88
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 382
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 298
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2083
    target 88
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2083
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2083
    target 1179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1179
    target 1178
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1179
    target 463
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1179
    target 281
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1179
    target 819
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1179
    target 298
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1179
    target 428
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1889
    target 1179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1939
    target 1179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2048
    target 1179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2191
    target 1179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2371
    target 1179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1179
    target 238
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 239
    target 238
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 355
    target 238
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 238
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 612
    target 238
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 612
    target 370
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 612
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 736
    target 612
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1126
    target 612
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1384
    target 612
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1384
    target 175
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1384
    target 307
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1384
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2375
    target 1384
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 175
    target 174
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 843
    target 175
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1207
    target 175
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 175
    target 102
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1880
    target 175
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 373
    target 175
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 377
    target 175
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 189
    target 175
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 563
    target 175
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1690
    target 175
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 279
    target 175
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 175
    target 30
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 749
    target 175
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 749
    target 417
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1708
    target 749
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 749
    target 58
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1708
    target 417
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 30
    target 29
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 840
    target 30
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1097
    target 30
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1616
    target 30
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 370
    target 30
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 659
    target 30
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 872
    target 30
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1381
    target 30
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 373
    target 30
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1986
    target 30
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1140
    target 30
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 506
    target 30
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1762
    target 30
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1762
    target 1499
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1762
    target 721
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1762
    target 786
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 786
    target 721
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1499
    target 786
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 722
    target 721
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1499
    target 721
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 722
    target 598
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1536
    target 722
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 722
    target 322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 722
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 722
    target 438
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 722
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 438
    target 322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 438
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 438
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 438
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 438
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1277
    target 438
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 438
    target 321
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1447
    target 438
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 438
    target 115
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1171
    target 438
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 504
    target 438
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1170
    target 438
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1596
    target 438
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 599
    target 438
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1589
    target 438
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 438
    target 249
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2192
    target 438
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 438
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 628
    target 438
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2192
    target 322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 249
    target 143
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 471
    target 249
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 249
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 470
    target 249
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2090
    target 249
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 797
    target 249
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 322
    target 249
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 732
    target 249
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 732
    target 109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 992
    target 732
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 732
    target 312
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 732
    target 365
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 732
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 312
    target 311
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 470
    target 312
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 312
    target 219
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 787
    target 312
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 471
    target 312
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 312
    target 143
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 312
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1252
    target 311
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 994
    target 311
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 994
    target 457
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 998
    target 994
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1508
    target 994
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1966
    target 994
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 994
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 994
    target 862
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 994
    target 623
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2213
    target 994
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2213
    target 1386
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1386
    target 1069
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1386
    target 1114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1594
    target 1386
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1386
    target 862
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1595
    target 1594
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1594
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1595
    target 862
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1114
    target 688
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1145
    target 1114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1114
    target 314
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1114
    target 570
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1114
    target 1076
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1114
    target 808
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1114
    target 645
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1359
    target 1114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1225
    target 1114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1225
    target 15
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1359
    target 1358
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1359
    target 688
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1358
    target 803
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1358
    target 570
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1543
    target 1358
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1358
    target 229
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1358
    target 515
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1358
    target 302
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1760
    target 1358
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1358
    target 569
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 570
    target 569
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 569
    target 229
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1542
    target 569
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 865
    target 569
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 569
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1541
    target 569
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 569
    target 236
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 832
    target 569
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 715
    target 569
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 569
    target 156
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 569
    target 155
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 156
    target 155
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2243
    target 155
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1453
    target 155
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1453
    target 1452
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2025
    target 1453
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 715
    target 156
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 715
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1281
    target 715
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1267
    target 715
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 715
    target 266
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 838
    target 715
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 715
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 715
    target 500
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 715
    target 206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1743
    target 715
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 500
    target 499
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 639
    target 500
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 751
    target 500
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1449
    target 500
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 500
    target 266
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 500
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 500
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1449
    target 652
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 751
    target 266
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 751
    target 505
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 931
    target 751
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 751
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 751
    target 639
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 751
    target 160
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1647
    target 751
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 972
    target 751
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 751
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 972
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 972
    target 167
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1458
    target 972
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1237
    target 972
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 972
    target 196
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2137
    target 972
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2137
    target 457
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 196
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 504
    target 196
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 196
    target 51
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1874
    target 196
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 540
    target 196
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1154
    target 196
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1155
    target 1154
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1285
    target 1154
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1154
    target 463
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1874
    target 1154
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1159
    target 1154
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1154
    target 753
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 753
    target 601
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 753
    target 573
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 753
    target 696
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1872
    target 753
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1874
    target 753
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 753
    target 154
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 794
    target 753
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 783
    target 753
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 753
    target 574
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 574
    target 573
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 696
    target 574
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 783
    target 574
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1872
    target 574
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 574
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 881
    target 678
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 394
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 916
    target 678
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1125
    target 678
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 678
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 63
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 783
    target 678
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 198
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 657
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 676
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 617
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 573
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 431
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 378
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 506
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1139
    target 678
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 442
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 436
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 496
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 678
    target 401
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 402
    target 401
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 496
    target 401
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 401
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 856
    target 401
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 401
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1125
    target 401
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 401
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 401
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 401
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 401
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 762
    target 401
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 762
    target 435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 762
    target 394
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 762
    target 695
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 762
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1869
    target 762
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 762
    target 431
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1869
    target 483
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1869
    target 1347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1347
    target 483
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1347
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 483
    target 482
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 727
    target 483
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 787
    target 483
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 483
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 727
    target 726
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 727
    target 198
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1435
    target 727
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1418
    target 727
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1562
    target 727
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1004
    target 727
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1913
    target 727
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1888
    target 727
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 992
    target 727
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1888
    target 656
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1888
    target 1075
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1075
    target 1074
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 656
    target 218
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 696
    target 656
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 814
    target 656
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 656
    target 162
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 656
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1286
    target 656
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 656
    target 468
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 829
    target 656
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 656
    target 150
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 787
    target 656
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1023
    target 656
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1973
    target 656
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1782
    target 656
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2179
    target 656
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 656
    target 436
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1583
    target 656
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 656
    target 482
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 794
    target 656
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1583
    target 57
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1583
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1583
    target 829
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 57
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 829
    target 57
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 83
    target 57
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 205
    target 57
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 564
    target 57
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 123
    target 57
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 206
    target 205
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 205
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2173
    target 205
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1723
    target 205
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1211
    target 205
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2351
    target 205
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2351
    target 1795
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1795
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1795
    target 530
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2068
    target 1795
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1795
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1795
    target 1155
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2322
    target 1795
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1795
    target 586
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 586
    target 585
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1793
    target 586
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 914
    target 586
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 914
    target 63
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1151
    target 914
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 914
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 914
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1758
    target 914
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 914
    target 623
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1013
    target 914
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 914
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1435
    target 914
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 914
    target 701
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 914
    target 365
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 914
    target 884
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1890
    target 914
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 914
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 914
    target 738
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 914
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 914
    target 78
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 79
    target 78
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 78
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 18
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 128
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 607
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 669
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 663
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1125
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 478
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 120
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 629
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 977
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1473
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 165
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1335
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 17
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 55
    target 17
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 55
    target 54
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 92
    target 55
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1037
    target 55
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1247
    target 55
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1339
    target 55
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 640
    target 55
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 55
    target 21
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1654
    target 55
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1238
    target 55
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 55
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1238
    target 93
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1363
    target 1238
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1238
    target 21
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1238
    target 22
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1654
    target 1238
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1238
    target 640
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1238
    target 664
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1715
    target 1238
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1238
    target 977
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2284
    target 1238
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1897
    target 1238
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2330
    target 1238
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1454
    target 1238
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1238
    target 54
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1238
    target 92
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1339
    target 1238
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1238
    target 1037
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1454
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1701
    target 1454
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1454
    target 784
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1454
    target 807
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1454
    target 640
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1454
    target 735
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1454
    target 1247
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1454
    target 92
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1454
    target 509
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1454
    target 54
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 509
    target 296
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 857
    target 509
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 509
    target 436
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1271
    target 509
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 509
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1910
    target 509
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2185
    target 509
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1163
    target 509
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 509
    target 297
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2407
    target 509
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 297
    target 296
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1271
    target 899
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 899
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 899
    target 255
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 580
    target 579
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 580
    target 383
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1112
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1202
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1248
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1150
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 580
    target 384
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1654
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 842
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 769
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1835
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1925
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2013
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1487
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1103
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2305
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1355
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2127
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2293
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 580
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2268
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2282
    target 580
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 580
    target 21
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2282
    target 842
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2268
    target 842
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2293
    target 842
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2127
    target 171
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2127
    target 384
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2127
    target 842
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 171
    target 170
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 481
    target 171
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 968
    target 171
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 189
    target 171
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1929
    target 171
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1264
    target 171
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1105
    target 171
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 280
    target 171
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 171
    target 169
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 169
    target 168
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 901
    target 169
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1584
    target 169
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1162
    target 169
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1853
    target 169
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 997
    target 169
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 345
    target 169
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1574
    target 169
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1514
    target 169
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2404
    target 169
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2231
    target 169
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2231
    target 738
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1574
    target 997
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1574
    target 880
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1574
    target 376
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1574
    target 375
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1574
    target 187
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1574
    target 836
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 187
    target 62
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1907
    target 187
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1907
    target 880
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1907
    target 62
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 63
    target 62
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 448
    target 62
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 859
    target 62
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 302
    target 62
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1760
    target 62
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 427
    target 62
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2176
    target 62
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2176
    target 2175
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2176
    target 316
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 316
    target 315
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 716
    target 448
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 717
    target 448
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 717
    target 716
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 716
    target 218
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 716
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2076
    target 716
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 376
    target 375
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 680
    target 376
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 376
    target 179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 918
    target 376
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 376
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1236
    target 376
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 923
    target 376
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 651
    target 376
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 880
    target 376
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 376
    target 58
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 421
    target 376
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 918
    target 880
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 918
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 345
    target 162
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 630
    target 345
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1095
    target 345
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 454
    target 345
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 971
    target 345
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 975
    target 345
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2111
    target 345
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 631
    target 345
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 740
    target 345
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 740
    target 162
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 740
    target 630
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 975
    target 740
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 971
    target 740
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 740
    target 454
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 740
    target 631
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 631
    target 630
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 971
    target 631
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 975
    target 631
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 631
    target 162
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2111
    target 248
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2111
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 248
    target 247
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 496
    target 248
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1050
    target 248
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1814
    target 248
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1844
    target 248
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 756
    target 248
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 693
    target 248
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 693
    target 692
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1240
    target 693
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2398
    target 693
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 756
    target 755
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1298
    target 756
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 756
    target 591
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1283
    target 756
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1212
    target 756
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1215
    target 756
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 756
    target 91
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 756
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 952
    target 756
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 756
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 756
    target 559
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 756
    target 335
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 336
    target 335
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1215
    target 335
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 559
    target 558
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 559
    target 90
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 559
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1290
    target 559
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 559
    target 493
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1246
    target 559
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1246
    target 197
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1510
    target 1246
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1246
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1246
    target 1184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1246
    target 1212
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1246
    target 591
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1246
    target 1183
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2224
    target 1246
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1246
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1246
    target 755
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1246
    target 731
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1246
    target 861
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 861
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 861
    target 91
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1212
    target 861
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1215
    target 861
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 861
    target 731
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 731
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1184
    target 731
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 731
    target 91
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1212
    target 731
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1183
    target 731
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 755
    target 731
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2224
    target 731
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2224
    target 1212
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1184
    target 1183
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1183
    target 90
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1212
    target 1183
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1183
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1184
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1212
    target 1184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1510
    target 1509
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1510
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1510
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2342
    target 1509
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1509
    target 747
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 748
    target 747
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 747
    target 179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 747
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 747
    target 563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 747
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 747
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 747
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2324
    target 747
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 747
    target 379
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 379
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 835
    target 379
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 379
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1055
    target 379
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 379
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 379
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 422
    target 379
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 684
    target 379
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 379
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 379
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 379
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 379
    target 179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1276
    target 1051
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 540
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 11
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 926
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 547
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 741
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1283
    target 1051
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 624
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 768
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1919
    target 1051
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1256
    target 1051
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1354
    target 1051
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 468
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 51
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 443
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 477
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1051
    target 891
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 891
    target 477
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 891
    target 741
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 926
    target 891
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 891
    target 742
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1712
    target 891
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 891
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1919
    target 891
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1143
    target 891
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 891
    target 186
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1637
    target 891
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 891
    target 624
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 891
    target 244
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 891
    target 712
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 891
    target 245
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2186
    target 891
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2159
    target 891
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2159
    target 245
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2186
    target 477
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2186
    target 245
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2186
    target 741
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 245
    target 244
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 477
    target 245
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 245
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 383
    target 245
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 624
    target 245
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 742
    target 245
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 468
    target 245
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 450
    target 244
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1487
    target 244
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1637
    target 244
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 741
    target 244
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 758
    target 244
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 624
    target 244
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1864
    target 244
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1864
    target 1137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1864
    target 758
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1137
    target 462
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1137
    target 711
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1137
    target 939
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1137
    target 409
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1137
    target 410
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 410
    target 409
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 711
    target 410
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 939
    target 410
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 462
    target 410
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 711
    target 409
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 939
    target 409
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 939
    target 938
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 939
    target 711
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 939
    target 764
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 939
    target 74
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1000
    target 939
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2117
    target 939
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 939
    target 271
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1001
    target 939
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1723
    target 939
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 939
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1001
    target 1000
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2117
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 733
    target 711
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 733
    target 200
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1130
    target 733
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1569
    target 733
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 733
    target 324
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 813
    target 733
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 733
    target 284
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 733
    target 300
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 733
    target 77
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 885
    target 733
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 733
    target 453
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 733
    target 323
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 733
    target 301
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 733
    target 259
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 733
    target 283
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 284
    target 283
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 772
    target 283
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 454
    target 283
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 778
    target 283
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1569
    target 283
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 283
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 709
    target 283
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 300
    target 283
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 813
    target 283
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 453
    target 283
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 301
    target 283
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 885
    target 283
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 323
    target 283
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 283
    target 259
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 130
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 402
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 601
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 777
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1016
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1230
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 959
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 292
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 573
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1495
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 975
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 385
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1635
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 136
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 885
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 386
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 672
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 178
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1628
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 611
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 707
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 243
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 129
    target 69
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 301
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1496
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 783
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1460
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 696
    target 129
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1460
    target 797
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1460
    target 1400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1460
    target 41
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1460
    target 365
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1460
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1460
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 42
    target 41
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 365
    target 41
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 243
    target 41
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 397
    target 41
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 397
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 718
    target 397
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 734
    target 397
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 397
    target 352
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1218
    target 397
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1697
    target 397
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 520
    target 397
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1375
    target 397
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 397
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 397
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 397
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 571
    target 397
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 397
    target 342
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 978
    target 397
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 397
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 397
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 397
    target 356
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 613
    target 397
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 613
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 613
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 916
    target 613
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 613
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 613
    target 342
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 613
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1113
    target 613
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1113
    target 394
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1113
    target 787
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1113
    target 657
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1113
    target 1023
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1113
    target 884
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1113
    target 435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1782
    target 1113
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2179
    target 1113
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1113
    target 794
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1367
    target 1113
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1113
    target 978
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1113
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1367
    target 794
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1367
    target 787
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1678
    target 1367
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1367
    target 482
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2179
    target 1367
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1678
    target 794
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 357
    target 356
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 571
    target 356
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 356
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1375
    target 356
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1697
    target 356
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 356
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 356
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 718
    target 356
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 158
    target 157
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 389
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 556
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 769
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 844
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 872
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 876
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 591
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1090
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1008
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1377
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 158
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1434
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1461
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 892
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 854
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 705
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1624
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 822
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 225
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1740
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 818
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1890
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1937
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1885
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1959
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1967
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 158
    target 103
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1309
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1015
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 352
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2114
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1608
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 289
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2153
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1181
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 691
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 224
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 623
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 288
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1877
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2288
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 158
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 158
    target 70
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 207
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 158
    target 104
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1328
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1052
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 736
    target 158
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1052
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1052
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1052
    target 404
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1052
    target 288
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1052
    target 705
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2153
    target 1052
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1328
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1328
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1328
    target 794
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1328
    target 289
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1328
    target 705
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 104
    target 103
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 71
    target 70
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 96
    target 70
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1132
    target 70
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1718
    target 70
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1434
    target 70
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 70
    target 3
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 3
    target 2
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 246
    target 3
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1193
    target 3
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1690
    target 3
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1132
    target 3
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1131
    target 3
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 152
    target 3
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 288
    target 3
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 152
    target 151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1132
    target 152
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 152
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1715
    target 152
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1193
    target 152
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1385
    target 152
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1824
    target 152
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 152
    target 2
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1690
    target 152
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1824
    target 1193
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1824
    target 246
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1385
    target 1193
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1385
    target 1024
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1025
    target 1024
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1193
    target 1024
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1024
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1908
    target 1025
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1908
    target 1247
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1908
    target 21
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1132
    target 1131
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1193
    target 1131
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1894
    target 1131
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1894
    target 1692
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1894
    target 209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1894
    target 1345
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1894
    target 1758
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1894
    target 1369
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1369
    target 368
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1414
    target 1369
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1369
    target 1010
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1369
    target 1345
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1692
    target 1369
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1369
    target 209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1758
    target 1369
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1010
    target 209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1345
    target 1010
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1758
    target 1010
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1692
    target 1010
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1414
    target 209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1414
    target 373
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1692
    target 1414
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1414
    target 794
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1414
    target 1345
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 369
    target 368
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 640
    target 368
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1821
    target 368
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 807
    target 368
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 776
    target 368
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1997
    target 368
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1252
    target 368
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1829
    target 368
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1829
    target 1828
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1829
    target 697
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 697
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 697
    target 268
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 268
    target 267
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 795
    target 268
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 506
    target 268
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2026
    target 268
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 690
    target 268
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 494
    target 268
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 495
    target 494
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 738
    target 494
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 494
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1411
    target 494
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 817
    target 494
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1603
    target 494
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 494
    target 369
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 494
    target 106
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 494
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1434
    target 494
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1821
    target 494
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 817
    target 816
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 817
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2345
    target 817
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1412
    target 1411
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1411
    target 659
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2165
    target 1411
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1752
    target 1411
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1411
    target 1140
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1752
    target 1412
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1752
    target 90
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1478
    target 495
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 691
    target 690
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 828
    target 690
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 690
    target 302
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 847
    target 690
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1021
    target 690
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 690
    target 59
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 690
    target 128
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 690
    target 255
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 690
    target 534
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 690
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 534
    target 533
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1537
    target 534
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1710
    target 534
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1710
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 67
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 482
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 207
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 591
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 725
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1034
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1598
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 239
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1357
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 280
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2321
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 568
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1090
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 96
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 71
    target 66
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 568
    target 567
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 959
    target 568
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1338
    target 568
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1437
    target 568
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 568
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 568
    target 21
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 995
    target 568
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1230
    target 568
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 996
    target 568
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1423
    target 568
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 568
    target 136
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 568
    target 145
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2085
    target 568
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1635
    target 568
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1517
    target 568
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1826
    target 568
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2415
    target 568
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1826
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1826
    target 996
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1517
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1517
    target 145
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2085
    target 1517
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1517
    target 996
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2085
    target 996
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 145
    target 144
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 177
    target 145
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 387
    target 145
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 241
    target 145
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 240
    target 145
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 215
    target 145
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 241
    target 240
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 895
    target 240
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1124
    target 240
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 240
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1775
    target 240
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 386
    target 240
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1635
    target 240
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 959
    target 240
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1983
    target 240
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1230
    target 240
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 387
    target 240
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1983
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1775
    target 387
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 895
    target 854
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 895
    target 241
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1340
    target 895
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 895
    target 391
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 391
    target 390
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1333
    target 391
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1340
    target 391
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 391
    target 363
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1897
    target 391
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 504
    target 391
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 956
    target 391
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1119
    target 391
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 391
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2255
    target 391
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1119
    target 209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 956
    target 788
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 956
    target 186
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 956
    target 261
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 956
    target 640
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 956
    target 92
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 956
    target 807
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 956
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 261
    target 260
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 360
    target 261
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 550
    target 261
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 879
    target 261
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1008
    target 261
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1409
    target 261
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 296
    target 261
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2130
    target 261
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2221
    target 261
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1152
    target 261
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1153
    target 1152
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1152
    target 126
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2299
    target 1152
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 126
    target 125
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 710
    target 126
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 710
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 856
    target 710
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 710
    target 508
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 508
    target 507
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 508
    target 120
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 508
    target 292
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 856
    target 508
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 792
    target 508
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 792
    target 120
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 507
    target 229
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 980
    target 507
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 515
    target 507
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 989
    target 507
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 990
    target 507
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1614
    target 507
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 970
    target 507
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1614
    target 515
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1614
    target 990
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1614
    target 970
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1614
    target 229
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 990
    target 989
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 990
    target 970
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1306
    target 990
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1541
    target 990
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1561
    target 990
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 990
    target 980
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 990
    target 515
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 990
    target 229
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1649
    target 1561
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1561
    target 515
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1649
    target 935
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1649
    target 1618
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1649
    target 515
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1618
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1618
    target 1047
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1618
    target 655
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1618
    target 1549
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1618
    target 1604
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1604
    target 1395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1604
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1604
    target 655
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1604
    target 291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 291
    target 290
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 437
    target 291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 291
    target 204
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 366
    target 291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 811
    target 291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1055
    target 291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1896
    target 291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 469
    target 291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 291
    target 166
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1168
    target 291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 608
    target 291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 555
    target 291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 503
    target 291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1613
    target 291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1912
    target 291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 596
    target 291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 291
    target 151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1912
    target 577
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1912
    target 1220
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1220
    target 422
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1220
    target 1181
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1220
    target 124
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1220
    target 1217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1220
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1220
    target 591
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 124
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 463
    target 124
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1054
    target 124
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1079
    target 124
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1392
    target 124
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1883
    target 124
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 354
    target 124
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2202
    target 124
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2202
    target 1079
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 577
    target 576
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 577
    target 334
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 577
    target 151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1679
    target 577
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 576
    target 110
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 576
    target 564
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 576
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 110
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 110
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 206
    target 110
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 110
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 934
    target 110
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 575
    target 110
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1140
    target 110
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 110
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 654
    target 110
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 151
    target 110
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 654
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 654
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 654
    target 564
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 654
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1129
    target 654
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 886
    target 654
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 886
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 886
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1211
    target 886
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1129
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1129
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 934
    target 564
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 934
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 934
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1613
    target 503
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1613
    target 166
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1613
    target 437
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 503
    target 166
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 689
    target 503
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 992
    target 503
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 503
    target 334
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1896
    target 503
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1387
    target 689
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 689
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1375
    target 689
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1860
    target 689
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1804
    target 689
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 830
    target 689
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1115
    target 689
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 830
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2405
    target 830
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1860
    target 1297
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2101
    target 1860
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2101
    target 1297
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1297
    target 878
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1297
    target 1155
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1578
    target 1297
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1903
    target 1297
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1297
    target 1231
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1297
    target 362
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2062
    target 1297
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2062
    target 1578
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2062
    target 1100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2062
    target 1231
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 363
    target 362
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 362
    target 257
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 362
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 362
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 589
    target 362
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1691
    target 362
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1777
    target 362
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 362
    target 122
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1450
    target 362
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 857
    target 362
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 450
    target 362
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 362
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 122
    target 121
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 122
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 659
    target 122
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1623
    target 122
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 398
    target 122
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1878
    target 122
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2046
    target 122
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 122
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2046
    target 398
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2046
    target 1780
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1780
    target 596
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1780
    target 597
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1780
    target 1325
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1780
    target 593
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2138
    target 1780
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2287
    target 1780
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1780
    target 1322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1322
    target 406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1322
    target 1239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2012
    target 1322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1322
    target 713
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 1322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 714
    target 713
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 713
    target 52
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2102
    target 713
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1448
    target 713
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1048
    target 713
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1032
    target 713
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1068
    target 713
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 713
    target 596
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1485
    target 713
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2287
    target 713
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 714
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2012
    target 611
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1239
    target 588
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1239
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1239
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1239
    target 607
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1239
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 406
    target 405
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 420
    target 406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1572
    target 406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1656
    target 406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 406
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1118
    target 406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2043
    target 406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2166
    target 406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2266
    target 406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 767
    target 406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1666
    target 406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2267
    target 406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 406
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2267
    target 1400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1666
    target 1118
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1666
    target 1400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2166
    target 457
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1118
    target 1117
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 1118
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 1117
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1117
    target 471
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2167
    target 1117
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1572
    target 1400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1572
    target 1435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1572
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2287
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2138
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 593
    target 165
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 593
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1325
    target 869
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1325
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 597
    target 596
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 675
    target 597
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 619
    target 597
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 597
    target 310
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 869
    target 597
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 597
    target 492
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1316
    target 597
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 493
    target 492
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1026
    target 492
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1145
    target 492
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 310
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 675
    target 310
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 675
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1455
    target 675
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1455
    target 720
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1653
    target 1455
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1455
    target 570
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 720
    target 719
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 719
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 398
    target 383
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1373
    target 398
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1408
    target 398
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1964
    target 398
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 398
    target 121
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1661
    target 1408
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1408
    target 1400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1408
    target 1351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1661
    target 1451
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1877
    target 1661
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1661
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1661
    target 904
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 904
    target 903
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2103
    target 904
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2103
    target 1979
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2103
    target 464
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2103
    target 1980
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1980
    target 1979
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 465
    target 464
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1581
    target 464
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 464
    target 269
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 900
    target 464
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 464
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1030
    target 464
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 464
    target 270
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 480
    target 464
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 480
    target 370
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 840
    target 480
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 480
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 270
    target 269
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 270
    target 165
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 707
    target 270
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 270
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1670
    target 270
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1150
    target 270
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 402
    target 270
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 270
    target 170
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 783
    target 270
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1670
    target 1519
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1519
    target 970
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1612
    target 1519
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2126
    target 1519
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1519
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1612
    target 1155
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1612
    target 1102
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1102
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1102
    target 924
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1434
    target 1102
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1102
    target 666
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1102
    target 48
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 48
    target 47
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 924
    target 923
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 924
    target 279
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 924
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 924
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1598
    target 924
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1014
    target 924
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1030
    target 900
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1030
    target 706
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1767
    target 1030
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1701
    target 1030
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1767
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1767
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 287
    target 286
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 900
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 333
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 470
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1162
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 479
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 985
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1278
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 911
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 973
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 369
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1029
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 629
    target 287
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1029
    target 273
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1029
    target 274
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1243
    target 1029
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1029
    target 280
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1751
    target 1029
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1029
    target 628
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1029
    target 257
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1029
    target 773
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1029
    target 333
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1029
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1029
    target 834
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1524
    target 1029
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1524
    target 726
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1524
    target 394
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1913
    target 1524
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 834
    target 833
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 834
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 837
    target 834
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 834
    target 274
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1243
    target 834
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 834
    target 273
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 834
    target 773
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 837
    target 773
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1751
    target 837
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1243
    target 837
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 833
    target 773
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 833
    target 208
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1243
    target 833
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 209
    target 208
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1243
    target 208
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 366
    target 208
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1345
    target 208
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 208
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 273
    target 208
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 274
    target 208
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1682
    target 208
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1545
    target 208
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1751
    target 208
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 773
    target 208
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1682
    target 961
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1682
    target 773
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1682
    target 1243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1751
    target 1682
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 961
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1293
    target 961
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 961
    target 58
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1293
    target 370
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1293
    target 736
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1293
    target 735
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1293
    target 707
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1949
    target 1293
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1293
    target 794
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1751
    target 1243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1751
    target 1136
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1751
    target 773
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1751
    target 1545
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 274
    target 273
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 366
    target 274
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1545
    target 274
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 366
    target 273
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1545
    target 273
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 911
    target 479
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 911
    target 629
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1256
    target 911
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 911
    target 470
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 985
    target 911
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 911
    target 553
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1278
    target 911
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 911
    target 333
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 911
    target 369
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 553
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 553
    target 470
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 624
    target 553
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 985
    target 553
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1256
    target 553
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 553
    target 479
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 629
    target 553
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 553
    target 109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 750
    target 553
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1532
    target 553
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 553
    target 471
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1533
    target 1532
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1555
    target 1532
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 750
    target 460
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 750
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 65
    target 64
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 544
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 664
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1268
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1230
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 488
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1189
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1798
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 546
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 460
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1219
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 849
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 459
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1922
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1895
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 545
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2118
    target 65
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2118
    target 460
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 546
    target 545
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 545
    target 460
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1321
    target 545
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 664
    target 545
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 545
    target 544
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1321
    target 787
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1433
    target 1321
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1321
    target 649
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1321
    target 482
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 649
    target 648
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 649
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1433
    target 649
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1817
    target 648
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1817
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1895
    target 460
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1922
    target 460
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 460
    target 459
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 544
    target 459
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 849
    target 460
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1219
    target 460
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 583
    target 546
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 546
    target 544
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 546
    target 460
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1268
    target 546
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 583
    target 510
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 583
    target 141
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1428
    target 583
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 583
    target 352
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1871
    target 583
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 583
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 583
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1871
    target 1870
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2020
    target 1870
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1870
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1870
    target 141
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2020
    target 1565
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1566
    target 1565
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1565
    target 1300
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1565
    target 650
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 650
    target 251
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 973
    target 650
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 650
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 890
    target 650
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 890
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1447
    target 890
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2301
    target 890
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 903
    target 890
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1300
    target 489
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 490
    target 489
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 489
    target 23
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 24
    target 23
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 223
    target 23
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 718
    target 23
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 102
    target 23
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 209
    target 23
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 852
    target 223
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 223
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 888
    target 223
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 888
    target 887
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 888
    target 852
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 888
    target 853
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 888
    target 851
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1374
    target 888
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2360
    target 888
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2024
    target 888
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2024
    target 318
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2024
    target 1374
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 318
    target 317
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 728
    target 318
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 852
    target 318
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 873
    target 318
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1522
    target 318
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1360
    target 318
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1135
    target 318
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2233
    target 318
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1374
    target 318
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1136
    target 1135
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1360
    target 1135
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1166
    target 1135
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1623
    target 1135
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1167
    target 1166
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2222
    target 1166
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2413
    target 2222
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2431
    target 2222
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1623
    target 1360
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1522
    target 852
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1522
    target 317
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 873
    target 728
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 852
    target 317
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2360
    target 852
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1374
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 1374
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 851
    target 850
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1711
    target 851
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1481
    target 851
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1960
    target 851
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2037
    target 851
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 851
    target 604
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 851
    target 810
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 810
    target 280
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 810
    target 604
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 604
    target 427
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1401
    target 604
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1830
    target 604
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 604
    target 328
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1484
    target 604
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 604
    target 329
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1547
    target 604
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 604
    target 292
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2312
    target 604
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 604
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2312
    target 76
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 77
    target 76
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1898
    target 76
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1902
    target 76
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 776
    target 76
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2319
    target 76
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 184
    target 76
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2399
    target 76
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1548
    target 1547
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1547
    target 1263
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1547
    target 971
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2074
    target 1547
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2109
    target 1547
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1858
    target 1547
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2181
    target 1547
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1547
    target 1376
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1548
    target 1376
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2181
    target 63
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1858
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2109
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2109
    target 807
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1263
    target 1262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1688
    target 1263
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1779
    target 1263
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1263
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1777
    target 1263
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2242
    target 1263
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1736
    target 1263
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2422
    target 1263
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2430
    target 1263
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1263
    target 1084
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1085
    target 1084
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1084
    target 324
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1736
    target 1735
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2273
    target 1736
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1602
    target 1262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1602
    target 1601
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1548
    target 518
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 519
    target 518
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1092
    target 518
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1571
    target 518
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 863
    target 518
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 540
    target 518
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1558
    target 518
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1698
    target 518
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1698
    target 1182
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1698
    target 941
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1698
    target 942
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1698
    target 1471
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1698
    target 519
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1698
    target 1424
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1698
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1700
    target 1698
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2110
    target 1698
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2110
    target 942
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1700
    target 1471
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1700
    target 1182
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1975
    target 1700
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1700
    target 706
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1700
    target 942
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1700
    target 519
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1424
    target 465
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1464
    target 1424
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1579
    target 1424
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1597
    target 1424
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1851
    target 1424
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1424
    target 942
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1424
    target 120
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1424
    target 1182
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2291
    target 1424
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1633
    target 1424
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1424
    target 519
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1633
    target 1151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1634
    target 1633
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1652
    target 1633
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1633
    target 1466
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1633
    target 1579
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1633
    target 1344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1344
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1344
    target 1151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1466
    target 1151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1652
    target 823
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 823
    target 162
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 823
    target 807
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1863
    target 823
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 823
    target 120
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 864
    target 823
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 864
    target 707
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 864
    target 180
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 864
    target 519
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1276
    target 864
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 180
    target 179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 449
    target 180
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 558
    target 180
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 180
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 180
    target 174
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 497
    target 180
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1312
    target 180
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 295
    target 180
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 189
    target 180
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 180
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 380
    target 180
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1162
    target 180
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 644
    target 180
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 644
    target 643
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 644
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 644
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 644
    target 190
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1019
    target 644
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 644
    target 189
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 644
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 644
    target 380
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 644
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 644
    target 497
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 644
    target 449
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 644
    target 610
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1886
    target 644
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 684
    target 644
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1886
    target 266
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1886
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1886
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1886
    target 1435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 610
    target 337
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1215
    target 610
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 863
    target 610
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 793
    target 610
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 610
    target 449
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 610
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2123
    target 610
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 610
    target 497
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 610
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 610
    target 380
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 610
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 610
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2123
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2123
    target 90
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2123
    target 1215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2123
    target 558
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2123
    target 684
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 793
    target 605
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 862
    target 793
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1254
    target 793
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 793
    target 99
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 793
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 863
    target 793
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 793
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 793
    target 562
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 793
    target 80
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 793
    target 337
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 793
    target 563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 793
    target 493
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 81
    target 80
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 306
    target 80
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 563
    target 80
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1227
    target 80
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 80
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 606
    target 80
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 863
    target 80
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 337
    target 80
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 606
    target 605
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 606
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 606
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 606
    target 12
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 863
    target 606
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 606
    target 337
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1227
    target 563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1227
    target 562
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1227
    target 337
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2002
    target 1227
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1227
    target 1217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1227
    target 863
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1254
    target 1227
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1227
    target 99
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2002
    target 1031
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 337
    target 306
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 809
    target 306
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1204
    target 306
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 306
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 306
    target 99
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 306
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1590
    target 306
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2350
    target 306
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 761
    target 306
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 306
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 306
    target 90
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 761
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 761
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 761
    target 179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 761
    target 497
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1590
    target 591
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1590
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1590
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1590
    target 189
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1590
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1204
    target 493
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1204
    target 809
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1921
    target 1204
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 809
    target 808
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 809
    target 493
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 563
    target 562
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 562
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 562
    target 423
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 562
    target 337
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 883
    target 562
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 562
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 424
    target 423
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 748
    target 423
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 423
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2364
    target 424
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 493
    target 424
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 100
    target 99
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1215
    target 99
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 337
    target 99
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 563
    target 99
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1254
    target 1215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1254
    target 337
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1254
    target 563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 605
    target 337
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 605
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 337
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 563
    target 337
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1215
    target 337
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 337
    target 12
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 337
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 863
    target 337
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 337
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1019
    target 380
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1019
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1019
    target 449
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1019
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1019
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 190
    target 189
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 416
    target 190
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 497
    target 190
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 190
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 190
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 380
    target 190
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1162
    target 190
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1721
    target 190
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 190
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 190
    target 174
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 449
    target 190
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 780
    target 190
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 780
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 859
    target 780
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 780
    target 174
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1721
    target 687
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 497
    target 416
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 416
    target 380
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1843
    target 416
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 643
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 380
    target 189
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 380
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 380
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 380
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 449
    target 380
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 471
    target 380
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 380
    target 174
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2156
    target 380
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1162
    target 380
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 497
    target 380
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 380
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 501
    target 380
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1312
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1312
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 497
    target 493
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 497
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 497
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 497
    target 449
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 497
    target 189
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 497
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 497
    target 105
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 497
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 497
    target 174
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 666
    target 497
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 449
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 449
    target 189
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 449
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 449
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 449
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1863
    target 120
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1851
    target 519
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1851
    target 942
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1464
    target 120
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1464
    target 1151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1471
    target 1470
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1471
    target 942
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1471
    target 1182
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1471
    target 519
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1664
    target 1470
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2208
    target 1470
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2382
    target 1470
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 942
    target 941
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1182
    target 942
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 942
    target 519
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1364
    target 942
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1364
    target 1182
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 941
    target 519
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1182
    target 941
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1182
    target 519
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 863
    target 862
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 863
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1559
    target 863
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 863
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 863
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1174
    target 863
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 863
    target 372
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1002
    target 863
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2369
    target 863
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 863
    target 206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2369
    target 206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2369
    target 2057
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2057
    target 2056
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1002
    target 377
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1002
    target 373
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1002
    target 842
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 1002
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 373
    target 372
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 377
    target 372
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 372
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1175
    target 1174
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1174
    target 872
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1174
    target 29
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1699
    target 1174
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1174
    target 884
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1174
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 1174
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1174
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1174
    target 354
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1174
    target 575
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1699
    target 222
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1699
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1175
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1559
    target 377
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1559
    target 373
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 329
    target 328
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 859
    target 329
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1484
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1484
    target 1016
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1334
    target 328
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1334
    target 128
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1334
    target 1021
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1830
    target 1556
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1556
    target 1273
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1556
    target 1401
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1556
    target 280
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1556
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1556
    target 1151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1273
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1273
    target 436
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1273
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1481
    target 102
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 850
    target 343
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2344
    target 850
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 344
    target 343
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 443
    target 343
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 853
    target 852
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 853
    target 707
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 887
    target 852
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 887
    target 24
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 852
    target 364
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1251
    target 852
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1252
    target 1251
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1251
    target 365
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1251
    target 8
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1251
    target 771
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1251
    target 1080
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1080
    target 51
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1080
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1080
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1917
    target 1080
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1283
    target 1080
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1080
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2259
    target 1080
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1298
    target 1080
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1080
    target 540
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 772
    target 771
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 885
    target 771
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 778
    target 771
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 771
    target 279
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 771
    target 575
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 325
    target 24
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 184
    target 24
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 251
    target 24
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 366
    target 24
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1076
    target 24
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 517
    target 24
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 551
    target 24
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 517
    target 516
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 551
    target 517
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2198
    target 517
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 517
    target 133
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 325
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1820
    target 325
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1839
    target 325
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1953
    target 325
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 655
    target 325
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1820
    target 655
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 927
    target 490
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1355
    target 490
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 736
    target 490
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1395
    target 490
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1566
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1566
    target 1180
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1566
    target 1122
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1657
    target 1566
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2424
    target 1566
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1566
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1657
    target 1180
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1657
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1123
    target 1122
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1122
    target 141
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1416
    target 1122
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1122
    target 826
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1122
    target 510
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1122
    target 976
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1122
    target 557
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1122
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2154
    target 1122
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1122
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1428
    target 1122
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2059
    target 1122
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2059
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2059
    target 741
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2154
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2154
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 976
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 826
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1416
    target 498
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1416
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1180
    target 302
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2203
    target 1180
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1214
    target 1180
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1214
    target 524
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2203
    target 1725
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1725
    target 1105
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1725
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2021
    target 1725
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2021
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2021
    target 1048
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2021
    target 1955
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1955
    target 929
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1955
    target 725
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1955
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 929
    target 928
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 929
    target 563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1625
    target 929
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1065
    target 929
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1957
    target 929
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1957
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1065
    target 348
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 349
    target 348
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 348
    target 293
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 945
    target 348
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1066
    target 348
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 348
    target 39
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 348
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1694
    target 348
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1694
    target 821
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2146
    target 1694
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 822
    target 821
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1069
    target 821
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 954
    target 821
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1066
    target 661
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1066
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1066
    target 361
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 361
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 361
    target 255
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 361
    target 39
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 945
    target 361
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 361
    target 293
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 361
    target 349
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 523
    target 361
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 661
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1186
    target 661
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 661
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1187
    target 661
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1187
    target 1186
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1187
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1186
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 945
    target 869
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 945
    target 870
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1226
    target 945
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2187
    target 945
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2187
    target 870
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 293
    target 292
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 491
    target 293
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 552
    target 293
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1151
    target 293
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1997
    target 293
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 552
    target 153
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 552
    target 292
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1151
    target 552
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 154
    target 153
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 349
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1031
    target 349
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1625
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1428
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1428
    target 142
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1428
    target 320
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 320
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 514
    target 320
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 320
    target 148
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 997
    target 320
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 339
    target 320
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 967
    target 320
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 801
    target 320
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 770
    target 320
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1302
    target 320
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 320
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 320
    target 141
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 320
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 598
    target 320
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 770
    target 142
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 770
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 801
    target 142
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1442
    target 801
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1442
    target 1003
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1442
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1442
    target 1155
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 967
    target 142
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1302
    target 967
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 967
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 148
    target 142
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 339
    target 148
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 148
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 514
    target 142
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 514
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 142
    target 141
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 339
    target 142
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 319
    target 142
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1850
    target 142
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2160
    target 142
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 598
    target 142
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1850
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1850
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1850
    target 85
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 141
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1076
    target 141
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1539
    target 141
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 141
    target 15
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1381
    target 141
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1302
    target 141
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 339
    target 141
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 141
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1539
    target 808
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1539
    target 1306
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1539
    target 1076
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 510
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1268
    target 479
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1268
    target 460
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 884
    target 544
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 544
    target 460
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 460
    target 64
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1754
    target 64
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2038
    target 64
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 567
    target 460
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 664
    target 460
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1111
    target 460
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 460
    target 450
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1278
    target 479
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1278
    target 985
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1278
    target 688
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1278
    target 1256
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1278
    target 369
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1278
    target 629
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 985
    target 629
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1256
    target 985
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 985
    target 369
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 985
    target 479
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 479
    target 478
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 479
    target 471
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 479
    target 333
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 683
    target 479
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1402
    target 479
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 479
    target 109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1298
    target 479
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 629
    target 479
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 479
    target 33
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1256
    target 479
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 479
    target 470
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 470
    target 33
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1402
    target 1283
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1402
    target 333
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1402
    target 1298
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1402
    target 51
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1402
    target 471
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1402
    target 629
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1402
    target 470
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1402
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1402
    target 540
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 683
    target 176
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 683
    target 186
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 683
    target 473
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1228
    target 683
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 765
    target 683
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1228
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1228
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 269
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1702
    target 465
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1703
    target 465
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1703
    target 1702
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1451
    target 903
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 903
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 1373
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1623
    target 1622
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1815
    target 1623
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1815
    target 1151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1622
    target 1151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 572
    target 121
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1576
    target 121
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1931
    target 121
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1931
    target 359
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1931
    target 572
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2346
    target 1931
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1931
    target 700
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 359
    target 358
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1292
    target 359
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1352
    target 359
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1352
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1477
    target 1352
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1477
    target 1476
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1477
    target 1234
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1525
    target 1477
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1526
    target 1525
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1525
    target 1089
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1525
    target 346
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1995
    target 1526
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1526
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1526
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1526
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1526
    target 1087
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1526
    target 992
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1526
    target 600
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1235
    target 1234
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1234
    target 1169
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1169
    target 547
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1235
    target 1169
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 358
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2420
    target 358
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1578
    target 1231
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1231
    target 1100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1578
    target 1100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 878
    target 877
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 878
    target 533
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 878
    target 285
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 878
    target 192
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 878
    target 705
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 192
    target 191
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 390
    target 192
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 877
    target 192
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 285
    target 192
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 857
    target 192
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 533
    target 192
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 192
    target 54
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2336
    target 192
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 285
    target 21
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 377
    target 285
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 672
    target 285
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1734
    target 285
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 877
    target 285
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 533
    target 285
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1734
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 877
    target 533
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 609
    target 608
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 633
    target 608
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 608
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1534
    target 608
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 615
    target 608
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2314
    target 608
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1348
    target 608
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1348
    target 633
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2031
    target 1348
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1348
    target 634
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1348
    target 1091
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1091
    target 634
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1091
    target 635
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2150
    target 1091
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1091
    target 26
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 26
    target 25
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 981
    target 26
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 646
    target 26
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1393
    target 26
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 668
    target 26
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1808
    target 26
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1053
    target 26
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1094
    target 26
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 946
    target 26
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 294
    target 26
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 937
    target 26
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 937
    target 82
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1094
    target 937
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 83
    target 82
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 294
    target 82
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1489
    target 82
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1146
    target 82
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 646
    target 82
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2134
    target 82
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1393
    target 82
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1053
    target 82
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1353
    target 82
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 946
    target 82
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2134
    target 646
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1146
    target 294
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1146
    target 1094
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1994
    target 1146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1393
    target 1146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1994
    target 294
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1393
    target 294
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1094
    target 294
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1053
    target 946
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1094
    target 946
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1370
    target 946
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 946
    target 668
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 946
    target 646
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1094
    target 1053
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1393
    target 1094
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1053
    target 646
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1053
    target 668
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 668
    target 667
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 668
    target 51
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 668
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 668
    target 646
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1283
    target 668
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1298
    target 668
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 668
    target 25
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 981
    target 668
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 981
    target 667
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 667
    target 25
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 647
    target 646
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2232
    target 646
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 635
    target 634
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2162
    target 635
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2162
    target 1269
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2162
    target 176
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2168
    target 2162
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2168
    target 633
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2168
    target 1269
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1269
    target 512
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1346
    target 1269
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1269
    target 633
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1269
    target 1244
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2216
    target 1269
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2216
    target 513
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2216
    target 1346
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2216
    target 512
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 513
    target 512
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1346
    target 513
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1245
    target 1244
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1882
    target 1245
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1346
    target 512
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2314
    target 618
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1534
    target 615
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2347
    target 615
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2443
    target 2347
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1534
    target 609
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1505
    target 633
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1107
    target 633
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 633
    target 609
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1504
    target 633
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1108
    target 633
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1108
    target 1107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1505
    target 1504
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1168
    target 334
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1168
    target 842
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1896
    target 1168
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1168
    target 290
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 167
    target 166
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 596
    target 166
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1147
    target 166
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1055
    target 166
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 469
    target 166
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 186
    target 166
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 437
    target 166
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 290
    target 166
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 265
    target 166
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1147
    target 551
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1147
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1356
    target 1147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 842
    target 437
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1031
    target 437
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 523
    target 437
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 437
    target 160
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 334
    target 290
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1396
    target 1395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1549
    target 1395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1395
    target 655
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1395
    target 1047
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1396
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1549
    target 1249
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1549
    target 1047
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2302
    target 1549
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1249
    target 655
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1249
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1249
    target 1047
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1875
    target 1249
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1875
    target 655
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1048
    target 1047
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 940
    target 935
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1028
    target 935
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1045
    target 935
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 935
    target 200
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1045
    target 200
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1045
    target 154
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1375
    target 1045
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1306
    target 623
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1306
    target 541
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1306
    target 32
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 989
    target 456
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 989
    target 970
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1398
    target 989
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 989
    target 229
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 989
    target 120
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 989
    target 515
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 457
    target 456
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1283
    target 456
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 663
    target 456
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 456
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 980
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 980
    target 515
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 980
    target 229
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 280
    target 125
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 536
    target 125
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2401
    target 125
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2402
    target 2401
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 536
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1378
    target 536
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1862
    target 536
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1738
    target 536
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 536
    target 444
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 445
    target 444
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1060
    target 444
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 455
    target 444
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2096
    target 444
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2106
    target 444
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 444
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2106
    target 1060
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2106
    target 455
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2096
    target 455
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 455
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1060
    target 455
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1060
    target 445
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1738
    target 1128
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1128
    target 1127
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1742
    target 1128
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1378
    target 1008
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1378
    target 314
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 402
    target 260
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 260
    target 256
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 257
    target 256
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 292
    target 256
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 807
    target 256
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 788
    target 787
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1011
    target 788
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 788
    target 396
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1072
    target 788
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 788
    target 671
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 788
    target 670
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1176
    target 788
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1177
    target 1176
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1177
    target 266
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1177
    target 670
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 671
    target 670
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1490
    target 670
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2286
    target 670
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1557
    target 671
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 396
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 718
    target 396
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2197
    target 1011
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1283
    target 1011
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1333
    target 222
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1333
    target 186
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 390
    target 102
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1340
    target 32
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1340
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1340
    target 387
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1390
    target 1340
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1541
    target 1340
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1390
    target 1003
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1541
    target 1390
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1390
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 387
    target 386
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1095
    target 387
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 387
    target 241
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 959
    target 387
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1928
    target 387
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1635
    target 387
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 178
    target 177
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1230
    target 177
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1554
    target 177
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 385
    target 177
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1628
    target 177
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 177
    target 69
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 177
    target 135
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 177
    target 154
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 400
    target 177
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2380
    target 177
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 959
    target 177
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1144
    target 177
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1144
    target 136
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1144
    target 672
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2411
    target 1144
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1144
    target 154
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 136
    target 135
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 154
    target 135
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 672
    target 135
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1554
    target 241
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1423
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1423
    target 996
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 996
    target 995
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1437
    target 996
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 996
    target 959
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 996
    target 567
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1007
    target 996
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 996
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1007
    target 1006
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1007
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1007
    target 602
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1648
    target 1007
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1648
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1648
    target 602
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 603
    target 602
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 602
    target 567
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 629
    target 603
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1006
    target 603
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1789
    target 1006
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2072
    target 1006
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2135
    target 1006
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1339
    target 1006
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1006
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 382
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 557
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 575
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 622
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 381
    target 206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 381
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 381
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 381
    target 279
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1351
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 381
    target 236
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 381
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 565
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 498
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 381
    target 58
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 532
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 680
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1017
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2113
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1140
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 381
    target 272
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1341
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 381
    target 176
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 381
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1467
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2078
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1196
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1189
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 811
    target 381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 381
    target 4
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1196
    target 632
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1196
    target 581
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1335
    target 1196
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1366
    target 1196
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1196
    target 1069
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2218
    target 1196
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2218
    target 1335
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1366
    target 581
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1366
    target 1109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1366
    target 906
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 906
    target 905
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 906
    target 832
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1110
    target 1109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1335
    target 1109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 582
    target 581
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 682
    target 581
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 681
    target 581
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1335
    target 581
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1003
    target 581
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1069
    target 581
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 682
    target 681
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1289
    target 681
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1642
    target 681
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1335
    target 681
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1642
    target 1641
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1642
    target 682
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2374
    target 1289
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1289
    target 682
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1335
    target 682
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1003
    target 682
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 582
    target 541
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2257
    target 582
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2257
    target 541
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 632
    target 458
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 632
    target 28
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 632
    target 31
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 699
    target 632
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1695
    target 632
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1695
    target 458
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1695
    target 488
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2027
    target 1695
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2027
    target 487
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2027
    target 488
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 488
    target 487
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 487
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 699
    target 698
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1674
    target 699
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 898
    target 698
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1101
    target 698
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1101
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1323
    target 1101
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1446
    target 1101
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1918
    target 1101
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1101
    target 341
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 341
    target 340
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 578
    target 341
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 952
    target 341
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 845
    target 341
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1918
    target 341
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1036
    target 341
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1323
    target 341
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 458
    target 341
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1036
    target 1035
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1036
    target 578
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1036
    target 845
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1035
    target 845
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 845
    target 578
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 578
    target 340
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1918
    target 458
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1918
    target 1446
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1918
    target 1323
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1323
    target 458
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 898
    target 458
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 898
    target 117
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 118
    target 117
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 134
    target 117
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 523
    target 117
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1605
    target 117
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1605
    target 1003
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1605
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1605
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2078
    target 917
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2078
    target 982
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2078
    target 1301
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1301
    target 1217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1301
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1301
    target 917
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 982
    target 373
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 917
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 917
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1570
    target 917
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 917
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1570
    target 998
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1570
    target 520
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1468
    target 1467
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1467
    target 564
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1341
    target 1143
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1341
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2113
    target 2086
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2113
    target 1351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2113
    target 812
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2086
    target 812
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 532
    target 531
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 532
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1503
    target 532
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1486
    target 532
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1503
    target 1486
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1503
    target 531
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 531
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 566
    target 565
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 842
    target 566
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2334
    target 566
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2373
    target 566
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1789
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1437
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1437
    target 567
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1437
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1338
    target 365
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1338
    target 1252
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 567
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1357
    target 1034
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1357
    target 563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1357
    target 15
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1357
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1357
    target 161
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 162
    target 161
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 725
    target 161
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1034
    target 161
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1822
    target 161
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2207
    target 161
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2207
    target 669
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2300
    target 1822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1598
    target 6
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 7
    target 6
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 374
    target 6
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1014
    target 6
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 463
    target 6
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1434
    target 6
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 623
    target 374
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1034
    target 370
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2107
    target 1034
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1034
    target 725
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2107
    target 1132
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 725
    target 724
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 725
    target 463
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 973
    target 725
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1105
    target 724
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 67
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 59
    target 58
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 600
    target 59
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 111
    target 59
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 97
    target 59
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1621
    target 59
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1621
    target 600
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1840
    target 1621
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1914
    target 1621
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 98
    target 97
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1021
    target 1020
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1587
    target 1021
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1021
    target 128
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2041
    target 1021
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1021
    target 127
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1693
    target 1021
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1689
    target 1021
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1689
    target 128
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1693
    target 128
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 128
    target 127
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2041
    target 128
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1587
    target 128
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1020
    target 128
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 847
    target 846
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 847
    target 795
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1189
    target 267
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 235
    target 234
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 501
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1162
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1140
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1394
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 346
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1089
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 481
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2220
    target 235
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1394
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1394
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 776
    target 77
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1256
    target 369
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 369
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 629
    target 369
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1345
    target 209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2223
    target 1345
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2227
    target 1345
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1692
    target 1345
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 535
    target 209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1692
    target 209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1753
    target 209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1655
    target 209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2309
    target 209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2316
    target 209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1758
    target 209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 209
    target 38
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 39
    target 38
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1410
    target 38
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 902
    target 38
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 319
    target 38
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 902
    target 901
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1494
    target 902
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1494
    target 1493
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1531
    target 1410
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2189
    target 1410
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2316
    target 2297
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2297
    target 506
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1655
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1070
    target 535
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 535
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1530
    target 535
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1577
    target 535
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1714
    target 535
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 782
    target 535
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1714
    target 1530
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1658
    target 1577
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1577
    target 1242
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1917
    target 1577
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1577
    target 370
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1577
    target 1241
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1242
    target 1241
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1917
    target 1241
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1658
    target 1242
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1917
    target 1658
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1530
    target 1070
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2067
    target 1070
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2067
    target 1952
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2067
    target 1284
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2418
    target 2067
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2067
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2432
    target 2067
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1284
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1284
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1284
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1952
    target 1331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1952
    target 1878
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1952
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1952
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1332
    target 1331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1331
    target 832
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1331
    target 999
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 999
    target 950
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 950
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 950
    target 571
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 950
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 950
    target 342
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 950
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 950
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1375
    target 950
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1690
    target 1332
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1758
    target 1692
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1193
    target 1192
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1317
    target 1193
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1193
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1193
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1193
    target 246
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1193
    target 2
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1193
    target 1132
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1690
    target 1193
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1317
    target 1132
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1192
    target 1132
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1970
    target 1192
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1970
    target 1969
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2367
    target 1970
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1132
    target 246
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 246
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1132
    target 2
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 2
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1718
    target 1717
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1718
    target 1676
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1677
    target 1676
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1717
    target 1677
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1132
    target 288
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1755
    target 1132
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1132
    target 872
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1132
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1755
    target 1690
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1755
    target 473
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 207
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1309
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1608
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 103
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1015
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1724
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 623
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1343
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 818
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1009
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1624
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 225
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2064
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 892
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 854
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 705
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 288
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1885
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1937
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 872
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 556
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1377
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1090
    target 96
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1009
    target 1008
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1343
    target 741
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1724
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1724
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1937
    target 1724
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1724
    target 1550
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1877
    target 389
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1877
    target 1792
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1181
    target 323
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1181
    target 300
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1181
    target 224
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1181
    target 377
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1181
    target 207
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1181
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1181
    target 736
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1792
    target 1181
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2153
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 289
    target 288
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 623
    target 289
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1377
    target 289
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 289
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1008
    target 289
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 818
    target 289
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 289
    target 157
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1740
    target 289
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1015
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1015
    target 705
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1309
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1885
    target 103
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1937
    target 992
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 818
    target 422
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 818
    target 50
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 818
    target 705
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 818
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1055
    target 818
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 50
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 838
    target 50
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 50
    target 18
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1267
    target 50
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1290
    target 50
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 50
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1743
    target 50
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 206
    target 50
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1281
    target 50
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 363
    target 50
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1206
    target 50
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2010
    target 50
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1206
    target 838
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1281
    target 1206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2142
    target 1206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1267
    target 1206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1206
    target 18
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2143
    target 1206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1743
    target 1206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2143
    target 2142
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 225
    target 224
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 872
    target 225
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 705
    target 704
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 854
    target 705
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1379
    target 705
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1014
    target 705
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 876
    target 705
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 705
    target 157
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 705
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1298
    target 705
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1377
    target 705
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1008
    target 705
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 705
    target 51
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 705
    target 207
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 705
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 705
    target 288
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 705
    target 623
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1607
    target 854
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1498
    target 854
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1499
    target 1498
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1607
    target 1499
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1434
    target 563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1434
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 165
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1379
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 71
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 623
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 288
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 876
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 157
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1377
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1155
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1090
    target 71
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1008
    target 493
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1008
    target 157
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1008
    target 589
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1090
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1090
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 342
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 357
    target 342
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 342
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 571
    target 342
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1218
    target 342
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 352
    target 342
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2075
    target 342
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 342
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 382
    target 342
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 506
    target 342
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 520
    target 342
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 718
    target 342
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 353
    target 342
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 571
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 571
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1134
    target 571
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2097
    target 571
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 571
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2098
    target 2097
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1134
    target 666
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1288
    target 1134
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1283
    target 1134
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1134
    target 782
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1162
    target 1134
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1134
    target 432
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1375
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1375
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 520
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 520
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 520
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 520
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 614
    target 520
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 520
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 916
    target 520
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 978
    target 520
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 520
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 614
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 638
    target 614
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 614
    target 185
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 614
    target 186
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 186
    target 185
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 229
    target 185
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1855
    target 185
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1855
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1855
    target 1336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1855
    target 765
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1855
    target 186
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1218
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 353
    target 352
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 382
    target 352
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 352
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 357
    target 352
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 718
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1439
    target 718
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1580
    target 718
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 718
    target 133
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 718
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 916
    target 718
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2313
    target 718
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 718
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1580
    target 394
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1439
    target 703
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1468
    target 1439
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1439
    target 498
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 703
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1223
    target 703
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 703
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 703
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 703
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1316
    target 703
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 802
    target 703
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 803
    target 802
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 912
    target 802
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 802
    target 302
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 913
    target 912
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 912
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 69
    target 68
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1274
    target 69
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 429
    target 69
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 825
    target 69
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 385
    target 69
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1230
    target 69
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 430
    target 69
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1628
    target 69
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1635
    target 69
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 430
    target 429
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1274
    target 430
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 430
    target 68
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 825
    target 430
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 825
    target 68
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 825
    target 429
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1274
    target 825
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2082
    target 825
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2082
    target 1274
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2082
    target 68
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1750
    target 429
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 429
    target 68
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1274
    target 429
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1274
    target 68
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 707
    target 54
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 707
    target 463
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1774
    target 707
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 707
    target 659
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 787
    target 707
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 783
    target 707
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2245
    target 707
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1495
    target 707
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 707
    target 591
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1680
    target 707
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1680
    target 555
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1680
    target 772
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2245
    target 165
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2325
    target 1774
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1628
    target 154
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1628
    target 1230
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1628
    target 385
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 178
    target 154
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 672
    target 136
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 672
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 696
    target 672
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 672
    target 170
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1354
    target 672
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 672
    target 573
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 386
    target 385
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 386
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 386
    target 241
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 154
    target 136
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 385
    target 136
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1635
    target 136
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1635
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1635
    target 1582
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1635
    target 385
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1582
    target 959
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1582
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1496
    target 385
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1230
    target 385
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 696
    target 385
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 292
    target 280
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 807
    target 292
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 402
    target 292
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1151
    target 292
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 959
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 959
    target 154
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1230
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1016
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1207
    target 777
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 777
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1203
    target 130
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 573
    target 130
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1564
    target 130
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1564
    target 1203
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1944
    target 1564
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1564
    target 1217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1564
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 778
    target 453
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 778
    target 589
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 885
    target 778
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 778
    target 259
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 778
    target 323
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 778
    target 301
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 778
    target 575
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2132
    target 778
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 778
    target 324
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 778
    target 284
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 778
    target 772
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 778
    target 279
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 885
    target 772
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 772
    target 453
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 772
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 772
    target 307
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 259
    target 258
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 284
    target 259
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 885
    target 259
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 454
    target 259
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 453
    target 259
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 301
    target 259
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 300
    target 259
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1130
    target 259
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1569
    target 259
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 323
    target 259
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 454
    target 258
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 300
    target 258
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 301
    target 300
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 453
    target 301
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 301
    target 284
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 324
    target 301
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1130
    target 301
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 301
    target 230
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 323
    target 301
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 454
    target 301
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 813
    target 301
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1569
    target 301
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 885
    target 301
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2389
    target 301
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 324
    target 323
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 323
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 454
    target 323
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 323
    target 284
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 323
    target 300
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 813
    target 323
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1569
    target 323
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1298
    target 323
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1283
    target 323
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 540
    target 323
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 453
    target 323
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 454
    target 453
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 453
    target 324
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 453
    target 284
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 885
    target 453
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1569
    target 453
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 885
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 885
    target 575
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 885
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 200
    target 77
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 300
    target 284
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 324
    target 300
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 813
    target 300
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 813
    target 284
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1978
    target 284
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1569
    target 284
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1716
    target 284
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 709
    target 284
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1716
    target 652
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 400
    target 324
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 454
    target 324
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 709
    target 324
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1569
    target 324
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1569
    target 454
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1130
    target 813
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1712
    target 1637
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1637
    target 477
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1712
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1712
    target 624
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1729
    target 1712
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1712
    target 741
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1712
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1712
    target 742
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1729
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1729
    target 624
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1729
    target 383
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 742
    target 741
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 742
    target 624
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 443
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 629
    target 443
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1354
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1354
    target 629
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1354
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1256
    target 11
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1256
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1256
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1256
    target 629
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 926
    target 741
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 926
    target 477
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 926
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 926
    target 624
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 926
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 926
    target 768
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 11
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 395
    target 11
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 477
    target 11
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 805
    target 11
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 344
    target 11
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 805
    target 629
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1276
    target 1151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1932
    target 1276
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 685
    target 684
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 835
    target 684
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 684
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 684
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1055
    target 684
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1215
    target 684
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 684
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 684
    target 422
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 684
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 684
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 684
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 684
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 684
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 684
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 685
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 685
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 685
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 685
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 685
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 685
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 835
    target 685
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 37
    target 36
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 189
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 87
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 262
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 263
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1162
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 295
    target 37
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 263
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 263
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 211
    target 36
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1811
    target 36
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1811
    target 835
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1811
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1055
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1407
    target 1055
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1407
    target 422
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1407
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 835
    target 558
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 295
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 590
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 943
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 955
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 100
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 393
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 591
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1171
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1893
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 100
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 179
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 563
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 211
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1197
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2423
    target 100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1197
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1197
    target 591
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1197
    target 755
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 393
    target 392
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1073
    target 393
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1059
    target 393
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2315
    target 393
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 658
    target 393
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1280
    target 393
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 563
    target 393
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1280
    target 1279
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1280
    target 658
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1279
    target 688
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 658
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1073
    target 1059
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1215
    target 955
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 955
    target 755
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 955
    target 91
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 955
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 943
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 87
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 188
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 295
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 189
    target 87
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1447
    target 188
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 295
    target 179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 754
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 295
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 295
    target 174
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 295
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1749
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1756
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1773
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1784
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1867
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1879
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1290
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1998
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1690
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 295
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2140
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 295
    target 189
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2433
    target 295
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 211
    target 210
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 807
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 211
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1215
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 211
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2311
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 262
    target 211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2311
    target 652
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 198
    target 197
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 790
    target 197
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 974
    target 197
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1349
    target 197
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 791
    target 197
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 467
    target 197
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 791
    target 790
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 974
    target 791
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1349
    target 791
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 791
    target 198
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 791
    target 467
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 974
    target 198
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 974
    target 790
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 974
    target 467
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1349
    target 974
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 790
    target 467
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 790
    target 198
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1349
    target 790
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 91
    target 90
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 855
    target 90
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1215
    target 90
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 90
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1834
    target 855
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1834
    target 1491
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1215
    target 558
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 558
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 558
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 755
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1901
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 591
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1212
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2228
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2265
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1296
    target 336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1296
    target 755
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1296
    target 91
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1013
    target 952
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 952
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 952
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1031
    target 952
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 543
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1093
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1825
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 86
    target 81
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1825
    target 1215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1962
    target 1093
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1093
    target 591
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2354
    target 1962
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 543
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1212
    target 91
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1283
    target 91
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 755
    target 91
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 540
    target 91
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 591
    target 91
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 91
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1215
    target 591
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 1215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1215
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1283
    target 541
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1283
    target 892
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1283
    target 314
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1283
    target 266
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1283
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 592
    target 591
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 591
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1636
    target 591
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 755
    target 591
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 822
    target 591
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1155
    target 592
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1857
    target 592
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 592
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1941
    target 592
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2094
    target 592
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 592
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 592
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2304
    target 592
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 592
    target 29
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1857
    target 626
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 626
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1523
    target 626
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 626
    target 202
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 202
    target 201
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 413
    target 202
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 331
    target 202
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 313
    target 202
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 314
    target 202
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1048
    target 202
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2310
    target 202
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 314
    target 313
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1520
    target 313
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1523
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2073
    target 1814
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2073
    target 1050
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 975
    target 971
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 975
    target 677
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 975
    target 630
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 975
    target 162
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 975
    target 454
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 677
    target 676
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1388
    target 677
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2214
    target 677
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 857
    target 677
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2215
    target 2214
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2341
    target 2214
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2341
    target 2215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2359
    target 2341
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2341
    target 1388
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1389
    target 1388
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1388
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 971
    target 162
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 971
    target 630
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 971
    target 454
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 709
    target 454
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 454
    target 162
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 630
    target 162
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 997
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2028
    target 997
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2436
    target 997
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 752
    target 280
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 506
    target 280
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1480
    target 280
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1151
    target 280
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1819
    target 280
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 280
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2016
    target 280
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 659
    target 280
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2016
    target 63
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1480
    target 63
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1105
    target 1104
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1105
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1805
    target 1105
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1806
    target 1805
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1500
    target 170
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1103
    target 384
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1103
    target 842
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2079
    target 1103
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1487
    target 758
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1487
    target 842
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1155
    target 769
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 769
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 842
    target 54
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 842
    target 383
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 842
    target 579
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 842
    target 216
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 842
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 842
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 216
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 384
    target 383
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 579
    target 384
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2262
    target 384
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1150
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1237
    target 1150
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1632
    target 1150
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1859
    target 1150
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1150
    target 165
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1150
    target 45
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1150
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 46
    target 45
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1859
    target 45
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 165
    target 45
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1859
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1859
    target 807
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1859
    target 165
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1632
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1746
    target 1632
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 624
    target 383
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 383
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 741
    target 383
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 857
    target 589
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 857
    target 450
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 857
    target 257
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 857
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 857
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 807
    target 806
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1611
    target 807
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1800
    target 807
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 807
    target 504
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2116
    target 807
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1545
    target 807
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 807
    target 54
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 963
    target 806
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 784
    target 589
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 784
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 784
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1701
    target 706
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1897
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2284
    target 21
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1984
    target 1715
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 22
    target 21
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1363
    target 977
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 93
    target 92
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 93
    target 54
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1247
    target 21
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 977
    target 21
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 641
    target 640
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 640
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 640
    target 54
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1085
    target 641
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1339
    target 977
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1339
    target 92
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 977
    target 92
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 206
    target 92
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 395
    target 54
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 54
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 977
    target 54
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 120
    target 54
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 286
    target 54
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 344
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 468
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 768
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 629
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 477
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1336
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 395
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1444
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 741
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 624
    target 10
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1444
    target 477
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1335
    target 1155
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1792
    target 1335
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1335
    target 1003
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1383
    target 1335
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 165
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1023
    target 165
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 787
    target 165
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 482
    target 165
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2149
    target 165
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2410
    target 165
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2179
    target 165
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1473
    target 589
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1865
    target 1473
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 120
    target 119
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1151
    target 120
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 856
    target 120
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 193
    target 120
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 194
    target 193
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 193
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1786
    target 193
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1797
    target 193
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1797
    target 856
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1797
    target 1155
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1797
    target 659
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1797
    target 241
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1786
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1786
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1786
    target 570
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 856
    target 119
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 663
    target 1
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 957
    target 663
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1764
    target 663
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 859
    target 663
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1521
    target 663
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1521
    target 363
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2376
    target 669
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 607
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 607
    target 481
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2326
    target 607
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 607
    target 394
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2326
    target 431
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2326
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2254
    target 128
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 547
    target 128
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 40
    target 18
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1281
    target 18
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 838
    target 18
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 859
    target 18
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1743
    target 18
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 738
    target 659
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 738
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 819
    target 738
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 782
    target 738
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 822
    target 735
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 944
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 998
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 822
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1592
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 923
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 872
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1071
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 822
    target 651
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 822
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1014
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 862
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 822
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1792
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 822
    target 804
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 822
    target 736
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2272
    target 822
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 822
    target 404
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 822
    target 288
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 822
    target 224
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 822
    target 623
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1592
    target 1069
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1890
    target 1326
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2277
    target 1890
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1326
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1326
    target 333
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 884
    target 575
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 884
    target 794
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 979
    target 884
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 979
    target 862
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 979
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 979
    target 377
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 979
    target 659
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2212
    target 979
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 701
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 701
    target 659
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 859
    target 858
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 860
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1172
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 859
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 919
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1417
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1472
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1538
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2007
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2050
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2053
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1541
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2200
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2184
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2061
    target 859
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 859
    target 413
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2184
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2054
    target 2053
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2053
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2054
    target 860
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1538
    target 1298
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1538
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2209
    target 1172
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 860
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1013
    target 457
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1013
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1758
    target 1643
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1643
    target 691
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2001
    target 1151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2340
    target 2001
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1793
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1567
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1639
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1706
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1785
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1916
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1982
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 575
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2378
    target 262
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 262
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1706
    target 189
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 530
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 925
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 334
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1237
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 709
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1427
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 694
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1458
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 923
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1545
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2152
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 524
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1988
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 457
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 665
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2172
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2428
    target 7
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2172
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 665
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2152
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 695
    target 694
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 694
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1155
    target 694
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 694
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 925
    target 286
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 925
    target 113
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1847
    target 113
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1723
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1723
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1723
    target 415
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 415
    target 414
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2206
    target 415
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 414
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 414
    target 183
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2070
    target 414
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2070
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 184
    target 183
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2173
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2179
    target 787
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2179
    target 482
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1023
    target 186
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1023
    target 794
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 829
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 469
    target 468
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 477
    target 468
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 468
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 624
    target 468
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 468
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 666
    target 468
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 395
    target 394
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 402
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1285
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1397
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 768
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 435
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 395
    target 218
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2040
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 477
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 741
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 624
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 431
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 395
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 436
    target 395
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1419
    target 1397
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1397
    target 988
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 988
    target 987
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2065
    target 1419
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2144
    target 1419
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1419
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 419
    target 162
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2409
    target 162
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 815
    target 814
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1913
    target 1562
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1913
    target 1563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1563
    target 1562
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1619
    target 1563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1563
    target 726
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1563
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1620
    target 1619
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2182
    target 1620
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1418
    target 782
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1562
    target 1418
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1586
    target 1435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1669
    target 1435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1866
    target 1435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1968
    target 1435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2042
    target 1435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2226
    target 1435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2253
    target 1435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2271
    target 1435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2327
    target 1435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2429
    target 1435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2440
    target 1435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2327
    target 2042
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2042
    target 1866
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2269
    target 726
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1562
    target 726
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2269
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 787
    target 482
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 484
    target 482
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1782
    target 482
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2397
    target 482
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 484
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1380
    target 484
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1170
    target 484
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1868
    target 484
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 484
    target 115
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1868
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1868
    target 115
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1868
    target 1596
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1868
    target 1380
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1381
    target 1380
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1380
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1380
    target 115
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1380
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1380
    target 1170
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 695
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 856
    target 194
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 856
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 856
    target 364
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 856
    target 478
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 496
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 496
    target 394
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 496
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 496
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 496
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2128
    target 496
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 506
    target 496
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 496
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 496
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 916
    target 496
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 496
    target 435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2128
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 436
    target 435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 789
    target 436
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1351
    target 436
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 436
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 436
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1491
    target 436
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 442
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 442
    target 435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 442
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 442
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 442
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1139
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1139
    target 1125
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1139
    target 435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1139
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 378
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 378
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 378
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 617
    target 378
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 378
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 378
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 378
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 431
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 431
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 431
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 431
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 431
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 431
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 431
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1125
    target 431
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 431
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 431
    target 333
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1201
    target 431
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1201
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1201
    target 394
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1201
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1224
    target 1201
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1201
    target 657
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1224
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1224
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1224
    target 676
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 617
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 617
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 617
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 617
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 676
    target 435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2022
    target 676
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 147
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 264
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 402
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 916
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 357
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 435
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1403
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 394
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 353
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 657
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1615
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 147
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 978
    target 147
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1615
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1615
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1615
    target 978
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 108
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 146
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1125
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 457
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1552
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 218
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 357
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2104
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 435
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 264
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 176
    target 107
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2104
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1552
    target 1165
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1552
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1950
    target 1552
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1552
    target 1518
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1518
    target 1165
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1518
    target 1164
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1165
    target 1164
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1226
    target 1165
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1165
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 63
    target 16
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1727
    target 63
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1553
    target 63
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2047
    target 63
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 264
    target 63
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2250
    target 63
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1727
    target 1557
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1727
    target 16
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1287
    target 16
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1557
    target 16
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1287
    target 746
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 746
    target 745
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 746
    target 584
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1337
    target 746
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 746
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 746
    target 673
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1924
    target 746
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 746
    target 15
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1810
    target 746
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1810
    target 673
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2348
    target 1810
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1924
    target 674
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1924
    target 1111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 674
    target 673
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 674
    target 204
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1337
    target 1336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1337
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1337
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 745
    target 462
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 506
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1350
    target 796
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 916
    target 796
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 796
    target 394
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1125
    target 657
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1125
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1125
    target 394
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 435
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 394
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 481
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 881
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 357
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 402
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 357
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 506
    target 357
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 916
    target 160
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 916
    target 394
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 916
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 916
    target 506
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 916
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 916
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1579
    target 916
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1873
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 506
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 435
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 394
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 264
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 146
    target 108
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 394
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 394
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1986
    target 394
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 402
    target 394
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 881
    target 435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 881
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 506
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 657
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 402
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1720
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 402
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2080
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 402
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1673
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 435
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1403
    target 402
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1673
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1673
    target 1541
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 164
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 659
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1315
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1430
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1741
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1974
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 427
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2032
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1748
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2060
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1450
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1626
    target 163
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1626
    target 1315
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1747
    target 1626
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1748
    target 1626
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1626
    target 1056
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1626
    target 1057
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1057
    target 1056
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1747
    target 1057
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1748
    target 1747
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2060
    target 327
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2060
    target 794
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 327
    target 326
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1450
    target 1315
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1315
    target 164
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1450
    target 164
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 657
    target 435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 435
    target 218
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1124
    target 435
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 435
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 435
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 783
    target 782
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 783
    target 573
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1872
    target 783
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 783
    target 696
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 794
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 804
    target 794
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1782
    target 794
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1841
    target 794
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 794
    target 787
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 794
    target 417
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 794
    target 680
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1872
    target 573
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1872
    target 696
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 696
    target 573
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1210
    target 573
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2238
    target 1285
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 1285
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1155
    target 1004
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1155
    target 40
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1155
    target 206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1951
    target 1155
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1155
    target 968
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1155
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2149
    target 1155
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2308
    target 1155
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2328
    target 1155
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1951
    target 621
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1951
    target 1452
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1951
    target 524
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1951
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 51
    target 32
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 827
    target 51
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1459
    target 51
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 266
    target 51
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 314
    target 51
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 892
    target 51
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1645
    target 51
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 541
    target 51
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1459
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1459
    target 540
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1459
    target 1298
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 827
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1237
    target 457
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1237
    target 563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1458
    target 563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1458
    target 457
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 469
    target 167
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1647
    target 469
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 331
    target 330
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 353
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1229
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1342
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1031
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1681
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1233
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1696
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1188
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1988
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2039
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1149
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2210
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1209
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2270
    target 331
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 331
    target 40
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 331
    target 109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2039
    target 1031
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2039
    target 660
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2039
    target 1233
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1031
    target 660
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1233
    target 660
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 660
    target 540
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 869
    target 660
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1968
    target 660
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1149
    target 660
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1233
    target 1031
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1233
    target 15
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1233
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1233
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 931
    target 930
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1298
    target 931
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 931
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 931
    target 457
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 505
    target 504
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 505
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 639
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1267
    target 838
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1281
    target 838
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1743
    target 838
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 266
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1398
    target 266
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 266
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 353
    target 266
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 266
    target 251
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1298
    target 266
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 540
    target 266
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2337
    target 266
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1743
    target 1267
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1281
    target 1267
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 978
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 353
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1938
    target 353
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 353
    target 160
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2121
    target 1938
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1938
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 832
    target 156
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1048
    target 832
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 832
    target 596
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 467
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 257
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 363
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 156
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 237
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1787
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 978
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 933
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 466
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 865
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 618
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 236
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2386
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2406
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2417
    target 49
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 467
    target 466
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 466
    target 257
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 466
    target 363
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1100
    target 466
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 933
    target 932
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 933
    target 808
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 933
    target 570
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 933
    target 688
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1216
    target 932
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1216
    target 688
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1948
    target 1216
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1221
    target 1216
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1216
    target 236
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1216
    target 808
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1221
    target 775
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1221
    target 1209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1221
    target 1149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1221
    target 252
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1221
    target 150
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 253
    target 252
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 253
    target 214
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 215
    target 214
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 214
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 501
    target 214
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1157
    target 214
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1087
    target 214
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 214
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1158
    target 214
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 214
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2392
    target 214
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1158
    target 1157
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2018
    target 1157
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1157
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1948
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 865
    target 156
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1542
    target 156
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1760
    target 1209
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1760
    target 1148
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1149
    target 1148
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 303
    target 302
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 302
    target 14
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1059
    target 302
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1077
    target 302
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 618
    target 302
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1768
    target 302
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1502
    target 302
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 302
    target 29
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2100
    target 302
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2240
    target 302
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1502
    target 570
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1502
    target 803
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 15
    target 14
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 564
    target 14
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2280
    target 14
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1887
    target 14
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2400
    target 14
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2400
    target 1887
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 515
    target 229
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 970
    target 515
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1543
    target 515
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 229
    target 228
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1086
    target 229
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1745
    target 229
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 229
    target 156
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 236
    target 229
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 970
    target 229
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1086
    target 413
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 803
    target 73
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1746
    target 803
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1145
    target 570
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 808
    target 570
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1059
    target 570
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 570
    target 236
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 570
    target 413
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 570
    target 303
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 570
    target 156
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1077
    target 570
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 688
    target 570
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 314
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 511
    target 314
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 540
    target 314
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1485
    target 314
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1298
    target 314
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1048
    target 314
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1145
    target 314
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2361
    target 314
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2395
    target 314
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1943
    target 1069
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1173
    target 862
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 862
    target 377
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1173
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1173
    target 894
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 44
    target 43
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 836
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 915
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1194
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 840
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1270
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 589
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1512
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 872
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1766
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1803
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1923
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1934
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 239
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 257
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 363
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 450
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1140
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1207
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 46
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 370
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 241
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2285
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 382
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2296
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2323
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 486
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 623
    target 44
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 486
    target 485
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1803
    target 418
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1544
    target 43
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2131
    target 1508
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 998
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 457
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1314
    target 457
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 463
    target 457
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 819
    target 457
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1981
    target 457
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1298
    target 457
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2237
    target 457
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2237
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1981
    target 365
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1252
    target 29
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1861
    target 1252
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1252
    target 1171
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2049
    target 1252
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2157
    target 1252
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 797
    target 470
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 797
    target 109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 471
    target 470
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 629
    target 470
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 470
    target 365
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 470
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 143
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1589
    target 322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 599
    target 598
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 599
    target 321
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 599
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2015
    target 599
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2015
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1596
    target 322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1596
    target 1170
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1596
    target 115
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1596
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1170
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1170
    target 115
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1170
    target 322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 504
    target 322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 504
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1171
    target 322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1371
    target 1171
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1371
    target 548
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 549
    target 548
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 116
    target 115
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 217
    target 115
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 319
    target 115
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 322
    target 321
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 321
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1277
    target 322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 400
    target 399
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 458
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 542
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 765
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 839
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1046
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1058
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1081
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1195
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1329
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1368
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1511
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1527
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1609
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1726
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1759
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1796
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1809
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1899
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1769
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1999
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 413
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2099
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2119
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2151
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2241
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2260
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2276
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 400
    target 322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2318
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2357
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 400
    target 186
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2396
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2403
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1988
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2435
    target 400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2318
    target 1961
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2256
    target 1809
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1558
    target 1368
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 839
    target 477
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 339
    target 338
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1156
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1540
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 339
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 373
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1935
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 598
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 628
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 339
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 709
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1737
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2275
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2339
    target 339
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 339
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1737
    target 575
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1737
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1540
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 338
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1536
    target 182
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 182
    target 181
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 598
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 506
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 506
    target 176
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1381
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1381
    target 307
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2193
    target 1381
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 659
    target 176
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 659
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 659
    target 473
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1616
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1599
    target 29
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2019
    target 29
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2251
    target 1599
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 279
    target 176
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1591
    target 279
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 279
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2161
    target 279
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2252
    target 279
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2320
    target 279
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2356
    target 279
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2161
    target 377
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2161
    target 176
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2161
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1690
    target 102
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1261
    target 563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1852
    target 563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 563
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 563
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1336
    target 563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 563
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 563
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 966
    target 189
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 596
    target 189
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1162
    target 189
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 377
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 377
    target 354
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 373
    target 334
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2191
    target 373
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1088
    target 373
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1088
    target 305
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1088
    target 226
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1977
    target 1088
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1088
    target 227
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 227
    target 226
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 305
    target 227
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1977
    target 227
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1977
    target 226
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 305
    target 226
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1880
    target 102
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 102
    target 101
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 708
    target 102
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 709
    target 708
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1106
    target 708
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 525
    target 101
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1486
    target 1207
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1207
    target 564
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2298
    target 174
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 174
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1126
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1126
    target 422
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1324
    target 1217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1956
    target 1217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1217
    target 589
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1956
    target 1324
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1804
    target 1324
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 371
    target 370
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 417
    target 370
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 418
    target 370
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 370
    target 355
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 820
    target 371
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 820
    target 819
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 820
    target 281
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1889
    target 820
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2092
    target 820
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 820
    target 298
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2092
    target 2017
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2017
    target 298
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 575
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 354
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2000
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 493
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1451
    target 239
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 239
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2048
    target 1452
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1889
    target 299
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 299
    target 298
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 819
    target 299
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 281
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 463
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 428
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 298
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1963
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 819
    target 86
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 272
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 176
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 83
    target 34
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 841
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1017
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1143
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1355
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 137
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1468
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1687
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 811
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 346
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1189
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1887
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 382
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 206
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2093
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2141
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1140
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 978
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 622
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 557
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 575
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 83
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 334
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2248
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1351
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1486
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2391
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1772
    target 83
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1772
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2248
    target 2188
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2188
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 841
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 841
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1486
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1486
    target 557
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1486
    target 1468
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1486
    target 34
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1546
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2133
    target 1546
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1631
    target 1546
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1631
    target 1506
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1631
    target 476
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 477
    target 476
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1211
    target 498
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1211
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1211
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1211
    target 206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1211
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1211
    target 564
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1468
    target 564
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1468
    target 653
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1468
    target 1351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 382
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 89
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1140
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 176
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 137
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2091
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1351
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 206
    target 89
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 89
    target 34
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 653
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 812
    target 653
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1351
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 206
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 56
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 575
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 56
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 151
    target 56
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 5
    target 4
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 206
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 811
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1189
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1351
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 812
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 137
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 272
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1017
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 498
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 382
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1887
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1140
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 622
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 575
    target 5
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 973
    target 334
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1351
    target 622
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 622
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1099
    target 557
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 564
    target 557
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1099
    target 1098
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1099
    target 575
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1794
    target 575
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1917
    target 575
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 575
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 618
    target 575
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 575
    target 564
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1141
    target 1140
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1140
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1143
    target 1140
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1140
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1140
    target 176
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1140
    target 186
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1141
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 350
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 537
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 779
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 949
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1003
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1255
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1456
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1790
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1837
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2050
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1987
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 351
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2139
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2174
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1617
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2061
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2363
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2370
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 564
    target 351
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1617
    target 355
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1987
    target 481
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2332
    target 1987
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1351
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1351
    target 564
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1351
    target 812
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1351
    target 498
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1351
    target 1017
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 812
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 960
    target 812
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1429
    target 812
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1838
    target 812
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1838
    target 206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1429
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2190
    target 1429
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2190
    target 1257
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1257
    target 206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1485
    target 811
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 811
    target 555
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 138
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1320
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1415
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1568
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1757
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1802
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 137
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1989
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2011
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2077
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1973
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2008
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1143
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 451
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2362
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 564
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 498
    target 137
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 452
    target 451
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2009
    target 2008
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2008
    target 1528
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1529
    target 1528
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2009
    target 1528
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2235
    target 1528
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2235
    target 2009
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2077
    target 782
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1757
    target 688
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 498
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 824
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 978
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1704
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1042
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 382
    target 247
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1954
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2061
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1143
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 782
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1014
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2050
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 382
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 382
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 564
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 382
    target 206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 481
    target 382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 176
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 564
    target 176
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1761
    target 176
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2368
    target 176
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 35
    target 34
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 564
    target 34
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 151
    target 34
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 206
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 206
    target 151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 498
    target 206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 564
    target 206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1003
    target 206
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 412
    target 411
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 411
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 412
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2217
    target 1813
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1391
    target 1355
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1946
    target 1355
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1355
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1355
    target 1078
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1507
    target 1391
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1946
    target 1391
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1391
    target 572
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1507
    target 572
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 765
    target 85
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1191
    target 765
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1336
    target 765
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 782
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 589
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 309
    target 60
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1452
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 375
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 970
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2317
    target 309
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1078
    target 739
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 471
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 471
    target 218
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 768
    target 471
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 629
    target 471
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 471
    target 333
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 471
    target 109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 471
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1210
    target 471
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 547
    target 218
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 768
    target 547
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 781
    target 547
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1275
    target 547
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 741
    target 547
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 624
    target 547
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 547
    target 364
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 547
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1403
    target 547
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1210
    target 547
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2208
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2208
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2208
    target 365
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 365
    target 364
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 526
    target 365
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1403
    target 365
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1683
    target 365
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1684
    target 365
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1210
    target 365
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2058
    target 365
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 365
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1684
    target 1683
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1684
    target 624
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1684
    target 741
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1684
    target 768
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1684
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1683
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 526
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 526
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1210
    target 218
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1210
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1210
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1403
    target 1210
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1210
    target 109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 624
    target 218
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1253
    target 624
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 741
    target 624
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 768
    target 624
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1832
    target 624
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 624
    target 477
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1832
    target 1831
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1142
    target 572
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 700
    target 572
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1946
    target 572
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 243
    target 242
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1403
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 243
    target 218
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 243
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1992
    target 243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 109
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 922
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1403
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1143
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 218
    target 42
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 768
    target 741
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 741
    target 477
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2441
    target 741
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 768
    target 477
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 477
    target 218
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1120
    target 218
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1482
    target 218
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2437
    target 218
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1483
    target 1482
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2394
    target 1482
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1121
    target 1120
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1258
    target 1120
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1120
    target 521
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 522
    target 521
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1258
    target 521
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1258
    target 522
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1667
    target 1121
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1545
    target 1243
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 774
    target 773
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1545
    target 773
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1136
    target 773
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1136
    target 774
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1545
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1545
    target 40
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 739
    target 540
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 739
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 739
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 344
    target 212
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1447
    target 322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1447
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1447
    target 85
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1447
    target 1077
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 782
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 782
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2366
    target 1746
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 432
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 432
    target 160
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 523
    target 432
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 160
    target 159
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 217
    target 160
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1226
    target 160
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 160
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1398
    target 160
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 523
    target 159
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 159
    target 40
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 405
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 1304
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 760
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1479
    target 1400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1400
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2439
    target 1400
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1776
    target 1479
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1304
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1878
    target 992
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 85
    target 84
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 286
    target 85
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 85
    target 32
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1551
    target 85
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1076
    target 85
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2249
    target 85
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2372
    target 85
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2051
    target 1961
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 831
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1077
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 523
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 217
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 319
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 264
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 478
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 322
    target 217
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1115
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 555
    target 151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 151
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 564
    target 151
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 629
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1884
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 477
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1940
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1965
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1968
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2120
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2225
    target 344
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1884
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 964
    target 963
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1730
    target 963
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2148
    target 963
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2155
    target 963
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1911
    target 363
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1945
    target 363
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 363
    target 257
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 589
    target 257
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1942
    target 623
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 236
    target 156
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1462
    target 156
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 303
    target 156
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1585
    target 1462
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2004
    target 1462
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2095
    target 1462
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2004
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1645
    target 1585
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1585
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2088
    target 767
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 767
    target 766
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 808
    target 767
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2426
    target 767
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1769
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1250
    target 920
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1427
    target 333
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1427
    target 1031
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1427
    target 588
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1327
    target 477
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 629
    target 477
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 419
    target 418
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 908
    target 907
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1425
    target 908
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1425
    target 907
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1705
    target 1425
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2388
    target 1425
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1705
    target 874
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 907
    target 874
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1077
    target 222
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1077
    target 322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1077
    target 72
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1077
    target 920
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1996
    target 1077
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1077
    target 948
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1996
    target 73
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2230
    target 1996
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1302
    target 611
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1302
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1302
    target 787
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1302
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1302
    target 236
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 808
    target 688
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 808
    target 621
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 642
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 688
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1043
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 319
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 322
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1791
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 319
    target 186
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1856
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 901
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1226
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2124
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 628
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1909
    target 319
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 758
    target 757
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 848
    target 758
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1958
    target 758
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 758
    target 450
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1958
    target 757
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1958
    target 450
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 759
    target 757
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1061
    target 759
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1236
    target 759
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 611
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1610
    target 611
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1044
    target 611
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 901
    target 611
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2335
    target 611
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2427
    target 611
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1920
    target 977
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1991
    target 977
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2145
    target 977
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 977
    target 450
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2442
    target 977
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1336
    target 186
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 186
    target 58
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1909
    target 1876
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2279
    target 1336
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 222
    target 221
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 251
    target 222
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 222
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1421
    target 222
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1421
    target 251
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1076
    target 688
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1048
    target 688
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1145
    target 688
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1043
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 712
    target 450
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 589
    target 450
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2034
    target 315
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2434
    target 315
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 304
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1018
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1064
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 628
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 123
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2014
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2071
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2170
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 322
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 501
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1282
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2352
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2384
    target 123
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2194
    target 1018
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2194
    target 1026
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1264
    target 304
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 236
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2414
    target 116
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 948
    target 73
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1818
    target 948
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2234
    target 1818
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 555
    target 554
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2005
    target 883
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 883
    target 657
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 883
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 730
    target 446
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2158
    target 1840
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1840
    target 307
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1840
    target 600
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1840
    target 346
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1840
    target 1089
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1644
    target 691
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1926
    target 691
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1926
    target 1644
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1017
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1017
    target 4
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1017
    target 272
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1017
    target 498
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1017
    target 441
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 35
    target 4
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 441
    target 4
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 600
    target 307
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 600
    target 346
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1089
    target 600
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1288
    target 441
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1398
    target 1288
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1298
    target 1288
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 588
    target 498
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 498
    target 441
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 498
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 272
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 441
    target 272
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2036
    target 1005
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 346
    target 307
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1089
    target 307
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 58
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 680
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 346
    target 35
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 743
    target 427
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 743
    target 219
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 220
    target 219
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 427
    target 220
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 427
    target 150
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 427
    target 219
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 501
    target 427
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1089
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1089
    target 1087
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1089
    target 346
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 346
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1985
    target 346
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1087
    target 346
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 501
    target 219
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 219
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 219
    target 150
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1557
    target 893
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1557
    target 1087
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 894
    target 893
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 893
    target 584
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2307
    target 893
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 962
    target 893
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 894
    target 584
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1111
    target 584
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 584
    target 462
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 584
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 584
    target 204
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 962
    target 894
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 894
    target 204
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1111
    target 894
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 894
    target 462
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1915
    target 12
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 13
    target 12
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1012
    target 13
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 764
    target 763
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 775
    target 764
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1190
    target 764
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1209
    target 764
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 919
    target 764
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1260
    target 764
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 764
    target 150
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 628
    target 322
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1845
    target 1087
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1845
    target 204
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1149
    target 271
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1149
    target 919
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1149
    target 74
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1149
    target 1031
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1209
    target 1149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1209
    target 596
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1209
    target 271
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1209
    target 919
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 919
    target 271
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 919
    target 74
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1191
    target 271
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1191
    target 74
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1190
    target 271
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1190
    target 150
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1732
    target 1190
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1259
    target 1190
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1190
    target 74
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1260
    target 1259
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1259
    target 150
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 775
    target 74
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 775
    target 271
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 763
    target 74
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 763
    target 271
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 75
    target 74
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2115
    target 15
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 150
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 271
    target 150
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 150
    target 74
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1743
    target 1281
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1260
    target 271
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1685
    target 1260
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1260
    target 204
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1260
    target 74
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2055
    target 462
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1111
    target 462
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2055
    target 1111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 271
    target 74
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1645
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1485
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1485
    target 596
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 619
    target 618
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 973
    target 619
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2393
    target 1606
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 73
    target 72
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2263
    target 73
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1553
    target 204
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 481
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1082
    target 481
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1600
    target 481
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2358
    target 481
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 870
    target 869
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 523
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 264
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 364
    target 146
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 282
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1665
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 958
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 199
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1452
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 992
    target 184
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1448
    target 958
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1448
    target 700
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1448
    target 621
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1448
    target 52
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1226
    target 870
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 901
    target 39
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 560
    target 333
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1213
    target 560
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1900
    target 560
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1068
    target 1048
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1048
    target 596
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2416
    target 1048
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1506
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1398
    target 1031
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 700
    target 52
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 920
    target 596
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 596
    target 72
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 241
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1068
    target 920
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 621
    target 52
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 958
    target 52
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 524
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1399
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 523
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 973
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 920
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1465
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2229
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2306
    target 523
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1465
    target 551
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1465
    target 620
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1465
    target 992
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 868
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1426
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1071
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1783
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1971
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1085
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2333
    target 709
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1971
    target 488
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2289
    target 1062
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1308
    target 224
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 422
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1365
    target 422
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 651
    target 422
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 736
    target 422
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 422
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 422
    target 288
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 422
    target 355
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 539
    target 422
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1365
    target 355
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1365
    target 799
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1365
    target 736
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 892
    target 540
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1313
    target 892
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 892
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 892
    target 623
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1298
    target 892
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 799
    target 736
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 799
    target 355
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 404
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 561
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1208
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1469
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 473
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2196
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 421
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 923
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1307
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 735
    target 98
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1451
    target 944
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1451
    target 1003
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2204
    target 1451
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 652
    target 651
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 651
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 736
    target 735
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 736
    target 355
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1014
    target 736
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 736
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 736
    target 207
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 680
    target 473
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 735
    target 473
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1038
    target 473
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 473
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1236
    target 473
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1071
    target 473
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 473
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1792
    target 473
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 652
    target 473
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1792
    target 880
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1792
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1792
    target 1781
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1792
    target 652
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1781
    target 231
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2180
    target 1781
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2180
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 232
    target 231
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 403
    target 231
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 231
    target 1
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 276
    target 231
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 408
    target 231
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1199
    target 231
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 407
    target 231
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1452
    target 231
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 426
    target 231
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 231
    target 32
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 277
    target 231
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1733
    target 231
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1733
    target 277
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1733
    target 232
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1733
    target 276
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1733
    target 408
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1733
    target 1
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 277
    target 276
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 407
    target 277
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1199
    target 277
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 277
    target 232
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 403
    target 277
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 277
    target 1
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 426
    target 277
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 408
    target 277
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 426
    target 1
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 426
    target 232
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1199
    target 426
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 426
    target 407
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 426
    target 408
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 426
    target 276
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 426
    target 403
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 408
    target 407
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1199
    target 407
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 407
    target 1
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 407
    target 232
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 407
    target 276
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 407
    target 403
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1199
    target 1
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1199
    target 232
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1199
    target 403
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1199
    target 276
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1199
    target 408
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 408
    target 1
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 408
    target 232
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 408
    target 276
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 408
    target 403
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 276
    target 1
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 403
    target 276
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 403
    target 232
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 403
    target 1
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 686
    target 232
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 232
    target 1
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 232
    target 32
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 983
    target 20
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1663
    target 983
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1307
    target 983
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 983
    target 735
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1660
    target 983
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1892
    target 1663
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1663
    target 404
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1663
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1892
    target 1891
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 20
    target 19
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 366
    target 20
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 623
    target 404
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 623
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 589
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2383
    target 589
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 207
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 652
    target 288
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1042
    target 652
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 355
    target 354
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1014
    target 355
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 735
    target 355
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2211
    target 954
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2377
    target 2211
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2377
    target 2178
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2178
    target 2177
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 880
    target 680
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1071
    target 652
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1236
    target 652
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1379
    target 652
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1014
    target 652
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 652
    target 404
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 804
    target 652
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 652
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 417
    target 375
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 880
    target 375
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 836
    target 375
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 375
    target 58
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 923
    target 375
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1236
    target 375
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 804
    target 375
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 375
    target 179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 421
    target 404
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 880
    target 404
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1071
    target 404
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 880
    target 836
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 836
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 417
    target 58
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 880
    target 58
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1379
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1038
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 923
    target 804
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1660
    target 923
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 923
    target 880
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 923
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2236
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 880
    target 179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 880
    target 804
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 880
    target 354
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1236
    target 880
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2246
    target 880
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 880
    target 735
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1660
    target 880
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1782
    target 787
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1307
    target 1071
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1307
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1071
    target 804
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1071
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1236
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 421
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 561
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 421
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1709
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 804
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1298
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2385
    target 421
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1660
    target 804
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1660
    target 179
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2294
    target 1660
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 804
    target 561
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 804
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2329
    target 804
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 804
    target 111
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 875
    target 874
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 230
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2205
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 111
    target 46
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1496
    target 752
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1496
    target 140
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1650
    target 286
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1404
    target 40
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 629
    target 109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1492
    target 109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2171
    target 109
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1535
    target 1492
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1593
    target 789
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1188
    target 936
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1188
    target 40
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2258
    target 1742
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 657
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1316
    target 657
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2419
    target 657
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1491
    target 712
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1933
    target 920
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 920
    target 72
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1349
    target 198
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1349
    target 467
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 32
    target 31
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 31
    target 27
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1264
    target 541
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2025
    target 27
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2147
    target 2025
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1742
    target 973
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1742
    target 723
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1742
    target 1076
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 303
    target 236
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 588
    target 332
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1205
    target 588
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1463
    target 1205
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1452
    target 541
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1452
    target 27
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1452
    target 1076
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1452
    target 32
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 255
    target 254
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 737
    target 255
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2295
    target 255
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2381
    target 255
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 621
    target 620
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 662
    target 621
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 621
    target 551
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 621
    target 27
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 621
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 621
    target 524
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1145
    target 621
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 28
    target 27
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 467
    target 198
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1356
    target 198
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1849
    target 1848
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2303
    target 1849
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 237
    target 236
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 440
    target 236
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 440
    target 439
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2003
    target 439
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2292
    target 2003
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1739
    target 1679
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1679
    target 1145
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1713
    target 766
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1713
    target 286
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2006
    target 194
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2006
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1573
    target 1085
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2278
    target 2199
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1739
    target 27
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 539
    target 538
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1027
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1310
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1311
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1638
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1672
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1927
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1972
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2163
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2331
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2349
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2355
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2425
    target 539
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2122
    target 27
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 645
    target 618
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2244
    target 72
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2338
    target 72
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 958
    target 620
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 958
    target 27
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1145
    target 958
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 958
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1383
    target 1382
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1383
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 333
    target 332
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1788
    target 332
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1076
    target 1003
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1076
    target 666
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1443
    target 27
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1004
    target 1003
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1003
    target 32
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1160
    target 488
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1516
    target 488
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1161
    target 1160
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 541
    target 540
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 541
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2087
    target 541
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2087
    target 94
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 95
    target 94
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 587
    target 27
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1318
    target 27
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 199
    target 27
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1319
    target 1318
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 195
    target 32
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1646
    target 1436
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 973
    target 723
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2247
    target 973
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1004
    target 992
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1163
    target 897
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 897
    target 896
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1291
    target 1100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2129
    target 1100
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2129
    target 1291
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1588
    target 702
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2125
    target 702
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2379
    target 275
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 366
    target 199
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1356
    target 199
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 551
    target 199
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 265
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 679
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1294
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1501
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 594
    target 265
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 595
    target 594
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1854
    target 594
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1575
    target 131
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 132
    target 131
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1083
    target 132
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1138
    target 40
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1422
    target 40
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1440
    target 1422
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1686
    target 1422
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1686
    target 1440
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2438
    target 286
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 333
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 502
    target 501
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 501
    target 106
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 501
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 986
    target 501
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1087
    target 501
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 501
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 666
    target 501
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 986
    target 105
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 106
    target 105
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 114
    target 105
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 502
    target 105
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1087
    target 105
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 149
    target 105
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 666
    target 105
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 540
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2274
    target 195
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2274
    target 540
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2274
    target 1298
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1497
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2136
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2261
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1162
    target 215
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1305
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1305
    target 1087
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1305
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1305
    target 666
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2201
    target 493
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 666
    target 493
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1162
    target 992
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 666
    target 364
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1087
    target 666
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 666
    target 106
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 666
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1067
    target 666
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 666
    target 502
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 666
    target 629
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1629
    target 666
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 666
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1630
    target 1629
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1629
    target 106
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1744
    target 1630
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1744
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2023
    target 1744
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2023
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1067
    target 149
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 149
    target 106
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 149
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 984
    target 978
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1807
    target 978
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1272
    target 992
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2066
    target 992
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1272
    target 620
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1272
    target 551
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1936
    target 106
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1765
    target 264
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1067
    target 106
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1067
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1087
    target 1067
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 251
    target 250
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1799
    target 251
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1087
    target 106
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1087
    target 114
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 114
    target 106
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 620
    target 551
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 620
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1356
    target 282
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1356
    target 551
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1356
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 551
    target 282
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 866
    target 551
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1560
    target 551
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 551
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 551
    target 367
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 909
    target 551
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 909
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 367
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1560
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 866
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 889
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1420
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1441
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1763
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 366
    target 282
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 1906
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2108
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2169
    target 366
    w 1.0
    weight 1.0
  ]
  edge
  [
    source 2412
    target 366
    w 1.0
    weight 1.0
  ]
]
