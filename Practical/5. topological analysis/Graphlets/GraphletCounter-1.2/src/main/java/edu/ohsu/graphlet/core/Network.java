package edu.ohsu.graphlet.core;

import java.util.Iterator;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Network represents the set of all nodes and their connections. I assume that every node in the network
 * has a unique String id.
 */
public interface Network {

    /**
     * Return the node with the specified id
     * @param id
     * @return
     */
    public Node getNode(String id);

    /**
     * Creates a node with the given Id in the network
     * @param id
     * @return The new node
     */
    public Node createNode(String id);

    /**
     * Add the node to the network
     * @param node
     */
    public void addNode(Node node);

    /**
     * Adds an edge between node1 and node2, ensuring that both nodes connect to each other.
     * @param node1
     * @param node2
     */
    public void addUndirectedEdge(Node node1, Node node2);

    /**
     * Removes the given node from the network nondestructively by hiding the node from other nodes, while keeping
     * enough information to restore the node if required.
     * @param node
     */
    public void hideNode(Node node);

    /**
     * An iterator over all nodes in the network
     * @return
     */
    public Iterator<Node> nodeIterator();

}
