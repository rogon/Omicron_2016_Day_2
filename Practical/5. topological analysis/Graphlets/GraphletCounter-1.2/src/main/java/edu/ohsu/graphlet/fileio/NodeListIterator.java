package edu.ohsu.graphlet.fileio;

import edu.ohsu.graphlet.core.Network;
import edu.ohsu.graphlet.core.Node;

import java.util.Iterator;
import java.util.Set;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Given a set of node ids and a network implementation, returns an iterator over the actual
 * node objects identified by those ids in the network.
 */
public class NodeListIterator implements Iterator<Node> {
    private Network network;
    private Iterator<String> nodeIdIterator;

    public NodeListIterator(Set<String> selectedNodeIds, Network network) {
        this.network = network;
        nodeIdIterator = selectedNodeIds.iterator();
    }

    public boolean hasNext() {
        return nodeIdIterator.hasNext();
    }

    public Node next() {
        String nodeId = nodeIdIterator.next();        
        return network.getNode(nodeId);
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
