package edu.ohsu.graphlet.fileio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashSet;
import java.util.Set;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This class can read a list of node identifiers from a file, in cases when
 * the user has stored a list of nodes of interest.
 */
public class NodeListFileReader {

    /**
     * Reads the set of string node identifiers stored in the file referred to by fileReader.
     * @param fileReader
     * @return
     * @throws IOException
     */
    public Set<String> readNodeListFile(Reader fileReader) throws IOException {
        BufferedReader br = new BufferedReader(fileReader);

        Set<String> nodes = new HashSet<String>();
        String line;
        while((line = br.readLine()) != null) {
            nodes.add(line);
        }

        return nodes;
    }
}
