package edu.ohsu.graphlet.cytoscape;

import cytoscape.Cytoscape;
import cytoscape.plugin.CytoscapePlugin;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Cytoscape plugin class.
 */
public class GraphletCounterPlugin extends CytoscapePlugin {

    /**
     * Instantiates the plugin and addes the open action to the Cytoscape "Plugins" menu.
     */
    public GraphletCounterPlugin() {
        OpenGraphletCounterPanelAction action = new OpenGraphletCounterPanelAction();
        action.setPreferredMenu("Plugins");
        Cytoscape.getDesktop().getCyMenus().addAction(action);
    }

}
