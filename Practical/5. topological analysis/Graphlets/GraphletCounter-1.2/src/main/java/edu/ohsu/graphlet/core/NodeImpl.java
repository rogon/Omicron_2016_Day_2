package edu.ohsu.graphlet.core;

import java.util.Set;
import java.util.HashSet;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A simple implementation of Node using a java.util.Set to store arcs to the node's
 * neighbors in an undirected graph.
 */
public class NodeImpl extends BaseGraphletSignature implements Node {

    /**
     * Unique identifier for the node.
     */
    private String id;

    /**
     * Set of nodes that are adjacent to this node in an undirected graph.
     */
    private Set<Node> neighbors = new HashSet<Node>();

    public NodeImpl(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Set<Node> getNeighbors() {
        return neighbors;
    }

    public void addNeighbor(Node n) {
        neighbors.add(n);
        n.getNeighbors().add(this);
    }

    public String toString() { return id; }

    public void removeEdge(Node neighbor) {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NodeImpl)) return false;

        NodeImpl node = (NodeImpl) o;

        if (id != null ? !id.equals(node.id) : node.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
