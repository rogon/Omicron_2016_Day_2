package edu.ohsu.graphlet.cytoscape;

import cytoscape.task.Task;
import edu.ohsu.graphlet.core.GraphletCounter;
import edu.ohsu.graphlet.core.HasGraphletSignature;

import java.util.Collection;
import java.util.Iterator;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Generic Cytoscape task to be executed by the main Cytoscape runtime.
 */
public abstract class GraphletCountTask implements Task {
    /**
     * Returns a results container for a graphlet count task.
     * @return
     */
    abstract GraphletCountResults getResults();

    /**
     * Given the computes list of counts by nodes, calcualtes the mean count for each automorphism orbit
     * and returns the results.
     * @param signatures
     * @return
     */
    protected double[] calculateMeanCounts(Collection<HasGraphletSignature> signatures) {
        double[] results = new double[GraphletCounter.NUM_ORBITS];
        for (Iterator<HasGraphletSignature> iterator = signatures.iterator(); iterator.hasNext();) {
            int[] counts = iterator.next().getCounts();
            for (int i = 0; i < counts.length; i++) {
                results[i] += counts[i];
            }
        }
        for (int i = 0; i < results.length; i++) {
            results[i] /= signatures.size();
        }
        return results;
    }

}
