package edu.ohsu.graphlet;

import junit.framework.TestCase;
import junit.framework.Test;
import junit.framework.TestSuite;
import junit.framework.JUnit4TestAdapter;
import edu.ohsu.graphlet.core.AllCoreTests;
import edu.ohsu.graphlet.fileio.AllFileioTests;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */
public class AllGraphletTests extends TestCase {

    public static Test suite() {
        TestSuite suite = new TestSuite("Test for edu.ohsu.graphlet");
        if ("true".equals(System.getProperty("maven.test.skip"))) {
            System.err.println("SKIPPING TESTS");
            return suite;
        }

        suite.addTest(new JUnit4TestAdapter(AllCoreTests.class));
        suite.addTest(new JUnit4TestAdapter(AllFileioTests.class));

        return suite;
    }

}
