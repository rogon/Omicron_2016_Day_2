package edu.ohsu.graphlet.fileio;

import edu.ohsu.graphlet.core.Motif;

import static org.junit.Assert.*;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */
public class MFinderMembersFileReaderTest {

    @Test
    public void testReadMembersFile() throws Exception {
        String fileContent =
                "subgraph id = 38\n" +
                "Nreal : 4\n" +
                "=================================\n" +
                "Full list of 4 members:\n" +
                "\n" +
                "255     157     66\n" +
                "238     233     66\n" +
                "271     270     66\n" +
                "140     137     136\n" +
                " ";

        StringReader reader = new StringReader(fileContent);

        MotifFileReader fileReader = new MFinderMembersFileReader();
        List<Motif> motifList = fileReader.readMembersFile(reader);
        assertNotNull(motifList);
        assertEquals("38", motifList.get(0).getSubgraphId());
        Motif motif38 = motifList.get(0);
        assertEquals(4, motif38.getInstances().size());
        String[] members1 = {"255","157","66"};
        assertTrue(motif38.getInstances().get(0).getMembers().containsAll(Arrays.asList(members1)));
        String[] members2 = {"238","233","66"};
        assertTrue(motif38.getInstances().get(1).getMembers().containsAll(Arrays.asList(members2)));
        String[] members3 = {"271","270","66"};
        assertTrue(motif38.getInstances().get(2).getMembers().containsAll(Arrays.asList(members3)));
        String[] members4 = {"140","137","136"};
        assertTrue(motif38.getInstances().get(3).getMembers().containsAll(Arrays.asList(members4)));
    }

    @Test
    public void testValidateMembershipDefinition() throws IOException {
        String fileContent =
                "subgraph id = 38\n" +
                "Nreal : 4\n" +
                "=================================\n" +
                "Full list of 4 members:\n" +
                "\n" +
                "199     1     1\n";

        StringReader reader = new StringReader(fileContent);

        MotifFileReader fileReader = new MFinderMembersFileReader();
        try {
            List<Motif> motifList = fileReader.readMembersFile(reader);
            fail("Should have thrown an error about duplicate membership");
        } catch (MotifFileFormatException e) {

        }

    }

    @Test
    public void testValidateFileFormat() throws IOException {
        String fileContent =
                "28391u39 jr32u48932 feiowfno \n" +
                "Nfdnsjkfnks\n";

        StringReader reader = new StringReader(fileContent);

        MotifFileReader fileReader = new MFinderMembersFileReader();
        try {
            List<Motif> motifList = fileReader.readMembersFile(reader);
            fail("Should have thrown an error about a bad file header");
        } catch (MotifFileFormatException e) {

        }

    }

    @Test
    public void testReadMembersFileWithMultipleSubgraphs() throws Exception {
        String fileContent =
                "subgraph id = 6\n" +
                        "Nreal : 5\n" +
                        "=================================\n" +
                        "Full list of 5 members:\n" +
                        "\n" +
                        "41\t9\t40\t\n" +
                        "78\t63\t100\t\n" +
                        "66\t9\t73\t\n" +
                        "41\t40\t66\t\n" +
                        "9\t27\t78\t\n" +
                        "\n" +
                        "subgraph id = 12\n" +
                        "Nreal : 22\n" +
                        "=================================\n" +
                        "Full list of 22 members:\n" +
                        "\n" +
                        "78\t63\t26\t\n" +
                        "92\t66\t9\t\n" +
                        "40\t27\t2\t\n" +
                        "9\t78\t63\t\n" +
                        "26\t94\t78\t\n" +
                        "2\t41\t9\t\n" +
                        "2\t41\t40\t\n" +
                        "78\t100\t38\t\n" +
                        "13\t66\t9\t\n" +
                        "9\t27\t2\t\n" +
                        "38\t9\t27\t\n" +
                        "100\t9\t27\t\n" +
                        "92\t66\t73\t\n" +
                        "66\t9\t27\t\n" +
                        "13\t66\t73\t\n" +
                        "41\t66\t73\t\n" +
                        "27\t41\t66\t\n" +
                        "2\t41\t66\t\n" +
                        "94\t78\t100\t\n" +
                        "38\t9\t78\t\n" +
                        "41\t9\t78\t\n" +
                        "66\t9\t78\t";

        StringReader reader = new StringReader(fileContent);

        MotifFileReader fileReader = new MFinderMembersFileReader();
        List<Motif> motifList = fileReader.readMembersFile(reader);
        assertNotNull(motifList);
        assertEquals(2, motifList.size());
        assertEquals("6", motifList.get(0).getSubgraphId());
        assertEquals("12", motifList.get(1).getSubgraphId());
        assertEquals(5, motifList.get(0).getInstances().size());
        assertEquals(22, motifList.get(1).getInstances().size());
    }

}
