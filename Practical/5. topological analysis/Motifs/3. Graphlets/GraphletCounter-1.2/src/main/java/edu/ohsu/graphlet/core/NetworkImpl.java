package edu.ohsu.graphlet.core;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A basic implementation of an undirected network of nodes and edges using simple java.util.Maps for
 * node lists. Designed to work with Nodes of type edu.ohsu.graphlet.NodeImpl
 */
public class NetworkImpl implements Network {

    /**
     * Nodes indexed by their String Id
     */
    public Map<String, Node> nodesById = new HashMap<String, Node>();

    /**
     * Nodes that have been hidden by calls to hideNode()
      */
    public Map<String, Node> hiddenNodesById = new HashMap<String, Node>();

    /**
     * Returns the node with the given Id
     * @param id
     * @return
     */
    public Node getNode(String id) {
        return nodesById.get(id);
    }

    /**
     * Creates a new node with the given id
     * @param id
     * @return
     */
    public Node createNode(String id) {
        NodeImpl node = new NodeImpl(id);
        nodesById.put(id, node);
        return node;
    }

    /**
     * Adds node to the network
     * @param node
     */
    public void addNode(Node node) {
        nodesById.put(node.getId(), node);
    }

    /**
     * Adds an undirected edge between node1 and node2
     * @param node1
     * @param node2
     */
    public void addUndirectedEdge(Node node1, Node node2) {
        node1.addNeighbor(node2);
    }

    /**
     * Temporarily removes node from the network
     * @param node
     */
    public void hideNode(Node node) {
        for (Node neighbor : node.getNeighbors()) {
            neighbor.getNeighbors().remove(node);
        }
        nodesById.remove(node.getId());
        hiddenNodesById.put(node.getId(), node);
    }

    /**
     * Returns an iterator over all nodes in the network
     * @return
     */
    public Iterator<Node> nodeIterator() {
        return nodesById.values().iterator();
    }

    /**
     * Permanently removes the given node from the network
     * @param id
     */
    public void removeNode(String id) {
        Node node = nodesById.get(id);
        for (Node neighbor : node.getNeighbors()) {
            neighbor.getNeighbors().remove(node);
        }
        nodesById.remove(id);
    }

    /**
     * Restores a node that has been hidden to the network
     * @param id
     */
    public void restoreNode(String id) {
        Node node = hiddenNodesById.get(id);
        hiddenNodesById.remove(id);
        nodesById.put(id, node);
        for (Node neighbor : node.getNeighbors()) {
            neighbor.getNeighbors().add(node);
        }
    }
}
