package edu.ohsu.graphlet.core;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.*;

/**
 * This class is a version of CountResults specialized to handle counts for Motifs instead
 * of simple nodes in the network.
 */
public class MotifCountResults extends CountResults {

    Map<String, Set<String>> identifiersBySubgraphId = new HashMap<String, Set<String>>();

    /**
     * When adding a graphlet signature to the results we also want to keep track of what
     * subgraph it belongs to.
     * @param key
     * @param value
     * @return
     */
    @Override
    public HasGraphletSignature put(String key, HasGraphletSignature value) {
        MotifInstance motifInstance = (MotifInstance) value;
        String subgraphId = motifInstance.getMotif().getSubgraphId();
        if (! identifiersBySubgraphId.containsKey(subgraphId)) {
            identifiersBySubgraphId.put(subgraphId, new HashSet<String>());
        }
        identifiersBySubgraphId.get(subgraphId).add(key);
        return super.put(key, value);
    }

    /**
     * Returns all motif instances in the results that belong to the given subgraph ID.
     * @param subgraphId
     * @return
     */
    public Set<String> getIdentifiersForSubgraphId(String subgraphId) {
        return identifiersBySubgraphId.get(subgraphId);
    }

    /**
     * Returns a list of all the subgraph ids for which there are results
     */
    public Set<String> getSubgraphIds() {
        return identifiersBySubgraphId.keySet();
    }

    /**
     * Returns the subgraph ID (motif type) associated with this node.
     * @param id
     * @return
     */
    @Override
    protected String getSubgraphId(String id) {
        return ((MotifInstance) get(id)).getMotif().getSubgraphId();
    }
}
