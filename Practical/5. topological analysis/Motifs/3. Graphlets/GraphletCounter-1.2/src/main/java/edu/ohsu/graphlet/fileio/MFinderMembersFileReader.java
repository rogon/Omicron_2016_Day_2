package edu.ohsu.graphlet.fileio;

import edu.ohsu.graphlet.core.Motif;
import edu.ohsu.graphlet.core.MotifInstance;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Arrays;
import java.io.Reader;
import java.io.IOException;

/**
 * Copyright 2010 Christopher W Whelan
 *
 * This file is part of GraphletCounter.
 *
 * GraphletCounter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GraphletCounter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with GraphletCounter.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Class to read a motifs file of the type generated by the mfinder motif finding program.
 */
public class MFinderMembersFileReader implements MotifFileReader {

    /**
     * Given an open FileReader to an mfinds results file, returns a structure listing all of the
     * motifs, in terms of the string Ids of their constituent nodes.
     *
     * @param fileReader
     * @return
     * @throws IOException
     */
    public List<Motif> readMembersFile(Reader fileReader) throws IOException, MotifFileFormatException {

        Scanner scanner = new Scanner(fileReader);
        List<Motif> motifList = new ArrayList<Motif>();
        Motif motif = new Motif();

        // first line: subgraph id = 38
        String line = scanner.nextLine();

        if (! line.startsWith("subgraph id = ")) {
            throw new MotifFileFormatException("Motif file does not begin with \"subgraph id = \"; not a vaild mfinder file.");
        }

        String subgraphId = readSubgraphHeader(line, scanner);
        motif.setSubgraphId(subgraphId);

        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            if (line.trim().equals("")) continue;
            if (line.startsWith("subgraph id")) {
                motifList.add(motif);
                motif = new Motif();
                subgraphId = readSubgraphHeader(line, scanner);
                motif.setSubgraphId(subgraphId);
                continue;
            }

            List<String> members = Arrays.asList(line.trim().split("\\s+"));
            if (members.size() > 0) {
                MotifInstance motifInstance = new MotifInstance();
                motifInstance.setMotif(motif);
                motif.getInstances().add(motifInstance);
                for (String nodeId : members) {
                    if (motifInstance.containsNode(nodeId)) {
                        throw new MotifFileFormatException("Invalid motif definition: the motif " + line.trim() + " contains the node " + nodeId + " more than once.");
                    }
                    motifInstance.getMembers().add(nodeId);
                }
            }
        }

        motifList.add(motif);
        return motifList;
    }

    /**
     * Reads the subgraph header, eg:
     *
     * subgraph id = 78
     * Nreal : 1
     * =================================
     * Full list of 1 members:
     *
     *
     * @param currentLine
     * @param scanner
     * @return
     */
    private String readSubgraphHeader(String currentLine, Scanner scanner) {
        String subgraphId = currentLine.split(" = ")[1];
        String number = scanner.nextLine();
        String header = scanner.nextLine();
        String fullList = scanner.nextLine();
        String blank = scanner.nextLine();
        return subgraphId;
    }

}
